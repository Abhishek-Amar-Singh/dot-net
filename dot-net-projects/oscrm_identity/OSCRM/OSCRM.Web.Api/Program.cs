using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using OSCRM.Web.Api.Brokers.DateTimes;
using OSCRM.Web.Api.Brokers.Loggings;
using OSCRM.Web.Api.Brokers.Storages;
using OSCRM.Web.Api.Brokers.UserManagement;
using OSCRM.Web.Api.Models.Users;
using OSCRM.Web.Api.Services.Foundations.Users;
using OSCRM.Web.Api.UserManagement.Service.Models;
using OSCRM.Web.Api.UserManagement.Service.Services;
using System.Text;
using JsonStringEnumConverter = Newtonsoft.Json.Converters.StringEnumConverter;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
//--Add Authentication into Swagger
builder.Services.AddSwaggerGen(option =>
{
    option.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "Auth API", Version = "v1" });
    option.AddSecurityDefinition("Bearer", new Microsoft.OpenApi.Models.OpenApiSecurityScheme
    {
        In = Microsoft.OpenApi.Models.ParameterLocation.Header,
        Description = "Please enter a valid token",
        Name = "Authorization",
        Type = Microsoft.OpenApi.Models.SecuritySchemeType.Http,
        BearerFormat = "JWT",
        Scheme = "Bearer"
    });
    option.AddSecurityRequirement(new Microsoft.OpenApi.Models.OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme{Reference = new OpenApiReference{Type = ReferenceType.SecurityScheme,Id="Bearer"}},
            new string[]{}
        }
    });
});

//--AddNewtonSoftJson(services)
/*
If AddNewtonSoftJson services are not added the we'll get an error
Some services are not able to be constructed (Error while validating the service descriptor 'ServiceType: Microsoft.AspNetCore.Identity.DataProtectorTokenProvider`1[OSCRM.Web.Api.Models.Users.User] Lifetime: Transient 
*/
builder.Services.AddMvc().AddNewtonsoftJson(options =>
{
    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
    options.SerializerSettings.Converters.Add(new JsonStringEnumConverter());
    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
});

//--AddDbContext
builder.Services.AddDbContext<StorageBroker>();

//--AddBrokers
builder.Services.AddScoped<IUserManagementBroker, UserManagementBroker>();
builder.Services.AddScoped<IStorageBroker, StorageBroker>();
builder.Services.AddTransient<ILogger, Logger<LoggingBroker>>();
builder.Services.AddTransient<ILoggingBroker, LoggingBroker>();
builder.Services.AddTransient<IDateTimeBroker, DateTimeBroker>();

//--AddIdentityCore....it was giving an error for SignInManager service
//builder.Services.AddIdentityCore<User>()
//        .AddRoles<Role>()
//        .AddEntityFrameworkStores<StorageBroker>()
//        .AddDefaultTokenProviders();
//--AddIdentity...to resolve an error for SignInManager service
builder.Services.AddIdentity<User, Role>()
        .AddEntityFrameworkStores<StorageBroker>()
        .AddDefaultTokenProviders();


//--Add Config for Required Email Address while signing in....working fine
//builder.Services.Configure<IdentityOptions>(
//    option => {
//        option.SignIn.RequireConfirmedEmail = true;
//        option.SignIn.RequireConfirmedPhoneNumber = true;
//});

//--Add Authentication
builder.Services.AddAuthentication(option =>
{
    option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    option.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(option =>
{
    option.SaveToken = true;
    option.RequireHttpsMetadata = false;
    option.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
    {
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidIssuer = builder.Configuration["JWT:ValidIssuer"],
        ValidAudience = builder.Configuration["JWT:ValidAudience"],
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["JWT:Secret"]))
    };
});

//--Add Email Config
/* //--1st Way
builder.Services.Configure<EmailConfiguration>(builder.Configuration.GetSection("EmailConfiguration"));
var emailConfig = builder.Configuration.GetSection("EmailConfiguration").Get<EmailConfiguration>();
builder.Services.AddSingleton(emailConfig);
*/
//--2nd Way
var emailConfig = new EmailConfiguration();
builder.Configuration.GetSection("EmailConfiguration").Bind(emailConfig);
builder.Services.AddSingleton(emailConfig);

//--AddFoundationServices
builder.Services.AddTransient<IUserService, UserService>();
builder.Services.AddScoped<IEmailService, EmailService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
