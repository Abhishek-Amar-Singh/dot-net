﻿using Microsoft.AspNetCore.Identity;
using OSCRM.Web.Api.Models.Users;
using OSCRM.Web.Api.Models.Users.Create;

namespace OSCRM.Web.Api.Brokers.UserManagement
{
    public partial class UserManagementBroker
    {
        public async ValueTask<InsertUserAndRole> InsertUserAndRoleAsync(User user, string role)
        {
            var userWithRoleData = new InsertUserAndRole
            {
                UserId = user.Id,
                RoleName = role
            };
            await this.userManager.AddToRoleAsync(user, role);

            return userWithRoleData;
        }
    }
}
