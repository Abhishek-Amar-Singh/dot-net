﻿using Microsoft.AspNetCore.Identity;
using OSCRM.Web.Api.Models.Users;
using OSCRM.Web.Api.Models.Users.Create;

namespace OSCRM.Web.Api.Brokers.UserManagement
{
    public partial interface IUserManagementBroker
    {
        ValueTask<bool> IsRoleExistsAsync(string role);

        ValueTask<Role> InsertRoleAsync(CreateRole role);

        ValueTask<Role> SelectRoleByNameAsync(string roleName);

        IQueryable<Role> SelectAllRoles();
    }
}
