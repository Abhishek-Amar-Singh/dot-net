﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using OSCRM.Web.Api.Models.Users;
using OSCRM.Web.Api.Models.Users.Create;

namespace OSCRM.Web.Api.Brokers.UserManagement
{
    public partial class UserManagementBroker : IUserManagementBroker
    {
        private readonly UserManager<User> userManager;
        private readonly RoleManager<Role> roleManager;
        private readonly SignInManager<User> signInManager;

        public UserManagementBroker(
            UserManager<User> userManager,
            RoleManager<Role> roleManager,
            SignInManager<User> signInManager)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.signInManager = signInManager;
        }

        public async ValueTask<User> InsertUserAsync(RegisterUser registerUser)
        {
            var user = new User
            {
                FirstName = registerUser.FirstName,
                LastName = registerUser.LastName,
                Email = registerUser.Email,
                UserName = registerUser.Email,
                PhoneNumber = registerUser.PhoneNumber,
                Gender = registerUser.Gender,
                UserStatus = UserStatus.Activated,
                CreatedDate = DateTimeOffset.UtcNow
            };
            await this.userManager.CreateAsync(user, registerUser.Password);

            return user;
        }

        public async ValueTask<User> SelectUserByEmailAsync(string email) =>
            await this.userManager.FindByEmailAsync(email);

        public IQueryable<User> SelectAllUsers() =>
            this.userManager.Users;

        public async ValueTask<string> GenerateEmailConfirmationTokenAsync(User user) =>
            await this.userManager.GenerateEmailConfirmationTokenAsync(user);

        public async ValueTask<User> SelectUserByIdAsync(string userId) =>
            await this.userManager.FindByIdAsync(userId);

        public async ValueTask<IdentityResult> ConfirmEmailAsync(User user, string token) =>
            await this.userManager.ConfirmEmailAsync(user, token);

        public async ValueTask<bool> CheckPasswordAsync(User user, string password) =>
            await this.userManager.CheckPasswordAsync(user, password);

        public async ValueTask<IList<string>> GetRolesAsync(User user) =>
            await this.userManager.GetRolesAsync(user);

        public async ValueTask<SignInResult> PasswordSignInUserAsync(string emailAddress, string password) =>
            await this.signInManager.PasswordSignInAsync(emailAddress, password, false, false);

        public async ValueTask<User> TwoFactorEnabledAsync(User user)
        {
            user.TwoFactorEnabled = true;
            await this.userManager.UpdateAsync(user);
            return user;
        }

        public async ValueTask SignOutUserAsync() =>
            await this.signInManager.SignOutAsync();
    }
}
