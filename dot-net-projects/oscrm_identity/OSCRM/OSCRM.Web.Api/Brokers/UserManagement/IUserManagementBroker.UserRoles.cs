﻿using Microsoft.AspNetCore.Identity;
using OSCRM.Web.Api.Models.Users;
using OSCRM.Web.Api.Models.Users.Create;

namespace OSCRM.Web.Api.Brokers.UserManagement
{
    public partial interface IUserManagementBroker
    {
        ValueTask<InsertUserAndRole> InsertUserAndRoleAsync(User user, string role);
    }
}
