﻿using Microsoft.AspNetCore.Identity;
using OSCRM.Web.Api.Models.Users;
using OSCRM.Web.Api.Models.Users.Create;

namespace OSCRM.Web.Api.Brokers.UserManagement
{
    public partial class UserManagementBroker
    {
        public async ValueTask<bool> IsRoleExistsAsync(string role)
        {
            return await this.roleManager.RoleExistsAsync(role);
        }

        public async ValueTask<Role> InsertRoleAsync(CreateRole createRole)
        {
            var role = new Role
            {
                Name = createRole.RoleName
            };
            await this.roleManager.CreateAsync(role);

            return role;
        }

        public async ValueTask<Role> SelectRoleByNameAsync(string roleName) =>
            await this.roleManager.FindByNameAsync(roleName);

        public IQueryable<Role> SelectAllRoles() =>
            this.roleManager.Roles;
    }
}
