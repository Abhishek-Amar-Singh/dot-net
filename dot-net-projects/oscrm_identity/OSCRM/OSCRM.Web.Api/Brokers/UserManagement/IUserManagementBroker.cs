﻿using Microsoft.AspNetCore.Identity;
using OSCRM.Web.Api.Models.Users;
using OSCRM.Web.Api.Models.Users.Create;

namespace OSCRM.Web.Api.Brokers.UserManagement
{
    public partial interface IUserManagementBroker
    {
        ValueTask<User> InsertUserAsync(RegisterUser user);

        ValueTask<User> SelectUserByEmailAsync(string email);

        IQueryable<User> SelectAllUsers();

        ValueTask<string> GenerateEmailConfirmationTokenAsync(User user);

        ValueTask<User> SelectUserByIdAsync(string userId);

        ValueTask<IdentityResult> ConfirmEmailAsync(User user, string token);

        ValueTask<bool> CheckPasswordAsync(User user, string password);

        ValueTask<IList<string>> GetRolesAsync(User user);

        ValueTask<SignInResult> PasswordSignInUserAsync(string emailAddress, string password);

        ValueTask<User> TwoFactorEnabledAsync(User user);

        ValueTask SignOutUserAsync();
    }
}
