﻿

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using OSCRM.Web.Api.Models.Users;

namespace OSCRM.Web.Api.Brokers.Storages
{
    public partial class StorageBroker : IdentityDbContext<User, Role, Guid>, IStorageBroker
    {
        private readonly IConfiguration configuration;

        public StorageBroker(IConfiguration configuration)
        {
            this.configuration = configuration;
            this.Database.Migrate();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            //SeedRoles(builder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            var localDbConnectionStr = this.configuration.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(localDbConnectionStr);
        }

        //private static void SeedRoles(ModelBuilder builder)
        //{
        //    builder.Entity<Role>().HasData(
        //        new Role() { Name="Admin", NormalizedName="Administrator"},
        //        new Role() { Name = "User", NormalizedName = "User" },
        //        new Role() { Name = "Viewer", NormalizedName = "Viewer" },
        //        new Role() { Name="HR", NormalizedName = "Human Resource"}
        //    );
        //}
    }
}
