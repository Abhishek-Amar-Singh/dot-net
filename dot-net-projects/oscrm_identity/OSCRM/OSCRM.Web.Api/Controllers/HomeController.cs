﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using Org.BouncyCastle.Asn1.IsisMtt.X509;
using OSCRM.Web.Api.UserManagement.Service.Models;
using OSCRM.Web.Api.UserManagement.Service.Services;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;
using System;

namespace OSCRM.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IEmailService emailService;

        public HomeController(IEmailService emailService) =>
            this.emailService = emailService;

        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("TestConfirmEmail")]
        public ActionResult<string> TestConfirmEmail()
        {

            var message = new Message(
                new MailboxAddress[] {
                    new MailboxAddress("Abhishek Singh", "abhisheksingh.120120@gmail.com"),
                    new MailboxAddress("Madhu Singh", "mrs.madhu.b.singh@gmail.com"),
                    new MailboxAddress("Birendra Singh", "mr.birendra.singh@gmail.com")
                },
            "Test", "Email Confirmation Implementation in dot net 6 C# 10");
            
            //--Network connectivity issues: Check your network connectivity to ensure that you have an active internet connection.If you are on a restricted network(e.g., a corporate network), contact your network administrator to check if there are any restrictions or permissions required for outbound connections.
            emailService.SendEmail(message);
            return Ok(message.ToString());
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("Get")]
        public ActionResult<List<string>> Get()
        {
            try
            {
                return Ok(new List<string> { "C# 11.0", ".NET 7.0", "C# 10.0", ".NET 6.0" });
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
