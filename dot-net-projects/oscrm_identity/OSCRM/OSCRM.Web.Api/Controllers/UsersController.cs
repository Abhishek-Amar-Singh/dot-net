﻿using Microsoft.AspNetCore.Mvc;
using OSCRM.Web.Api.Models.Users;
using OSCRM.Web.Api.Models.Users.Create;
using OSCRM.Web.Api.Models.Users.Exceptions;
using OSCRM.Web.Api.Services.Foundations.Users;

namespace OSCRM.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;

        public UsersController(IUserService userService) => this.userService = userService;

        //--Users
        [Route("PostUserAsync")]
        [HttpPost]
        public async ValueTask<ActionResult<User>> PostUserAsync(RegisterUser user)
        {
            try
            {
                User registeredUser =
                await this.userService.RegisterUserAsync(user);

                return StatusCode(StatusCodes.Status201Created, registeredUser);
            }
            catch (AlreadyExistsUserException exception)
            {
                return StatusCode(StatusCodes.Status409Conflict, exception.Message);
            }
            catch(NotFoundRoleException exception)
            {
                return StatusCode(StatusCodes.Status404NotFound, exception.Message);
            }
            catch (Exception exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Something Went Wrong! {exception.Message}");
            }
        }

        [Route("GetUserByEmailAsync/{email}")]
        [HttpGet]
        public async ValueTask<ActionResult<User>> GetUserByEmailAsync(string email)
        {
            try
            {
                User storageUser =
                    await this.userService.RetrieveUserByEmailAsync(email);
                
                return StatusCode(StatusCodes.Status302Found, storageUser);
            }
            catch (NotFoundUserException exception)
            {
                return StatusCode(StatusCodes.Status404NotFound, exception.Message);
            }
            catch (Exception exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Something Went Wrong! {exception.Message}");
            }
        }

        [Route("GetAllUsers")]
        [HttpGet]
        public ActionResult<IQueryable<User>> GetAllUsers()
        {

            IQueryable<User> storageUsers =
                this.userService.RetrieveAllUsers();

            return Ok(storageUsers);
        }

        [Route("ConfirmEmailAsync")]
        [HttpGet]
        public async ValueTask<ActionResult> ConfirmEmailAsync(string userId, string token)
        {
            try
            {
                var storageUserIdentityResult =
                    await this.userService.ConfirmEmailAsync(userId, token);

                return StatusCode(StatusCodes.Status200OK, "Email Verified Successfully!");
            }
            catch (NotFoundUserException exception)
            {
                return StatusCode(StatusCodes.Status404NotFound, exception.Message);
            }
            catch (Exception exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Something Went Wrong! {exception.Message}");
            }
        }

        //--Login
        [Route("LoginUserAsync")]
        [HttpPost]
        public async ValueTask<ActionResult<string>> LoginUserAsync([FromBody] LoginUser loginUser)
        {
            try
            {
                var token = await this.userService.LoginUserAsync(loginUser.EmailAddress, loginUser.Password);
                return Ok(token);
            }
            catch(NotFoundUserException exception)
            {
                return NotFound(exception.Message);
            }
            catch(Exception e)
            {
                return Unauthorized(e.Message);
            }
        }

        [Route("EnableTwofactorAuthenticationAsync/{emailAddress}")]
        [HttpPut]
        public async ValueTask<ActionResult<User>> EnableTwofactorAuthenticationAsync(string emailAddress)
        {
            try
            {
                return await this.userService.EnableTwoFactorAuthenticationAsync(emailAddress);
            }
            catch(Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [Route("SignOutUserAsync")]
        [HttpGet]
        public async ValueTask SignOutUserAsync() =>
            await this.userService.SignOutUserAsync();

        //--Roles
        [Route("PostRoleAsync")]
        [HttpPost]
        public async ValueTask<ActionResult<User>> PostRoleAsync(CreateRole role)
        {
            try
            {
                Role createdRole =
                await this.userService.CreateRoleAsync(role);

                return StatusCode(StatusCodes.Status201Created, createdRole);
            }
            catch (AlreadyExistsRoleException exception)
            {
                return StatusCode(StatusCodes.Status409Conflict, exception.Message);
            }
            catch (Exception exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Something Went Wrong! {exception.Message}");
            }
        }

        [Route("GetRoleByNameAsync/{roleName}")]
        [HttpGet]
        public async ValueTask<ActionResult<Role>> GetRoleByNameAsync(string roleName)
        {
            try
            {
                Role storageRole =
                    await this.userService.RetrieveRoleByNameAsync(roleName);

                return StatusCode(StatusCodes.Status302Found, storageRole);
            }
            catch (NotFoundRoleException exception)
            {
                return StatusCode(StatusCodes.Status404NotFound, exception.Message);
            }
            catch (Exception exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Something Went Wrong! {exception.Message}");
            }
        }

        [Route("GetAllRoles")]
        [HttpGet]
        public ActionResult<IQueryable<Role>> GetAllRoles()
        {

            IQueryable<Role> storageRoles =
                this.userService.RetrieveAllRoles();

            return Ok(storageRoles);
        }
    }
}
