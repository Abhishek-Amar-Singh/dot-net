﻿using OSCRM.Web.Api.Models.Users.Create;
using OSCRM.Web.Api.Models.Users;
using OSCRM.Web.Api.Models.Users.Exceptions;
using System.Data;

namespace OSCRM.Web.Api.Services.Foundations.Users
{
    public partial class UserService
    {
        public async ValueTask<Role> CreateRoleAsync(CreateRole role)
        {
            var roleExistsStatus = await this.userManagementBroker.IsRoleExistsAsync(role.RoleName);
            if(roleExistsStatus is true)
            {
                throw new AlreadyExistsRoleException(role.RoleName);
            }
            else return await this.userManagementBroker.InsertRoleAsync(role);
        }

        public async ValueTask<Role> RetrieveRoleByNameAsync(string roleName)
        {
            var roleExistsStatus = await this.userManagementBroker.IsRoleExistsAsync(roleName);
            if (roleExistsStatus is true)
            {
                return await this.userManagementBroker.SelectRoleByNameAsync(roleName);
            }
            else throw new NotFoundRoleException(roleName);
        }

        public IQueryable<Role> RetrieveAllRoles() =>
            this.userManagementBroker.SelectAllRoles();
    }
}
