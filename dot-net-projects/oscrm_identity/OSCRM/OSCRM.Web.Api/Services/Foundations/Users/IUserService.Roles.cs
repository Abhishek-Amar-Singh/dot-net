﻿using OSCRM.Web.Api.Models.Users;
using OSCRM.Web.Api.Models.Users.Create;

namespace OSCRM.Web.Api.Services.Foundations.Users
{
    public partial interface IUserService
    {
        ValueTask<Role> CreateRoleAsync(CreateRole role);

        ValueTask<Role> RetrieveRoleByNameAsync(string roleName);

        IQueryable<Role> RetrieveAllRoles();
    }
}
