﻿using Microsoft.AspNetCore.Identity;
using OSCRM.Web.Api.Models.Users;
using OSCRM.Web.Api.Models.Users.Create;

namespace OSCRM.Web.Api.Services.Foundations.Users
{
    public partial interface IUserService
    {
        ValueTask<User> RegisterUserAsync(RegisterUser user);

        ValueTask<User> RetrieveUserByEmailAsync(string email);

        IQueryable<User> RetrieveAllUsers();

        ValueTask<User> RetrieveUserByIdAsync(string email);

        ValueTask<IdentityResult> ConfirmEmailAsync(string userId, string token);

        ValueTask<IList<string>> GetRolesAsync(User user);

        ValueTask<string> LoginUserAsync(string emailAddress, string password);

        ValueTask<User> EnableTwoFactorAuthenticationAsync(string emailAddress);

        ValueTask SignOutUserAsync();
    }
}
