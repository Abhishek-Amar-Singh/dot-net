﻿using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using MimeKit;
using Newtonsoft.Json;
using OSCRM.Web.Api.Brokers.DateTimes;
using OSCRM.Web.Api.Brokers.Loggings;
using OSCRM.Web.Api.Brokers.UserManagement;
using OSCRM.Web.Api.Models.Users;
using OSCRM.Web.Api.Models.Users.Create;
using OSCRM.Web.Api.Models.Users.Exceptions;
using OSCRM.Web.Api.UserManagement.Service.Models;
using OSCRM.Web.Api.UserManagement.Service.Services;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace OSCRM.Web.Api.Services.Foundations.Users
{
    public partial class UserService : IUserService
    {
        private readonly IUserManagementBroker userManagementBroker;
        private readonly IDateTimeBroker dateTimeBroker;
        private readonly ILoggingBroker loggingBroker;
        private readonly IEmailService emailService;
        private readonly IConfiguration configuration;

        public UserService(
            IUserManagementBroker userManagementBroker,
            IDateTimeBroker dateTimeBroker,
            ILoggingBroker loggingBroker,
            IEmailService emailService,
            IConfiguration configuration)
        {
            this.userManagementBroker = userManagementBroker;
            this.dateTimeBroker = dateTimeBroker;
            this.loggingBroker = loggingBroker;
            this.emailService = emailService;
            this.configuration = configuration;
        }

        public async ValueTask<User> RegisterUserAsync(RegisterUser registerUser)
        {
            var user = await this.userManagementBroker.SelectUserByEmailAsync(registerUser.Email);
            if (user is not null)
            {
                throw new AlreadyExistsUserException(registerUser.Email);
            }
            else
            {
                var roleExistsStatus = await this.userManagementBroker.IsRoleExistsAsync(registerUser.Role);
                if (roleExistsStatus is true)
                {
                    var userData = await this.userManagementBroker.InsertUserAsync(registerUser);
                    var userWithRoleData =
                        await this.userManagementBroker.InsertUserAndRoleAsync(userData, registerUser.Role);

                    var token = await this.userManagementBroker.GenerateEmailConfirmationTokenAsync(userData);
                    var confirmationLink =
                        $"https://localhost:7266/api/Users/ConfirmEmailAsync?userId={userData.Id}&token={token}";

                    //var confirmationLink = urlHelper.Action("ConfirmEmailAsync", "Users", new { userId = userData.Id, token = token });
                    ConfirmEmailAddressOnEmailServer(confirmationLink, userData);

                    return userData;
                }
                else
                {
                    throw new NotFoundRoleException(registerUser.Role);
                }
            }
        }

        public async ValueTask<User> RetrieveUserByEmailAsync(string email)
        {
            var user = await this.userManagementBroker.SelectUserByEmailAsync(email);
            if (user is null)
            {
                throw new NotFoundUserException(email);
            }
            else return user;
        }

        public IQueryable<User> RetrieveAllUsers() =>
            this.userManagementBroker.SelectAllUsers();

        private void ConfirmEmailAddressOnEmailServer(string confirmationLink, User user)
        {
            //var uri = new Uri(confirmationLink);
            //var query = HttpUtility.ParseQueryString(uri.Query);

            //int userId = Convert.ToInt32(query["userId"]);
            //string token = query["token"];

            var message = new Message(
                new MailboxAddress[] {
                    new MailboxAddress($"{user.FirstName} {user.LastName}", user.Email)
                },
                $"Confirm Email Address", $"<div><button><a href='{confirmationLink}'>Confirm</a></button></div>");

            emailService.SendEmail(message);
        }

        public async ValueTask<User> RetrieveUserByIdAsync(string userId)
        {
            var user = await this.userManagementBroker.SelectUserByIdAsync(userId);
            if (user is null)
            {
                throw new NotFoundUserException(userId);
            }
            else return user;
        }

        public async ValueTask<IdentityResult> ConfirmEmailAsync(string userId, string token)
        {
            var user = await this.userManagementBroker.SelectUserByIdAsync(userId);
            if (user is null)
            {
                throw new NotFoundUserException(userId);
            }
            else
            {
                var result = await this.userManagementBroker.ConfirmEmailAsync(user, token);
                if (result.Errors.Count() > 0)
                {
                    throw new Exception("Invalid token found...");
                }
                return result;
            }
        }

        public async ValueTask<string> LoginUserAsync(string emailAddress, string password)
        {
            var storageUser = await this.userManagementBroker.SelectUserByEmailAsync(emailAddress);
            if (storageUser is not null)
            {
                //--checking the password
                var pwdStatus = await this.userManagementBroker.CheckPasswordAsync(storageUser, password);

                if (pwdStatus is true)
                {
                    //--continue to signIn
                    var signInResult = await this.userManagementBroker.PasswordSignInUserAsync(emailAddress, password);
                    if (signInResult.Succeeded)
                    {
                        //--claimList creation
                        var authClaims = new List<Claim>
                        {
                            new Claim(ClaimTypes.Name, storageUser.Email),
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                        };
                        //--we add roles to the list
                        var userRoles = await this.userManagementBroker.GetRolesAsync(storageUser);
                        foreach (var role in userRoles)
                        {
                            authClaims.Add(new Claim(ClaimTypes.Role, role));
                        }
                        //--generate the token with the claims
                        var jwtToken = GenerateToken(authClaims);
                        //--returniung the token
                        var tokenDetails = new
                        {
                            token = new JwtSecurityTokenHandler().WriteToken(jwtToken),
                            expiration = jwtToken.ValidTo
                        };
                        string userJwtSecurityTokenHandler = JsonConvert.SerializeObject(new { Token = tokenDetails.token, Username = storageUser.UserName, UserId = storageUser.Id, Email = storageUser.Email });
                        return userJwtSecurityTokenHandler;
                    }
                    else
                    {
                        throw new Exception();
                    }

                }
                else
                {
                    throw new Exception($"Invalid password. Token cannot be generated.");
                }
            }
            else
            {
                throw new NotFoundUserException(emailAddress);
            }
        }

        public async ValueTask<IList<string>> GetRolesAsync(User user)
        {
            var result = await this.userManagementBroker.GetRolesAsync(user);
            return result;
        }

        public async ValueTask<User> EnableTwoFactorAuthenticationAsync(string emailAddress)
        {
            var storageUser = await this.userManagementBroker.SelectUserByEmailAsync(emailAddress);
            if (storageUser is not null)
            {
                var twoFactorEnabledUser = await this.userManagementBroker.TwoFactorEnabledAsync(storageUser);
                return twoFactorEnabledUser;
            }
            else
            {
                throw new Exception($"user having email={emailAddress} not found.");
            }
        }

        public async ValueTask SignOutUserAsync() =>
            await this.userManagementBroker.SignOutUserAsync();

        private JwtSecurityToken GenerateToken(List<Claim> authClaims)
        {
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]));
            var token = new JwtSecurityToken(
                issuer: configuration["JWT:ValidIssuer"],
                audience: configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddHours(1),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
            );

            return token;
        }
    }
}
