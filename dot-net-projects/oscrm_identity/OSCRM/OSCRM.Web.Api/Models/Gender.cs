﻿namespace OSCRM.Web.Api.Models
{
    public enum Gender
    {
        Female,
        Male,
        Other,
        Unknown
    }
}
