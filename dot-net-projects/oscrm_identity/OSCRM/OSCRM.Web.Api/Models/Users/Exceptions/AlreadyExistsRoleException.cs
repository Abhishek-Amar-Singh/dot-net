﻿namespace OSCRM.Web.Api.Models.Users.Exceptions
{
    public class AlreadyExistsRoleException : ApplicationException
    {
        public AlreadyExistsRoleException(string role)
            : base(message: $"User with the role={role} already exists.") { }
    }
}
