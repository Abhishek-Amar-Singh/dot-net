﻿namespace OSCRM.Web.Api.Models.Users.Exceptions
{
    public class NotFoundUserException : ApplicationException
    {
        public NotFoundUserException(string email)
            : base(message: $"Couldn't find user with email/userId: {email}.") { }
    }
}
