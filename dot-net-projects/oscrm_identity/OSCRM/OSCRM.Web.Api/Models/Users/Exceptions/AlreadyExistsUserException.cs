﻿namespace OSCRM.Web.Api.Models.Users.Exceptions
{
    public class AlreadyExistsUserException : ApplicationException
    {
        public AlreadyExistsUserException(string email)
            : base(message: $"User with the email={email} already exists.") { }
    }
}
