﻿namespace OSCRM.Web.Api.Models.Users.Exceptions
{
    public class NotFoundRoleException : ApplicationException
    {
        public NotFoundRoleException(string roleName)
            : base(message: $"Couldn't find role with name: {roleName}.") { }
    }
}
