﻿namespace OSCRM.Web.Api.Models.Users
{
    public enum UserStatus
    {
        Activated,
        Deactivated
    }
}
