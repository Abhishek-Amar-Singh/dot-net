﻿namespace OSCRM.Web.Api.Models.Users.Create
{
    public class RegisterUser
    {
        public string FirstName { get; set; } = null!;

        public string? LastName { get; set; }

        public string Email { get; set; } = null!;

        public string Password { get; set; } = null!;

        public string Role { get; set; } = null!;

        public string PhoneNumber { get; set; } = null!;

        public Gender Gender { get; set; }
    }
}
