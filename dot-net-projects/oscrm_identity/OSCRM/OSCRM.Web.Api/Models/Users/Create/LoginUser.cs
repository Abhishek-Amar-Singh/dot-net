﻿namespace OSCRM.Web.Api.Models.Users.Create
{
    public class LoginUser
    {
        public string EmailAddress { get; set; } = null!;
        public string Password { get; set; } = null!;
    }
}
