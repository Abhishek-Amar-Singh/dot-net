﻿namespace OSCRM.Web.Api.Models.Users.Create
{
    public class InsertUserAndRole
    {
        public Guid UserId { get; set; }

        public string RoleName { get; set; } = null!;
    }
}
