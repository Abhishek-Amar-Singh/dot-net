﻿
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace OSCRM.Web.Api.Models.Users
{
    public class User : IdentityUser<Guid>
    {
        public override Guid Id
        {
            get => base.Id;
            set => base.Id = value;
        }

        [Column(TypeName = "VARCHAR(150)")]
        public string FirstName { get; set; } = null!;

        [Column(TypeName = "VARCHAR(200)")]
        public string? LastName { get; set; }

        public override string PhoneNumber
        {

            get => base.PhoneNumber;
            set => base.PhoneNumber = value;
        }

        public Gender Gender { get; set; }

        public UserStatus UserStatus { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public Nullable<DateTimeOffset> UpdatedDate { get; set; }
    }
}
