﻿using MailKit.Net.Smtp;
using MimeKit;
using OSCRM.Web.Api.UserManagement.Service.Models;

namespace OSCRM.Web.Api.UserManagement.Service.Services
{
    public class EmailService : IEmailService
    {
        private readonly EmailConfiguration emailConfiguration;

        public EmailService(EmailConfiguration emailConfiguration) =>
            this.emailConfiguration = emailConfiguration;

        public void SendEmail(Message msg)
        {
            var emailMessage = CreateEmailMessage(msg);
            Send(emailMessage);
        }

        private MimeMessage CreateEmailMessage(Message msg)
        {
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress("Abhishek Amar Singh", emailConfiguration.From));
            emailMessage.To.AddRange(msg.To);
            emailMessage.Subject = msg.Subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html) { Text = msg.Content };
            return emailMessage;
        }

        private void Send(MimeMessage emailMessage)
        {
            using var client = new SmtpClient();
            try
            {
                client.Connect(emailConfiguration.SmtpServer, emailConfiguration.Port, true);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                client.Authenticate(emailConfiguration.Username, emailConfiguration.Password);

                client.Send(emailMessage);
            }
            catch
            {
                throw;
            }
            finally
            {
                client.Disconnect(true);
                client.Dispose();
            }
        }
    }
}
