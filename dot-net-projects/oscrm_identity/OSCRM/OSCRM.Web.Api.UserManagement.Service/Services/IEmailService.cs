﻿using OSCRM.Web.Api.UserManagement.Service.Models;

namespace OSCRM.Web.Api.UserManagement.Service.Services
{
    public interface IEmailService
    {
        void SendEmail(Message msg);
    }
}
