﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MiddlewareIntegration.Mail.Exceptions;
using MiddlewareIntegration.Mail.Models;
using MiddlewareIntegration.Mail.Services;
using MimeKit;

namespace MiddlewareIntegration.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IMailService mailService;

        public HomeController(IMailService mailService) =>
            this.mailService = mailService;

        [HttpGet]
        [Route("TestConfirmEmail")]
        public ActionResult<string> TestConfirmEmail()
        {
            try
            {
                var message = new Message(
                new MailboxAddress[] {
                    new MailboxAddress("Abhishek Singh", "abhisheksingh.120120@gmail.com"),
                    new MailboxAddress("Team Corbet", "teamcorbet123@gmail.com")
                },
                "Test",
                "Send mail implementation through middleware integration. This is done in C# 11.0 and .NET Core 6.");

                //--Network connectivity issues: Check your network connectivity to ensure that you have an active internet connection.If you are on a restricted network(e.g., a corporate network), contact your network administrator to check if there are any restrictions or permissions required for outbound connections.
                mailService.SendMail(message);
                return Ok("Mail Sent Successfully!");
            }
            catch (FailedMailServiceException failedMainServiceException)
            {
                return BadRequest(failedMainServiceException);
            }
        }
    }
}
