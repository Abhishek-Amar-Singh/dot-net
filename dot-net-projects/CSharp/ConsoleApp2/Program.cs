﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using System;

ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
using (var package = new ExcelPackage())
{
    var json = @"[
  {
    ""parent_category_id"": 28,
    ""insurer"": ""Manipal Cigna"",
    ""category_id"": 112,
    ""insurance_plan"": ""Manipal Cigna Pro Health Prime Protect"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 30200.92,
    ""annual_emi_score"": 79.80049908064092,
    ""room_description"": ""Single Private Room"",
    ""room_rent_score"": 100,
    ""no_claim_bonus"": ""Guaranteed Cumulative Bonus of 25% of Base SI each year subject to a max of 200% of the Base SI irrespective of claims"",
    ""ncb_score"": 100,
    ""recharge_sum_insured"": ""100% restoration of SI unlimited times for unlimited diseases"",
    ""si_recharge_score"": 100,
    ""pre_existing_disease"": ""For SI >= ₹ 7.5Lacs - 24 months"",
    ""ped_score"": 66.6666666666667,
    ""co_pay"": ""No Co-Pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Annually from 1st year onwards, for all adults insured. Limits / Test basis opted SI, offered through Network only."",
    ""hw_score"": 100,
    ""version"": null,
    ""claim_settled_ratio"": 0.8987,
    ""csr_score"": 50,
    ""ageing_of_claim"": 0.999,
    ""aoc_score"": 100,
    ""incurred_claim_ratio"": 0.7617,
    ""icr_score"": 100,
    ""network_hospitals"": 6500,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.5653,
    ""csr_abs_amt_score"": 0,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""180 days"",
      ""daycare_treatments"": ""More than 500 daycare procedures Covered up to SI"",
      ""ambulance_cover"": ""Covered up to SI"",
      ""domiciliary_hospitalisation"": ""Covered up to 10% of SI"",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""Available for 36 listed critical illnesses"",
      ""daily_allowance"": ""For SI up to ₹10L — ₹800 per hospitalisation per day, subject to a maximum of ₹5,600. For SI above ₹10L — ₹1,000 per hospitalisation per day, subject to a maximum of ₹7,000. Cover triggers after 48 hours of hospitalisation and is payable from the first day"",
      ""ayush_treatment"": ""Covered up to SI"",
      ""modern_treatment"": ""-""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""No sub-limits on room rent"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is more than 150%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available up to the coverage amount for related diseases"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 2 years"",
        ""is_pro"": ""True""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Rewards for healthy behaviour are available"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 89.8700%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 56.5300%."",
        ""is_pro"": ""False""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A favourable ICR of 76.1700% indicates high customer satisfaction based on the premium and claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""A favourable ageing of the claim for less than 3 months at 99.900%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""6500 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 81.44014972419228,
    ""onefin_ranking"": 1,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""1/39""
  },
  {
    ""parent_category_id"": 6,
    ""insurer"": ""Aditya Birla Health"",
    ""category_id"": 32,
    ""insurance_plan"": ""Aditya Birla Activ Health Platinum Enhanced"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 31996,
    ""annual_emi_score"": 76.85349356448647,
    ""room_description"": ""Shared Single Private A/C Room Any room above 4 lacs"",
    ""room_rent_score"": 100,
    ""no_claim_bonus"": ""50% increase max up to 100% for every claim free year"",
    ""ncb_score"": 66.6666666666667,
    ""recharge_sum_insured"": ""100% of SI for Unrelated diseases in the same policy year"",
    ""si_recharge_score"": 50,
    ""pre_existing_disease"": ""Covered after 3 years"",
    ""ped_score"": 33.3333333333333,
    ""co_pay"": ""No Co-Pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Annual"",
    ""hw_score"": 100,
    ""version"": null,
    ""claim_settled_ratio"": 0.9372,
    ""csr_score"": 75,
    ""ageing_of_claim"": 0.9941,
    ""aoc_score"": 100,
    ""incurred_claim_ratio"": 0.6956,
    ""icr_score"": 75,
    ""network_hospitals"": 10051,
    ""nh_score"": 75,
    ""claim_settled_ratio_abs_amt"": 0.7471,
    ""csr_abs_amt_score"": 50,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""180 days"",
      ""daycare_treatments"": ""Covered up to SI"",
      ""ambulance_cover"": ""Covered upto Actual Expenses"",
      ""domiciliary_hospitalisation"": ""Covered up to SI"",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""Available"",
      ""daily_allowance"": ""-"",
      ""ayush_treatment"": ""Covered up to SI"",
      ""modern_treatment"": ""Covers modern treatment methods like robotic surgeries, oral chemotherapy, etc""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""No sub-limits on room rent"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available once in a policy year upto the cover amount"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 3 years"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Rewards for healthy behaviour are available"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 93.7200%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 74.7100%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 69.5600% indicates fair pricing and payment of claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""A favourable ageing of the claim for less than 3 months at 99.4100%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""10051 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 75.9727147360126,
    ""onefin_ranking"": 3,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""3/39""
  },
  {
    ""parent_category_id"": 16,
    ""insurer"": ""Navi General"",
    ""category_id"": 78,
    ""insurance_plan"": ""Navi Health Plan 2"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 26566,
    ""annual_emi_score"": 85.76799317047544,
    ""room_description"": ""Actual"",
    ""room_rent_score"": 66.6666666666667,
    ""no_claim_bonus"": ""50% of BSI per year up to maximum of 150%"",
    ""ncb_score"": 100,
    ""recharge_sum_insured"": ""Automatic Restoration of Base Sum Insured during Policy Year Unlimited Number of Times"",
    ""si_recharge_score"": 75,
    ""pre_existing_disease"": ""Covered after 1 Year"",
    ""ped_score"": 100,
    ""co_pay"": ""Not Available"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Covered"",
    ""hw_score"": 100,
    ""version"": null,
    ""claim_settled_ratio"": 0.8252,
    ""csr_score"": 25,
    ""ageing_of_claim"": 0.9999,
    ""aoc_score"": 100,
    ""incurred_claim_ratio"": 0.2856,
    ""icr_score"": 50,
    ""network_hospitals"": 10000,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.47,
    ""csr_abs_amt_score"": 0,
    ""standard_features"": {
      ""pre_hospitalisation"": ""90 days"",
      ""post_hospitalisation"": ""180 days"",
      ""daycare_treatments"": ""Covered up to the limit of SI opted for"",
      ""ambulance_cover"": ""Covered"",
      ""domiciliary_hospitalisation"": ""Covered up to SI"",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""-"",
      ""daily_allowance"": ""₹1,000 (Shared room occupancy)"",
      ""ayush_treatment"": ""Covered up to SI"",
      ""modern_treatment"": ""Covered""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""Availability of the single private room category"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is more than 150%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available up to the coverage amount, an unlimited number of times, for unrelated diseases upto the cover amount for unlimted times for unrelated diseases "",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after or for less than 1 year"",
        ""is_pro"": ""True""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Rewards for healthy behaviour are available"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 82.5200%."",
        ""is_pro"": ""False""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 47.00%."",
        ""is_pro"": ""False""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 28.5600% indicates balanced pricing and payment of claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""A favourable ageing of the claim for less than 3 months at 99.9900%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""10000 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 75.73039795114263,
    ""onefin_ranking"": 4,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""4/39""
  },
  {
    ""parent_category_id"": 18,
    ""insurer"": ""Niva Bupa Health"",
    ""category_id"": 81,
    ""insurance_plan"": ""Niva Bupa Health Premia"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 32554,
    ""annual_emi_score"": 75.93741791436827,
    ""room_description"": ""No Capping"",
    ""room_rent_score"": 100,
    ""no_claim_bonus"": ""In case of claim free year increase of 50% of expiring Base Sum Insured in a Policy Year; maximum up to 100% of Base Sum Insured (In case of a claim reduction of accumulated Cumulative Bonus by 50% of expiring Base Sum Insured)"",
    ""ncb_score"": 66.6666666666667,
    ""recharge_sum_insured"": ""Unlimited Reinstatements of the sum insured for the same and different illness in a policy year"",
    ""si_recharge_score"": 100,
    ""pre_existing_disease"": ""Covered after 3 Years"",
    ""ped_score"": 33.3333333333333,
    ""co_pay"": ""No Co-Pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Annual (From Day 1); For defined list of tests; up to Rs. 500 for every Rs. 1 Lac Sum Insured (Individual policy: maximum Rs. 5,000 per Insured; Family Floater policy: maximum Rs. 10,000 per policy)"",
    ""hw_score"": 100,
    ""version"": null,
    ""claim_settled_ratio"": 0.9086,
    ""csr_score"": 75,
    ""ageing_of_claim"": 0.9999,
    ""aoc_score"": 100,
    ""incurred_claim_ratio"": 0.6212,
    ""icr_score"": 75,
    ""network_hospitals"": 8600,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.677,
    ""csr_abs_amt_score"": 25,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""180 days"",
      ""daycare_treatments"": ""Covered up to SI"",
      ""ambulance_cover"": ""Up to ₹2,000 per hospitalisation"",
      ""domiciliary_hospitalisation"": ""Covered up to SI"",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""Once for any condition for which hospitalisation is triggered"",
      ""daily_allowance"": ""₹1,000 per day (Upto the base SI:₹5L). ₹2,000 per day(SI : ₹7.5L and ₹15L). ₹4,000 per day(Above base SI:₹15L)"",
      ""ayush_treatment"": ""Covered up to SI"",
      ""modern_treatment"": ""Covered up to SI with a sub-limit of ₹1L on a few robotic surgeries""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""No sub-limits on room rent"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available up to the coverage amount for related diseases"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 3 years"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Rewards for healthy behaviour are available"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 90.8600%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 67.700%."",
        ""is_pro"": ""False""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 62.1200% indicates fair pricing and payment of claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""A favourable ageing of the claim for less than 3 months at 99.9900%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""8600 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 75.69789204097715,
    ""onefin_ranking"": 5,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""5/39""
  },
  {
    ""parent_category_id"": 23,
    ""insurer"": ""TATA-AIG"",
    ""category_id"": 99,
    ""insurance_plan"": ""Tata AIG Medicare"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 28603.2,
    ""annual_emi_score"": 82.42349619122669,
    ""room_description"": ""Any room category"",
    ""room_rent_score"": 100,
    ""no_claim_bonus"": ""50% increase in cumulative bonus for every claim free year upto a maximum of 100%. In the case a claim is made during the policy year the cumulative bonus would reduce by 50% in the following year."",
    ""ncb_score"": 66.6666666666667,
    ""recharge_sum_insured"": ""If you use up your entire sum insured in a policy year, we’ll restore 100% of the amount."",
    ""si_recharge_score"": 50,
    ""pre_existing_disease"": ""Covered after 3 Years"",
    ""ped_score"": 33.3333333333333,
    ""co_pay"": ""No Co-Pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Costs for preventive health check-ups will be covered up to a maximum of 1% of the policy sum insured or INR 10,000, whichever is lower."",
    ""hw_score"": 50,
    ""version"": null,
    ""claim_settled_ratio"": 0.8735,
    ""csr_score"": 50,
    ""ageing_of_claim"": 0.9355,
    ""aoc_score"": 50,
    ""incurred_claim_ratio"": 0.8653,
    ""icr_score"": 100,
    ""network_hospitals"": 7200,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.8029,
    ""csr_abs_amt_score"": 75,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""90 days"",
      ""daycare_treatments"": ""540+ daycare treatments "",
      ""ambulance_cover"": ""Up to ₹3,000"",
      ""domiciliary_hospitalisation"": ""Covered"",
      ""organ_donor_cover"": ""Available "",
      ""second_opinion"": ""Covered"",
      ""daily_allowance"": ""Up to ₹2,000 per day for choosing a shared accommodation "",
      ""ayush_treatment"": ""Covered"",
      ""modern_treatment"": ""-""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""No sub-limits on room rent"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available once in a policy year upto the cover amount"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 3 years"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available, irrespective of any annual claims"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 87.3500%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 80.2900%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A favourable ICR of 86.5300% indicates high customer satisfaction based on the premium and claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Neutral ageing of the claim for less than 3 months at 93.5500%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""7200 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 73.89371552403468,
    ""onefin_ranking"": 6,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""6/39""
  },
  {
    ""parent_category_id"": 28,
    ""insurer"": ""Manipal Cigna"",
    ""category_id"": 113,
    ""insurance_plan"": ""Manipal Cigna Pro Health Prime Advantage"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 34077.22,
    ""annual_emi_score"": 73.43672839506173,
    ""room_description"": ""Single Private Room"",
    ""room_rent_score"": 100,
    ""no_claim_bonus"": ""Guaranteed Cumulative Bonus of 25% of Base SI each year subject to a max of 200% of the Base SI irrespective of claims"",
    ""ncb_score"": 100,
    ""recharge_sum_insured"": ""100% restoration of SI unlimited times for unlimited diseases"",
    ""si_recharge_score"": 100,
    ""pre_existing_disease"": ""SI>= ₹ 7.5Lacs - 24 months"",
    ""ped_score"": 66.6666666666667,
    ""co_pay"": ""No Co-Pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Annually from 1st year onwards, for all adults insured. Limits / Test basis opted SI, offered through Network only."",
    ""hw_score"": 100,
    ""version"": null,
    ""claim_settled_ratio"": 0.8987,
    ""csr_score"": 50,
    ""ageing_of_claim"": 0.999,
    ""aoc_score"": 100,
    ""incurred_claim_ratio"": 0.7617,
    ""icr_score"": 100,
    ""network_hospitals"": 6500,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.5653,
    ""csr_abs_amt_score"": 0,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""180 days"",
      ""daycare_treatments"": ""Covered up to SI"",
      ""ambulance_cover"": ""Covered up to SI"",
      ""domiciliary_hospitalisation"": ""Covered up to 10% of SI"",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""Available for 36 listed critical illnesses"",
      ""daily_allowance"": ""For SI up to ₹10L — ₹800 per hospitalisation per day, subject to a maximum of ₹5,600. For SI above ₹10L — ₹1,000 per hospitalisation per day, subject to a maximum of ₹7,000. Cover triggers after 48 hours of hospitalisation and is payable from the first day."",
      ""ayush_treatment"": ""Covered up to SI"",
      ""modern_treatment"": ""-""
    },
    ""statements"": {
      ""pricing"": ""Affordable"",
      ""room_rent"": {
        ""statement"": ""No sub-limits on room rent"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is more than 150%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available up to the coverage amount for related diseases"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 2 years"",
        ""is_pro"": ""True""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Rewards for healthy behaviour are available"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 89.8700%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 56.5300%."",
        ""is_pro"": ""False""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A favourable ICR of 76.1700% indicates high customer satisfaction based on the premium and claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""A favourable ageing of the claim for less than 3 months at 99.900%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""6500 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 79.53101851851852,
    ""onefin_ranking"": 2,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""2/39""
  },
  {
    ""parent_category_id"": 21,
    ""insurer"": ""SBI General"",
    ""category_id"": 92,
    ""insurance_plan"": ""SBI General Aarogya Supreme Plus-Gold"",
    ""cover_plan"": 2000000,
    ""min_age"": 46,
    ""max_age"": 55,
    ""annual_emi"": 21288.38,
    ""annual_emi_score"": 94.43232860520095,
    ""room_description"": ""Single Private AC Room (upgrade option available)"",
    ""room_rent_score"": 100,
    ""no_claim_bonus"": ""15% up to a maximum of 100% (Enhanced Cumulative Bonus_Additional 50% to 200% NCB is available as an optional cover)"",
    ""ncb_score"": 100,
    ""recharge_sum_insured"": ""Refill 100% Basic Sum Insured on complete or Partial utilization of SI"",
    ""si_recharge_score"": 25,
    ""pre_existing_disease"": ""Covered after 4 years"",
    ""ped_score"": 0,
    ""co_pay"": ""10% / 20% Co-payment available"",
    ""co_pay_score"": 33.3333333333333,
    ""health_and_wellness"": ""Preventive health check-up every year from 1st renewal year"",
    ""hw_score"": 50,
    ""version"": null,
    ""claim_settled_ratio"": 0.8856,
    ""csr_score"": 50,
    ""ageing_of_claim"": 0.9504,
    ""aoc_score"": 75,
    ""incurred_claim_ratio"": 0.8192,
    ""icr_score"": 100,
    ""network_hospitals"": 6000,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.7833,
    ""csr_abs_amt_score"": 50,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""90 days"",
      ""daycare_treatments"": ""537 daycare procedures covered"",
      ""ambulance_cover"": ""₹5,000 per hospitalisation"",
      ""domiciliary_hospitalisation"": ""Covered up to SI"",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""-"",
      ""daily_allowance"": ""₹500 or ₹1,000 or ₹2,500 or ₹5,000 for 5/10/15/45 days, respectively"",
      ""ayush_treatment"": ""Covered up to SI"",
      ""modern_treatment"": ""Covered up to 25% of SI ""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""No sub-limits on room rent"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is more than 150%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available upto sum insured after its exhaustion, or at second claim"",
        ""is_pro"": ""False""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are not covered"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""20% co-pay for each claim"",
        ""is_pro"": ""False""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available, irrespective of any annual claims"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 88.5600%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 78.3300%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A favourable ICR of 81.9200% indicates high customer satisfaction based on the premium and claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Satisfying ageing of the claim for less than 3 months at 95.0400%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""6000 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 73.74636524822695,
    ""onefin_ranking"": 7,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""7/39""
  },
  {
    ""parent_category_id"": 6,
    ""insurer"": ""Aditya Birla Health"",
    ""category_id"": 30,
    ""insurance_plan"": ""Aditya Birla Activ Assure Diamond"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 27380,
    ""annual_emi_score"": 84.4316390858944,
    ""room_description"": ""Single Private A/C Room Any room upgrade available for SI 5 lacs and above"",
    ""room_rent_score"": 100,
    ""no_claim_bonus"": ""10% Increase upto 50% for every Claim Free year."",
    ""ncb_score"": 33.3333333333333,
    ""recharge_sum_insured"": ""Upto 150% of SI once a year(Max up to 50 Lacs)"",
    ""si_recharge_score"": 50,
    ""pre_existing_disease"": ""Covered after 4 years"",
    ""ped_score"": 0,
    ""co_pay"": ""No- Co-pay (Except For :Age at entry at 61 years and above ,20% Co-Payment is applicable)"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Annual"",
    ""hw_score"": 100,
    ""version"": null,
    ""claim_settled_ratio"": 0.9372,
    ""csr_score"": 75,
    ""ageing_of_claim"": 0.9941,
    ""aoc_score"": 100,
    ""incurred_claim_ratio"": 0.6956,
    ""icr_score"": 75,
    ""network_hospitals"": 10051,
    ""nh_score"": 75,
    ""claim_settled_ratio_abs_amt"": 0.7471,
    ""csr_abs_amt_score"": 50,
    ""standard_features"": {
      ""pre_hospitalisation"": ""30 days"",
      ""post_hospitalisation"": ""60 days"",
      ""daycare_treatments"": ""586 daycare procedures covered"",
      ""ambulance_cover"": ""₹ 2000"",
      ""domiciliary_hospitalisation"": ""Covered up to 10% of SI"",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""Available (for 15 listed critical illnesses)"",
      ""daily_allowance"": ""Available (Max 5 day per hospitalisation)"",
      ""ayush_treatment"": ""Available — from ₹20,000 to ₹50,000, depending on SI"",
      ""modern_treatment"": ""Covers modern treatment methods like robotic surgeries, oral chemotherapy, etc""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""No sub-limits on room rent"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 50%"",
        ""is_pro"": ""False""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available once in a policy year upto the cover amount"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are not covered"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Rewards for healthy behaviour are available"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 93.7200%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 74.7100%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 69.5600% indicates fair pricing and payment of claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""A favourable ageing of the claim for less than 3 months at 99.4100%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""10051 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 72.41282505910165,
    ""onefin_ranking"": 8,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""8/39""
  },
  {
    ""parent_category_id"": 17,
    ""insurer"": ""The New India Assurance"",
    ""category_id"": 79,
    ""insurance_plan"": ""New India Mediclaim Policy"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 26879.22,
    ""annual_emi_score"": 85.25377593905962,
    ""room_description"": ""Actual"",
    ""room_rent_score"": 66.6666666666667,
    ""no_claim_bonus"": ""25% of BSI per year up to maximum of 100%"",
    ""ncb_score"": 67,
    ""recharge_sum_insured"": ""Automatic Restoration of Base Sum Insured during Policy Year Unlimited Number of Times"",
    ""si_recharge_score"": 75,
    ""pre_existing_disease"": ""Covered after 1 Year"",
    ""ped_score"": 100,
    ""co_pay"": ""Not Available"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Covered"",
    ""hw_score"": 100,
    ""version"": null,
    ""claim_settled_ratio"": 0.9975,
    ""csr_score"": 100,
    ""ageing_of_claim"": 0.9293,
    ""aoc_score"": 25,
    ""incurred_claim_ratio"": 1.2454,
    ""icr_score"": 0,
    ""network_hospitals"": 1500,
    ""nh_score"": 0,
    ""claim_settled_ratio_abs_amt"": 1.117,
    ""csr_abs_amt_score"": 100,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""90 days"",
      ""daycare_treatments"": ""Covered up to the limit of SI opted for"",
      ""ambulance_cover"": ""Covered"",
      ""domiciliary_hospitalisation"": ""Covered up to SI"",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""-"",
      ""daily_allowance"": ""₹1,000 (Shared room occupancy)"",
      ""ayush_treatment"": ""-"",
      ""modern_treatment"": ""Covered""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""Availability of the single private room category"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available up to the coverage amount, an unlimited number of times, for unrelated diseases upto the cover amount for unlimted times for unrelated diseases "",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after or for less than 1 year"",
        ""is_pro"": ""True""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Rewards for healthy behaviour are available"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 99.7500%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 111.700%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""An unfavourable ICR of 124.5400% indicates the insurer’s adverse financial position"",
        ""is_pro"": ""False""
      },
      ""ageing_of_claim"": {
        ""statement"": ""The ageing of the claim for less than 3 months is 92.9300%"",
        ""is_pro"": ""False""
      },
      ""network_hospitals"": {
        ""statement"": ""1500 cashless network hospitals"",
        ""is_pro"": ""False""
      }
    },
    ""onefin_score"": 72.27613278171789,
    ""onefin_ranking"": 9,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""9/39""
  },
  {
    ""parent_category_id"": 10,
    ""insurer"": ""HDFC Ergo"",
    ""category_id"": 55,
    ""insurance_plan"": ""HDFC Ergo Optima Restore"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 38279.2,
    ""annual_emi_score"": 66.53828473863935,
    ""room_description"": ""No Sub limits on Room rent"",
    ""room_rent_score"": 100,
    ""no_claim_bonus"": ""Bonus of 50% of the Basic Sum Insured for every claim free policy year maximum up to 100%. In case of claim accumulated bonus will be reduced by 50%"",
    ""ncb_score"": 66.6666666666667,
    ""recharge_sum_insured"": ""100% restoration of your Basic Sum Insured instantly after the first claim,"",
    ""si_recharge_score"": 50,
    ""pre_existing_disease"": ""Covered after 3 years"",
    ""ped_score"": 33.3333333333333,
    ""co_pay"": ""No Co-Pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Available per person ranging from 1500 to 5000 depending on SI"",
    ""hw_score"": 50,
    ""version"": null,
    ""claim_settled_ratio"": 0.9564,
    ""csr_score"": 100,
    ""ageing_of_claim"": 0.9849,
    ""aoc_score"": 100,
    ""incurred_claim_ratio"": 0.9747,
    ""icr_score"": 25,
    ""network_hospitals"": 13000,
    ""nh_score"": 75,
    ""claim_settled_ratio_abs_amt"": 0.8549,
    ""csr_abs_amt_score"": 75,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""180 days"",
      ""daycare_treatments"": ""Covered upto SI"",
      ""ambulance_cover"": ""₹2,000 per hospitalisation"",
      ""domiciliary_hospitalisation"": ""Covered up to SI"",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""Covered"",
      ""daily_allowance"": ""₹800 per day, maximum up to ₹4,800 — (SI:₹3L and ₹15L). ₹1, 000 per day, maximum up to ₹6, 000 — (SI:₹20L and ₹50L)."",
      ""ayush_treatment"": "" - "",
      ""modern_treatment"": ""Covers advanced procedures like robotic surgeries, stem cell therapy, and oral chemotherapy""
    },
    ""statements"": {
      ""pricing"": ""Affordable"",
      ""room_rent"": {
        ""statement"": ""No sub-limits on room rent"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available once in a policy year upto the cover amount"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 3 years"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available, irrespective of any annual claims"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 95.6400%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 85.4900%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 97.4700% indicates expensive pricing and low payment of claims"",
        ""is_pro"": ""False""
      },
      ""ageing_of_claim"": {
        ""statement"": ""A favourable ageing of the claim for less than 3 months at 98.4900%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""13000 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 71.62815208825847,
    ""onefin_ranking"": 10,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""10/39""
  },
  {
    ""parent_category_id"": 23,
    ""insurer"": ""TATA-AIG"",
    ""category_id"": 101,
    ""insurance_plan"": ""Tata AIG Medicare Premier"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 34153.92,
    ""annual_emi_score"": 73.31080903598634,
    ""room_description"": ""Any room category"",
    ""room_rent_score"": 100,
    ""no_claim_bonus"": ""Actuals"",
    ""ncb_score"": 66.6666666666667,
    ""recharge_sum_insured"": ""If you use up your entire sum insured in a policy year, we’ll restore 100% of the amount."",
    ""si_recharge_score"": 50,
    ""pre_existing_disease"": ""Covered after 3 Years"",
    ""ped_score"": 33.3333333333333,
    ""co_pay"": ""No Co-Pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Costs for preventive health check-ups will be covered up to a maximum of 1% of the policy sum insured or INR 10,000, whichever is lower."",
    ""hw_score"": 50,
    ""version"": null,
    ""claim_settled_ratio"": 0.8735,
    ""csr_score"": 50,
    ""ageing_of_claim"": 0.9355,
    ""aoc_score"": 50,
    ""incurred_claim_ratio"": 0.8653,
    ""icr_score"": 100,
    ""network_hospitals"": 7200,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.8029,
    ""csr_abs_amt_score"": 75,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""90 days"",
      ""daycare_treatments"": ""540+ daycare treatments"",
      ""ambulance_cover"": ""Ambulance Cover up to ₹5,000"",
      ""domiciliary_hospitalisation"": ""Covered"",
      ""organ_donor_cover"": ""Available"",
      ""second_opinion"": ""Covered"",
      ""daily_allowance"": ""Up to ₹2,000 per day for choosing a shared accommodation"",
      ""ayush_treatment"": ""Covered"",
      ""modern_treatment"": ""-""
    },
    ""statements"": {
      ""pricing"": ""Affordable"",
      ""room_rent"": {
        ""statement"": ""No sub-limits on room rent"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available once in a policy year upto the cover amount"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 3 years"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available, irrespective of any annual claims"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 87.3500%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 80.2900%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A favourable ICR of 86.5300% indicates high customer satisfaction based on the premium and claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Neutral ageing of the claim for less than 3 months at 93.5500%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""7200 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 71.15990937746257,
    ""onefin_ranking"": 11,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""11/39""
  },
  {
    ""parent_category_id"": 20,
    ""insurer"": ""Reliance General Insurance"",
    ""category_id"": 88,
    ""insurance_plan"": ""Reliance Healthwise Gold"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 32113.7,
    ""annual_emi_score"": 76.66026398739164,
    ""room_description"": ""This plan does not have any limit on room expenses."",
    ""room_rent_score"": 100,
    ""no_claim_bonus"": ""5% increase for every claim free year maximum upto 20%"",
    ""ncb_score"": 33.3333333333333,
    ""recharge_sum_insured"": ""This plan does not offer restore benefit"",
    ""si_recharge_score"": 25,
    ""pre_existing_disease"": ""Covered after 2 years"",
    ""ped_score"": 66.6666666666667,
    ""co_pay"": ""No Co-Pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Reimbursement of cost of medical check\u0002up upto 1% of average Sum Insured for individual policies and upto 1.25% for Floater covers, once at the end of a block of four consecutive years provided there are no claims reported under the policies by any member, during the block."",
    ""hw_score"": 25,
    ""version"": null,
    ""claim_settled_ratio"": 0.9595,
    ""csr_score"": 100,
    ""ageing_of_claim"": 0.9865,
    ""aoc_score"": 100,
    ""incurred_claim_ratio"": 0.9876,
    ""icr_score"": 25,
    ""network_hospitals"": 8600,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.8388,
    ""csr_abs_amt_score"": 75,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""90 days"",
      ""daycare_treatments"": ""Available"",
      ""ambulance_cover"": ""Up to ₹750"",
      ""domiciliary_hospitalisation"": ""Covered up to 10% of SI "",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""5000"",
      ""daily_allowance"": ""Not Covered"",
      ""ayush_treatment"": ""-"",
      ""modern_treatment"": ""Covered up to 50% of SI""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""No sub-limits on room rent"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 50%"",
        ""is_pro"": ""False""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available upto sum insured after its exhaustion, or at second claim"",
        ""is_pro"": ""False""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 2 years"",
        ""is_pro"": ""True""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available in a claim-free year only"",
        ""is_pro"": ""False""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 95.9500%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 83.8800%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 98.7600% indicates expensive pricing and low payment of claims"",
        ""is_pro"": ""False""
      },
      ""ageing_of_claim"": {
        ""statement"": ""A favourable ageing of the claim for less than 3 months at 98.6500%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""8600 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 70.70641252955083,
    ""onefin_ranking"": 12,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""12/39""
  },
  {
    ""parent_category_id"": 10,
    ""insurer"": ""HDFC Ergo"",
    ""category_id"": 57,
    ""insurance_plan"": ""HDFC Ergo my:health Suraksha Platinum Smart"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 37109.82,
    ""annual_emi_score"": 68.4580706593118,
    ""room_description"": ""Actuals"",
    ""room_rent_score"": 66.6666666666667,
    ""no_claim_bonus"": ""25% for every claim free year upto 200% of SI"",
    ""ncb_score"": 100,
    ""recharge_sum_insured"": ""Yes upto 100% of SI for unrelated diseases for subsequent claim"",
    ""si_recharge_score"": 25,
    ""pre_existing_disease"": ""Covered after 3 years"",
    ""ped_score"": 33.3333333333333,
    ""co_pay"": ""No Co-Pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Annual"",
    ""hw_score"": 100,
    ""version"": null,
    ""claim_settled_ratio"": 0.9564,
    ""csr_score"": 100,
    ""ageing_of_claim"": 0.9849,
    ""aoc_score"": 100,
    ""incurred_claim_ratio"": 0.9747,
    ""icr_score"": 25,
    ""network_hospitals"": 13000,
    ""nh_score"": 75,
    ""claim_settled_ratio_abs_amt"": 0.8549,
    ""csr_abs_amt_score"": 75,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""180 days"",
      ""daycare_treatments"": ""At actuals"",
      ""ambulance_cover"": ""₹3,500 per hospitalisation (SI ₹20L and ₹50L). ₹15,000 per hospitalisation (SI: Above ₹50L)"",
      ""domiciliary_hospitalisation"": ""Available"",
      ""organ_donor_cover"": ""Covered"",
      ""second_opinion"": ""Available"",
      ""daily_allowance"": ""-"",
      ""ayush_treatment"": ""Covered"",
      ""modern_treatment"": ""Available""
    },
    ""statements"": {
      ""pricing"": ""Affordable"",
      ""room_rent"": {
        ""statement"": ""Availability of the single private room category"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is more than 150%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available upto sum insured after its exhaustion, or at second claim"",
        ""is_pro"": ""False""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 3 years"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Rewards for healthy behaviour are available"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 95.6400%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 85.4900%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 97.4700% indicates expensive pricing and low payment of claims"",
        ""is_pro"": ""False""
      },
      ""ageing_of_claim"": {
        ""statement"": ""A favourable ageing of the claim for less than 3 months at 98.4900%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""13000 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 70.53742119779353,
    ""onefin_ranking"": 13,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""13/39""
  },
  {
    ""parent_category_id"": 19,
    ""insurer"": ""Raheja QBE"",
    ""category_id"": 84,
    ""insurance_plan"": ""Raheja QBE Qube Super Saver"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 21722,
    ""annual_emi_score"": 93.72044917257683,
    ""room_description"": ""No Limit"",
    ""room_rent_score"": 100,
    ""no_claim_bonus"": ""Yes"",
    ""ncb_score"": 66.6666666666667,
    ""recharge_sum_insured"": ""Yes upto 100% of SI"",
    ""si_recharge_score"": 50,
    ""pre_existing_disease"": ""Covered after 4 years"",
    ""ped_score"": 0,
    ""co_pay"": ""Mandatory 20% Co-pay If entry age is above 60 Years"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Covered"",
    ""hw_score"": 50,
    ""version"": null,
    ""claim_settled_ratio"": 0.9469,
    ""csr_score"": 75,
    ""ageing_of_claim"": 0.933,
    ""aoc_score"": 50,
    ""incurred_claim_ratio"": 1.0954,
    ""icr_score"": 0,
    ""network_hospitals"": 5000,
    ""nh_score"": 25,
    ""claim_settled_ratio_abs_amt"": 0.8512,
    ""csr_abs_amt_score"": 75,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""90 days"",
      ""daycare_treatments"": ""140 daycare procedures Covered up to SI"",
      ""ambulance_cover"": ""₹1,500 (SI:₹3L and ₹9L). ₹2,500(SI: ₹10L and ₹50L)"",
      ""domiciliary_hospitalisation"": ""Yes"",
      ""organ_donor_cover"": ""20% of the SI"",
      ""second_opinion"": ""-"",
      ""daily_allowance"": ""-"",
      ""ayush_treatment"": ""Not Covered"",
      ""modern_treatment"": ""-""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""No sub-limits on room rent"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available once in a policy year upto the cover amount"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are not covered"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available, irrespective of any annual claims"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 94.6900%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 85.1200%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""An unfavourable ICR of 109.5400% indicates the insurer’s adverse financial position"",
        ""is_pro"": ""False""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Neutral ageing of the claim for less than 3 months at 93.300%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""5000 cashless network hospitals"",
        ""is_pro"": ""False""
      }
    },
    ""onefin_score"": 69.78280141843972,
    ""onefin_ranking"": 14,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""14/39""
  },
  {
    ""parent_category_id"": 7,
    ""insurer"": ""Care Health"",
    ""category_id"": 33,
    ""insurance_plan"": ""Care"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 29122,
    ""annual_emi_score"": 81.5717756763856,
    ""room_description"": ""Single Private Room"",
    ""room_rent_score"": 100,
    ""no_claim_bonus"": ""10% Increase upto 50% for every Claim Free year."",
    ""ncb_score"": 33.3333333333333,
    ""recharge_sum_insured"": ""Yes upto 100% of SI"",
    ""si_recharge_score"": 50,
    ""pre_existing_disease"": ""Covered after 4 years"",
    ""ped_score"": 0,
    ""co_pay"": ""No- Co-pay (Except For :Age at entry at 61 years and above ,20% Co-Payment is applicable)"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Annual, all Insured members"",
    ""hw_score"": 50,
    ""version"": null,
    ""claim_settled_ratio"": 0.8703,
    ""csr_score"": 50,
    ""ageing_of_claim"": 1,
    ""aoc_score"": 100,
    ""incurred_claim_ratio"": 0.6507,
    ""icr_score"": 75,
    ""network_hospitals"": 19000,
    ""nh_score"": 100,
    ""claim_settled_ratio_abs_amt"": 0.6947,
    ""csr_abs_amt_score"": 25,
    ""standard_features"": {
      ""pre_hospitalisation"": ""30 days"",
      ""post_hospitalisation"": ""60 days"",
      ""daycare_treatments"": ""540 daycare procedures covered"",
      ""ambulance_cover"": "" ₹2,000 per hospitalisation"",
      ""domiciliary_hospitalisation"": ""Covered up to 10% of SI"",
      ""organ_donor_cover"": ""₹ 1,00,000"",
      ""second_opinion"": ""Available"",
      ""daily_allowance"": ""Available (Max 5 day per hospitalisation)"",
      ""ayush_treatment"": ""Available (Specific amount as per SI ) "",
      ""modern_treatment"": ""Covers modern treatment methods like robotic surgeries, oral chemotherapy, etc""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""No sub-limits on room rent"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 50%"",
        ""is_pro"": ""False""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available once in a policy year upto the cover amount"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are not covered"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available, irrespective of any annual claims"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 87.0300%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 69.4700%."",
        ""is_pro"": ""False""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 65.0700% indicates fair pricing and payment of claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""A favourable ageing of the claim for less than 3 months at 100.0%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""19000 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 69.05486603624901,
    ""onefin_ranking"": 15,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""15/39""
  },
  {
    ""parent_category_id"": 22,
    ""insurer"": ""Star Health"",
    ""category_id"": 96,
    ""insurance_plan"": ""Star Health Comprehensive Health Insurance policy"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 35253,
    ""annual_emi_score"": 71.5064355135277,
    ""room_description"": ""Single Private Room"",
    ""room_rent_score"": 100,
    ""no_claim_bonus"": ""50% every year upto 100% for every claim free year"",
    ""ncb_score"": 66.6666666666667,
    ""recharge_sum_insured"": ""100% restoration for related illness and same member ,if sum insured is exhausted for the subsequent claim"",
    ""si_recharge_score"": 25,
    ""pre_existing_disease"": ""Covered after 3 years"",
    ""ped_score"": 33.3333333333333,
    ""co_pay"": ""No- Co-pay (Except For :Age at entry at 61 years and above ,20% Co-Payment is applicable)"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Health checkup for up to Rs 2K permissible once in a block of every claim free years of continous renewal"",
    ""hw_score"": 75,
    ""version"": null,
    ""claim_settled_ratio"": 0.825,
    ""csr_score"": 0,
    ""ageing_of_claim"": 0.9906,
    ""aoc_score"": 100,
    ""incurred_claim_ratio"": 0.8706,
    ""icr_score"": 100,
    ""network_hospitals"": 12000,
    ""nh_score"": 75,
    ""claim_settled_ratio_abs_amt"": 0.605,
    ""csr_abs_amt_score"": 25,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""90 days"",
      ""daycare_treatments"": ""101 daycare treatments covered"",
      ""ambulance_cover"": ""At actuals"",
      ""domiciliary_hospitalisation"": ""Covered up to SI"",
      ""organ_donor_cover"": ""Available"",
      ""second_opinion"": ""Available"",
      ""daily_allowance"": ""-"",
      ""ayush_treatment"": ""-"",
      ""modern_treatment"": ""Available""
    },
    ""statements"": {
      ""pricing"": ""Affordable"",
      ""room_rent"": {
        ""statement"": ""No sub-limits on room rent"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available upto sum insured after its exhaustion, or at second claim"",
        ""is_pro"": ""False""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 3 years"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""OPD coverage for future chronic illness is available"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 82.500%."",
        ""is_pro"": ""False""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 60.500%."",
        ""is_pro"": ""False""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A favourable ICR of 87.0600% indicates high customer satisfaction based on the premium and claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""A favourable ageing of the claim for less than 3 months at 99.0600%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""12000 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 68.74359732072499,
    ""onefin_ranking"": 16,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""16/39""
  },
  {
    ""parent_category_id"": 20,
    ""insurer"": ""Reliance General Insurance"",
    ""category_id"": 85,
    ""insurance_plan"": ""Reliance Health Gain Power"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 17897,
    ""annual_emi_score"": 100,
    ""room_description"": ""1% of Sum Insured Per Day"",
    ""room_rent_score"": 33.3333333333333,
    ""no_claim_bonus"": ""Yes"",
    ""ncb_score"": 66.6666666666667,
    ""recharge_sum_insured"": ""Yes upto 100% of SI"",
    ""si_recharge_score"": 50,
    ""pre_existing_disease"": ""Covered after 4 years"",
    ""ped_score"": 0,
    ""co_pay"": ""No Co-Pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Covered"",
    ""hw_score"": 50,
    ""version"": null,
    ""claim_settled_ratio"": 0.9595,
    ""csr_score"": 100,
    ""ageing_of_claim"": 0.9865,
    ""aoc_score"": 100,
    ""incurred_claim_ratio"": 0.9876,
    ""icr_score"": 25,
    ""network_hospitals"": 8600,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.8388,
    ""csr_abs_amt_score"": 75,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""90 days"",
      ""daycare_treatments"": ""140 daycare procedures Covered up to SI"",
      ""ambulance_cover"": ""₹1,500 (SI:₹3L and ₹9L). ₹2,500(SI: ₹10L and ₹50L)"",
      ""domiciliary_hospitalisation"": ""Yes"",
      ""organ_donor_cover"": ""20% of the SI"",
      ""second_opinion"": ""-"",
      ""daily_allowance"": ""-"",
      ""ayush_treatment"": ""Not Covered"",
      ""modern_treatment"": ""-""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""Sub-limits on room rent up to 1% – 2% of the sum insured"",
        ""is_pro"": ""False""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available once in a policy year upto the cover amount"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are not covered"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available, irrespective of any annual claims"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 95.9500%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 83.8800%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 98.7600% indicates expensive pricing and low payment of claims"",
        ""is_pro"": ""False""
      },
      ""ageing_of_claim"": {
        ""statement"": ""A favourable ageing of the claim for less than 3 months at 98.6500%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""8600 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 67.91666666666667,
    ""onefin_ranking"": 17,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""17/39""
  },
  {
    ""parent_category_id"": 22,
    ""insurer"": ""Star Health"",
    ""category_id"": 98,
    ""insurance_plan"": ""Star Health Medi-Classic Gold"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 29407,
    ""annual_emi_score"": 81.10388757551878,
    ""room_description"": ""Upto 5000/- per day Single Private A/C room"",
    ""room_rent_score"": 100,
    ""no_claim_bonus"": ""25% of Basic SI for every claim free year in the 2nd year 20% of Basic SI in the subsequent years subject to a maximum of @100%"",
    ""ncb_score"": 66.6666666666667,
    ""recharge_sum_insured"": ""200% Automatic Restoration once a year after exhausation of SI"",
    ""si_recharge_score"": 25,
    ""pre_existing_disease"": ""Covered after 4 years"",
    ""ped_score"": 0,
    ""co_pay"": ""No Co-Pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""1% of Average SI or maximum up to Rs 5,000 permissible once for every 4 continuous claim-free years"",
    ""hw_score"": 25,
    ""version"": null,
    ""claim_settled_ratio"": 0.825,
    ""csr_score"": 0,
    ""ageing_of_claim"": 0.9906,
    ""aoc_score"": 100,
    ""incurred_claim_ratio"": 0.8706,
    ""icr_score"": 100,
    ""network_hospitals"": 12000,
    ""nh_score"": 75,
    ""claim_settled_ratio_abs_amt"": 0.605,
    ""csr_abs_amt_score"": 25,
    ""standard_features"": {
      ""pre_hospitalisation"": ""30 days"",
      ""post_hospitalisation"": ""60 days"",
      ""daycare_treatments"": ""101 daycare treatments covered"",
      ""ambulance_cover"": ""₹2,000 per hospitalisation"",
      ""domiciliary_hospitalisation"": ""Not covered"",
      ""organ_donor_cover"": ""Available "",
      ""second_opinion"": ""-"",
      ""daily_allowance"": ""Not covered"",
      ""ayush_treatment"": ""Covered up to 25% of the basic SI, subject to a maximum of ₹25,000"",
      ""modern_treatment"": ""-""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""No sub-limits on room rent"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available upto sum insured after its exhaustion, or at second claim"",
        ""is_pro"": ""False""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are not covered"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available in a claim-free year only"",
        ""is_pro"": ""False""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 82.500%."",
        ""is_pro"": ""False""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 60.500%."",
        ""is_pro"": ""False""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A favourable ICR of 87.0600% indicates high customer satisfaction based on the premium and claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""A favourable ageing of the claim for less than 3 months at 99.0600%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""12000 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 67.87283293932231,
    ""onefin_ranking"": 18,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""18/39""
  },
  {
    ""parent_category_id"": 25,
    ""insurer"": ""Universal Sompo"",
    ""category_id"": 107,
    ""insurance_plan"": ""Universal Sompo Complete Health Insurance Plus"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 26280,
    ""annual_emi_score"": 86.23752298397689,
    ""room_description"": ""Actual"",
    ""room_rent_score"": 66.6666666666667,
    ""no_claim_bonus"": ""20% (up to 100%) The increased SI shall be decreased by 20% in event of claim but SI shall not be reduced"",
    ""ncb_score"": 66.6666666666667,
    ""recharge_sum_insured"": ""Covered upto 100% of BASE SI"",
    ""si_recharge_score"": 50,
    ""pre_existing_disease"": ""Covered after 1 Year"",
    ""ped_score"": 100,
    ""co_pay"": ""10-50%"",
    ""co_pay_score"": 0,
    ""health_and_wellness"": ""Limits in (Rs) On each continuous Renewal of the Policy 1000, 2200"",
    ""hw_score"": 25,
    ""version"": null,
    ""claim_settled_ratio"": 0.9075,
    ""csr_score"": 75,
    ""ageing_of_claim"": 0.9577,
    ""aoc_score"": 75,
    ""incurred_claim_ratio"": 1.1339,
    ""icr_score"": 0,
    ""network_hospitals"": 4000,
    ""nh_score"": 25,
    ""claim_settled_ratio_abs_amt"": 1,
    ""csr_abs_amt_score"": 100,
    ""standard_features"": {
      ""pre_hospitalisation"": ""30 days"",
      ""post_hospitalisation"": ""60 days"",
      ""daycare_treatments"": ""Covered up to SI"",
      ""ambulance_cover"": ""Up to 1% of SI or ₹5,000 or actuals, whichever is less"",
      ""domiciliary_hospitalisation"": ""Covered up to 20% of the base SI"",
      ""organ_donor_cover"": ""Covered upto SI"",
      ""second_opinion"": ""-"",
      ""daily_allowance"": ""₹2,000 per day (when hospitalisation exceeds 3 days); the maximum number of days payable are 7 days"",
      ""ayush_treatment"": ""Covered up to SI"",
      ""modern_treatment"": ""Covered""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""Availability of the single private room category"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available once in a policy year upto the cover amount"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after or for less than 1 year"",
        ""is_pro"": ""True""
      },
      ""co_pay"": {
        ""statement"": ""More than 30% co-pay for each claim"",
        ""is_pro"": ""False""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available in a claim-free year only"",
        ""is_pro"": ""False""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 90.7500%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 100.0%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""An unfavourable ICR of 113.3900% indicates the insurer’s adverse financial position"",
        ""is_pro"": ""False""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Satisfying ageing of the claim for less than 3 months at 95.7700%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""4000 cashless network hospitals"",
        ""is_pro"": ""False""
      }
    },
    ""onefin_score"": 66.91292356185974,
    ""onefin_ranking"": 19,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""19/39""
  },
  {
    ""parent_category_id"": 16,
    ""insurer"": ""Navi General"",
    ""category_id"": 76,
    ""insurance_plan"": ""Navi Health Plan 1"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 23098.5,
    ""annual_emi_score"": 91.4606317310218,
    ""room_description"": ""Actual"",
    ""room_rent_score"": 66.6666666666667,
    ""no_claim_bonus"": ""25% of BSI per year up to max 50%; 25% of BSI per year up to max 100%; 50% of BSI per year up to max 150%; or No Cover"",
    ""ncb_score"": 66.6666666666667,
    ""recharge_sum_insured"": ""Automatic Restoration of Base Sum Insured during Policy Year Once,Unlimited Number of Times or No Cover"",
    ""si_recharge_score"": 75,
    ""pre_existing_disease"": ""Covered after 4 years"",
    ""ped_score"": 0,
    ""co_pay"": ""Not Available"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Covered"",
    ""hw_score"": 100,
    ""version"": null,
    ""claim_settled_ratio"": 0.8252,
    ""csr_score"": 25,
    ""ageing_of_claim"": 0.9999,
    ""aoc_score"": 100,
    ""incurred_claim_ratio"": 0.2856,
    ""icr_score"": 50,
    ""network_hospitals"": 10000,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.47,
    ""csr_abs_amt_score"": 0,
    ""standard_features"": {
      ""pre_hospitalisation"": ""30, 60 or 90 days"",
      ""post_hospitalisation"": ""60, 90 or 180 days"",
      ""daycare_treatments"": ""Covered up to the limit of SI opted for"",
      ""ambulance_cover"": ""Covered"",
      ""domiciliary_hospitalisation"": ""Covered up to SI"",
      ""organ_donor_cover"": ""Up to SI or not covered"",
      ""second_opinion"": ""-"",
      ""daily_allowance"": ""₹1,000 (Shared room occupancy)"",
      ""ayush_treatment"": ""Covered up to SI"",
      ""modern_treatment"": ""Covered""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""Availability of the single private room category"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available up to the coverage amount, an unlimited number of times, for unrelated diseases upto the cover amount for unlimted times for unrelated diseases "",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are not covered"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Rewards for healthy behaviour are available"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 82.5200%."",
        ""is_pro"": ""False""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 47.00%."",
        ""is_pro"": ""False""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 28.5600% indicates balanced pricing and payment of claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""A favourable ageing of the claim for less than 3 months at 99.9900%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""10000 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 66.60485618597322,
    ""onefin_ranking"": 20,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""20/39""
  },
  {
    ""parent_category_id"": 27,
    ""insurer"": ""Bajaj Allianz"",
    ""category_id"": 111,
    ""insurance_plan"": ""Bajaj Allianz Health Guard-Gold Plan"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 34953.96,
    ""annual_emi_score"": 71.99737325978461,
    ""room_description"": ""Single Private Ac Room"",
    ""room_rent_score"": 100,
    ""no_claim_bonus"": ""50% of base Sum Insured per annum for first 2 years and later 10% of base Sum Insured per annum for next 5 years"",
    ""ncb_score"": 66.6666666666667,
    ""recharge_sum_insured"": ""In event of claim amount exceeding the limit of indemnity, Sum Insured would be increased by 20% maximum up to 5 Lacs."",
    ""si_recharge_score"": 25,
    ""pre_existing_disease"": ""Covered after 3 years(36 months)"",
    ""ped_score"": 33.3333333333333,
    ""co_pay"": ""Zone-wise Co-pay/Discount applicable"",
    ""co_pay_score"": 33.3333333333333,
    ""health_and_wellness"": ""1% of SI in a block of Continuous 3 years max upto 5,000/-"",
    ""hw_score"": 25,
    ""version"": null,
    ""claim_settled_ratio"": 0.943,
    ""csr_score"": 75,
    ""ageing_of_claim"": 0.9659,
    ""aoc_score"": 75,
    ""incurred_claim_ratio"": 0.9064,
    ""icr_score"": 25,
    ""network_hospitals"": 7600,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.8879,
    ""csr_abs_amt_score"": 75,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""90 days"",
      ""daycare_treatments"": ""Covered"",
      ""ambulance_cover"": ""Covered max. upto ₹20,000 per policy year"",
      ""domiciliary_hospitalisation"": ""-"",
      ""organ_donor_cover"": ""Available"",
      ""second_opinion"": ""-"",
      ""daily_allowance"": "" ₹500 per day for max. 10days (For Parent of child under the age12 years)"",
      ""ayush_treatment"": ""Covered max. upto ₹20,000 per policy year"",
      ""modern_treatment"": ""Available""
    },
    ""statements"": {
      ""pricing"": ""Affordable"",
      ""room_rent"": {
        ""statement"": ""No sub-limits on room rent"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available upto sum insured after its exhaustion, or at second claim"",
        ""is_pro"": ""False""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 3 years"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""20% co-pay for each claim"",
        ""is_pro"": ""False""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available in a claim-free year only"",
        ""is_pro"": ""False""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 94.300%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 88.7900%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 90.6400% indicates expensive pricing and low payment of claims"",
        ""is_pro"": ""False""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Satisfying ageing of the claim for less than 3 months at 96.5900%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""7600 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 64.30754531126871,
    ""onefin_ranking"": 21,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""21/39""
  },
  {
    ""parent_category_id"": 9,
    ""insurer"": ""Future Generali"",
    ""category_id"": 51,
    ""insurance_plan"": ""Future Health Total Superior"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 36426.6,
    ""annual_emi_score"": 69.57972156553717,
    ""room_description"": ""Upto SI"",
    ""room_rent_score"": 66.6666666666667,
    ""no_claim_bonus"": ""50% for every claim-free year to max 100%"",
    ""ncb_score"": 66.6666666666667,
    ""recharge_sum_insured"": ""Applicable"",
    ""si_recharge_score"": 50,
    ""pre_existing_disease"": ""Covered after 2 Years"",
    ""ped_score"": 66.6666666666667,
    ""co_pay"": ""20% co-payment where entry age is from 60 year to 64 years 25% co-payment where entry age is from 65 year to 69 years 30% co-payment where entry age is from 70 year to 74 years 40% co-payment where entry age is 75 years and above"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Not Available"",
    ""hw_score"": 0,
    ""version"": null,
    ""claim_settled_ratio"": 0.8145,
    ""csr_score"": 0,
    ""ageing_of_claim"": 0.9601,
    ""aoc_score"": 75,
    ""incurred_claim_ratio"": 0.8844,
    ""icr_score"": 100,
    ""network_hospitals"": 6000,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.7916,
    ""csr_abs_amt_score"": 50,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""120 days"",
      ""daycare_treatments"": ""409 daycare procedures covered"",
      ""ambulance_cover"": ""At actuals"",
      ""domiciliary_hospitalisation"": ""Maximum up to 10% of SI"",
      ""organ_donor_cover"": ""Covered"",
      ""second_opinion"": ""-"",
      ""daily_allowance"": ""-"",
      ""ayush_treatment"": ""Covered"",
      ""modern_treatment"": ""-""
    },
    ""statements"": {
      ""pricing"": ""Affordable"",
      ""room_rent"": {
        ""statement"": ""Availability of the single private room category"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available once in a policy year upto the cover amount"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 2 years"",
        ""is_pro"": ""True""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Health and wellness benefits are not available"",
        ""is_pro"": ""False""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 81.4500%."",
        ""is_pro"": ""False""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 79.1600%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A favourable ICR of 88.4400% indicates high customer satisfaction based on the premium and claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Satisfying ageing of the claim for less than 3 months at 96.0100%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""6000 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 63.790583136327825,
    ""onefin_ranking"": 22,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""22/39""
  },
  {
    ""parent_category_id"": 14,
    ""insurer"": ""Magma HDI"",
    ""category_id"": 72,
    ""insurance_plan"": ""Magma HDI One Health Premium"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 34391.1,
    ""annual_emi_score"": 72.92142763330706,
    ""room_description"": ""Covered up to SI No room rent capping"",
    ""room_rent_score"": 100,
    ""no_claim_bonus"": ""20% of SI subject to a maximum of 100%"",
    ""ncb_score"": 66.6666666666667,
    ""recharge_sum_insured"": ""Recharge up to 100% of SI, max 5 times a policy year"",
    ""si_recharge_score"": 25,
    ""pre_existing_disease"": ""Covered after 3 Years"",
    ""ped_score"": 33.3333333333333,
    ""co_pay"": ""Voluntary Co-Payment 10% or 20%"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Annual"",
    ""hw_score"": 100,
    ""version"": null,
    ""claim_settled_ratio"": 0.847,
    ""csr_score"": 25,
    ""ageing_of_claim"": 0.9234,
    ""aoc_score"": 25,
    ""incurred_claim_ratio"": 0.6642,
    ""icr_score"": 75,
    ""network_hospitals"": 4300,
    ""nh_score"": 25,
    ""claim_settled_ratio_abs_amt"": 0.6774,
    ""csr_abs_amt_score"": 25,
    ""standard_features"": {
      ""pre_hospitalisation"": ""30 days"",
      ""post_hospitalisation"": ""60 days"",
      ""daycare_treatments"": ""Covered up to SI"",
      ""ambulance_cover"": ""Up to ₹7,500"",
      ""domiciliary_hospitalisation"": ""Covered up to SI"",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""Covered"",
      ""daily_allowance"": ""₹1,000 per day"",
      ""ayush_treatment"": ""Covered up to SI"",
      ""modern_treatment"": ""Covered up to SI""
    },
    ""statements"": {
      ""pricing"": ""Affordable"",
      ""room_rent"": {
        ""statement"": ""No sub-limits on room rent"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available upto sum insured after its exhaustion, or at second claim"",
        ""is_pro"": ""False""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 3 years"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Rewards for healthy behaviour are available"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 84.700%."",
        ""is_pro"": ""False""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 67.7400%."",
        ""is_pro"": ""False""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 66.4200% indicates fair pricing and payment of claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""The ageing of the claim for less than 3 months is 92.3400%"",
        ""is_pro"": ""False""
      },
      ""network_hospitals"": {
        ""statement"": ""4300 cashless network hospitals"",
        ""is_pro"": ""False""
      }
    },
    ""onefin_score"": 63.543094956658784,
    ""onefin_ranking"": 23,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""23/39""
  },
  {
    ""parent_category_id"": 16,
    ""insurer"": ""Navi General"",
    ""category_id"": 77,
    ""insurance_plan"": ""Navi Health Plan 5"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 25146.98,
    ""annual_emi_score"": 88.09761623325453,
    ""room_description"": ""Actual"",
    ""room_rent_score"": 66.6666666666667,
    ""no_claim_bonus"": ""25% of BSI per year up to maximum of 50%"",
    ""ncb_score"": 33.3333333333333,
    ""recharge_sum_insured"": ""Automatic Restoration of Base Sum Insured during Policy Year Once"",
    ""si_recharge_score"": 50,
    ""pre_existing_disease"": ""Covered after 3 Years"",
    ""ped_score"": 33.3333333333333,
    ""co_pay"": ""Not Available"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Covered"",
    ""hw_score"": 100,
    ""version"": null,
    ""claim_settled_ratio"": 0.8252,
    ""csr_score"": 25,
    ""ageing_of_claim"": 0.9999,
    ""aoc_score"": 100,
    ""incurred_claim_ratio"": 0.2856,
    ""icr_score"": 50,
    ""network_hospitals"": 10000,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.47,
    ""csr_abs_amt_score"": 0,
    ""standard_features"": {
      ""pre_hospitalisation"": ""30 days"",
      ""post_hospitalisation"": ""60 days"",
      ""daycare_treatments"": ""Covered up to the limit of SI opted for"",
      ""ambulance_cover"": ""Covered"",
      ""domiciliary_hospitalisation"": ""Covered up to SI"",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""-"",
      ""daily_allowance"": ""₹1,000 (Shared room occupancy)"",
      ""ayush_treatment"": ""-"",
      ""modern_treatment"": ""Covered""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""Availability of the single private room category"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 50%"",
        ""is_pro"": ""False""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available once in a policy year upto the cover amount"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 3 years"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Rewards for healthy behaviour are available"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 82.5200%."",
        ""is_pro"": ""False""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 47.00%."",
        ""is_pro"": ""False""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 28.5600% indicates balanced pricing and payment of claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""A favourable ageing of the claim for less than 3 months at 99.9900%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""10000 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 63.51261820330969,
    ""onefin_ranking"": 24,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""24/39""
  },
  {
    ""parent_category_id"": 13,
    ""insurer"": ""Liberty General Insurance"",
    ""category_id"": 66,
    ""insurance_plan"": ""Liberty General Secure Health Connect -Secure Elite"",
    ""cover_plan"": 2000000,
    ""min_age"": 52,
    ""max_age"": 52,
    ""annual_emi"": 23078,
    ""annual_emi_score"": 91.49428684003152,
    ""room_description"": ""Cap on Room Rent (1% of annual sum insured in case of stay in Non ICU 2% of annual sum insured in case of stay in ICU)"",
    ""room_rent_score"": 33.3333333333333,
    ""no_claim_bonus"": ""We will increase Your Base Sum Insured by 10% subject to the maximum limit specified in the Policy Schedule at the end of the Policy Year"",
    ""ncb_score"": 33.3333333333333,
    ""recharge_sum_insured"": ""100% restoration of the Base Sum Insured amount once in a Policy Year if the Base Sum Insured and the Cumulative Bonus (if any) is insufficient as a result of previous Claims in that Policy Year."",
    ""si_recharge_score"": 50,
    ""pre_existing_disease"": ""Covered after 4 years"",
    ""ped_score"": 0,
    ""co_pay"": ""No Co-pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""The policy also includes one free health check-up at our network provider for each Insured Person above 18 years of Age, each Policy Year for specified tests."",
    ""hw_score"": 50,
    ""version"": null,
    ""claim_settled_ratio"": 0.9077,
    ""csr_score"": 75,
    ""ageing_of_claim"": 0.973,
    ""aoc_score"": 75,
    ""incurred_claim_ratio"": 0.893,
    ""icr_score"": 100,
    ""network_hospitals"": 5471,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.8183,
    ""csr_abs_amt_score"": 75,
    ""standard_features"": {
      ""pre_hospitalisation"": ""30 days"",
      ""post_hospitalisation"": ""60 days"",
      ""daycare_treatments"": ""405 daycare procedures covered"",
      ""ambulance_cover"": ""Up to ₹1,500 per hospitalisation"",
      ""domiciliary_hospitalisation"": ""Covered up to a maximum aggregate sub-limit of 20% of the SI"",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""Covered"",
      ""daily_allowance"": ""₹500 per day for a minimum of 3 days of hospitalisation, subject to a maximum of 10 days"",
      ""ayush_treatment"": ""Covered"",
      ""modern_treatment"": ""Covered up to 50% of SI""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""Sub-limits on room rent up to 1% – 2% of the sum insured"",
        ""is_pro"": ""False""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 50%"",
        ""is_pro"": ""False""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available once in a policy year upto the cover amount"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are not covered"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available, irrespective of any annual claims"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 90.7700%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 81.8300%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A favourable ICR of 89.300% indicates high customer satisfaction based on the premium and claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Satisfying ageing of the claim for less than 3 months at 97.300%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""5471 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 63.28161938534278,
    ""onefin_ranking"": 25,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""25/39""
  },
  {
    ""parent_category_id"": 19,
    ""insurer"": ""Raheja QBE"",
    ""category_id"": 82,
    ""insurance_plan"": ""Raheja QBE Qube Basic"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 46627,
    ""annual_emi_score"": 52.83359600735487,
    ""room_description"": ""Covered up to Sum Insured (except for Suite or above room category)"",
    ""room_rent_score"": 66.6666666666667,
    ""no_claim_bonus"": ""10% Every Year upto Max 100% as a loyalty addition in the form of NCB. (Based on continuity of the policy in the 2 & 3 year) With Optional Cover 20% increase of the expiring SI subject to a maximum of 200% of the base SI"",
    ""ncb_score"": 100,
    ""recharge_sum_insured"": ""Unlimited Reinstatements of the sum insured for the same and different illness in a policy year"",
    ""si_recharge_score"": 75,
    ""pre_existing_disease"": ""Covered after 2 Years"",
    ""ped_score"": 66.6666666666667,
    ""co_pay"": ""No Co-Pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Annual, Tests covered up to worth Rs 2,500 per Insured Person for 10,15,20 Lakhs, Annual, Tests covered up to worth Rs 5,000 per Insured Person for 30 Lakhs, Annual, Tests covered up to worth Rs 7,500 per Insured Person for 50 Lakhs"",
    ""hw_score"": 50,
    ""version"": null,
    ""claim_settled_ratio"": 0.9469,
    ""csr_score"": 75,
    ""ageing_of_claim"": 0.933,
    ""aoc_score"": 50,
    ""incurred_claim_ratio"": 1.0954,
    ""icr_score"": 0,
    ""network_hospitals"": 5000,
    ""nh_score"": 25,
    ""claim_settled_ratio_abs_amt"": 0.8512,
    ""csr_abs_amt_score"": 75,
    ""standard_features"": {
      ""pre_hospitalisation"": ""90 days"",
      ""post_hospitalisation"": ""180 days"",
      ""daycare_treatments"": ""Covered up to SI"",
      ""ambulance_cover"": ""For network hospitals — Covered up to SI. For non-network hospitals — covered up to ₹2,000 per event"",
      ""domiciliary_hospitalisation"": ""Covered up to SI"",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""Covered up to SI"",
      ""daily_allowance"": ""₹3,000 per day"",
      ""ayush_treatment"": ""Covered up to SI"",
      ""modern_treatment"": ""Covered up to SI with a sub-limit of ₹1L on a few robotic surgeries""
    },
    ""statements"": {
      ""pricing"": ""Affordable"",
      ""room_rent"": {
        ""statement"": ""Availability of the single private room category"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is more than 150%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available up to the coverage amount, an unlimited number of times, for unrelated diseases upto the cover amount for unlimted times for unrelated diseases "",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 2 years"",
        ""is_pro"": ""True""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available, irrespective of any annual claims"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 94.6900%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 85.1200%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""An unfavourable ICR of 109.5400% indicates the insurer’s adverse financial position"",
        ""is_pro"": ""False""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Neutral ageing of the claim for less than 3 months at 93.300%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""5000 cashless network hospitals"",
        ""is_pro"": ""False""
      }
    },
    ""onefin_score"": 62.10007880220647,
    ""onefin_ranking"": 26,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""26/39""
  },
  {
    ""parent_category_id"": 21,
    ""insurer"": ""SBI General"",
    ""category_id"": 93,
    ""insurance_plan"": ""SBI General Aarogya Premier Policy"",
    ""cover_plan"": 2000000,
    ""min_age"": 46,
    ""max_age"": 55,
    ""annual_emi"": 27003,
    ""annual_emi_score"": 85.0505647491463,
    ""room_description"": ""Actuals up to Sum Insured"",
    ""room_rent_score"": 66.6666666666667,
    ""no_claim_bonus"": ""10% of SI for each claim-free year up to 50%"",
    ""ncb_score"": 33.3333333333333,
    ""recharge_sum_insured"": ""Auto Reinstatement of base SI, If exhausted , at no additonal premium"",
    ""si_recharge_score"": 25,
    ""pre_existing_disease"": ""Covered after 4 years"",
    ""ped_score"": 0,
    ""co_pay"": ""No Co-Pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Reimbursement of health checkup upto Rs 5000 in case of claims for 4 consecutive years"",
    ""hw_score"": 25,
    ""version"": null,
    ""claim_settled_ratio"": 0.8856,
    ""csr_score"": 50,
    ""ageing_of_claim"": 0.9504,
    ""aoc_score"": 75,
    ""incurred_claim_ratio"": 0.8192,
    ""icr_score"": 100,
    ""network_hospitals"": 6000,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.7833,
    ""csr_abs_amt_score"": 50,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""90 days"",
      ""daycare_treatments"": ""537 daycare procedures covered"",
      ""ambulance_cover"": ""At actuals"",
      ""domiciliary_hospitalisation"": ""Covered up to SI"",
      ""organ_donor_cover"": ""Covered"",
      ""second_opinion"": ""-"",
      ""daily_allowance"": ""-"",
      ""ayush_treatment"": ""Covered up to SI"",
      ""modern_treatment"": ""-""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""Availability of the single private room category"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 50%"",
        ""is_pro"": ""False""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available upto sum insured after its exhaustion, or at second claim"",
        ""is_pro"": ""False""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are not covered"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available in a claim-free year only"",
        ""is_pro"": ""False""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 88.5600%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 78.3300%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A favourable ICR of 81.9200% indicates high customer satisfaction based on the premium and claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Satisfying ageing of the claim for less than 3 months at 95.0400%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""6000 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 61.97350275807723,
    ""onefin_ranking"": 27,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""27/39""
  },
  {
    ""parent_category_id"": 11,
    ""insurer"": ""IFFCO Tokio"",
    ""category_id"": 59,
    ""insurance_plan"": ""IFFCO Tokio Individual Health Protector"",
    ""cover_plan"": 2000000,
    ""min_age"": 46,
    ""max_age"": 55,
    ""annual_emi"": 30956.12,
    ""annual_emi_score"": 78.56067769897557,
    ""room_description"": ""S.I of 7 Lacs and above Covered up to actuals S.I below 7 Lacs Up to 1.50% of S.I. for class A cities and 1.25% of sum insured for other cities"",
    ""room_rent_score"": 67,
    ""no_claim_bonus"": ""The Cumulative Bonus shall be increased by 5%(five percent) of the basic sum insured at each renewal in respect of each claim free year of insurance for all insured person(s) on collective basis subject to maximum of 50%(fifty percent) of basic sum insured of the expiring policy."",
    ""ncb_score"": 66.6666666666667,
    ""recharge_sum_insured"": ""Available"",
    ""si_recharge_score"": 25,
    ""pre_existing_disease"": ""Covered after 4 years"",
    ""ped_score"": 0,
    ""co_pay"": ""No Co-Pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Insured person(s), on individual or collective basis, shall be entitled for reimbursement of cost of medical checkup once at the end of a block of every four claim-free policies with us in the subsequent renewal. The reimbursement shall not exceed the amount equal to 1% (one percent) of the average sum insured during the block of four claim free policies."",
    ""hw_score"": 100,
    ""version"": null,
    ""claim_settled_ratio"": 0.9496,
    ""csr_score"": 75,
    ""ageing_of_claim"": 0.8938,
    ""aoc_score"": 0,
    ""incurred_claim_ratio"": 1.3065,
    ""icr_score"": 0,
    ""network_hospitals"": 6900,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.9647,
    ""csr_abs_amt_score"": 100,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""90 days"",
      ""daycare_treatments"": ""160+ daycare procedures covered"",
      ""ambulance_cover"": ""Maximum up to 1% of SI or ₹2,500 per claim"",
      ""domiciliary_hospitalisation"": ""Covered up to a maximum aggregate sub-limit of 20% of SI"",
      ""organ_donor_cover"": ""Covered"",
      ""second_opinion"": ""Covered"",
      ""daily_allowance"": ""Up to 0.20% of  SI per day"",
      ""ayush_treatment"": ""-"",
      ""modern_treatment"": ""Covered up to 50% of SI""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""Availability of the single private room category"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available upto sum insured after its exhaustion, or at second claim"",
        ""is_pro"": ""False""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are not covered"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Rewards for healthy behaviour are available"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 94.9600%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 96.4700%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""An unfavourable ICR of 130.6500% indicates the insurer’s adverse financial position"",
        ""is_pro"": ""False""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Adverse ageing of the claim for less than 3 months at 89.3800%"",
        ""is_pro"": ""False""
      },
      ""network_hospitals"": {
        ""statement"": ""6900 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 60.28486997635934,
    ""onefin_ranking"": 28,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""28/39""
  },
  {
    ""parent_category_id"": 19,
    ""insurer"": ""Raheja QBE"",
    ""category_id"": 83,
    ""insurance_plan"": ""Raheja QBE Qube Comprehensive"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 21722,
    ""annual_emi_score"": 93.72044917257683,
    ""room_description"": ""1% of Sum Insured Per Day"",
    ""room_rent_score"": 33.3333333333333,
    ""no_claim_bonus"": ""Yes"",
    ""ncb_score"": 66.6666666666667,
    ""recharge_sum_insured"": ""Yes upto 100% of SI"",
    ""si_recharge_score"": 50,
    ""pre_existing_disease"": ""Covered after 4 years"",
    ""ped_score"": 0,
    ""co_pay"": ""Mandatory 20% Co-pay If entry age is above 60 Years"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Covered"",
    ""hw_score"": 50,
    ""version"": null,
    ""claim_settled_ratio"": 0.9469,
    ""csr_score"": 75,
    ""ageing_of_claim"": 0.933,
    ""aoc_score"": 50,
    ""incurred_claim_ratio"": 1.0954,
    ""icr_score"": 0,
    ""network_hospitals"": 5000,
    ""nh_score"": 25,
    ""claim_settled_ratio_abs_amt"": 0.8512,
    ""csr_abs_amt_score"": 75,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""90 days"",
      ""daycare_treatments"": ""140 daycare procedures Covered up to SI"",
      ""ambulance_cover"": ""₹1,500 (SI:₹3L and ₹9L). ₹2,500(SI: ₹10L and ₹50L)."",
      ""domiciliary_hospitalisation"": ""Yes"",
      ""organ_donor_cover"": ""20% of the SI"",
      ""second_opinion"": ""-"",
      ""daily_allowance"": ""-"",
      ""ayush_treatment"": ""Not Covered"",
      ""modern_treatment"": ""-""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""Sub-limits on room rent up to 1% – 2% of the sum insured"",
        ""is_pro"": ""False""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available once in a policy year upto the cover amount"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are not covered"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available, irrespective of any annual claims"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 94.6900%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 85.1200%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""An unfavourable ICR of 109.5400% indicates the insurer’s adverse financial position"",
        ""is_pro"": ""False""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Neutral ageing of the claim for less than 3 months at 93.300%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""5000 cashless network hospitals"",
        ""is_pro"": ""False""
      }
    },
    ""onefin_score"": 59.78280141843972,
    ""onefin_ranking"": 29,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""29/39""
  },
  {
    ""parent_category_id"": 12,
    ""insurer"": ""Kotak Mahindra General"",
    ""category_id"": 61,
    ""insurance_plan"": ""Kotak Health Premier 360"",
    ""cover_plan"": 2000000,
    ""min_age"": 52,
    ""max_age"": 52,
    ""annual_emi"": 23303,
    ""annual_emi_score"": 91.12490149724192,
    ""room_description"": ""Cap on Room Rent (1% of annual sum insured in case of stay in Non ICU 2% of annual sum insured in case of stay in ICU)"",
    ""room_rent_score"": 33.3333333333333,
    ""no_claim_bonus"": ""We will increase Your Base Sum Insured by 10% subject to the maximum limit specified in the Policy Schedule at the end of the Policy Year"",
    ""ncb_score"": 33.3333333333333,
    ""recharge_sum_insured"": ""100% restoration of the Base Sum Insured amount once in a Policy Year if the Base Sum Insured and the Cumulative Bonus (if any) is insufficient as a result of previous Claims in that Policy Year."",
    ""si_recharge_score"": 50,
    ""pre_existing_disease"": ""Covered after 4 years"",
    ""ped_score"": 0,
    ""co_pay"": ""No Co-pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""The policy also includes one free health check-up at our network provider for each Insured Person above 18 years of Age, each Policy Year for specified tests."",
    ""hw_score"": 50,
    ""version"": null,
    ""claim_settled_ratio"": 0.8679,
    ""csr_score"": 50,
    ""ageing_of_claim"": 0.969,
    ""aoc_score"": 75,
    ""incurred_claim_ratio"": 0.7211,
    ""icr_score"": 75,
    ""network_hospitals"": 5000,
    ""nh_score"": 25,
    ""claim_settled_ratio_abs_amt"": 0.8157,
    ""csr_abs_amt_score"": 75,
    ""standard_features"": {
      ""pre_hospitalisation"": ""30 days"",
      ""post_hospitalisation"": ""60 days"",
      ""daycare_treatments"": ""405 day care procedures covered"",
      ""ambulance_cover"": ""Ambulance cover up to ₹1,500"",
      ""domiciliary_hospitalisation"": ""Covered up to a maximum aggregate sub-limit of 20% of SI"",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""Covered"",
      ""daily_allowance"": ""₹500 per day for a minimum of 3 days of hospitalisation, subject to a maximum of 10 days"",
      ""ayush_treatment"": ""Covered"",
      ""modern_treatment"": ""Covered up to 50% of SI""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""Sub-limits on room rent up to 1% – 2% of the sum insured"",
        ""is_pro"": ""False""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 50%"",
        ""is_pro"": ""False""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available once in a policy year upto the cover amount"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are not covered"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available, irrespective of any annual claims"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 86.7900%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 81.5700%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 72.1100% indicates fair pricing and payment of claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Satisfying ageing of the claim for less than 3 months at 96.900%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""5000 cashless network hospitals"",
        ""is_pro"": ""False""
      }
    },
    ""onefin_score"": 59.4208037825059,
    ""onefin_ranking"": 30,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""30/39""
  },
  {
    ""parent_category_id"": 15,
    ""insurer"": ""National Insurance"",
    ""category_id"": 73,
    ""insurance_plan"": ""National Mediclaim Policy"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 43842.9,
    ""annual_emi_score"": 57.40428815340163,
    ""room_description"": ""Covered up to SI No room rent capping"",
    ""room_rent_score"": 100,
    ""no_claim_bonus"": ""33.33% of SI subject to a maximum of 100%"",
    ""ncb_score"": 66.6666666666667,
    ""recharge_sum_insured"": ""Recharge up to 100% of SI, max 5 times a policy year"",
    ""si_recharge_score"": 25,
    ""pre_existing_disease"": ""Covered after 2 Years"",
    ""ped_score"": 66.6666666666667,
    ""co_pay"": ""No Co-Pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Annual"",
    ""hw_score"": 100,
    ""version"": null,
    ""claim_settled_ratio"": 0.9161,
    ""csr_score"": 75,
    ""ageing_of_claim"": 0.8628,
    ""aoc_score"": 0,
    ""incurred_claim_ratio"": 1.2553,
    ""icr_score"": 0,
    ""network_hospitals"": null,
    ""nh_score"": 0,
    ""claim_settled_ratio_abs_amt"": 0.7195,
    ""csr_abs_amt_score"": 50,
    ""standard_features"": {
      ""pre_hospitalisation"": ""30 days"",
      ""post_hospitalisation"": ""60 days"",
      ""daycare_treatments"": ""Covered up to SI"",
      ""ambulance_cover"": ""Up to ₹10,000"",
      ""domiciliary_hospitalisation"": ""Covered up to SI"",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""Covered"",
      ""daily_allowance"": ""₹1,500 per day"",
      ""ayush_treatment"": ""Covered up to SI"",
      ""modern_treatment"": ""Covered up to SI""
    },
    ""statements"": {
      ""pricing"": ""Affordable"",
      ""room_rent"": {
        ""statement"": ""No sub-limits on room rent"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available upto sum insured after its exhaustion, or at second claim"",
        ""is_pro"": ""False""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 2 years"",
        ""is_pro"": ""True""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Rewards for healthy behaviour are available"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 91.6100%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 71.9500%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""An unfavourable ICR of 125.5300% indicates the insurer’s adverse financial position"",
        ""is_pro"": ""False""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Adverse ageing of the claim for less than 3 months at 86.2800%"",
        ""is_pro"": ""False""
      },
      ""network_hospitals"": {
        ""statement"": ""cashless network hospitals"",
        ""is_pro"": ""False""
      }
    },
    ""onefin_score"": 58.887953112687164,
    ""onefin_ranking"": 31,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""31/39""
  },
  {
    ""parent_category_id"": 12,
    ""insurer"": ""Kotak Mahindra General"",
    ""category_id"": 64,
    ""insurance_plan"": ""Kotak Health Premier Elite"",
    ""cover_plan"": 2000000,
    ""min_age"": 52,
    ""max_age"": 52,
    ""annual_emi"": 24730,
    ""annual_emi_score"": 88.78217756763856,
    ""room_description"": ""Cap on Room Rent (1% of annual sum insured in case of stay in Non ICU 2% of annual sum insured in case of stay in ICU)"",
    ""room_rent_score"": 33.3333333333333,
    ""no_claim_bonus"": ""We will increase Your Base Sum Insured by 10% subject to the maximum limit specified in the Policy Schedule at the end of the Policy Year"",
    ""ncb_score"": 33.3333333333333,
    ""recharge_sum_insured"": ""100% restoration of the Base Sum Insured amount once in a Policy Year if the Base Sum Insured and the Cumulative Bonus (if any) is insufficient as a result of previous Claims in that Policy Year."",
    ""si_recharge_score"": 50,
    ""pre_existing_disease"": ""Covered after 4 years"",
    ""ped_score"": 0,
    ""co_pay"": ""No Co-pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""The policy also includes one free health check-up at our network provider for each Insured Person above 18 years of Age, each Policy Year for specified tests."",
    ""hw_score"": 50,
    ""version"": null,
    ""claim_settled_ratio"": 0.8679,
    ""csr_score"": 50,
    ""ageing_of_claim"": 0.969,
    ""aoc_score"": 75,
    ""incurred_claim_ratio"": 0.7211,
    ""icr_score"": 75,
    ""network_hospitals"": 5000,
    ""nh_score"": 25,
    ""claim_settled_ratio_abs_amt"": 0.8157,
    ""csr_abs_amt_score"": 75,
    ""standard_features"": {
      ""pre_hospitalisation"": ""30 days"",
      ""post_hospitalisation"": ""60 days"",
      ""daycare_treatments"": ""405 daycare procedures covered"",
      ""ambulance_cover"": ""Up to ₹1,500 per hospitalisation"",
      ""domiciliary_hospitalisation"": ""Covered up to a maximum aggregate sub-limit of 20% of the SI"",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""Covered"",
      ""daily_allowance"": ""₹500 per day for a minimum of 3 days of hospitalisation, subject to a maximum of 10 days"",
      ""ayush_treatment"": ""Covered"",
      ""modern_treatment"": ""Covered up to 50% of SI""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""Sub-limits on room rent up to 1% – 2% of the sum insured"",
        ""is_pro"": ""False""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 50%"",
        ""is_pro"": ""False""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available once in a policy year upto the cover amount"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are not covered"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available, irrespective of any annual claims"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 86.7900%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 81.5700%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 72.1100% indicates fair pricing and payment of claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Satisfying ageing of the claim for less than 3 months at 96.900%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""5000 cashless network hospitals"",
        ""is_pro"": ""False""
      }
    },
    ""onefin_score"": 58.717986603624894,
    ""onefin_ranking"": 32,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""32/39""
  },
  {
    ""parent_category_id"": 12,
    ""insurer"": ""Kotak Mahindra General"",
    ""category_id"": 60,
    ""insurance_plan"": ""Kotak Health Care Premium"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 25647,
    ""annual_emi_score"": 87.27672708169162,
    ""room_description"": ""Cap on Room Rent (1% of annual sum insured in case of stay in Non ICU 2% of annual sum insured in case of stay in ICU)"",
    ""room_rent_score"": 33.3333333333333,
    ""no_claim_bonus"": ""Avail a cumulative bonus of 10% on your Base Annual Sum Insured on renewal of this Policy provided there has been no claim throughout the policy period up to a maximum of 50% of Base Annual Sum Insured. If any Claim is made under the Policy after a Cumulative Bonus has been applied under the Policy then the accrued Cumulative Bonus under the Policy will reduce by 10% on the commencement of the next Policy Year or the next Renewal of the Policy (as applicable)."",
    ""ncb_score"": 33.3333333333333,
    ""recharge_sum_insured"": ""Optional"",
    ""si_recharge_score"": 50,
    ""pre_existing_disease"": ""Covered after 4 years"",
    ""ped_score"": 0,
    ""co_pay"": ""No Co-Pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""The policy also includes one free health check-up at our network provider for each Insured Person above 18 years of Age, each Policy Year for specified tests."",
    ""hw_score"": 50,
    ""version"": null,
    ""claim_settled_ratio"": 0.8679,
    ""csr_score"": 50,
    ""ageing_of_claim"": 0.969,
    ""aoc_score"": 75,
    ""incurred_claim_ratio"": 0.7211,
    ""icr_score"": 75,
    ""network_hospitals"": 5000,
    ""nh_score"": 25,
    ""claim_settled_ratio_abs_amt"": 0.8157,
    ""csr_abs_amt_score"": 75,
    ""standard_features"": {
      ""pre_hospitalisation"": ""30 days"",
      ""post_hospitalisation"": ""60 days"",
      ""daycare_treatments"": ""150 named daycare procedures covered"",
      ""ambulance_cover"": ""Up to ₹1,500 per hospitalisation"",
      ""domiciliary_hospitalisation"": ""Covered up to a maximum aggregate sub-limit of 20% of the SI"",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""-"",
      ""daily_allowance"": ""₹500 per day for a minimum of 3 days of hospitalisation, subject to a maximum of 10 days"",
      ""ayush_treatment"": ""Covered"",
      ""modern_treatment"": ""Covered up to 50% of SI""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""Sub-limits on room rent up to 1% – 2% of the sum insured"",
        ""is_pro"": ""False""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 50%"",
        ""is_pro"": ""False""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available once in a policy year upto the cover amount"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are not covered"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available, irrespective of any annual claims"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 86.7900%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 81.5700%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 72.1100% indicates fair pricing and payment of claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Satisfying ageing of the claim for less than 3 months at 96.900%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""5000 cashless network hospitals"",
        ""is_pro"": ""False""
      }
    },
    ""onefin_score"": 58.26635145784081,
    ""onefin_ranking"": 33,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""33/39""
  },
  {
    ""parent_category_id"": 12,
    ""insurer"": ""Kotak Mahindra General"",
    ""category_id"": 65,
    ""insurance_plan"": ""Kotak Health Premier Total"",
    ""cover_plan"": 2000000,
    ""min_age"": 52,
    ""max_age"": 52,
    ""annual_emi"": 27109,
    ""annual_emi_score"": 84.87654320987654,
    ""room_description"": ""Cap on Room Rent (1% of annual sum insured in case of stay in Non ICU 2% of annual sum insured in case of stay in ICU)"",
    ""room_rent_score"": 33.3333333333333,
    ""no_claim_bonus"": ""We will increase Your Base Sum Insured by 10% subject to the maximum limit specified in the Policy Schedule at the end of the Policy Year"",
    ""ncb_score"": 33.3333333333333,
    ""recharge_sum_insured"": ""100% restoration of the Base Sum Insured amount once in a Policy Year if the Base Sum Insured and the Cumulative Bonus (if any) is insufficient as a result of previous Claims in that Policy Year."",
    ""si_recharge_score"": 50,
    ""pre_existing_disease"": ""Covered after 4 years"",
    ""ped_score"": 0,
    ""co_pay"": ""No Co-pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""The policy also includes one free health check-up at our network provider for each Insured Person above 18 years of Age, each Policy Year for specified tests."",
    ""hw_score"": 50,
    ""version"": null,
    ""claim_settled_ratio"": 0.8679,
    ""csr_score"": 50,
    ""ageing_of_claim"": 0.969,
    ""aoc_score"": 75,
    ""incurred_claim_ratio"": 0.7211,
    ""icr_score"": 75,
    ""network_hospitals"": 5000,
    ""nh_score"": 25,
    ""claim_settled_ratio_abs_amt"": 0.8157,
    ""csr_abs_amt_score"": 75,
    ""standard_features"": {
      ""pre_hospitalisation"": ""30 days"",
      ""post_hospitalisation"": ""60 days"",
      ""daycare_treatments"": ""405 daycare procedures covered"",
      ""ambulance_cover"": ""Up to ₹1,500 per hospitalisation"",
      ""domiciliary_hospitalisation"": ""Covered up to a maximum aggregate sub-limit of 20% of the SI"",
      ""organ_donor_cover"": ""Covered up to SI"",
      ""second_opinion"": ""Covered"",
      ""daily_allowance"": ""₹500 per day for a minimum of 3 days of hospitalisation, subject to a maximum of 10 days"",
      ""ayush_treatment"": ""Covered"",
      ""modern_treatment"": ""Covered up to 50% of SI""
    },
    ""statements"": {
      ""pricing"": ""Priced low"",
      ""room_rent"": {
        ""statement"": ""Sub-limits on room rent up to 1% – 2% of the sum insured"",
        ""is_pro"": ""False""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 50%"",
        ""is_pro"": ""False""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available once in a policy year upto the cover amount"",
        ""is_pro"": ""True""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are not covered"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available, irrespective of any annual claims"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 86.7900%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 81.5700%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 72.1100% indicates fair pricing and payment of claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Satisfying ageing of the claim for less than 3 months at 96.900%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""5000 cashless network hospitals"",
        ""is_pro"": ""False""
      }
    },
    ""onefin_score"": 57.54629629629629,
    ""onefin_ranking"": 34,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""34/39""
  },
  {
    ""parent_category_id"": 29,
    ""insurer"": ""ICICI Lombard"",
    ""category_id"": 116,
    ""insurance_plan"": ""ICICI Lombard Health Shield Plus"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 57855,
    ""annual_emi_score"": 34.400446545836616,
    ""room_description"": ""Actuals"",
    ""room_rent_score"": 66.6666666666667,
    ""no_claim_bonus"": ""10% of cover amount for every claim free year max up to 50%"",
    ""ncb_score"": 33.3333333333333,
    ""recharge_sum_insured"": ""Unlimited reset benefit: Reset upto 100% of the entire sum insured. Reset won't trigger on first claim and for same illness claimed by same person again.The reset amount can only be used for all future claims within the same policy year"",
    ""si_recharge_score"": 25,
    ""pre_existing_disease"": ""Covered after 2 Years"",
    ""ped_score"": 66.6666666666667,
    ""co_pay"": ""No Co-pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Complimentary Health Check Up Coupons: One coupon per individual policy and two coupons per Floater policy will be offered.Need to use ICICI Lombard network providers only."",
    ""hw_score"": 100,
    ""version"": null,
    ""claim_settled_ratio"": 0.8797,
    ""csr_score"": 50,
    ""ageing_of_claim"": 0.9707,
    ""aoc_score"": 75,
    ""incurred_claim_ratio"": 0.9167,
    ""icr_score"": 25,
    ""network_hospitals"": 6500,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.6795,
    ""csr_abs_amt_score"": 25,
    ""standard_features"": {
      ""pre_hospitalisation"": ""30 days"",
      ""post_hospitalisation"": ""60 days"",
      ""daycare_treatments"": ""150 daycare procedures covered"",
      ""ambulance_cover"": ""Up to ₹1,500 per hospitalisation"",
      ""domiciliary_hospitalisation"": ""Covered"",
      ""organ_donor_cover"": ""Up to ₹10L (as per the plans opted for)"",
      ""second_opinion"": ""Covered"",
      ""daily_allowance"": ""-"",
      ""ayush_treatment"": ""Covered"",
      ""modern_treatment"": ""-""
    },
    ""statements"": {
      ""pricing"": ""Priced fairly"",
      ""room_rent"": {
        ""statement"": ""Availability of the single private room category"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 50%"",
        ""is_pro"": ""False""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available upto sum insured after its exhaustion, or at second claim"",
        ""is_pro"": ""False""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 2 years"",
        ""is_pro"": ""True""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Rewards for healthy behaviour are available"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 87.9700%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 67.9500%."",
        ""is_pro"": ""False""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 91.6700% indicates expensive pricing and low payment of claims"",
        ""is_pro"": ""False""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Satisfying ageing of the claim for less than 3 months at 97.0700%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""6500 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 48.653467297084326,
    ""onefin_ranking"": 35,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""35/39""
  },
  {
    ""parent_category_id"": 8,
    ""insurer"": ""Cholamandalam MS General Insurance"",
    ""category_id"": 43,
    ""insurance_plan"": ""Chola Healthline Enrich"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 63774,
    ""annual_emi_score"": 24.683149461518255,
    ""room_description"": ""Actual"",
    ""room_rent_score"": 66.6666666666667,
    ""no_claim_bonus"": ""50% of the insured sum for every no-claim year. Max of 100 % of SI"",
    ""ncb_score"": 66.6666666666667,
    ""recharge_sum_insured"": ""Unlimited claims, until the sum insured is exhausted"",
    ""si_recharge_score"": 25,
    ""pre_existing_disease"": ""Covered after 4 years"",
    ""ped_score"": 0,
    ""co_pay"": ""No Co-payment for age up to 55 years"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Health Check up expenses once after 2 claim free years"",
    ""hw_score"": 25,
    ""version"": null,
    ""claim_settled_ratio"": 0.8925,
    ""csr_score"": 50,
    ""ageing_of_claim"": 0.9323,
    ""aoc_score"": 50,
    ""incurred_claim_ratio"": 1.1708,
    ""icr_score"": 0,
    ""network_hospitals"": 10000,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.781,
    ""csr_abs_amt_score"": 50,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""90 days"",
      ""daycare_treatments"": ""141 daycare treatments covered"",
      ""ambulance_cover"": "" ₹2,000 per hospitalisation"",
      ""domiciliary_hospitalisation"": ""Covered up to 7 days"",
      ""organ_donor_cover"": ""Covered"",
      ""second_opinion"": ""-"",
      ""daily_allowance"": ""-"",
      ""ayush_treatment"": ""Covered"",
      ""modern_treatment"": ""-""
    },
    ""statements"": {
      ""pricing"": ""Priced High"",
      ""room_rent"": {
        ""statement"": ""Availability of the single private room category"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 100%"",
        ""is_pro"": ""True""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available upto sum insured after its exhaustion, or at second claim"",
        ""is_pro"": ""False""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are not covered"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available in a claim-free year only"",
        ""is_pro"": ""False""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 89.2500%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 78.100%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""An unfavourable ICR of 117.0800% indicates the insurer’s adverse financial position"",
        ""is_pro"": ""False""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Neutral ageing of the claim for less than 3 months at 93.2300%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""10000 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 40.94661150512215,
    ""onefin_ranking"": 36,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""36/39""
  },
  {
    ""parent_category_id"": 29,
    ""insurer"": ""ICICI Lombard"",
    ""category_id"": 118,
    ""insurance_plan"": ""ICICI Lombard Health Elite Plus"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 78809,
    ""annual_emi_score"": 0,
    ""room_description"": ""Actuals"",
    ""room_rent_score"": 66.6666666666667,
    ""no_claim_bonus"": ""10% of cover amount for every claim free year max up to 50%"",
    ""ncb_score"": 33.3333333333333,
    ""recharge_sum_insured"": ""Unlimited reset benefit: Reset upto 100% of the entire sum insured. Reset won't trigger on first claim and for same illness claimed by same person again.The reset amount can only be used for all future claims within the same policy year"",
    ""si_recharge_score"": 25,
    ""pre_existing_disease"": ""Covered after 2 Years"",
    ""ped_score"": 66.6666666666667,
    ""co_pay"": ""No Co-pay"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Complimentary Health Check Up Coupons: One coupon per individual policy and two coupons per Floater policy will be offered.Need to use ICICI Lombard network providers only."",
    ""hw_score"": 100,
    ""version"": null,
    ""claim_settled_ratio"": 0.8797,
    ""csr_score"": 50,
    ""ageing_of_claim"": 0.9707,
    ""aoc_score"": 75,
    ""incurred_claim_ratio"": 0.9167,
    ""icr_score"": 25,
    ""network_hospitals"": 6500,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.6795,
    ""csr_abs_amt_score"": 25,
    ""standard_features"": {
      ""pre_hospitalisation"": ""30 days"",
      ""post_hospitalisation"": ""60 days"",
      ""daycare_treatments"": ""150 daycare procedures covered"",
      ""ambulance_cover"": ""Up to ₹1,500 per hospitalisation"",
      ""domiciliary_hospitalisation"": ""Covered"",
      ""organ_donor_cover"": ""Up to ₹10L (as per the plans opted for)"",
      ""second_opinion"": ""Covered"",
      ""daily_allowance"": ""-"",
      ""ayush_treatment"": ""Covered"",
      ""modern_treatment"": ""-""
    },
    ""statements"": {
      ""pricing"": ""Most expensive"",
      ""room_rent"": {
        ""statement"": ""Availability of the single private room category"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 50%"",
        ""is_pro"": ""False""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available upto sum insured after its exhaustion, or at second claim"",
        ""is_pro"": ""False""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 2 years"",
        ""is_pro"": ""True""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Rewards for healthy behaviour are available"",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 87.9700%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 67.9500%."",
        ""is_pro"": ""False""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 91.6700% indicates expensive pricing and low payment of claims"",
        ""is_pro"": ""False""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Satisfying ageing of the claim for less than 3 months at 97.0700%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""6500 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 38.333333333333336,
    ""onefin_ranking"": 37,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""37/39""
  },
  {
    ""parent_category_id"": 8,
    ""insurer"": ""Cholamandalam MS General Insurance"",
    ""category_id"": 44,
    ""insurance_plan"": ""Chola Healthline Privilege"",
    ""cover_plan"": 2000000,
    ""min_age"": 51,
    ""max_age"": 55,
    ""annual_emi"": 69576,
    ""annual_emi_score"": 15.157932755450487,
    ""room_description"": ""Actual"",
    ""room_rent_score"": 66.6666666666667,
    ""no_claim_bonus"": ""5% of the insured sum for every no-claim year. Max of 50 % of SI"",
    ""ncb_score"": 33.3333333333333,
    ""recharge_sum_insured"": ""Unlimited claims, until the sum insured is exhausted"",
    ""si_recharge_score"": 25,
    ""pre_existing_disease"": ""Covered after 4 years"",
    ""ped_score"": 0,
    ""co_pay"": ""No Co-pay for age above 55 years"",
    ""co_pay_score"": 100,
    ""health_and_wellness"": ""Health Check up expenses once after 2 claim free years"",
    ""hw_score"": 25,
    ""version"": null,
    ""claim_settled_ratio"": 0.8925,
    ""csr_score"": 50,
    ""ageing_of_claim"": 0.9323,
    ""aoc_score"": 50,
    ""incurred_claim_ratio"": 1.1708,
    ""icr_score"": 0,
    ""network_hospitals"": 10000,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.781,
    ""csr_abs_amt_score"": 50,
    ""standard_features"": {
      ""pre_hospitalisation"": ""60 days"",
      ""post_hospitalisation"": ""90 days"",
      ""daycare_treatments"": ""141 daycare treatments covered"",
      ""ambulance_cover"": "" ₹5,000 per hospitalisation"",
      ""domiciliary_hospitalisation"": ""Covered up to 7 days"",
      ""organ_donor_cover"": ""Covered"",
      ""second_opinion"": ""25000"",
      ""daily_allowance"": ""-"",
      ""ayush_treatment"": ""Covered"",
      ""modern_treatment"": ""-""
    },
    ""statements"": {
      ""pricing"": ""Priced High"",
      ""room_rent"": {
        ""statement"": ""Availability of the single private room category"",
        ""is_pro"": ""True""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 50%"",
        ""is_pro"": ""False""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI is available upto sum insured after its exhaustion, or at second claim"",
        ""is_pro"": ""False""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are not covered"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""No co-pay within the policy"",
        ""is_pro"": ""True""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available in a claim-free year only"",
        ""is_pro"": ""False""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 89.2500%."",
        ""is_pro"": ""True""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 78.100%."",
        ""is_pro"": ""True""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""An unfavourable ICR of 117.0800% indicates the insurer’s adverse financial position"",
        ""is_pro"": ""False""
      },
      ""ageing_of_claim"": {
        ""statement"": ""Neutral ageing of the claim for less than 3 months at 93.2300%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""10000 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 34.75571315996848,
    ""onefin_ranking"": 38,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""38/39""
  },
  {
    ""parent_category_id"": 16,
    ""insurer"": ""Navi General"",
    ""category_id"": 75,
    ""insurance_plan"": ""Navi Health Select"",
    ""cover_plan"": 2000000,
    ""min_age"": 46,
    ""max_age"": 55,
    ""annual_emi"": 56376.86,
    ""annual_emi_score"": 36.827127659574465,
    ""room_description"": ""Up to 1% of SI per day"",
    ""room_rent_score"": 0,
    ""no_claim_bonus"": ""Increase in SI by 5% of SI (excluding CB) per year up to 50% of SI (excluding CB)"",
    ""ncb_score"": 33.3333333333333,
    ""recharge_sum_insured"": ""Not Available"",
    ""si_recharge_score"": 0,
    ""pre_existing_disease"": ""Covered after 3 years(36 months)"",
    ""ped_score"": 33.3333333333333,
    ""co_pay"": ""Copayment of 20% of admissible claim if treatment taken in non-network hospital (not applicable to optional covers)"",
    ""co_pay_score"": 33.3333333333333,
    ""health_and_wellness"": ""Every 2 yrs., up to INR 1,000"",
    ""hw_score"": 25,
    ""version"": null,
    ""claim_settled_ratio"": 0.8252,
    ""csr_score"": 25,
    ""ageing_of_claim"": 0.9999,
    ""aoc_score"": 100,
    ""incurred_claim_ratio"": 0.2856,
    ""icr_score"": 50,
    ""network_hospitals"": 10000,
    ""nh_score"": 50,
    ""claim_settled_ratio_abs_amt"": 0.47,
    ""csr_abs_amt_score"": 0,
    ""standard_features"": {
      ""pre_hospitalisation"": ""30 days"",
      ""post_hospitalisation"": ""60 days"",
      ""daycare_treatments"": ""140+ daycare procedures covered"",
      ""ambulance_cover"": ""Up to ₹2,500 in a policy period"",
      ""domiciliary_hospitalisation"": ""Available for any expenses incurred on domiciliary hospitalisation"",
      ""organ_donor_cover"": ""Only hospitalisation expenses covered"",
      ""second_opinion"": ""One medical second opinion for each major illness (as listed in Appendix II to the policy)"",
      ""daily_allowance"": ""Daily hospital cash of ₹500 provided for a maximum of 5 days"",
      ""ayush_treatment"": ""Covered up to SI"",
      ""modern_treatment"": ""Covered up to 25% of SI, for each treatment""
    },
    ""statements"": {
      ""pricing"": ""Priced fairly"",
      ""room_rent"": {
        ""statement"": ""Availability of the shared room category"",
        ""is_pro"": ""False""
      },
      ""no_claim_bonus"": {
        ""statement"": ""The NCB benefit over time is up to 50%"",
        ""is_pro"": ""False""
      },
      ""recharge_of_si"": {
        ""statement"": ""A RSI benefit is not available"",
        ""is_pro"": ""False""
      },
      ""pre_existing_disease"": {
        ""statement"": ""PED are covered after 3 years"",
        ""is_pro"": ""False""
      },
      ""co_pay"": {
        ""statement"": ""20% co-pay for each claim"",
        ""is_pro"": ""False""
      },
      ""health_and_wellness"": {
        ""statement"": ""Annual health check-up is available in a claim-free year only"",
        ""is_pro"": ""False""
      },
      ""claim_settled_ratio"": {
        ""statement"": ""A Claim Settlement Ratio (No. of claims) of 82.5200%."",
        ""is_pro"": ""False""
      },
      ""claim_settled_ratio_abs_amt"": {
        ""statement"": ""A Claim Settlement Ratio (Amount of claims) of 47.00%."",
        ""is_pro"": ""False""
      },
      ""incurred_claim_ratio"": {
        ""statement"": ""A ICR of 28.5600% indicates balanced pricing and payment of claims"",
        ""is_pro"": ""True""
      },
      ""ageing_of_claim"": {
        ""statement"": ""A favourable ageing of the claim for less than 3 months at 99.9900%"",
        ""is_pro"": ""True""
      },
      ""network_hospitals"": {
        ""statement"": ""10000 cashless network hospitals"",
        ""is_pro"": ""True""
      }
    },
    ""onefin_score"": 30.423138297872335,
    ""onefin_ranking"": 39,
    ""total_ranking"": 39,
    ""ranking_out_of_total"": ""39/39""
  }
]";

    var workbook = package.Workbook;
    var worksheet = workbook.Worksheets.Add("51-55_20Lacs"); // Add a worksheet

    // Insert data into cells
    worksheet.Cells["A1"].Value = "Insurer";
    worksheet.Cells["B1"].Value = "Insurance Plan";
    worksheet.Cells["C1"].Value = "Min Age";
    worksheet.Cells["D1"].Value = "Max Age";
    worksheet.Cells["E1"].Value = "Cover Plan";
    worksheet.Cells["F1"].Value = "1 Finance Score";
    worksheet.Cells["G1"].Value = "1 Finance Rank";
    worksheet.Cells["H1"].Value = "Total Ranking";
    worksheet.Cells["I1"].Value = "1 Finance Rank / Total Ranking";
    worksheet.Cells["J1"].Value = "Pricing";
    worksheet.Cells["K1"].Value = "Product Card";
    worksheet.Cells["L1"].Value = "Price Scoring";
    worksheet.Cells["M1"].Value = "Room Rent Scoring";
    worksheet.Cells["N1"].Value = "No Claim Bonus (NCB) Scoring";
    worksheet.Cells["O1"].Value = "Recharge of SI (RSI) Scoring";
    worksheet.Cells["P1"].Value = "Pre-Existing Disease (PED) Scoring";
    worksheet.Cells["Q1"].Value = "Co-Payment (Co-Pay) Scoring";
    worksheet.Cells["R1"].Value = "Health and Wellness Scoring";
    worksheet.Cells["S1"].Value = "Claim Settlement Ratio(CSR-Number) Scoring";
    worksheet.Cells["T1"].Value = "Claim Settlement Ratio (CSR-Amount) Scoring";
    worksheet.Cells["U1"].Value = "Incurred Claim Ratio (ICR) Scoring";
    worksheet.Cells["V1"].Value = "Ageing of Claim Scoring";
    worksheet.Cells["W1"].Value = "Network and Hospitals Scoring";
    worksheet.Cells["X1"].Value = "Pros";
    worksheet.Cells["Y1"].Value = "Cons";

    JArray jsonArray = JArray.Parse(json);

    IList<dynamic> prosList = new List<dynamic>();
    IList<dynamic> consList = new List<dynamic>();
    

    int i = 1;
    foreach (JObject jsonObject in jsonArray)
    {
        i += 1;
        Console.WriteLine($"------------{i}--------------");
        worksheet.Cells[$"A{i}"].Value = Convert.ToString(jsonObject["insurer"]);
        worksheet.Cells[$"B{i}"].Value = Convert.ToString(jsonObject["insurance_plan"]);
        worksheet.Cells[$"C{i}"].Value = Convert.ToString(jsonObject["min_age"]);
        worksheet.Cells[$"D{i}"].Value = Convert.ToString(jsonObject["max_age"]);
        worksheet.Cells[$"E{i}"].Value = Convert.ToString(jsonObject["cover_plan"]);
        worksheet.Cells[$"F{i}"].Value = Convert.ToString(jsonObject["onefin_score"]);
        worksheet.Cells[$"G{i}"].Value = Convert.ToString(jsonObject["onefin_ranking"]);
        worksheet.Cells[$"H{i}"].Value = Convert.ToString(jsonObject["total_ranking"]);
        worksheet.Cells[$"I{i}"].Value = Convert.ToString(jsonObject["ranking_out_of_total"]);
        worksheet.Cells[$"J{i}"].Value = Convert.ToString(jsonObject["annual_emi"]);
        worksheet.Cells[$"K{i}"].Value = Convert.ToString(jsonObject["statements"]["pricing"]);
        worksheet.Cells[$"L{i}"].Value = Convert.ToString(jsonObject["annual_emi_score"]);
        worksheet.Cells[$"M{i}"].Value = Convert.ToString(jsonObject["room_rent_score"]);
        worksheet.Cells[$"N{i}"].Value = Convert.ToString(jsonObject["ncb_score"]);
        worksheet.Cells[$"O{i}"].Value = Convert.ToString(jsonObject["si_recharge_score"]);
        worksheet.Cells[$"P{i}"].Value = Convert.ToString(jsonObject["ped_score"]);
        worksheet.Cells[$"Q{i}"].Value = Convert.ToString(jsonObject["co_pay_score"]);
        worksheet.Cells[$"R{i}"].Value = Convert.ToString(jsonObject["hw_score"]);
        worksheet.Cells[$"S{i}"].Value = Convert.ToString(jsonObject["csr_score"]);
        worksheet.Cells[$"T{i}"].Value = Convert.ToString(jsonObject["csr_abs_amt_score"]);
        worksheet.Cells[$"U{i}"].Value = Convert.ToString(jsonObject["icr_score"]);
        worksheet.Cells[$"V{i}"].Value = Convert.ToString(jsonObject["aoc_score"]);
        worksheet.Cells[$"W{i}"].Value = Convert.ToString(jsonObject["nh_score"]);

        string[] arr = { "room_rent", "no_claim_bonus", "recharge_of_si", "pre_existing_disease", "co_pay",
            "health_and_wellness" , "health_and_wellness","claim_settled_ratio","claim_settled_ratio_abs_amt",
            "incurred_claim_ratio","ageing_of_claim", "network_hospitals"
        };

        string concatenatedValue = "";
        string concatenatedValue2 = "";
        foreach (var s in arr)
        {
            if (jsonObject["statements"][s]["is_pro"].ToString().Equals("True"))
            {
                concatenatedValue += jsonObject["statements"][s]["statement"] + Environment.NewLine;
                prosList.Add(jsonObject["statements"][s]["statement"]);
            }
            else if (jsonObject["statements"][s]["is_pro"].ToString().Equals("False"))
            {
                concatenatedValue2 += jsonObject["statements"][s]["statement"] + Environment.NewLine;
            }
        }

        worksheet.Cells[$"X{i}"].Value = concatenatedValue;
        worksheet.Cells[$"Y{i}"].Value = concatenatedValue2;

        //Console.WriteLine(jsonObject["statements"]["pricing"]);
        //Console.WriteLine(jsonObject["statements"]["room_rent"]["is_pro"]);
        //Console.WriteLine($"{jsonObject["statements"]["room_rent"]["statement"]}\n" +
        //    $"{jsonObject["statements"]["no_claim_bonus"]["statement"]}\n" +
        //    $"{jsonObject["statements"]["recharge_of_si"]["statement"]}\n" +
        //    $"{jsonObject["statements"]["pre_existing_disease"]["statement"]}\n" +
        //    $"{jsonObject["statements"]["co_pay"]["statement"]}\n" +
        //    $"{jsonObject["statements"]["health_and_wellness"]["statement"]}\n" +
        //    $"{jsonObject["statements"]["claim_settled_ratio"]["statement"]}\n" +
        //    $"{jsonObject["statements"]["claim_settled_ratio_abs_amt"]["statement"]}\n" +
        //    $"{jsonObject["statements"]["incurred_claim_ratio"]["statement"]}\n" +
        //    $"{jsonObject["statements"]["ageing_of_claim"]["statement"]}\n" +
        //    $"{jsonObject["statements"]["network_hospitals"]["statement"]}\n");
        //Console.WriteLine("=====================");
    }


    // Save the Excel file
    FileInfo excelFile = new FileInfo("output.xlsx");
    package.SaveAs(excelFile);
}


public class Statement
{
    public string Pricing { get; set; }
    public dynamic RoomRentState { get; set; }
}