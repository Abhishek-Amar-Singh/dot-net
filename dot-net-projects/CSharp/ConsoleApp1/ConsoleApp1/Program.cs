﻿
using Humanizer;

string greeting = "    CSharp!    ";
Console.WriteLine($"[{greeting}]");
Console.WriteLine($"[{greeting.TrimStart()}]");
Console.WriteLine($"[{greeting.TrimEnd()}]");
Console.WriteLine($"[{greeting.Trim()}]");

string say = "Hello World!";
say = say.Replace("Hello", "Greetings");
Console.WriteLine(say);
Console.WriteLine(say.ToUpper());
Console.WriteLine(say.ToLower());
Console.WriteLine(say);

string songLyrics = "You say greetings, I say hello";
Console.WriteLine(songLyrics.Contains("greetings"));
Console.WriteLine(songLyrics.Contains("goodbye"));
Console.WriteLine(songLyrics.StartsWith("you"));
Console.WriteLine(songLyrics.EndsWith("hello"));

int a = 7, b = 4, c = 3;
int result = (a + b) / c;
Console.WriteLine(result);//Integer division gives you integer result

int imin = int.MinValue;
int imax = int.MaxValue;
Console.WriteLine($"The range of integers is {imin} to {imax}");
Console.WriteLine($"{imin} = {imin.ToWords()}\n{imax} = {imax.ToWords()}");

int what = imax + 3;
Console.WriteLine($"An exampe of overflow: {what}");//An exampe of overflow: -2147483646
what = imin - 3;
Console.WriteLine($"An exampe of underflow: {what}");//An exampe of underflow: 2147483645

double dmin = double.MinValue;
double dmax = double.MaxValue;
Console.WriteLine($"The range of double is {dmin} to {dmax}");
//The range of double is -1.7976931348623157E+308 to 1.7976931348623157E+308

double d = 1.0 / 3.0;
Console.WriteLine(d);//0.3333333333333333
d = 1 / 3;
Console.WriteLine(d);//0

decimal mind =  decimal.MinValue;
decimal maxd =  decimal.MaxValue;
Console.WriteLine($"The range of decimal is {mind} to {maxd}");
//The range of decimal is -79228162514264337593543950335 to 79228162514264337593543950335

decimal dec = 1.0M / 3.0M;
Console.WriteLine(dec);//0.3333333333333333333333333333
dec = 1M / 3M;
Console.WriteLine(d);//0

decimal min = decimal.MinValue;
decimal max = decimal.MaxValue;
Console.WriteLine($"The range of the decimal type is {min} and {max}");
//The range of the decimal type is -79228162514264337593543950335 and 79228162514264337593543950335

int min2 = int.MinValue;
int max2 = int.MaxValue;
Console.WriteLine($"The range of the int type is {min2} and {max2}");
//The range of the int type is -2147483648 and 2147483647

long min3 = long.MinValue;
long max3 = long.MaxValue;
Console.WriteLine($"The range of the long type is {min3} and {max3}");
//The range of the long type is -9223372036854775808 and 9223372036854775807

short min4 = short.MinValue;
short max4 = short.MaxValue;
Console.WriteLine($"The range of the short type is {min4} and {max4}");
//The range of the short type is -32768 and 32767
