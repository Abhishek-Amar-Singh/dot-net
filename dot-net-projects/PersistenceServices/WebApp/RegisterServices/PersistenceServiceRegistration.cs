﻿using WebApp.Services.Scoped;
using WebApp.Services.Singleton;
using WebApp.Services.Transient;

namespace WebApp.RegisterServices
{
    public static class PersistenceServiceRegistration
    {
        public static IServiceCollection AddPersistenceServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<ISingletonService, SingletonService>();
            services.AddScoped<IScopedService, ScopedService>();
            services.AddTransient<ITransientService, TransientService>();
            services.AddSingleton<ISingletonService2, SingletonService2>();

            return services;
        }
    }
}
