﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Models;
using WebApp.Services.Scoped;
using WebApp.Services.Singleton;
using WebApp.Services.Transient;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ISingletonService _singletonService;
        private readonly IScopedService _scopedService;
        private readonly ITransientService _transientService;
        private readonly ISingletonService2 _singletonService2;

        public HomeController(ILogger<HomeController> logger, ISingletonService _singletonService, IScopedService _scopedService, ITransientService _transientService, ISingletonService2 _singletonService2)
        {
            _logger = logger;
            this._singletonService = _singletonService;
            this._scopedService = _scopedService;
            this._transientService = _transientService;
            this._singletonService2 = _singletonService2;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult SingletonService()
        {
            var model = _singletonService.GetGuid();
            return View("Singleton", model);
        }
        [HttpGet]
        public IActionResult ScopedService()
        {
            var model = _scopedService.GetGuid();
            return View("Scoped", model);
        }
        [HttpGet]
        public IActionResult TransientService()
        {
            var model = _transientService.GetGuid();
            return View("Transient", model);
        }

        [HttpGet]
        public IActionResult SingletonService2()
        {
            var model = _singletonService2.GetGuid();
            return View("Singleton2", model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}