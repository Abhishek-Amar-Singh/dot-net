﻿namespace WebApp.Services.Singleton
{
    public interface ISingletonService
    {
        string GetGuid();
    }
}
