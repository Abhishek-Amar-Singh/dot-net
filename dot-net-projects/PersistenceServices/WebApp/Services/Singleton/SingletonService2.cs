﻿namespace WebApp.Services.Singleton
{

    public class SingletonService2 : ISingletonService2, IDisposable
    {
        private string guid;

        public SingletonService2()
        {
            guid = Guid.NewGuid().ToString();
        }

        public void Dispose()
        {
            guid = null;
        }

        public string GetGuid()
        {
            return guid;
        }

    }
}
