﻿namespace WebApp.Services.Transient
{
    public interface ITransientService
    {
        string GetGuid();
    }
}
