﻿namespace WebApp.Services.Scoped
{
    public class ScopedService : IScopedService
    {
        private string guid;

        public ScopedService()
        {
            guid = Guid.NewGuid().ToString();
        }

        public string GetGuid()
        {
            return guid;
        }

    }
}
