﻿namespace WebApp.Services.Scoped
{
    public interface IScopedService
    {
        string GetGuid();
    }
}
