﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Charts.Models
{
    public class SalesData
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Column(TypeName = "varchar(35)")]
        public string MonthAndYear { get; set; } = null!;
        public double TotalSales { get; set; }
    }
}
