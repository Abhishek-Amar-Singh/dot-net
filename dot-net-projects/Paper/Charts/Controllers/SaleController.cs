﻿using Charts.Models;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using DotNet.Highcharts;
using Microsoft.AspNetCore.Mvc;
using System.Drawing;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Charts.Controllers
{
    public class SaleController : Controller
    {
        private readonly IList<SalesData> salesDataList;
        public SaleController()
        {
            salesDataList = new List<SalesData>()
            {
                new SalesData(){MonthAndYear="Jan", TotalSales=1000},
                new SalesData(){MonthAndYear="Feb", TotalSales=800},
                new SalesData(){MonthAndYear="Mar", TotalSales=1500},
                new SalesData(){MonthAndYear="Apr", TotalSales=1000},
                new SalesData(){MonthAndYear="May", TotalSales=2500},
                new SalesData(){MonthAndYear="Jun", TotalSales=3000},
                new SalesData(){MonthAndYear="Jul", TotalSales=2300},
                new SalesData(){MonthAndYear="Sept", TotalSales=3300},
                new SalesData(){MonthAndYear="Oct", TotalSales=2300},
                new SalesData(){MonthAndYear="Nov", TotalSales=1000},
                new SalesData(){MonthAndYear="Dec", TotalSales=2000}
            };
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ShowSalesData()
        {
            return View();
        }

        public IActionResult ShowSalesDataApex()
        {
            return View();
        }

        [HttpGet]
        public IList<object> GetSalesData()
        {
            IList<object> data = new List<object>();
            List<string> labels = salesDataList.Select(sd => sd.MonthAndYear).ToList();
            data.Add(labels);
            List<double> salesNumber = salesDataList.Select(sd => sd.TotalSales).ToList();
            data.Add(salesNumber);
            return data;
        }

        [HttpGet]
        public IActionResult ShowSalesDataHighChart()
        {
            Highcharts columnChart = new Highcharts("columnchart");

            columnChart.InitChart(new Chart()
            {
                Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                BackgroundColor = new BackColorOrGradient(System.Drawing.Color.AliceBlue),
                Style = "fontWeight: 'bold', fontSize: '17px'",
                BorderColor = System.Drawing.Color.LightBlue,
                BorderRadius = 0,
                BorderWidth = 2

            });

            columnChart.SetTitle(new Title()
            {
                Text = "Sachin Vs Dhoni"
            });

            columnChart.SetSubtitle(new Subtitle()
            {
                Text = "Played 9 Years Together From 2004 To 2012"
            });

            columnChart.SetXAxis(new XAxis()
            {
                Type = AxisTypes.Category,
                Title = new XAxisTitle() { Text = "Years", Style = "fontWeight: 'bold', fontSize: '17px'" },
                Categories = new[] { "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012" }
            });

            columnChart.SetYAxis(new YAxis()
            {
                Title = new YAxisTitle()
                {
                    Text = "Runs",
                    Style = "fontWeight: 'bold', fontSize: '17px'"
                },
                ShowFirstLabel = true,
                ShowLastLabel = true,
                Min = 0
            });

            columnChart.SetLegend(new Legend
            {
                Enabled = true,
                BorderColor = System.Drawing.Color.CornflowerBlue,
                BorderRadius = 6,
                BackgroundColor = new BackColorOrGradient(ColorTranslator.FromHtml("#FFADD8E6"))
            });

            columnChart.SetSeries(new Series[]
            {
                new Series{

                    Name = "Sachin Tendulkar",
                    Data = new Data(new object[] { 812, 412, 628, 1425, 460, 972, 204, 513, 315 })
                },
                new Series()
                {
                    Name = "M S Dhoni",
                    Data = new Data(new object[] { 19, 895, 821, 1103, 1097, 1198, 600, 764, 524, })
                }
            });

            return View(columnChart);
        }
    }
}
