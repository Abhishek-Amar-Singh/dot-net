﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    public class Nation
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Column(TypeName = "varchar(25)")]
        public string Name { get; set; } = null!;
        [Column(TypeName="varchar(25)")]
        public string Capital { get; set; } = null!;
        [Column(TypeName ="nvarchar(5)")]
        public string DialingCode { get; set; } = null!;
    }
}
