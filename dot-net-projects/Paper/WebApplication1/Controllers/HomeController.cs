﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using System.Diagnostics;
using WebApplication1.Contexts;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IList<Nation> nationList;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
            nationList = new List<Nation>();
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ImportExcel(IFormFile theFile)
        {
            using(var stream = new MemoryStream())
            {
                await theFile.CopyToAsync(stream);
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowCount = worksheet.Dimension.Rows;
                    for(int row=2; row<=rowCount; row++)
                    {
                        Nation nation = new Nation()
                        {
                            Name = worksheet.Cells[row, 1].Value.ToString(),
                            Capital = worksheet.Cells[row, 2].Value.ToString(),
                            DialingCode = worksheet.Cells[row, 3].Value.ToString()
                        };
                        nationList.Add(nation);
                        using (var nationDbContext = new NationDbContext())
                        {
                            var countryList = nationDbContext.Nations.ToList();
                            var nations = new List<Nation>();
                            foreach(var country in countryList)
                            {
                                if (!nation.Name.Equals(country.Name))
                                {
                                    //nationDbContext.Nations.Add(nation);
                                    nations.Add(nation);
                                }
                                else
                                {
                                    //nationDbContext.Entry<Nation>(nation).State = EntityState.Modified;
                                }
                            }
                        }
                    }
                }
            }
            return View(nationList);
        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}