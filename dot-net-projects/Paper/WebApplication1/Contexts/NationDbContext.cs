﻿using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;

namespace WebApplication1.Contexts
{
    public class NationDbContext : DbContext
    {
        public NationDbContext()
        {
        }

        public NationDbContext(DbContextOptions<NationDbContext> dbContextOptions) : base(dbContextOptions)
        {
        }

        //Table creation
        public DbSet<Nation> Nations { get; set; }
    }
}
