﻿using Microsoft.AspNetCore.Mvc;
using VideoWebApp1.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace VideoWebApp1.Controllers
{
    public class VideoController : Controller
    {
        public readonly IConfiguration _configuration;

        public VideoController(IConfiguration _configuration)
        {
            this._configuration = _configuration;
        }

        [HttpGet]
        public IActionResult Index()
        {
            List<Video> videoList = new List<Video>();
            var localDbConnectionString = _configuration["ConnectionStrings:localDbConnection"];
            SqlConnection con = new SqlConnection(localDbConnectionString);
            var query = "SELECT * FROM VideoWebApp1;";
            SqlCommand cmd = new SqlCommand(query, con);
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Video video = new Video();
                video.VideoId = (Guid)reader["VideoId"];
                video.VideoName = reader["VideoName"].ToString();
                video.VideoPath = reader["VideoPath"].ToString();
                videoList.Add(video);
            }
            return View(videoList);
        }

        [HttpPost]
        public IActionResult Index(IFormFile videoFile)
        {
            if(videoFile is not null)
            {
                string fileName = Path.GetFileName(videoFile.FileName);
                string path = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, "wwwroot", "UploadedVideos"));
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                var fileStream = new FileStream(Path.Combine(path, fileName), FileMode.Create);
                videoFile.CopyTo(fileStream);

                var localDbConnectionString = _configuration["ConnectionStrings:localDbConnection"];
                SqlConnection con = new SqlConnection(localDbConnectionString);
                var query = "INSERT INTO VideoWebApp1 VALUES (@VideoId, @VideoName, @VideoPath);";
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();
                cmd.Parameters.AddWithValue("@VideoId", new Guid());
                cmd.Parameters.AddWithValue("@VideoName", fileName);
                cmd.Parameters.AddWithValue("@VideoPath", Path.Combine(path, fileName));
                cmd.ExecuteNonQuery();
                con.Close();
                ViewData["Message"] = "Records Saved Successfully!";
            }
            return RedirectToAction("Index");
        }
    }
}
