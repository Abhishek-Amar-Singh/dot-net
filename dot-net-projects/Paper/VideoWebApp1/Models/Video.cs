﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VideoWebApp1.Models
{
    public class Video
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid VideoId { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string VideoName { get; set; } = null!;
        [Column(TypeName = "varchar(2000)")]
        public string VideoPath { get; set; } = null!;
    }
}
