﻿using Microsoft.AspNetCore.Mvc;
using WebApplication2.Models;

namespace MVC.Boilerplate.Components
{
    public class FileUploadViewComponent : ViewComponent
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<FileUploadViewComponent> _logger;

        public FileUploadViewComponent(IConfiguration configuration, ILogger<FileUploadViewComponent> logger)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public IViewComponentResult Invoke(string FieldName)
        {
            _logger.LogInformation("File Upload View Component Initiated");
            ViewBag.AllowedEx = _configuration.GetSection("FileUploadSettings").GetSection("AllowedFileExtension").Value;
            ViewBag.Size = _configuration.GetSection("FileUploadSettings").GetSection("MaxFileSizeMb").Value;
            ViewBag.FileErrMess = _configuration.GetSection("FileUploadSettings").GetSection("FileNotAllowedErrorMessage").Value;
            ViewBag.SizeErrMess = _configuration.GetSection("FileUploadSettings").GetSection("FileSizeExceedErrorMessage").Value;
            _logger.LogInformation("File Upload View Component Terminated");
            return View("FileUpload", FieldName);
        }
    }
}