﻿namespace WebApplication2.Models
{
    public class FileUploadErrorMessage
    {
        public string ErrorMessage { get; set; }
        public int filesize { get; set; }
    }
}