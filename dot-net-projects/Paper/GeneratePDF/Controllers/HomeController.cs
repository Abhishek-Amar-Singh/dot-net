﻿using GeneratePDF.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection.Metadata;
using Document = iTextSharp.text.Document;

namespace GeneratePDF.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult PDF()
        {

            using (MemoryStream ms = new MemoryStream())
            {
                Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                PdfWriter pdfWriter = PdfWriter.GetInstance(document, ms);
                document.Open();

                var image = iTextSharp.text.Image.GetInstance("wwwroot/img/CorbetLogo.png");
                image.Alignment = Element.ALIGN_CENTER;
                document.Add(image);

                Paragraph para1 = new Paragraph("This is my first paragraph of pdf", new Font(Font.FontFamily.HELVETICA, 20));
                para1.Alignment = Element.ALIGN_CENTER;
                document.Add(para1);

                Paragraph para2 = new Paragraph("This is my second paragraph of pdf", new Font(Font.FontFamily.HELVETICA, 15));
                para2.Alignment = Element.ALIGN_CENTER;
                document.Add(para2);

                Paragraph para3 = new Paragraph("This is my third paragraph of pdf", new Font(Font.FontFamily.HELVETICA, 10));
                para3.Alignment = Element.ALIGN_CENTER;
                document.Add(para3);

                PdfPTable pdfPTable = new PdfPTable(4);
                PdfPCell pdfPCell1 = new PdfPCell(new Phrase("Date", new Font(Font.FontFamily.HELVETICA, 10)));
                pdfPCell1.BackgroundColor = BaseColor.LIGHT_GRAY;
                pdfPCell1.Border = Rectangle.BOTTOM_BORDER | Rectangle.TOP_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                pdfPCell1.BorderWidthBottom = 1f;
                pdfPCell1.BorderWidthTop = 1f;
                pdfPCell1.BorderWidthLeft = 1f;
                pdfPCell1.BorderWidthRight = 1f;
                pdfPCell1.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfPCell1.VerticalAlignment = Element.ALIGN_CENTER;
                pdfPTable.AddCell(pdfPCell1);

                PdfPCell pdfPCell2 = new PdfPCell(new Phrase("Name", new Font(Font.FontFamily.HELVETICA, 10)));
                pdfPCell2.BackgroundColor = BaseColor.LIGHT_GRAY;
                pdfPCell2.Border = Rectangle.BOTTOM_BORDER | Rectangle.TOP_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                pdfPCell2.BorderWidthBottom = 1f;
                pdfPCell2.BorderWidthTop = 1f;
                pdfPCell2.BorderWidthLeft = 1f;
                pdfPCell2.BorderWidthRight = 1f;
                pdfPCell2.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfPCell2.VerticalAlignment = Element.ALIGN_CENTER;
                pdfPTable.AddCell(pdfPCell2);

                PdfPCell pdfPCell3 = new PdfPCell(new Phrase("First Name", new Font(Font.FontFamily.HELVETICA, 10)));
                pdfPCell3.BackgroundColor = BaseColor.LIGHT_GRAY;
                pdfPCell3.Border = Rectangle.BOTTOM_BORDER | Rectangle.TOP_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                pdfPCell3.BorderWidthBottom = 1f;
                pdfPCell3.BorderWidthTop = 1f;
                pdfPCell3.BorderWidthLeft = 1f;
                pdfPCell3.BorderWidthRight = 1f;
                pdfPCell3.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfPCell3.VerticalAlignment = Element.ALIGN_CENTER;
                pdfPTable.AddCell(pdfPCell3);

                PdfPCell pdfPCell4 = new PdfPCell(new Phrase("Last Name", new Font(Font.FontFamily.HELVETICA, 10)));
                pdfPCell4.BackgroundColor = BaseColor.LIGHT_GRAY;
                pdfPCell4.Border = Rectangle.BOTTOM_BORDER | Rectangle.TOP_BORDER | Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER;
                pdfPCell4.BorderWidthBottom = 1f;
                pdfPCell4.BorderWidthTop = 1f;
                pdfPCell4.BorderWidthLeft = 1f;
                pdfPCell4.BorderWidthRight = 1f;
                pdfPCell4.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfPCell4.VerticalAlignment = Element.ALIGN_CENTER;
                pdfPTable.AddCell(pdfPCell4);

                for (int i = 0; i < 100; i++)
                {
                    PdfPCell pdfPCell_1 = new PdfPCell(new Phrase(i.ToString()));
                    PdfPCell pdfPCell_2 = new PdfPCell(new Phrase((i + 1).ToString()));
                    PdfPCell pdfPCell_3 = new PdfPCell(new Phrase((i + 2).ToString()));
                    PdfPCell pdfPCell_4 = new PdfPCell(new Phrase((i + 3).ToString()));

                    pdfPCell_1.HorizontalAlignment = Element.ALIGN_CENTER;
                    pdfPCell_2.HorizontalAlignment = Element.ALIGN_CENTER;
                    pdfPCell_3.HorizontalAlignment = Element.ALIGN_CENTER;
                    pdfPCell_4.HorizontalAlignment = Element.ALIGN_CENTER;

                    pdfPTable.AddCell(pdfPCell_1);
                    pdfPTable.AddCell(pdfPCell_2);
                    pdfPTable.AddCell(pdfPCell_3);
                    pdfPTable.AddCell(pdfPCell_4);
                }
                document.Add(pdfPTable);
                document.Close();
                pdfWriter.Close();
                var constant = ms.ToArray();
                string fileName = "FirstDocument.pdf";
                var generatedFile = File(constant, "application/vnd", fileName);
                //return File(constant, "application/vnd", fileName);

                string path = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, "wwwroot", "UploadedFiles"));
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                var generatedFileStreamResult = new FileStreamResult(new MemoryStream(generatedFile.FileContents), generatedFile.ContentType);
                generatedFileStreamResult.FileDownloadName = generatedFile.FileDownloadName;
                using (var generatedFileStream = generatedFileStreamResult.FileStream)
                {
                    IFormFile theFile = new FormFile(generatedFileStream, 0, generatedFileStream.Length, "name", generatedFileStreamResult.FileDownloadName);
                    using (var fileStream = new FileStream(Path.Combine(path, theFile.FileName), FileMode.Create))
                    {
                        theFile.CopyTo(fileStream);
                    }
                }
                var s = SendMail($"{path}/{generatedFile.FileDownloadName}", "abhisheksingh.120120@gmail.com", "tejaspawar1320@gmail.com");
                var a = s;
                return Json(true);
            }
        }

        public string SendMail(string filePath, string emailFrom, string emailTo)
        {
            MailAddress sendTo = new MailAddress(emailTo);
            MailAddress sendFrom = new MailAddress(emailFrom);
            Attachment billAttachment = new Attachment(filePath, MediaTypeNames.Application.Octet);
            MailMessage message = new MailMessage(sendFrom, sendTo);
            message.Subject = "Order successful! Here is your bill invoice";
            message.Body = $"Hello User, <br> Thank you for your purchase. Below attached is the invoice of your order, incase of " +
                $"any queries please write back to us. Happy Learnin! <br> Regards, <br> Team Udemy.";
            message.Attachments.Add(billAttachment);
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential(emailFrom, "password"),
                EnableSsl = true
            };
            client.Send(message);
            return "success";
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}