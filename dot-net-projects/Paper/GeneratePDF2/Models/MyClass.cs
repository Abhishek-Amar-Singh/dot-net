﻿using System.ComponentModel.DataAnnotations;

namespace GeneratePDF2.Models
{
    public class MyClass
    {
        public int Id { get; set; } = 999;
        public string OrderCode { get; set; } = "COKJGG74";
        public string InvoiceNumber { get; set; } = "CORB-2022/12-90";
        public int UserId { get; set; } = 55;
        public string UserName { get; set; } = "Abhi";
        public string DelivaryAddress { get; set; } = "102 Pali Hill";
        public string city { get; set; } = "Mumbai";
        public string PhoneNumber { get; set; } = "0997278387892";
        public string ProductName { get; set; }
        public int Quantity { get; set; } = 9;
        public double UnitPrice { get; set; } = 90982.9;
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ReceivedDate { get; set; } = DateTime.Now;
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ExpectedDate { get; set; } = DateTime.Now;
        public double TotalPrice { get; set; } = 3894.39;
        public bool Status { get; set; } = true;
    }
}