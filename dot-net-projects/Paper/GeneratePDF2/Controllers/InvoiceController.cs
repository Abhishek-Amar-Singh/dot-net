﻿using GeneratePDF2.Models;
using Microsoft.AspNetCore.Mvc;
using SelectPdf;
using System.Net.Mail;
using System.Net.Mime;
using System.Net;
using System.Reflection.Metadata;

// View To PDF
namespace GeneratePDF2.Controllers
{
    public class InvoiceController : Controller
    {
        public IActionResult Index()
        {
            return View(Invoice.GetOne());
        }

        public FileResult GeneratePdf(string html)
        {
            html = html.Replace("strtTag", "<").Replace("EngTag", ">");
            HtmlToPdf objHtmlToPdf = new HtmlToPdf();
            //objHtmlToPdf.Options.PdfPageOrientation = PdfPageOrientation.Landscape;
            //objHtmlToPdf.Options.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
            PdfDocument objDoc = objHtmlToPdf.ConvertHtmlString(html);
            byte[] pdf = objDoc.Save();
            objDoc.Close();
            return File(pdf, "application/pdf", "MyPdf.pdf");
        }

        public IActionResult GeneratePdfAndSendOnMail(string html)
        {
            html = html.Replace("strtTag", "<").Replace("EngTag", ">");
            HtmlToPdf objHtmlToPdf = new HtmlToPdf();
            //objHtmlToPdf.Options.PdfPageOrientation = PdfPageOrientation.Landscape;
            objHtmlToPdf.Options.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
            PdfDocument objDoc = objHtmlToPdf.ConvertHtmlString(html);
            byte[] pdf = objDoc.Save();
            objDoc.Close();
            var generatedFile = File(pdf, "application/pdf", Invoice.GetOne().InvoiceNo + "-" + Invoice.GetOne().CustomerName + "-Pdf.pdf");

            string path = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, "wwwroot", "UploadedFiles"));
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var generatedFileStreamResult = new FileStreamResult(new MemoryStream(generatedFile.FileContents), generatedFile.ContentType);
            generatedFileStreamResult.FileDownloadName = generatedFile.FileDownloadName;
            using (var generatedFileStream = generatedFileStreamResult.FileStream)
            {
                IFormFile theFile = new FormFile(generatedFileStream, 0, generatedFileStream.Length, "name", generatedFileStreamResult.FileDownloadName);
                using (var fileStream = new FileStream(Path.Combine(path, theFile.FileName), FileMode.Create))
                {
                    theFile.CopyTo(fileStream);
                }
            }
            var sentMailStatusMessage = SendMail($"{path}/{generatedFile.FileDownloadName}", "abhisheksingh.120120@gmail.com", "tejaspawar1320@gmail.com");
            return Json(sentMailStatusMessage);
        }

        public string SendMail(string filePath, string emailFrom, string emailTo)
        {
            MailAddress sendTo = new MailAddress(emailTo);
            MailAddress sendFrom = new MailAddress(emailFrom);
            Attachment billAttachment = new Attachment(filePath, MediaTypeNames.Application.Octet);
            MailMessage message = new MailMessage(sendFrom, sendTo);
            message.Subject = "Order Placed successfully! Here is your bill invoice";
            message.IsBodyHtml = true;
            message.Body = $"Hello User, <br> Thank you for your purchase. Below attached is the invoice of your order, incase of " +
                $"any queries please write back to us. Purchase Again! <br> Regards, <br> Team Corbet.";
            message.Attachments.Add(billAttachment);
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential(emailFrom, "ayuzrsbhiumlyqcb"),
                EnableSsl = true
            };
            client.Send(message);
            return "Invoice mail sent successfully!";
        }

        public string SendMail2(string emailFrom, string emailTo)
        {
            MailAddress sendTo = new MailAddress(emailTo);
            MailAddress sendFrom = new MailAddress(emailFrom);
            MailMessage message = new MailMessage(sendFrom, sendTo);
            message.Subject = "Order Placed successfully! Here is your bill invoice";
            message.IsBodyHtml = true;
            message.Body = $"<button><a href='https://localhost:7221/Invoice/Btn1'>Accept</a><button><br/><button><a href='https://localhost:7221/Login/ResetPassword'>Reject</a><button>";
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential(emailFrom, "password"),
                EnableSsl = true
            };
            client.Send(message);
            return "Invoice mail sent successfully!";
        }

        public IActionResult Btn1()
        {
            return View();
        }
        public IActionResult Btn2()
        {
            return View();
        }

        public IActionResult Index1()
        {
            return View(Invoice.GetOne());
        }
    }
}
