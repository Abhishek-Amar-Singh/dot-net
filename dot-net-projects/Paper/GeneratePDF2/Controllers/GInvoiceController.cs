﻿using System.Net.Mail;
using System.Net.Mime;
using System.Net;

using Microsoft.AspNetCore.Mvc;
using SelectPdf;
using Newtonsoft.Json;
using GeneratePDF2.Controllers;

namespace GeneratePDF2.Models.GInvoice
{
    public class GInvoiceController : Controller
    {
        public IActionResult Index()
        {
            MyClass gInvoice = new MyClass();
            return View(gInvoice);
        }

        public IActionResult GeneratePdfAndSendOnMail(string html, int userId)
        {
            html = html.Replace("StartTag", "<").Replace("EngTag", ">");
            HtmlToPdf objHtmlToPdf = new HtmlToPdf();
            objHtmlToPdf.Options.PdfPageOrientation = PdfPageOrientation.Landscape;
            objHtmlToPdf.Options.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
            PdfDocument objDoc = objHtmlToPdf.ConvertHtmlString(html);
            byte[] pdf = objDoc.Save();
            objDoc.Close();
            var generatedFile = File(pdf, "application/pdf", Invoice.GetOne().InvoiceNo + "-" + Invoice.GetOne().CustomerName + "-Pdf.pdf");

            string path = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, "wwwroot", "UploadedFiles"));
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var generatedFileStreamResult = new FileStreamResult(new MemoryStream(generatedFile.FileContents), generatedFile.ContentType);
            generatedFileStreamResult.FileDownloadName = generatedFile.FileDownloadName;
            using (var generatedFileStream = generatedFileStreamResult.FileStream)
            {
                IFormFile theFile = new FormFile(generatedFileStream, 0, generatedFileStream.Length, "name", generatedFileStreamResult.FileDownloadName);
                using (var fileStream = new FileStream(Path.Combine(path, theFile.FileName), FileMode.Create))
                {
                    theFile.CopyTo(fileStream);
                }
            }
            var sentMailStatusMessage = SendMail($"{path}/{generatedFile.FileDownloadName}", "abhisheksingh.120120@gmail.com", "tejaspawar1320@gmail.com");
            return Json(sentMailStatusMessage);
        }

        public string SendMail(string filePath, string emailFrom, string emailTo)
        {
            MailAddress sendTo = new MailAddress(emailTo);
            MailAddress sendFrom = new MailAddress(emailFrom);
            Attachment billAttachment = new Attachment(filePath, MediaTypeNames.Application.Octet);
            MailMessage message = new MailMessage(sendFrom, sendTo);
            message.Subject = "Order Placed successfully! Here is your bill invoice";
            message.IsBodyHtml = true;
            message.Body = $"Hello User, <br> Thank you for your purchase. Below attached is the invoice of your order, incase of " +
                $"any queries please write back to us. Purchase Again! <br> Regards, <br> Team Corbet.";
            message.Attachments.Add(billAttachment);
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential(emailFrom, "pyuzrsbvaumlyqcs"),
                EnableSsl = true
            };
            client.Send(message);
            return "Invoice mail sent successfully!";
        }
    }
}
