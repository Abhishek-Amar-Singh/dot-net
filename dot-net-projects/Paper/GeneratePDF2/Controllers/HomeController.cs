﻿using GeneratePDF2.Models;
using Microsoft.AspNetCore.Mvc;
using SelectPdf;
using System.Diagnostics;
using System.Net.Mail;
using System.Net.Mime;
using System.Net;
using System.IO;

namespace GeneratePDF2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IList<Product> productList;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
            productList = new List<Product>()
            {
                new Product(){Id=1, Name="Tomato", Price=2000, Quantity=2},
                new Product(){Id=2, Name="Watermelon", Price=1900.3,  Quantity=1},
                new Product(){Id=3, Name="Alika", Price=800.8,  Quantity=5}
            };
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Main()
        {
            return View();
        }

        public IActionResult MainPV()
        {
            return PartialView("_Main", productList);
        }

        public IActionResult GiveOrderAndSendMail(string html)
        {
            html = html.Replace("StartTag", "<").Replace("EngTag", ">");
            var btns = $"<br/><button><a href='https://localhost:7156/Home/BtnState?status=accepted'>Accept</a></button> <button><a href='https://localhost:7156/Home/BtnState?status=rejected'>Reject</a></button>";
            var subject = "You received an order from Corbet (Purchase Staff)";

            SendMail(subject, html + btns, "abhisheksingh.120120@gmail.com", "abhisheksingh.120120@gmail.com");
            ViewBag.emailSentToSupplierStatusMessage = "<script type='text/javascript'>Swal.fire('Order Given Successfully!','Please wait for the supplier response.','success').then(()=>window.location.href='/Home/Index');</script>";
            return View("Main");
        }
        public void SendMail(string subject, string html, string emailFrom, string emailTo)
        {
            MailAddress sendTo = new MailAddress(emailTo);
            MailAddress sendFrom = new MailAddress(emailFrom);
            MailMessage message = new MailMessage(sendFrom, sendTo);
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = html;
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential(emailFrom, "pyuzrsbvaumlyqcs"),
                EnableSsl = true
            };
            client.Send(message);
        }

        public string BtnState(string status)
        {
            if (status == "accepted")
            {
                //edit quantity of stocks logic here
                return "Order Accepted";
            }
            else
            {
                //no action done
                return "Order Rejected";
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}