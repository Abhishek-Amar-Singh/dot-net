﻿using data_warehouse.lakemaster_api.DAL;
using data_warehouse.lakemaster_api.Models.BusinessModels;
using lakemaster_api.Bootstrap;

namespace lakemaster_api.Repository
{
    public class FcFWPCommentsRepository : GenericRepository<LMDbContext, FcFWPComments>
    {
        public FcFWPCommentsRepository(LMDbContext dbContext) : base(dbContext)
        {
        }


    }

    class C
    {
        private readonly FcFWPCommentsRepository g;
        public C(FcFWPCommentsRepository g)
        {
            this.g = g;
        }
        public void Reg()
        {
            this.g.ModifyEntities();
        }
    }
}
