﻿using data_warehouse.lakemaster_api.DAL;
using Microsoft.EntityFrameworkCore;

namespace lakemaster_api.Bootstrap
{
    public class GenericRepository<TContext, TEntity> : IGenericRepository<TEntity>
        where TContext : DbContext
        where TEntity : class
    {
        private readonly TContext dbContext;
        private readonly DbSet<TEntity> dbSet;

        public GenericRepository(TContext dbContext)
        {
            dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            dbSet = dbContext.Set<TEntity>();
        }

        public void AddEntity(TEntity entity)
        {
            dbSet.Add(entity);
            dbContext.SaveChanges();
        }

        public void AddEntities(List<TEntity> entities)
        {
            dbSet.AddRange(entities);
            dbContext.SaveChanges();
        }

        public TEntity GetEntityById(Guid id)
        {
            return dbSet.Find(id);
        }
        
        public List<TEntity> GetAllEntitiesById(Guid id, string nameofId)
        {
            return dbSet.Where(x => (Guid)x.GetType().GetProperty(nameofId).GetValue(x) == id).ToList();
        }

        public List<TEntity> GetAllEntities()
        {
            return dbSet.ToList();
        }

        public void ModifyEntity(TEntity entity)
        {
            dbSet.Update(entity);
            dbContext.SaveChanges();
        }

        public void ModifyEntities(List<TEntity> entities)
        {
            dbSet.UpdateRange(entities);
            dbContext.SaveChanges();
        }

        public void RemoveEntity(TEntity entity)
        {
            dbSet.Remove(entity);
            dbContext.SaveChanges();
        }

        public void RemoveEntities(List<TEntity> entities)
        {
            dbSet.RemoveRange(entities);
            dbContext.SaveChanges();
        }
    }
}
