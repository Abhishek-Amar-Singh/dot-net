﻿namespace lakemaster_api.Bootstrap
{
    public interface IGenericRepository<T> where T : class
    {
        T GetEntityById(Guid id);
        List<T> GetAllEntitiesById(Guid id, string nameofId);
        List<T> GetAllEntities();
        void AddEntity(T entity);
        void AddEntities(T entity);
        void ModifyEntity(T entity);
        void ModifyEntities(T entity);
        void RemoveEntity(T entity);
        void RemoveEntities(T entity);
    }
}
