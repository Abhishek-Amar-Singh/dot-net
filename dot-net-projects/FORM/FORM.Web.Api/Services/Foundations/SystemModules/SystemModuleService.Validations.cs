﻿using EFxceptions.Models.Exceptions;
using FORM.Web.Api.Models.SystemModules;
using FORM.Web.Api.Models.SystemModules.Exceptions;
using FORM.Web.Api.Models.SystemModules.VmModels;
using System.Runtime.CompilerServices;

namespace FORM.Web.Api.Services.Foundations.SystemModules
{
    public partial class SystemModuleService
    {
        private void ValidateSystemModuleOnCreate(SystemModule systemModule)
        {
            ValidateSystemModule(systemModule);

            Validate(
                (Rule: IsInvalidX(systemModule.ModuleName), Parameter: nameof(SystemModule.ModuleName))
            );
        }

        private static dynamic IsInvalidX(int id) => new
        {
            Condition = id == default,
            Message = "Id is required"
        };

        private static dynamic IsInvalidX(int? id) => new
        {
            Condition = id == default,
            Message = "Id is required"
        };

        private static dynamic IsInvalidX(string text) => new
        {
            Condition = String.IsNullOrWhiteSpace(text),
            Message = "Text is required"
        };

        private static dynamic IsInvalidX(bool? status) => new
        {
            Condition = status is null,
            Message = "status is required"
        };

        private static dynamic IsInvalidX(DateTime? dateTime) => new
        {
            Condition = dateTime == default,
            Message = "DateTime is required"
        };

        private static dynamic IsSame(
            DateTime? firstDate,
            DateTime? secondDate,
            string secondDateName) => new
            {
                Condition = firstDate == secondDate,
                Message = $"Date is the same as {secondDateName}"
            };

        private static void ValidateSystemModule(SystemModule systemModule)
        {
            if (systemModule.ModuleName is null)
            {
                throw new NullSystemModuleException();
            }
        }

        private async ValueTask<bool> ValidateSystemModuleAlreadyExists(SystemModule systemModule)
        {
            var systemModuleData = await this.storageBroker
                .SelectSystemModuleByNameAsync(systemModule.ModuleName);
            return (systemModuleData is null) ? false :
                throw new AlreadyExistsSystemModuleException();
        }

        private async ValueTask ValidateSystemModuleAgainstModify(SystemModule systemModule)
        {
            var systemModuleData = await this.storageBroker
                .SelectSystemModuleByIdAsync(systemModule.SystemModuleId);
            if (systemModuleData.ModuleName == systemModule.ModuleName &&
                systemModuleData.IsActive == systemModule.IsActive)
            {
                throw new UnchangedSystemModuleException(systemModule.SystemModuleId);
            }
            else
            {
                var _systemModuleData = await this.storageBroker
                .SelectSystemModuleByNameAsync(systemModule.ModuleName);
                if (_systemModuleData is not null &&
                    _systemModuleData.SystemModuleId != systemModule.SystemModuleId &&
                    _systemModuleData.ModuleName == systemModule.ModuleName)
                {
                    throw new CannotChangeSystemModuleException(
                    systemModule.ModuleName, _systemModuleData.SystemModuleId);
                }
            }
        }

        private static void ValidateSystemModuleId(int systemModuleId)
        {
            if (systemModuleId == default)
            {
                throw new InvalidSystemModuleException(
                    parameterName: nameof(SystemModule.SystemModuleId),
                    parameterValue: systemModuleId);
            }
        }

        private static void ValidateSystemModuleName(string systemModuleName)
        {
            if (systemModuleName == default)
            {
                throw new InvalidSystemModuleException(
                    parameterName: nameof(SystemModule.ModuleName),
                    parameterValue: systemModuleName);
            }
        }

        private static void ValidateStorageSystemModule(SystemModule storageSystemModule, int systemModuleId)
        {
            if (storageSystemModule is null)
            {
                throw new NotFoundSystemModuleException(systemModuleId);
            }
        }

        private static void ValidateSystemModuleIsActive(
            SystemModule storageSystemModule, int systemModuleId)
        {
            if (storageSystemModule.IsActive is false)
            {
                throw new NotActiveSystemModuleException(systemModuleId);
            }
        }

        private static void ValidateStorageSystemModule(SystemModule storageSystemModule, string systemModuleName)
        {
            if (storageSystemModule is null)
            {
                throw new NotFoundSystemModuleException();
            }
        }

        private void ValidateSystemModuleOnModify(SystemModule systemModule)
        {
            ValidateSystemModule(systemModule);

            Validate(
                (Rule: IsInvalidX(systemModule.SystemModuleId), Parameter: nameof(SystemModule.SystemModuleId)),
                (Rule: IsInvalidX(systemModule.ModuleName), Parameter: nameof(systemModule.ModuleName)),
                (Rule: IsInvalidX(systemModule.IsActive), Parameter: nameof(SystemModule.IsActive))
            );
        }

        private static void Validate(params (dynamic Rule, string Parameter)[] validations)
        {
            var invalidSystemModuleException = new InvalidSystemModuleException();

            foreach ((dynamic rule, string parameter) in validations)
            {
                if (rule.Condition)
                {
                    invalidSystemModuleException.UpsertDataList(
                        key: parameter,
                        value: rule.Message);
                }
            }

            invalidSystemModuleException.ThrowIfContainsErrors();
        }
    }
}
