﻿using EFxceptions.Models.Exceptions;
using FORM.Web.Api.Models.SystemModules;
using FORM.Web.Api.Models.SystemModules.Exceptions;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using System.Data.SqlClient;
using Xeptions;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace FORM.Web.Api.Services.Foundations.SystemModules
{
    public partial class SystemModuleService
    {
        private delegate IQueryable<SystemModule> ReturningSystemModulesFunction();
        private delegate ValueTask<SystemModule> ReturningSystemModuleFunction();

        private IQueryable<SystemModule> TryCatch(
            ReturningSystemModulesFunction returningSystemModulesFunction)
        {
            try
            {
                return returningSystemModulesFunction();
            }
            catch (SqlException sqlException)
            {
                throw CreateAndLogCriticalDependencyException(sqlException);
            }
            catch (NpgsqlException npgsqlException)
            {
                throw CreateAndLogCriticalDependencyException(npgsqlException);
            }
            catch (Exception exception)
            {
                var failedSystemModuleServiceException =
                    new FailedSystemModuleServiceException(exception);

                throw CreateAndLogServiceException(failedSystemModuleServiceException);
            }
        }

        private async ValueTask<SystemModule> TryCatch(
            ReturningSystemModuleFunction returningSystemModuleFunction)
        {
            try
            {
                return await returningSystemModuleFunction();
            }
            catch (NullSystemModuleException nullSystemModuleException)
            {
                throw CreateAndLogValidationException(nullSystemModuleException);
            }
            catch (InvalidSystemModuleException invalidSystemModuleException)
            {
                throw CreateAndLogValidationException(invalidSystemModuleException);
            }
            catch (NotFoundSystemModuleException nullSystemModuleException)
            {
                throw CreateAndLogValidationException(nullSystemModuleException);
            }
            catch (SqlException sqlException)
            {
                var failedSystemModuleStorageException =
                    new FailedSystemModuleStorageException(sqlException);

                throw CreateAndLogCriticalDependencyException(failedSystemModuleStorageException);
            }
            catch (PostgresException postgresException)
            {
                var failedSystemModuleStorageException =
                    new FailedSystemModuleStorageException(postgresException);

                throw CreateAndLogCriticalDependencyException(failedSystemModuleStorageException);
            }
            catch (AlreadyExistsSystemModuleException alreadyExistsSystemModuleException)
            {
                throw CreateAndLogDependencyValidationException(alreadyExistsSystemModuleException);
            }
            catch (DuplicateKeyException duplicateKeyException)
            {
                var alreadyExistsSystemModuleException =
                    new AlreadyExistsSystemModuleException(duplicateKeyException);

                throw CreateAndLogDependencyValidationException(alreadyExistsSystemModuleException);
            }
            catch (DbUpdateConcurrencyException dbUpdateConcurrencyException)
            {
                var lockedSystemModuleException = new LockedSystemModuleException(dbUpdateConcurrencyException);

                throw CreateAndLogDependencyException(lockedSystemModuleException);
            }
            catch (DbUpdateException dbUpdateException)
            {
                var failedSystemModuleStorageException =
                    new FailedSystemModuleStorageException(dbUpdateException);

                throw CreateAndLogDependencyException(failedSystemModuleStorageException);
            }
            catch (UnchangedSystemModuleException unchangedSystemModuleException)
            {
                throw CreateAndLogValidationException(unchangedSystemModuleException);
            }
            catch (CannotChangeSystemModuleException unchangedSystemModuleException)
            {
                throw CreateAndLogValidationException(unchangedSystemModuleException);
            }
            catch (NotActiveSystemModuleException notActiveSystemModuleException)
            {
                throw CreateAndLogValidationException(notActiveSystemModuleException);
            }
            catch (Exception exception)
            {
                var failedSystemModuleServiceException =
                    new FailedSystemModuleServiceException(exception);

                throw CreateAndLogServiceException(failedSystemModuleServiceException);
            }
        }

        private SystemModuleDependencyException CreateAndLogDependencyException(Exception exception)
        {
            var systemModuleDependencyException = new SystemModuleDependencyException(exception);
            this.loggingBroker.LogError(systemModuleDependencyException);

            return systemModuleDependencyException;
        }

        private SystemModuleDependencyException CreateAndLogCriticalDependencyException(Exception exception)
        {
            var systemModuleDependencyException = new SystemModuleDependencyException(exception);
            this.loggingBroker.LogCritical(systemModuleDependencyException);

            return systemModuleDependencyException;
        }

        private SystemModuleServiceException CreateAndLogServiceException(Exception exception)
        {
            var systemModuleServiceException = new SystemModuleServiceException(exception);
            this.loggingBroker.LogError(systemModuleServiceException);

            return systemModuleServiceException;
        }

        private SystemModuleValidationException CreateAndLogValidationException(Exception exception)
        {
            var systemModuleValidationException = new SystemModuleValidationException(exception);
            this.loggingBroker.LogError(systemModuleValidationException);

            return systemModuleValidationException;
        }

        private SystemModuleDependencyValidationException CreateAndLogDependencyValidationException(Xeption exception)

        {
            var systemModuleDependencyValidationException =
                new SystemModuleDependencyValidationException(exception);

            this.loggingBroker.LogError(systemModuleDependencyValidationException);

            return systemModuleDependencyValidationException;
        }
    }
}

