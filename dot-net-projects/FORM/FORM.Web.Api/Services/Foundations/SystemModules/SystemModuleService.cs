﻿using FORM.Web.Api.Brokers.DateTimes;
using FORM.Web.Api.Brokers.Loggings;
using FORM.Web.Api.Brokers.Storages;
using FORM.Web.Api.Models.SystemModules;
using FORM.Web.Api.Models.SystemModules.Exceptions;
using FORM.Web.Api.Models.SystemModules.VmModels;
using System.Collections.Generic;

namespace FORM.Web.Api.Services.Foundations.SystemModules
{
    public partial class SystemModuleService : ISystemModuleService
    {
        private readonly IStorageBroker storageBroker;
        private readonly ILoggingBroker loggingBroker;
        private readonly IDateTimeBroker dateTimeBroker;

        public SystemModuleService(
            IStorageBroker storageBroker,
            IDateTimeBroker dateTimeBroker,
            ILoggingBroker loggingBroker)
        {
            this.storageBroker = storageBroker;
            this.dateTimeBroker = dateTimeBroker;
            this.loggingBroker = loggingBroker;
        }

        /*
        public IQueryable<SystemModuleVm> RetrieveSystemModuleDetailsById(int systemModuleId) =>
            this.storageBroker.SelectSystemModuleDetailsById(systemModuleId);
        */

        public ValueTask<SystemModule> CreateSystemModuleAsync(
            SystemModule systemModule, int userId) =>
        TryCatch(async () =>
        {
            ValidateSystemModuleOnCreate(systemModule);
            await ValidateSystemModuleAlreadyExists(systemModule);

            return await this.storageBroker.InsertSystemModuleAsync(systemModule, userId);
        });

        
        public ValueTask<SystemModule> RetrieveSystemModuleByIdAsync(int systemModuleId) =>
        TryCatch(async () =>
        {
            ValidateSystemModuleId(systemModuleId);
            SystemModule maybeSystemModule = await this.storageBroker.SelectSystemModuleByIdAsync(systemModuleId);
            ValidateStorageSystemModule(maybeSystemModule, systemModuleId);
            ValidateSystemModuleIsActive(maybeSystemModule, systemModuleId);

            return maybeSystemModule;
        });

        public ValueTask<SystemModule> RetrieveSystemModuleByNameAsync(string systemModuleName) =>
        TryCatch(async () =>
        {
            ValidateSystemModuleName(systemModuleName);
            SystemModule maybeSystemModule = await this.storageBroker.SelectSystemModuleByNameAsync(systemModuleName);
            ValidateStorageSystemModule(maybeSystemModule, systemModuleName);

            return maybeSystemModule;
        });

        
        public ValueTask<SystemModule> ModifySystemModuleAsync(SystemModule systemModule, int userId) =>
        TryCatch(async () =>
        {
            ValidateSystemModuleOnModify(systemModule);
            await ValidateSystemModuleAgainstModify(systemModule);

            var maybeSystemModule =
                await this.storageBroker.SelectSystemModuleByIdAsync(systemModule.SystemModuleId);

            ValidateStorageSystemModule(maybeSystemModule, systemModule.SystemModuleId);


            return await this.storageBroker.UpdateSystemModuleAsync(systemModule, userId);
        });

        public ValueTask<SystemModule> RemoveSystemModuleByIdAsync(
            int systemModuleId, int userId) =>
        TryCatch(async () =>
        {
            ValidateSystemModuleId(systemModuleId);

            var maybeSystemModule =
                await this.storageBroker.SelectSystemModuleByIdAsync(systemModuleId);

            ValidateStorageSystemModule(maybeSystemModule, systemModuleId);

            return await this.storageBroker.DeleteSystemModuleAsync(systemModuleId, userId);
        });

        public ValueTask<SystemModule> PutBackSystemModuleByIdAsync(
            int systemModuleId, int userId) =>
        TryCatch(async () =>
        {
            ValidateSystemModuleId(systemModuleId);

            var maybeSystemModule =
                await this.storageBroker.SelectSystemModuleByIdAsync(systemModuleId);

            ValidateStorageSystemModule(maybeSystemModule, systemModuleId);

            return await this.storageBroker.UndoDeleteSystemModuleAsync(systemModuleId, userId);
        });

        public IQueryable<SystemModule> RetrieveAllSystemModules() =>
        TryCatch(() => this.storageBroker.SelectAllSystemModules());
    }
}
