﻿using Dapper;
using Npgsql;
using System.Data;

namespace FORM.Web.Api.Brokers.Storages
{
    public partial class StorageBroker : IStorageBroker
    {
        private readonly string? connectionString;

        public StorageBroker(IConfiguration configuration)
        {
            this.connectionString = configuration.GetConnectionString("localDbConnectionStr");
        }

        private async ValueTask<T> DapperCrudAsync<T>(string sql, DynamicParameters parameters)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                await connection.OpenAsync();
                var result = await connection.QueryAsync<T>(sql, parameters, commandType: CommandType.Text);

                return result.FirstOrDefault();
            }
        }

        private IQueryable<T> SelectAll<T>(string sql)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                var results = connection.Query<T>(sql, commandType: CommandType.Text);

                return results.AsQueryable();
            }
        }
    }
}
