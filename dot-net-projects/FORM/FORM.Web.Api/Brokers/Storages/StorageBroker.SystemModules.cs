﻿using Dapper;
using FORM.Web.Api.Models;
using FORM.Web.Api.Models.SystemModules;
using System.Data;

namespace FORM.Web.Api.Brokers.Storages
{
    public partial class StorageBroker
    {
        public async ValueTask<SystemModule> InsertSystemModuleAsync(
            SystemModule systemModule, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_system_module(@system_module_id, @module_name, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("system_module_id", systemModule.SystemModuleId, DbType.Int32);
            parameters.Add("module_name", systemModule.ModuleName, DbType.String);
            parameters.Add("is_active", systemModule.IsActive, DbType.Boolean);
            parameters.Add("is_deleted", systemModule.IsDeleted, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.INSERT.ToString(), DbType.String);

            return await DapperCrudAsync<SystemModule>(sql, parameters);
        }

        /*
        public IQueryable<SystemModuleVm> SelectSystemModuleDetailsById(int systemModuleId) =>
            this.Set<SystemModuleVm>().FromSqlRaw($"SELECT * FROM fn_get_system_module_details({systemModuleId});");
        */

        public async ValueTask<SystemModule> SelectSystemModuleByNameAsync(string moduleName)
        {
            var sql = "SELECT * FROM public.fn_crud_on_system_module(@system_module_id, @module_name, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("system_module_id", null, DbType.Int32);
            parameters.Add("module_name", moduleName, DbType.String);
            parameters.Add("is_active", null, DbType.Boolean);
            parameters.Add("is_deleted", null, DbType.Boolean);
            parameters.Add("user_id", null, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.RETRIEVE.ToString(), DbType.String);

            return await DapperCrudAsync<SystemModule>(sql, parameters);
        }

        public async ValueTask<SystemModule> SelectSystemModuleByIdAsync(int systemModuleId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_system_module(@system_module_id, @module_name, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("system_module_id", systemModuleId, DbType.Int32);
            parameters.Add("module_name", null, DbType.String);
            parameters.Add("is_active", null, DbType.Boolean);
            parameters.Add("is_deleted", null, DbType.Boolean);
            parameters.Add("user_id", null, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.RETRIEVE.ToString(), DbType.String);

            return await DapperCrudAsync<SystemModule>(sql, parameters);
        }

        public async ValueTask<SystemModule> UpdateSystemModuleAsync(
            SystemModule systemModule, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_system_module(@system_module_id, @module_name, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("system_module_id", systemModule.SystemModuleId, DbType.Int32);
            parameters.Add("module_name", systemModule.ModuleName, DbType.String);
            parameters.Add("is_active", systemModule.IsActive, DbType.Boolean);
            parameters.Add("is_deleted", systemModule.IsDeleted, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.UPDATE.ToString(), DbType.String);

            return await DapperCrudAsync<SystemModule>(sql, parameters);
        }

        public async ValueTask<SystemModule> DeleteSystemModuleAsync(
            int systemModuleId, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_system_module(@system_module_id, @module_name, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("system_module_id", systemModuleId, DbType.Int32);
            parameters.Add("module_name", null, DbType.String);
            parameters.Add("is_active", false, DbType.Boolean);
            parameters.Add("is_deleted", true, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.DELETE.ToString(), DbType.String);

            return await DapperCrudAsync<SystemModule>(sql, parameters);
        }

        public async ValueTask<SystemModule> UndoDeleteSystemModuleAsync(
            int systemModuleId, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_system_module(@system_module_id, @module_name, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("system_module_id", systemModuleId, DbType.Int32);
            parameters.Add("module_name", null, DbType.String);
            parameters.Add("is_active", true, DbType.Boolean);
            parameters.Add("is_deleted", false, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.UNDODELETE.ToString(), DbType.String);

            return await DapperCrudAsync<SystemModule>(sql, parameters);
        }

        public IQueryable<SystemModule> SelectAllSystemModules() =>
            SelectAll<SystemModule>("SELECT * FROM fn_get_all_system_modules();");

    }
}
