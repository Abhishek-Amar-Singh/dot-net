﻿using FORM.Web.Api.Models.SystemModules;
using FORM.Web.Api.Models.SystemModules.VmModels;

namespace FORM.Web.Api.Brokers.Storages
{
    public partial interface IStorageBroker
    {
        //IQueryable<SystemModuleVm> SelectSystemModuleDetailsById(int systemModuleId);

        ValueTask<SystemModule> InsertSystemModuleAsync(SystemModule systemModule, int userId);

        ValueTask<SystemModule> SelectSystemModuleByNameAsync(string moduleName);

        ValueTask<SystemModule> SelectSystemModuleByIdAsync(int systemModuleId);

        ValueTask<SystemModule> UpdateSystemModuleAsync(SystemModule systemModule, int userId);

        ValueTask<SystemModule> DeleteSystemModuleAsync(int systemModuleId, int userId);

        ValueTask<SystemModule> UndoDeleteSystemModuleAsync(int systemModuleId, int userId);

        IQueryable<SystemModule> SelectAllSystemModules();
    }
}
