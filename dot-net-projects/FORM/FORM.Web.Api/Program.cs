using FORM.Web.Api.Brokers.DateTimes;
using FORM.Web.Api.Brokers.Loggings;
using FORM.Web.Api.Brokers.Storages;
using FORM.Web.Api.Services.Foundations.SystemModules;
using Npgsql;
using System.Data;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


//--Add services to the container.
builder.Services.AddLogging();

var connectionString = builder.Configuration.GetConnectionString("localDbConnectionStr");
builder.Services.AddSingleton<IDbConnection>((sp) => new NpgsqlConnection(connectionString));

//builder.Services.AddScoped<IDbConnection>(c =>
//{
//    //var configuration = c.GetRequiredService<IConfiguration>();
//    var connectionString = builder.Configuration.GetConnectionString("localDbConnectionStr");
//    return new NpgsqlConnection(connectionString);
//});

//--Add Brokers
builder.Services.AddScoped<IStorageBroker, StorageBroker>();
builder.Services.AddTransient<ILoggingBroker, LoggingBroker>();
builder.Services.AddScoped<IDateTimeBroker, DateTimeBroker>();

//--Add Services
builder.Services.AddScoped<ISystemModuleService, SystemModuleService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
