﻿namespace FORM.Web.Api.Models.SystemModules.DtoModels
{
    public class CreateSystemModuleDto
    {
        public string ModuleName { get; set; } = null!;
    }
}
