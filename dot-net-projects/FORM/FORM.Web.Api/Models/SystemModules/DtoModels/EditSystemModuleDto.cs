﻿namespace FORM.Web.Api.Models.SystemModules.DtoModels
{
    public class EditSystemModuleDto
    {
        public int Id { get; set; }

        public string ModuleName { get; set; } = null!;

        public bool? IsActive { get; set; }
    }
}
