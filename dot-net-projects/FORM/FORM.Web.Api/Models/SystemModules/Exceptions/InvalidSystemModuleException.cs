﻿using Xeptions;

namespace FORM.Web.Api.Models.SystemModules.Exceptions
{
    public class InvalidSystemModuleException : Xeption
    {
        public InvalidSystemModuleException(string parameterName, object parameterValue)
            : base(message: $"Invalid system module, " +
                  $"parameter name: {parameterName}, " +
                  $"parameter value: {parameterValue}.")
        { }

        public InvalidSystemModuleException()
            : base(message: "Invalid system module. Please fix the errors and try again.") { }
    }
}
