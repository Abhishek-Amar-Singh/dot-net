﻿using Xeptions;

namespace FORM.Web.Api.Models.SystemModules.Exceptions
{
    public class SystemModuleServiceException:Xeption
    {
        public SystemModuleServiceException(Exception innerException)
            : base(message: "Failed system module service error occurred, contact support.", innerException)
        { }
     
        
    }
}
