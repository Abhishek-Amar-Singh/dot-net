﻿using FORM.Web.Api.Models.SystemModules.VmModels;

namespace FORM.Web.Api.Models.SystemModules.Exceptions
{
    public class NotActiveSystemModuleException : Exception
    {
        public NotActiveSystemModuleException()
            : base(message: $"System module is not active. To activate it, please try undo delete.") { }

        public NotActiveSystemModuleException(int systemModuleId)
            : base(message: $"System module having id: {systemModuleId} is not active." +
                  $"To activate it, please try undo delete.") { }
    }
}
