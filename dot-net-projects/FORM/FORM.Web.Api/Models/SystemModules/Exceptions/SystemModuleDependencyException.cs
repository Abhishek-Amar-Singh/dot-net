﻿namespace FORM.Web.Api.Models.SystemModules.Exceptions
{
    public class SystemModuleDependencyException:Exception
    {
        public SystemModuleDependencyException(Exception innerException)
            : base(message: "Service dependency error occurred, contact support.", innerException) 
        { }

    }
}
