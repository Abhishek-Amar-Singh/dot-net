﻿namespace FORM.Web.Api.Models.SystemModules.Exceptions
{
    public class LockedSystemModuleException : Exception
    {
        public LockedSystemModuleException(Exception innerException)
            : base(message: "Locked system module record exception, please try again later.", innerException) { }
    }
}
