﻿namespace FORM.Web.Api.Models.SystemModules.Exceptions
{
    public class UnchangedSystemModuleException : Exception
    {
        public UnchangedSystemModuleException(int systemModuleId)
            : base(message: $"System module with id: {systemModuleId} remains unchanged.") { }
    }
}
