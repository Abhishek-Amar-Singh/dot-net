﻿namespace FORM.Web.Api.Models.SystemModules.VmModels
{
    public class SystemModuleVm
    {
        public int SystemModuleId { get; set; }

        public string? ModuleName { get; set; }

        public string? FieldTitle { get; set; }

        public string? ControlName { get; set; }

        public string? EventName { get; set; }

        public string? ParameterName { get; set; }

        public string? RegexFor { get; set; }

        public string? Pattern { get; set; }

        public string[]? ErrorMessage { get; set; }

        public string? OptionKey { get; set; }

        public string? OptionValue { get; set; }

        public bool? IsDefault { get; set; }
    }
}
