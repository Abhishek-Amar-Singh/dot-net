﻿namespace FORM.Web.Api.Models
{
    public enum OPERATIONS
    {
        INSERT,
        UPDATE,
        DELETE,
        UNDODELETE,
        RETRIEVE
    }
}
