﻿

namespace ConsoleApp1
{
    public class Numbers
    {
        public void addition()
        {
            Console.WriteLine("This is addition method");
        }

        public virtual void subtraction()
        {
            Console.WriteLine("This is subtraction method");
        }

    }

    public class Calculate : Numbers
    {
        public void addition()
        {

            Console.WriteLine("This is addition method in the derived class");
        }

        public override void subtraction()
        {

            Console.WriteLine("This is subtraction method override in derived class");
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            Calculate calc = new Calculate();
            Numbers nmbr = calc;
            calc.addition();
            nmbr.addition();
            calc.subtraction();
            nmbr.subtraction();
        }
    }
}