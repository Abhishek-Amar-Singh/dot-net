﻿using MailKit;
using MailKit.Net.Smtp;
using MiddlewareIntegration.Mail.Exceptions;
using MiddlewareIntegration.Mail.Models;
using MimeKit;
using System.Net.Sockets;
using AuthenticationException = MailKit.Security.AuthenticationException;

namespace MiddlewareIntegration.Mail.Services
{
    public class MailService : IMailService
    {
        private readonly MailConfig mailConfiguration;

        public MailService(MailConfig mailConfiguration) =>
            this.mailConfiguration = mailConfiguration;

        public void SendMail(Message msg)
        {
            var mailMessage = CreateMessageToMail(msg);
            Send(mailMessage);
        }

        private MimeMessage CreateMessageToMail(Message msg)
        {
            var mailMessage = new MimeMessage();
            mailMessage.From.Add(new MailboxAddress(mailConfiguration.Name, mailConfiguration.From));
            mailMessage.To.AddRange(msg.To);
            mailMessage.Subject = msg.Subject;
            mailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html) { Text = msg.Content };
            return mailMessage;
        }

        private void Send(MimeMessage emailMessage)
        {
            using var client = new SmtpClient();
            try
            {
                client.Connect(mailConfiguration.SmtpServer, mailConfiguration.Port, true);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                client.Authenticate(mailConfiguration.Username, mailConfiguration.Password);

                client.Send(emailMessage);
            }
            catch (SmtpProtocolException smtpProtocolException)
            {
                throw new FailedMailServiceException(smtpProtocolException);
            }
            catch (AuthenticationException authenticationException)
            {
                throw new FailedMailServiceException(authenticationException);
            }
            catch (SocketException socketException)
            {
                throw new FailedMailServiceException(socketException);
            }
            catch (SmtpCommandException smtpCommandException)
            {
                throw new FailedMailServiceException(smtpCommandException);
            }
            catch (ServiceNotConnectedException serviceNotConnectedException)
            {
                throw new FailedMailServiceException(serviceNotConnectedException);
            }
            catch (Exception exception)
            {
                throw new FailedMailServiceException(exception);
            }
            finally
            {
                client.Disconnect(true);
                client.Dispose();
            }
        }
    }
}
