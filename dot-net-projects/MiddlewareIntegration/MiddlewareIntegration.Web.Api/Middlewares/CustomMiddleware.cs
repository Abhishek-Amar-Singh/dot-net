﻿namespace MiddlewareIntegration.Web.Api.Middlewares
{
    public class CustomMiddleware
    {
        private readonly RequestDelegate _next;

        public CustomMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            // Your custom logic here
            Console.WriteLine("Custom Middleware: Before the request");

            await _next(context);

            Console.WriteLine("Custom Middleware: After the request");
        }
    }
}
