﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;

namespace MiddlewareIntegration.Web.Api.Filters
{
    public class CustomAuthorizeAttribute : TypeFilterAttribute
    {
        public CustomAuthorizeAttribute() : base(typeof(CustomAuthorizeFilter)) { }

        private class CustomAuthorizeFilter : AuthorizeAttribute, IAuthorizationFilter
        {
            private readonly IConfiguration config;

            public CustomAuthorizeFilter(
                IConfiguration config)
            {
                this.config = config;
            }

            public void OnAuthorization(AuthorizationFilterContext context)
            {
                try
                {
                    var allowAnonymousAttribute =
                        context.ActionDescriptor.EndpointMetadata.OfType<AllowAnonymousAttribute>().FirstOrDefault();
                    if (allowAnonymousAttribute is not null) return;

                    bool result = context.HttpContext.User.Identity.IsAuthenticated;
                    if (!result)
                    {
                        context.Result = new UnauthorizedResult();
                        return;
                    }

                    if (!context.HttpContext.Request.Headers.ContainsKey("Authorization"))
                    {
                        result = false;
                    }

                    string token = string.Empty;
                    if (result)
                    {
                        token = context.HttpContext.Request.Headers.FirstOrDefault(x => x.Key == "Authorization").Value;
                        token = token.Substring("Bearer ".Length).Trim();
                        var handler = new JwtSecurityTokenHandler();
                        var jwtToken = handler.ReadJwtToken(token);

                        if (!ValidateTokenParameters(jwtToken))
                        {
                            result = false;
                        }
                    }

                    if (result is false)
                    {
                        context.Result = new UnauthorizedResult();
                    }
                }
                catch (Exception)
                {
                    context.Result = new UnauthorizedResult();
                }
            }

            private bool ValidateTokenParameters(JwtSecurityToken jwtToken)
            {
                if (!jwtToken.Issuer.StartsWith(this.config["base_url"])) return false;

                var authDateTime = DateTime.UtcNow;
                if (!(jwtToken.ValidFrom <= authDateTime && jwtToken.ValidTo >= authDateTime)) return false;

                if (jwtToken.Header.Alg != SecurityAlgorithms.RsaSha256) return false;

                //var storageUser = this.dbContext.UsersDbSet.FirstOrDefault(x => x.user_code == Guid.Parse(jwtToken.Subject) && x.is_active == true);
                //if (storageUser is null) return false;

                if (!this.config["ClientId"].Equals(jwtToken.Payload["client_id"])) return false;

                var scopeArray = JsonConvert.DeserializeObject<string[]>(Convert.ToString(jwtToken.Payload["scope"]));
                if (!ValidateArrayFromToken(scopeArray, "scope")) return false;

                var amrArray = JsonConvert.DeserializeObject<string[]>(Convert.ToString(jwtToken.Payload["amr"]));
                if (!ValidateArrayFromToken(amrArray, "amr")) return false;

                return true;
            }

            private bool ValidateArrayFromToken(string[] arr, string name)
            {
                string[] myAppScopeArray = this.config.GetSection(name).Get<string[]>();
                return arr.All(x => myAppScopeArray.Contains(x));
            }
        }
    }
}
