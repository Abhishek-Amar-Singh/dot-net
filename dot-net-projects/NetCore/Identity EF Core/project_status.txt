On PutRoleStatusAsync

{
  "description": "Human Resources, is a crucial organizational function responsible for managing and developing a company's most valuable asset: its people. HR encompasses various activities, including recruitment, hiring, onboarding, training, performance management, employee relations, compensation and benefits administration, and ensuring compliance with employment laws and regulations. It plays a pivotal role in fostering a positive work culture, supporting employee well-being, and aligning organizational goals with the needs and aspirations of the workforce. Overall, HR strives to maximize the potential and productivity of employees, contributing to the overall success and growth of the organization.",
  "isActive": true,
  "id": "cc6f8c4b-a76a-458b-7596-08db671adc45",
  "name": "HR",
"userId": "cc6f8c4b-a76a-458b-7596-08db671adc45"
}