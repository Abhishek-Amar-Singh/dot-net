﻿using Microsoft.EntityFrameworkCore;
using OSCRM.Web.Api.Models.Roles;
using OSCRM.Web.Api.Models.Users;
using OSCRM.Web.Api.Models.Users.Vms;

namespace OSCRM.Web.Api.Brokers.AuthManagement
{
    public partial class AuthManagementBroker
    {
        public async ValueTask<User> InsertUserAsync(User user)
        {
            await this.userManager.CreateAsync(user, user.Password);
            user.CreatedBy = user.Id;
            await this.userManager.UpdateAsync(user);

            return user;
        }

        public async ValueTask<User> SelectUserByEmailAsync(string email) =>
            await this.userManager.FindByEmailAsync(email);

        public async ValueTask<UserRespectiveRole> InsertUserToRoleAsync(User user, Role role)
        {
            var userRespectiveRole = new UserRespectiveRole
            {
                FullName = $"{user.FirstName}{(user.MiddleName is null ? "" : " "+user.MiddleName)} {user.LastName}",
                Email = user.Email,
                RoleName = role.Name
            };
            await this.userManager.AddToRoleAsync(user, role.Name);

            return userRespectiveRole;
        }

        public async ValueTask<User> UpdateUserAsync(User user)
        {
            await this.userManager.UpdateAsync(user);

            return user;
        }

        public IQueryable<User> SelectAllUsers() => this.userManager.Users;

        public IQueryable<User> SelectAllUsers(bool isActive) =>
            this.userManager.Users.Where(x => x.IsActive == isActive);

        public async ValueTask<User> SelectUserByIdAsync(Guid id) =>
            await this.userManager.Users.FirstOrDefaultAsync(x => x.Id == id);
    }
}
