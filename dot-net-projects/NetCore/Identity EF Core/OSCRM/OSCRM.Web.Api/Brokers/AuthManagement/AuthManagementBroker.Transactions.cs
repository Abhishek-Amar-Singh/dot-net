﻿using Microsoft.EntityFrameworkCore.Storage;

namespace OSCRM.Web.Api.Brokers.AuthManagement
{
    public partial class AuthManagementBroker
    {
        /*
        public async ValueTask BeginTransactionAsync() =>
            await this.storageBroker.Database.BeginTransactionAsync();//--this method automatically commits/rollback the transaction
        */

        /*====
         If you need to return the IDbContextTransaction object representing the transaction, allowing you to have more control 
         over the transaction (e.g., committing or rolling back explicitly)
        =====*/
        public async ValueTask<IDbContextTransaction> BeginTransactionAsync() =>
            await this.storageBroker.Database.BeginTransactionAsync();
    }
}
