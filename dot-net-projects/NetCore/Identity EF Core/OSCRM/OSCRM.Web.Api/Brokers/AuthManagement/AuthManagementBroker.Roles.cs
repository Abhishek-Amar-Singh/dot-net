﻿using Microsoft.EntityFrameworkCore;
using OSCRM.Web.Api.Models.Roles;

namespace OSCRM.Web.Api.Brokers.AuthManagement
{
    public partial class AuthManagementBroker
    {
        public async ValueTask<Role> InsertRoleAsync(Role role)
        {
            await this.roleManager.CreateAsync(role);

            return role;
        }

        public IQueryable<Role> SelectAllRoles() => this.roleManager.Roles;

        public IQueryable<Role> SelectAllRoles(bool isActive) =>
            this.roleManager.Roles.Where(x => x.IsActive == isActive);

        public async ValueTask<Role> SelectRoleByNameAsync(string roleName) =>
            await this.roleManager.FindByNameAsync(roleName);

        public async ValueTask<Role> SelectRoleByIdAsync(Guid roleId) =>
            await this.roleManager.Roles.FirstOrDefaultAsync(x => x.Id == roleId);

        public async ValueTask<Role> UpdateRoleAsync(Role role)
        {
            await this.roleManager.UpdateAsync(role);

            return role;
        }
        
        public async ValueTask<Role> DeleteRoleAsync(Role role)
        {
            await this.roleManager.DeleteAsync(role);

            return role;
        }

        public async ValueTask<Role> UpdateRoleStatusAsync(Role role)
        {
            await this.roleManager.UpdateAsync(role);

            return role;
        }
    }
}
