﻿using OSCRM.Web.Api.Models.Roles;

namespace OSCRM.Web.Api.Brokers.AuthManagement
{
    public partial interface IAuthManagementBroker
    {
        ValueTask<Role> InsertRoleAsync(Role role);
        IQueryable<Role> SelectAllRoles();
        IQueryable<Role> SelectAllRoles(bool isActive);
        ValueTask<Role> SelectRoleByNameAsync(string roleName);
        ValueTask<Role> SelectRoleByIdAsync(Guid roleId);
        ValueTask<Role> UpdateRoleAsync(Role role);
        ValueTask<Role> DeleteRoleAsync(Role role);
        ValueTask<Role> UpdateRoleStatusAsync(Role role);
    }
}
