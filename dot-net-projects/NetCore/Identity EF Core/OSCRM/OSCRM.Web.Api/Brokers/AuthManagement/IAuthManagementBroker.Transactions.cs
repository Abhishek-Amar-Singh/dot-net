﻿using Microsoft.EntityFrameworkCore.Storage;

namespace OSCRM.Web.Api.Brokers.AuthManagement
{
    public partial interface IAuthManagementBroker
    {
        ValueTask<IDbContextTransaction> BeginTransactionAsync();
    }
}
