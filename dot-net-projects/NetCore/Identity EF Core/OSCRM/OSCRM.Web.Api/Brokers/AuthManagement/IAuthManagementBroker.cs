﻿using Microsoft.AspNetCore.Identity;
using OSCRM.Web.Api.Models.Users;

namespace OSCRM.Web.Api.Brokers.AuthManagement
{
    public partial interface IAuthManagementBroker
    {
        ValueTask<string> GenerateEmailConfirmationTokenAsync(User user);
        ValueTask<IdentityResult> ConfirmEmailAsync(User user, string token);
        ValueTask<bool> CheckPasswordAsync(User user, string password);
        ValueTask<SignInResult> PasswordSignInUserAsync(string email, string password);
        ValueTask<IList<string>> GetRolesAsync(User user);
    }
}
