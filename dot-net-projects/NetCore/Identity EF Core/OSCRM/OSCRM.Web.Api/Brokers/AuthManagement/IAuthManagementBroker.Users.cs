﻿using OSCRM.Web.Api.Models.Roles;
using OSCRM.Web.Api.Models.Users;
using OSCRM.Web.Api.Models.Users.Vms;

namespace OSCRM.Web.Api.Brokers.AuthManagement
{
    public partial interface IAuthManagementBroker
    {
        ValueTask<User> InsertUserAsync(User user);
        ValueTask<User> SelectUserByEmailAsync(string email);
        ValueTask<UserRespectiveRole> InsertUserToRoleAsync(User user, Role role);
        ValueTask<User> UpdateUserAsync(User user);
        IQueryable<User> SelectAllUsers();
        IQueryable<User> SelectAllUsers(bool isActive);
        ValueTask<User> SelectUserByIdAsync(Guid id);
    }
}
