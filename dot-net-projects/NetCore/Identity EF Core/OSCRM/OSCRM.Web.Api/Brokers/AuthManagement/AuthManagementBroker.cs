﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Storage;
using OSCRM.Web.Api.Brokers.Storages;
using OSCRM.Web.Api.Models.Roles;
using OSCRM.Web.Api.Models.Users;

namespace OSCRM.Web.Api.Brokers.AuthManagement
{
    public partial class AuthManagementBroker : IAuthManagementBroker
    {
        private readonly UserManager<User> userManager;
        private readonly RoleManager<Role> roleManager;
        private readonly StorageBroker storageBroker;
        private readonly SignInManager<User> signInManager;

        public AuthManagementBroker(
            UserManager<User> userManager,
            RoleManager<Role> roleManager,
            StorageBroker storageBroker,
            SignInManager<User> signInManager)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.storageBroker = storageBroker;
            this.signInManager = signInManager;
        }

        public async ValueTask<string> GenerateEmailConfirmationTokenAsync(User user) =>
            await this.userManager.GenerateEmailConfirmationTokenAsync(user);

        public async ValueTask<IdentityResult> ConfirmEmailAsync(User user, string token) =>
            await this.userManager.ConfirmEmailAsync(user, token);

        public async ValueTask<bool> CheckPasswordAsync(User user, string password) =>
            await this.userManager.CheckPasswordAsync(user, password);

        public async ValueTask<SignInResult> PasswordSignInUserAsync(string email, string password) =>
            await this.signInManager.PasswordSignInAsync(email, password, false, false);

        public async ValueTask<IList<string>> GetRolesAsync(User user) =>
            await this.userManager.GetRolesAsync(user);

    }
}
