﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using OSCRM.Web.Api.Models.Roles;
using OSCRM.Web.Api.Models.Users;
using System;
using System.Reflection.Emit;

namespace OSCRM.Web.Api.Brokers.Storages
{
    public partial class StorageBroker : IdentityDbContext<User, Role, Guid>, IStorageBroker
    {
        private readonly IConfiguration configuration;

        public StorageBroker(IConfiguration configuration)
        {
            this.configuration = configuration;
            this.Database.Migrate();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            var localDbConnectionStr = this.configuration.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(localDbConnectionStr);
        }
    }
}
