﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace OSCRM.Web.Api.Models.Users
{
    public class User : IdentityUser<Guid>
    {
        public override Guid Id { get => base.Id; set => base.Id = value; }


        [Column(TypeName = "varchar(50)")]
        public string FirstName { get; set; } = null!;

        [Column(TypeName = "varchar(50)")]
        public string? MiddleName { get; set; }

        [Column(TypeName = "varchar(50)")]
        public string LastName { get; set; } = null!;

        [Column(TypeName = "date")]
        public DateTime DOB { get; set; }
        public int GenderId { get; set; }
        public int DistrictId { get; set; }
        public int CityId { get; set; }
        public int StateId { get; set; }
        public int NationId { get; set; }
        [Column(TypeName = "varchar(6)")]
        public string? Pincode { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string? Address { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string MobileNumber { get; set; } = null!;
        public bool MobileNumberConfirmed { get; set; }
        public bool IsActive { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTimeOffset? LastModified { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTimeOffset? DeletedOn { get; set; }
        public Guid? DeletedBy { get; set; }
        [NotMapped]
        public string Password { get; set; } = null!;
        [NotMapped]
        public string ConfirmPassword { get; set; } = null!;
        [NotMapped]
        public Guid RoleId { get; set; }

        public string? RefreshToken { get; set; }
        public DateTime RefreshTokenExpiryTime { get; set; }
    }
}
