﻿namespace OSCRM.Web.Api.Models.Users.Exceptions
{
    public class FailedUserStorageException : Exception
    {
        public FailedUserStorageException(Exception innerException)
            : base(message: "Failed post storage error occurred, contact support.", innerException)
        { }
    }
}
