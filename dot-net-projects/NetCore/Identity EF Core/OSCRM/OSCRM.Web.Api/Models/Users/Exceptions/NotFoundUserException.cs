﻿
namespace OSCRM.Web.Api.Models.Users.Exceptions
{
    public class NotFoundUserException : Exception
    {
        public NotFoundUserException(Guid userId)
            : base(message: $"Couldn't find user with id: {userId}.") { }

        public NotFoundUserException(string emailAddress)
            : base(message: $"Couldn't find user with username or email address: {emailAddress}.") { }
    }
}
