﻿
namespace OSCRM.Web.Api.Models.Users.Exceptions
{
    public class NullUserException : Exception
    {
        public NullUserException() : base(message: "The user is null.") { }
    }
}
