﻿
using Newtonsoft.Json;
using System.Collections.Generic;

namespace OSCRM.Web.Api.Models.Users.Exceptions
{
    public class InvalidUserException : Exception
    {
        //private IDictionary<string, IList<string>> dictErrors = new Dictionary<string, IList<string>>();

        public InvalidUserException(string parameterName, object parameterValue)
            : base(message: $"Invalid user, " +
                  $"parameter name: {parameterName}, " +
                  $"parameter value: {parameterValue}.")
        { }

        public InvalidUserException()
            : base(message: "Invalid user. Please fix the errors and try again.") { }


        //public void MaintainErrorList(string key, string value)
        //{
        //    if (dictErrors.ContainsKey(key))
        //    {
        //        dictErrors[key].Add(value);
        //    }
        //    else dictErrors[key] = new List<string> { value };
        //}

        //public void ThrowIfContainsErrors()
        //{
        //    var json = JsonConvert.SerializeObject(dictErrors);
        //    if (dictErrors.Count > 0)
        //    {
        //        throw new InvalidUserException(json);
        //    }
        //}
    }
}
