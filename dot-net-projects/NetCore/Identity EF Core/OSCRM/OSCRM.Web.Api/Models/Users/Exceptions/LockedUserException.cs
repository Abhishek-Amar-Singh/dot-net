﻿
namespace OSCRM.Web.Api.Models.Users.Exceptions
{
    public class LockedUserException : Exception
    {
        public LockedUserException(Exception innerException)
            : base(message: "Locked user record exception, please try again later.", innerException) { }
    }
}
