﻿namespace OSCRM.Web.Api.Models.Users.Exceptions
{
    public class UnchangedUserException : Exception
    {
        public UnchangedUserException():base(message: "The user is unchanged.")
        {
            
        }
    }
    public class UnchangedUserStatusException : Exception
    {
        public UnchangedUserStatusException(string status) : base(message: $"The user is already {status}.")
        {

        }
    }
}
