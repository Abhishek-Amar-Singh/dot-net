﻿namespace OSCRM.Web.Api.Models.Users.Exceptions
{
    public class UserDependencyValidationException : Exception
    {
        public UserDependencyValidationException(Exception innerException)
            : base(message: "User dependency validation error occurred, please try again.", innerException)
        { }
    }
}
