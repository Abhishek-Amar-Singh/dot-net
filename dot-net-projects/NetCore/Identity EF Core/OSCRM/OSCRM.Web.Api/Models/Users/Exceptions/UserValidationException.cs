﻿
namespace OSCRM.Web.Api.Models.Users.Exceptions
{
    public class UserValidationException : Exception
    {
        public UserValidationException(Exception innerException)
            : base(message: "Invalid input, contact support.", innerException) { }
    }
}
