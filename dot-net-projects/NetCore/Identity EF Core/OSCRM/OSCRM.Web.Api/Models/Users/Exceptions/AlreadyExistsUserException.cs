﻿namespace OSCRM.Web.Api.Models.Users.Exceptions
{
    public class AlreadyExistsUserException : Exception
    {
        public AlreadyExistsUserException(User user)
            : base(message: $"User having email={user.Email} is already exists.") { }


        public AlreadyExistsUserException(User user, Guid roleId)
            : base(message: $"User having email={user.Email} with roleId={roleId} is already exists.") { }

    }
}
