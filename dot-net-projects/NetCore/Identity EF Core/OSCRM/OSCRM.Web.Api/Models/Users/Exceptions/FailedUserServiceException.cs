﻿
namespace OSCRM.Web.Api.Models.Users.Exceptions
{
    public class FailedUserServiceException : Exception
    {
        public FailedUserServiceException(Exception innerException)
            : base(message: "Failed user service error occurred, contact support.", innerException)
        { }
    }
}
