﻿namespace OSCRM.Web.Api.Models.Users.Dtos
{
    public class RegisterUser
    {
        public string FirstName { get; set; } = string.Empty;
        public string? MiddleName { get; set; }
        public string LastName { get; set;} = string.Empty;
        public string? Email { get; set; }
        public string? Password { get; set; }
        public string? ConfirmPassword { get; set; }
        public string? PhoneNumber { get; set; }
        public DateOnly DOB { get; set; }
        public int DistrictId { get; set; }
        public int CityId { get; set; }
        public int StateId { get; set; }
        public int NationId { get; set; }
        public string? Address { get; set; }
        public string? Pincode { get; set; }

    }
}
