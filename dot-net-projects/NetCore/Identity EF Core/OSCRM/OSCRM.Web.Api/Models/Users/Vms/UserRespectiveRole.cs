﻿namespace OSCRM.Web.Api.Models.Users.Vms
{
    public class UserRespectiveRole
    {
        public string FullName { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string RoleName { get; set; } = null!;
    }
}
