﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;

namespace OSCRM.Web.Api.Models.Home.Dtos
{
    public class RegisterUser
    {
        public string FirstName { get; set; } = string.Empty;
        public string? MiddleName { get; set; }
        public string LastName { get; set; } = string.Empty;
        public string EmailAddress { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public string ConfirmPassword { get; set; } = string.Empty;
        public DateTime DOB { get; set; }
        public int GenderId { get; set; }
        public int DistrictId { get; set; }
        public int CityId { get; set; }
        public int StateId { get; set; }
        public int NationId { get; set; }
        public string? Pincode { get; set; }
        public string? Address { get; set; }
        public string MobileNumber { get; set; } = string.Empty;
        public string? PhoneNumber { get; set; }
        public Guid RoleId { get; set; }
    }
}
