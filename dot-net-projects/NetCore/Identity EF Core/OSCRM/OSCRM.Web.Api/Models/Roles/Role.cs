﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace OSCRM.Web.Api.Models.Roles
{
    public class Role : IdentityRole<Guid>
    {
        [Column(TypeName = "varchar(max)")]
        public string? Description { get; set; }
        public bool IsActive { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTimeOffset? LastModified { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTimeOffset? DeletedOn { get; set; }
        public Guid? DeletedBy { get; set; }
    }
}
