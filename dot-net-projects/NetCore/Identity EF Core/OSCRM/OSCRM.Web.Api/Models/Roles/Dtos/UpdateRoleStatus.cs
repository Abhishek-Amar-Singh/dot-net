﻿namespace OSCRM.Web.Api.Models.Roles.Dtos
{
    public class UpdateRoleStatus
    {
        public Guid RoleId { get; set; }
        public bool IsActive { get; set; }
        public Guid UserId { get; set; }
    }
}
