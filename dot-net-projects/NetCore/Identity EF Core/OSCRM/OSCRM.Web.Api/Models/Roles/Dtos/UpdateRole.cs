﻿namespace OSCRM.Web.Api.Models.Roles.Dtos
{
    public class UpdateRole
    {
        public Guid Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public bool IsActive { get; set; }
        public Guid UserId { get; set; }
    }
}
