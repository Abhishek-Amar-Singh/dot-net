﻿namespace OSCRM.Web.Api.Models.Roles.Dtos
{
    public class CreateRole
    {
        public string Name { get; set; } = string.Empty;
        public string? Description { get; set; }
        public Guid UserId { get; set; }
    }
}
