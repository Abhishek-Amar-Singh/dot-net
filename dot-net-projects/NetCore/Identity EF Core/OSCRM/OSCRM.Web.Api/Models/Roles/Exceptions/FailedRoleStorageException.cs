﻿namespace OSCRM.Web.Api.Models.Roles.Exceptions
{
    public class FailedRoleStorageException : Exception
    {
        public FailedRoleStorageException(Exception innerException)
            : base(message: "Failed post storage error occurred, contact support.", innerException)
        { }
    }
}
