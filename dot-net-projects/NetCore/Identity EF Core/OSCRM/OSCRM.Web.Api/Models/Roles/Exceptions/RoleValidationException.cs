﻿
namespace OSCRM.Web.Api.Models.Roles.Exceptions
{
    public class RoleValidationException : Exception
    {
        public RoleValidationException(Exception innerException)
            : base(message: "Invalid input, contact support.", innerException) { }
    }
}
