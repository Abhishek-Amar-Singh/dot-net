﻿
using Newtonsoft.Json;
using System.Collections.Generic;

namespace OSCRM.Web.Api.Models.Roles.Exceptions
{
    public class InvalidRoleException : Exception
    {
        private IDictionary<string, IList<string>> dictErrors = new Dictionary<string, IList<string>>();

        public InvalidRoleException(string parameterName, object parameterValue)
            : base(message: $"Invalid role, " +
                  $"parameter name: {parameterName}, " +
                  $"parameter value: {parameterValue}.")
        { }

        public InvalidRoleException()
            : base(message: "Invalid role. Please fix the errors and try again.") { }


        //public void MaintainErrorList(string key, string value)
        //{
        //    if (dictErrors.ContainsKey(key))
        //    {
        //        dictErrors[key].Add(value);
        //    }
        //    else dictErrors[key] = new List<string> { value };
        //}

        //public void ThrowIfContainsErrors()
        //{
        //    var json = JsonConvert.SerializeObject(dictErrors);
        //    if (dictErrors.Count > 0)
        //    {
        //        throw new InvalidRoleException(json);
        //    }
        //}
    }
}
