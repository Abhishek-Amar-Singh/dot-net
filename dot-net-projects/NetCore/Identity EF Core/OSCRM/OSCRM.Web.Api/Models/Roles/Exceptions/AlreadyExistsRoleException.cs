﻿namespace OSCRM.Web.Api.Models.Roles.Exceptions
{
    public class AlreadyExistsRoleException : Exception
    {
        public AlreadyExistsRoleException(Role role)
            : base(message: $"Role having name={role.Name} is already exists on id={role.Id}.") { }
    }
}
