﻿namespace OSCRM.Web.Api.Models.Roles.Exceptions
{
    public class InactiveRoleException : Exception
    {
        public InactiveRoleException(string roleName):base(message: $"Role with name={roleName} is inactive.") { }
    }
}
