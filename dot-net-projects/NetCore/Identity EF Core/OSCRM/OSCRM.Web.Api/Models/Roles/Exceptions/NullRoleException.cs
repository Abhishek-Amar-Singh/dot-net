﻿
namespace OSCRM.Web.Api.Models.Roles.Exceptions
{
    public class NullRoleException : Exception
    {
        public NullRoleException() : base(message: "The role is null.") { }
    }
}
