﻿
namespace OSCRM.Web.Api.Models.Roles.Exceptions
{
    public class RoleServiceException : Exception
    {
        public RoleServiceException(Exception innerException)
            : base(message: "Service error occurred, contact support.", innerException) { }
    }
}
