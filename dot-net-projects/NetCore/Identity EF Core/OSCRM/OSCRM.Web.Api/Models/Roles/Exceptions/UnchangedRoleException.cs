﻿namespace OSCRM.Web.Api.Models.Roles.Exceptions
{
    public class UnchangedRoleException : Exception
    {
        public UnchangedRoleException():base(message: "The role is unchanged.")
        {
            
        }
    }
    public class UnchangedRoleStatusException : Exception
    {
        public UnchangedRoleStatusException(string status) : base(message: $"The role is already {status}.")
        {

        }
    }
}
