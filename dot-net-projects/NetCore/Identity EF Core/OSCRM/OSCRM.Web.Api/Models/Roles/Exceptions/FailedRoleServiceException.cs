﻿
namespace OSCRM.Web.Api.Models.Roles.Exceptions
{
    public class FailedRoleServiceException : Exception
    {
        public FailedRoleServiceException(Exception innerException)
            : base(message: "Failed role service error occurred, contact support.", innerException)
        { }
    }
}
