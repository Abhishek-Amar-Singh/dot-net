﻿
namespace OSCRM.Web.Api.Models.Roles.Exceptions
{
    public class RoleDependencyException : Exception
    {
        public RoleDependencyException(Exception innerException)
            : base(message: "Service dependency error occurred, contact support.", innerException) { }
    }
}
