﻿
namespace OSCRM.Web.Api.Models.Roles.Exceptions
{
    public class LockedRoleException : Exception
    {
        public LockedRoleException(Exception innerException)
            : base(message: "Locked role record exception, please try again later.", innerException) { }
    }
}
