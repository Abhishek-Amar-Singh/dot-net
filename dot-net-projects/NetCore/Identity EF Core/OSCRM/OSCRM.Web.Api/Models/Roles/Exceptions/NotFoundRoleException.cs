﻿
namespace OSCRM.Web.Api.Models.Roles.Exceptions
{
    public class NotFoundRoleException : Exception
    {
        public NotFoundRoleException(Guid roleId)
            : base(message: $"Couldn't find role with id: {roleId}.") { }

        public NotFoundRoleException(string roleName)
            : base(message: $"Couldn't find role with name: {roleName}.") { }
    }
}
