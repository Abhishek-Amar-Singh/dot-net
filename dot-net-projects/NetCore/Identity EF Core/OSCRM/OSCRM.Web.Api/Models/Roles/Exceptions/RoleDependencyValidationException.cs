﻿namespace OSCRM.Web.Api.Models.Roles.Exceptions
{
    public class RoleDependencyValidationException : Exception
    {
        public RoleDependencyValidationException(Exception innerException)
            : base(message: "Role dependency validation error occurred, please try again.", innerException)
        { }
    }
}
