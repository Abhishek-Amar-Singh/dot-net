﻿namespace OSCRM.Web.Api.Models.Auth.Exceptions
{
    public class InvalidTokenException : Exception
    {
        public InvalidTokenException():base(message: "Token is not valid.") { }

        public InvalidTokenException(string parameterName, object parameterValue)
            : base(message: $"Invalid token, " +
                  $"parameter name: {parameterName}, " +
                  $"parameter value: {parameterValue}.")
        { }
    }
}
