﻿namespace OSCRM.Web.Api.Models.Auth.Exceptions
{
    public class InvalidClientRequestException : Exception
    {
        public InvalidClientRequestException():base(message: "Invalid Client Request:: Do not generate the token!")
        {
            
        }
    }
}
