﻿
namespace OSCRM.Web.Api.Models.Auth.Exceptions
{
    public class FailedToSignInException : Exception
    {
        public FailedToSignInException(string email):base($"Sign-in failed. Please provide valid credentials for {email} and try again. If your credentials are valid, please note that you cannot sign in without confirming your email address.")
        {
            
        }
    }
}
