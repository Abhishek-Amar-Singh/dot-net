﻿namespace OSCRM.Web.Api.Models.Auth.Exceptions
{
    public class InvalidPasswordException : Exception
    {
        public InvalidPasswordException(string email):base(message: $"Password is invalid for user having email={email}. Hence, cannot generate the token.")
        {
            
        }
    }
}
