﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OSCRM.Web.Api.Migrations.MSSQL
{
    /// <inheritdoc />
    public partial class MyMigration7 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AreFilesUploaded",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "FileUrl",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "FilesUrl",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "IsFileUploaded",
                table: "AspNetUsers");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DOB",
                table: "AspNetUsers",
                type: "date",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "date",
                oldNullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "DOB",
                table: "AspNetUsers",
                type: "date",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.AddColumn<bool>(
                name: "AreFilesUploaded",
                table: "AspNetUsers",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FileUrl",
                table: "AspNetUsers",
                type: "varchar(2000)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FilesUrl",
                table: "AspNetUsers",
                type: "varchar(2000)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsFileUploaded",
                table: "AspNetUsers",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
