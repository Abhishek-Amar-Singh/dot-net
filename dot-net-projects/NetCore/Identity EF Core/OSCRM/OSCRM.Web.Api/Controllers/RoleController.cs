﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using OSCRM.Web.Api.Models.Roles;
using OSCRM.Web.Api.Models.Roles.Dtos;
using OSCRM.Web.Api.Models.Roles.Exceptions;
using OSCRM.Web.Api.Services.Foundations.Roles;
using System.Globalization;

namespace OSCRM.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService roleService;

        public RoleController(IRoleService roleService) => this.roleService = roleService;

        [Route("PostRoleAsync")]
        [HttpPost]
        public async ValueTask<ActionResult<Role>> PostRoleAsync(CreateRole _role)
        {
            try
            {
                var role = new Role
                {
                    Name = ToTitleCase(_role.Name),
                    Description = _role.Description,
                    IsActive = true,
                    CreatedOn = DateTimeOffset.UtcNow,
                    CreatedBy = _role.UserId
                };
                var storageRole = await this.roleService.CreateRoleAsync(role);

                return StatusCode(201, storageRole);
            }
            catch (RoleDependencyValidationException roleDependencyValidationException)
                when (roleDependencyValidationException.InnerException is AlreadyExistsRoleException)
            {
                return StatusCode(409, roleDependencyValidationException.InnerException);
            }
            catch (RoleDependencyValidationException roleDependencyValidationException)
            {
                return StatusCode(400, roleDependencyValidationException);
            }
            catch (RoleValidationException roleValidationException)
                when (roleValidationException.InnerException is InvalidRoleException)
            {
                return StatusCode(400, roleValidationException.InnerException);
            }
            catch (RoleValidationException roleValidationException)
            {
                return StatusCode(400, roleValidationException);
            }
            catch (RoleDependencyException roleDependencyException)
            {
                return StatusCode(500, roleDependencyException);
            }
            catch (RoleServiceException roleServiceException)
            {
                return StatusCode(500, roleServiceException);
            }
        }

        [Route("GetAllRoles")]
        [HttpGet]
        public ActionResult<IQueryable<Role>> GetAllRoles()
        {
            try
            {
                IQueryable<Role> storageRoles = this.roleService.RetrieveAllRoles();

                return Ok(storageRoles);
            }
            catch (RoleDependencyException roleDependencyException)
            {
                return StatusCode(500, roleDependencyException);
            }
            catch (RoleServiceException roleServiceException)
            {
                return StatusCode(500, roleServiceException);
            }
        }

        [Route("GetAllRoles/{isActive:bool}")]
        [HttpGet]
        public ActionResult<IQueryable<Role>> GetAllRoles(bool isActive)
        {
            try
            {
                IQueryable<Role> storageRoles =
                    this.roleService.RetrieveAllRoles(isActive);

                return Ok(storageRoles);
            }
            catch (RoleValidationException roleValidationException)
                when (roleValidationException.InnerException is InvalidRoleException)
            {
                return StatusCode(400, roleValidationException.InnerException);
            }
            catch (RoleValidationException roleValidationException)
            {
                return StatusCode(400, roleValidationException);
            }
            catch (RoleDependencyException roleDependencyException)
            {
                return StatusCode(500, roleDependencyException);
            }
            catch (RoleServiceException roleServiceException)
            {
                return StatusCode(500, roleServiceException);
            }
        }

        [Route("GetRoleAsync/{roleName}")]
        [HttpGet]
        public async ValueTask<ActionResult<Role>> GetRoleAsync(string roleName)
        {
            try
            {
                return await this.roleService.RetrieveRoleByNameAsync(ToTitleCase(roleName));
            }
            catch (RoleValidationException roleValidationException)
                when (roleValidationException.InnerException is InvalidRoleException)
            {
                return StatusCode(400, roleValidationException.InnerException);
            }
            catch (RoleValidationException roleValidationException)
                when (roleValidationException.InnerException is NotFoundRoleException)
            {
                return StatusCode(404, roleValidationException.InnerException);
            }
            catch (RoleValidationException roleValidationException)
            {
                return StatusCode(400, roleValidationException);
            }
            catch (RoleDependencyException roleDependencyException)
            {
                return StatusCode(500, roleDependencyException);//return Problem(roleDependencyException.Message);
            }
            catch (RoleServiceException roleServiceException)
            {
                return StatusCode(500, roleServiceException);//return Problem(studentServiceException.Message);
            }
        }

        [Route("GetRoleAsync/{roleId:Guid}")]
        [HttpGet]
        public async ValueTask<ActionResult<Role>> GetRoleAsync(Guid roleId)
        {
            try
            {
                return await this.roleService.RetrieveRoleByIdAsync(roleId);
            }
            catch (RoleValidationException roleValidationException)
                when (roleValidationException.InnerException is InvalidRoleException)
            {
                return StatusCode(400, roleValidationException.InnerException);
            }
            catch (RoleValidationException roleValidationException)
                when (roleValidationException.InnerException is NotFoundRoleException)
            {
                return StatusCode(404, roleValidationException.InnerException);
            }
            catch (RoleValidationException roleValidationException)
            {
                return StatusCode(400, roleValidationException);
            }
            catch (RoleDependencyException roleDependencyException)
            {
                return StatusCode(500, roleDependencyException);//return Problem(roleDependencyException.Message);
            }
            catch (RoleServiceException roleServiceException)
            {
                return StatusCode(500, roleServiceException);//return Problem(studentServiceException.Message);
            }
        }

        [Route("PutRoleAsync")]
        [HttpPut]
        public async ValueTask<ActionResult<Role>> PutRoleAsync(UpdateRole _role)
        {
            try
            {
                var role = new Role
                {
                    Id = _role.Id,
                    Name = (_role.Name is null) ? _role.Name : ToTitleCase(_role.Name),
                    Description = _role.Description,
                    IsActive = _role.IsActive
                };
                var storageRole = await this.roleService.ModifyRoleAsync(role, _role.UserId);

                return StatusCode(200, storageRole);
            }
            catch (RoleValidationException roleValidationException)
                when (roleValidationException.InnerException is NullRoleException)
            {
                return StatusCode(400, roleValidationException.InnerException);
            }
            catch (RoleValidationException roleValidationException)
                when (roleValidationException.InnerException is InvalidRoleException)
            {
                return StatusCode(400, roleValidationException.InnerException);
            }
            catch (RoleValidationException roleValidationException)
                when (roleValidationException.InnerException is NotFoundRoleException)
            {
                return StatusCode(404, roleValidationException.InnerException);
            }
            catch (RoleValidationException roleValidationException)
            {
                return StatusCode(400, roleValidationException);
            }
            catch (RoleDependencyValidationException roleDependencyValidationException)
                when (roleDependencyValidationException.InnerException is UnchangedRoleException)
            {
                return StatusCode(409, roleDependencyValidationException.InnerException);
            }
            catch (RoleDependencyValidationException roleDependencyValidationException)
            {
                return StatusCode(400, roleDependencyValidationException);
            }
            catch (RoleDependencyException roleDependencyException)
            {
                return StatusCode(500, roleDependencyException);
            }
            catch (RoleServiceException roleServiceException)
            {
                return StatusCode(500, roleServiceException);
            }
        }

        [Route("DeleteRoleAsync")]
        [HttpDelete]
        public async ValueTask<ActionResult<Role>> DeleteRoleAsync(Guid roleId)
        {
            try
            {
                return await this.roleService.RemoveRoleAsync(roleId);
            }
            catch (RoleValidationException roleValidationException)
                when (roleValidationException.InnerException is InvalidRoleException)
            {
                return StatusCode(400, roleValidationException.InnerException);
            }
            catch (RoleValidationException roleValidationException)
                when (roleValidationException.InnerException is NotFoundRoleException)
            {
                return StatusCode(404, roleValidationException.InnerException);
            }
            catch (RoleValidationException roleValidationException)
            {
                return StatusCode(400, roleValidationException);
            }
            catch (RoleDependencyException roleDependencyException)
            {
                return StatusCode(500, roleDependencyException);
            }
            catch (FailedRoleServiceException failedRoleServiceException)
            {
                return StatusCode(500, failedRoleServiceException);
            }
        }

        [Route("PutRoleStatusAsync")]
        [HttpPut]
        public async ValueTask<ActionResult<Role>> PutRoleStatusAsync(UpdateRoleStatus roleStatusObj)
        {
            try
            {
                var role = new Role
                {
                    Id = roleStatusObj.RoleId,
                    IsActive = roleStatusObj.IsActive
                };
                var storageRole = await this.roleService.ModifyRoleStatusAsync(role, roleStatusObj.UserId);

                return StatusCode(200, storageRole);
            }
            catch (RoleValidationException roleValidationException)
                when (roleValidationException.InnerException is NullRoleException)
            {
                return StatusCode(400, roleValidationException.InnerException);
            }
            catch (RoleValidationException roleValidationException)
                when (roleValidationException.InnerException is InvalidRoleException)
            {
                return StatusCode(400, roleValidationException.InnerException);
            }
            catch (RoleValidationException roleValidationException)
                when (roleValidationException.InnerException is NotFoundRoleException)
            {
                return StatusCode(404, roleValidationException.InnerException);
            }
            catch (RoleValidationException roleValidationException)
            {
                return StatusCode(400, roleValidationException);
            }
            catch (RoleDependencyValidationException roleDependencyValidationException)
                when (roleDependencyValidationException.InnerException is UnchangedRoleStatusException)
            {
                return StatusCode(409, roleDependencyValidationException.InnerException);
            }
            catch (RoleDependencyValidationException roleDependencyValidationException)
            {
                return StatusCode(400, roleDependencyValidationException);
            }
            catch (RoleDependencyException roleDependencyException)
            {
                return StatusCode(500, roleDependencyException);
            }
            catch (RoleServiceException roleServiceException)
            {
                return StatusCode(500, roleServiceException);
            }
        }

        private string ToTitleCase(string input)
        {
            TextInfo textInfo = CultureInfo.CurrentCulture.TextInfo;
            string[] words = input.Split(' ');

            for (int i = 0; i < words.Length; i++)
            {
                words[i] = textInfo.ToTitleCase(words[i].ToLower());
            }

            return string.Join(" ", words);
        }
    }
}
