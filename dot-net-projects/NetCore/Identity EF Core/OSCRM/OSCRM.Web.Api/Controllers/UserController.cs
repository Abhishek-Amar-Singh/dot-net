﻿using Microsoft.AspNetCore.Mvc;
using OSCRM.Web.Api.Mail.Exceptions;
using OSCRM.Web.Api.Models.Home.Dtos;
using OSCRM.Web.Api.Models.Roles.Exceptions;
using OSCRM.Web.Api.Models.Users;
using OSCRM.Web.Api.Models.Users.Exceptions;
using OSCRM.Web.Api.Services.Foundations.Users;
using System.Globalization;

namespace OSCRM.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;

        public UserController(IUserService userService) => this.userService = userService;

        [Route("PostUserAsync")]
        [HttpPost]
        public async ValueTask<ActionResult<User>> PostUserAsync([FromBody] RegisterUser registerUser)
        {
            try
            {
                User user = new User
                {
                    FirstName = ToTitleCase(registerUser.FirstName),
                    MiddleName = registerUser.MiddleName is not null ? ToTitleCase(registerUser.MiddleName) : registerUser.MiddleName,
                    LastName = ToTitleCase(registerUser.LastName),
                    Email = ToLowerCase(registerUser.EmailAddress),
                    UserName = ToLowerCase(registerUser.EmailAddress),
                    Password = registerUser.Password,
                    ConfirmPassword = registerUser.ConfirmPassword,
                    DOB = registerUser.DOB,
                    GenderId = registerUser.GenderId,
                    DistrictId = registerUser.DistrictId,
                    CityId = registerUser.CityId,
                    StateId = registerUser.StateId,
                    NationId = registerUser.NationId,
                    Pincode = registerUser.Pincode,
                    Address = registerUser.Address,
                    MobileNumber = registerUser.MobileNumber,
                    PhoneNumber = registerUser.PhoneNumber,
                    RoleId = registerUser.RoleId,
                    IsActive = true,
                    CreatedOn = DateTimeOffset.UtcNow
                };
                var storageUser = await this.userService.RegisterUserAsync(user);

                return StatusCode(201, storageUser);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is NullUserException)
            {
                return StatusCode(400, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is InvalidUserException)
            {
                return StatusCode(400, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is InvalidRoleException)
            {
                return StatusCode(400, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is NotFoundUserException)
            {
                return StatusCode(404, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is NotFoundRoleException)
            {
                return StatusCode(404, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is InactiveRoleException)
            {
                return StatusCode(403, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
            {
                return StatusCode(400, userValidationException);
            }
            catch (UserDependencyValidationException userDependencyValidationException)
                when (userDependencyValidationException.InnerException is AlreadyExistsUserException)
            {
                return StatusCode(409, userDependencyValidationException.InnerException);
            }
            catch (UserDependencyValidationException userDependencyValidationException)
            {
                return StatusCode(400, userDependencyValidationException);
            }
            catch (UserDependencyException userDependencyException)
                when (userDependencyException.InnerException is FailedMailServiceException)
            {
                return StatusCode(500, userDependencyException.InnerException);
            }
            catch (UserDependencyException userDependencyException)
            {
                return StatusCode(500, userDependencyException);
            }
            catch (UserServiceException userServiceException)
            {
                return StatusCode(500, userServiceException);
            }
        }

        [Route("GetAllUsers")]
        [HttpGet]
        public ActionResult<IQueryable<User>> GetAllUsers()
        {
            try
            {
                IQueryable<User> storageUsers = this.userService.RetrieveAllUsers();

                return StatusCode(200, storageUsers);
            }
            catch (UserDependencyException userDependencyException)
            {
                return StatusCode(500, userDependencyException);
            }
            catch (UserServiceException userServiceException)
            {
                return StatusCode(500, userServiceException);
            }
        }

        [Route("GetAllUsers/{isActive:bool}")]
        [HttpGet]
        public ActionResult<IQueryable<User>> GetAllUsers(bool isActive)
        {
            try
            {
                IQueryable<User> storageUsers =
                    this.userService.RetrieveAllUsers(isActive);

                return Ok(storageUsers);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is InvalidUserException)
            {
                return StatusCode(400, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
            {
                return StatusCode(400, userValidationException);
            }
            catch (UserDependencyException userDependencyException)
            {
                return StatusCode(500, userDependencyException);
            }
            catch (UserServiceException userServiceException)
            {
                return StatusCode(500, userServiceException);
            }
        }

        [Route("GetUserAsync/{email}")]
        [HttpGet]
        public async ValueTask<ActionResult<User>> GetUserAsync(string email)
        {
            try
            {
                var storageUser = await this.userService.RetrieveUserByEmailAsync(ToLowerCase(email));

                return StatusCode(200, storageUser);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is InvalidUserException)
            {
                return StatusCode(400, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is NotFoundUserException)
            {
                return StatusCode(404, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
            {
                return StatusCode(400, userValidationException);
            }
            catch (UserDependencyException userDependencyException)
            {
                return StatusCode(500, userDependencyException);
            }
            catch (UserServiceException userServiceException)
            {
                return StatusCode(500, userServiceException);
            }
        }


        [Route("GetUserAsync/{id:Guid}")]
        [HttpGet]
        public async ValueTask<ActionResult<User>> GetUserAsync(Guid id)
        {
            try
            {
                var storageUser = await this.userService.RetrieveUserByIdAsync(id);

                return StatusCode(200, storageUser);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is InvalidUserException)
            {
                return StatusCode(400, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is NotFoundUserException)
            {
                return StatusCode(404, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
            {
                return StatusCode(400, userValidationException);
            }
            catch (UserDependencyException userDependencyException)
            {
                return StatusCode(500, userDependencyException);
            }
            catch (UserServiceException userServiceException)
            {
                return StatusCode(500, userServiceException);
            }
        }

        private string ToTitleCase(string input) =>
            new CultureInfo("en-US", false).TextInfo.ToTitleCase(input);

        private string ToLowerCase(string input) =>
            new CultureInfo("en-US", false).TextInfo.ToLower(input);
    }
}
