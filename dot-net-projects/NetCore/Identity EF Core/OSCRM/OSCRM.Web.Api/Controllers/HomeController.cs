﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using OSCRM.Web.Api.Mail.Models;
using OSCRM.Web.Api.Mail.Services;
using OSCRM.Web.Api.Models.Home.Dtos;
using OSCRM.Web.Api.Models.Users;

namespace OSCRM.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IMailService mailService;

        public HomeController(IMailService mailService) =>
            this.mailService = mailService;

        [HttpGet]
        [Route("TestConfirmEmail")]
        public ActionResult<string> TestConfirmEmail()
        {

            var message = new Message(
                new MailboxAddress[] {
                    new MailboxAddress("Abhishek Singh", "abhisheksingh.120120@gmail.com"),
                    new MailboxAddress("Team Corbet", "teamcorbet123@gmail.com")
                    //new MailboxAddress("Madhu Singh", "mrs.madhu.b.singh@gmail.com"),
                    //new MailboxAddress("Birendra Singh", "mr.birendra.singh@gmail.com")
                },
            "Test", "Send mail implementation in OSCRM.Web.Api.Mail and consume it into OSCRM.Web.Api (HomeController). This is done in C# 11.0 and .NET Core 6.");

            //--Network connectivity issues: Check your network connectivity to ensure that you have an active internet connection.If you are on a restricted network(e.g., a corporate network), contact your network administrator to check if there are any restrictions or permissions required for outbound connections.
            mailService.SendMail(message);
            return Ok(message.ToString());
        }

        [Authorize(Roles = "Admin")]
        [Route("GetData")]
        [HttpGet]
        public IActionResult GetData()
        {
            try
            {
                return Ok(new[]
                {
                    new { version = 10, name = "C#" },
                    new { version = 6, name = ".NET"}
                });
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }


        [Route("PostUser")]
        [HttpPost]
        public ActionResult<User> PostUser([FromForm] RegisterUser registerUser)
        {
            var a = registerUser;
            return Ok(a);
        }
    }
}
