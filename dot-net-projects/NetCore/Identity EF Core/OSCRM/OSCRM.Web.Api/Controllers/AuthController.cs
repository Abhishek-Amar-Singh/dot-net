﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using OSCRM.Web.Api.Models.Auth.Dtos;
using OSCRM.Web.Api.Models.Auth.Exceptions;
using OSCRM.Web.Api.Models.Users.Exceptions;
using OSCRM.Web.Api.Services.Foundations.Auth;

namespace OSCRM.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService authService;

        public AuthController(IAuthService authService) => this.authService = authService;

        [Route("VerifyEmailAsync")]
        [HttpPut]
        public async ValueTask<ActionResult<string>> VerifyEmailAsync(string email, string token)
        {
            try
            {
                var response = await this.authService.ConfirmEmailAsync(email, token);

                return StatusCode(200, response);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is InvalidUserException)
            {

                return StatusCode(400, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is InvalidTokenException)
            {
                return StatusCode(400, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is NotFoundUserException)
            {
                return StatusCode(404, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
            {
                return StatusCode(400, userValidationException);
            }
            catch (UserDependencyException userDependencyException)
            {
                return StatusCode(500, userDependencyException);
            }
            catch (UserServiceException userServiceException)
            {
                return StatusCode(500, userServiceException);
            }
        }

        [Route("LoginUserAsync")]
        [HttpPost]
        public async ValueTask<ActionResult<string>> LoginUserAsync([FromBody] LoginUser loginUser)
        {
            try
            {
                var response = await this.authService.LoginUserAsync(loginUser.EmailAddress, loginUser.Password);

                return StatusCode(200, response);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is NotFoundUserException)
            {
                return StatusCode(404, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is InvalidPasswordException)
            {
                return StatusCode(401, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is FailedToSignInException)
            {
                return StatusCode(401, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
            {
                return StatusCode(400, userValidationException);
            }
            catch (UserDependencyException userDependencyException)
            {
                return StatusCode(500, userDependencyException);
            }
            catch (UserServiceException userServiceException)
            {
                return StatusCode(500, userServiceException);
            }
        }

        [Route("RefreshAsync")]
        [HttpPost]
        public async ValueTask<ActionResult<string>> RefreshAsync(TokenApiModel tokenApiModel)
        {
            try
            {
                var response = await this.authService.RefreshAsync(tokenApiModel.AccessToken, tokenApiModel.RefreshToken);

                return StatusCode(200, response);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is InvalidTokenException)
            {
                return StatusCode(400, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is SecurityTokenException)
            {
                return StatusCode(400, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is InvalidClientRequestException)
            {
                return StatusCode(400, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
            {
                return StatusCode(400, userValidationException);
            }
            catch (UserDependencyException userDependencyException)
            {
                return StatusCode(500, userDependencyException);
            }
            catch (UserServiceException userServiceException)
            {
                return StatusCode(500, userServiceException);
            }
        }

        [Route("RevokeAsync")]
        [HttpPost, Authorize]
        public async ValueTask<ActionResult<string>> RevokeAsync()
        {
            try
            {
                var response = await this.authService.RevokeAsync(User.Identity.Name);

                return StatusCode(200, response);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is InvalidUserException)
            {
                return StatusCode(400, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
                when (userValidationException.InnerException is NotFoundUserException)
            {
                return StatusCode(404, userValidationException.InnerException);
            }
            catch (UserValidationException userValidationException)
            {
                return StatusCode(400, userValidationException);
            }
            catch (UserDependencyException userDependencyException)
            {
                return StatusCode(500, userDependencyException);
            }
            catch (UserServiceException userServiceException)
            {
                return StatusCode(500, userServiceException);
            }
        }
    }
}
