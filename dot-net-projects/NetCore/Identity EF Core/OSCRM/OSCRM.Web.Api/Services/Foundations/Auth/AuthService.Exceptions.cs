﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using OSCRM.Web.Api.Models.Auth.Exceptions;
using OSCRM.Web.Api.Models.Users.Exceptions;

namespace OSCRM.Web.Api.Services.Foundations.Auth
{
    public partial class AuthService
    {
        private delegate ValueTask<T> ReturningStringFunction<T>();

        private async ValueTask<T> TryCatch<T>(ReturningStringFunction<T> returningStringFunction)
        {
            try
            {
                return await returningStringFunction();
            }
            catch (InvalidUserException invalidUserException)
            {
                throw CreateAndLogValidationException(invalidUserException);
            }
            catch (InvalidTokenException invalidTokenException)
            {
                throw CreateAndLogValidationException(invalidTokenException);
            }
            catch (InvalidPasswordException invalidPasswordException)
            {
                throw CreateAndLogValidationException(invalidPasswordException);
            }
            catch (FailedToSignInException failedToSignInException)
            {
                throw CreateAndLogValidationException(failedToSignInException);
            }
            catch (NotFoundUserException notFoundUserException)
            {
                throw CreateAndLogValidationException(notFoundUserException);
            }
            catch (SecurityTokenException securityTokenException)
            {
                throw CreateAndLogValidationException(securityTokenException);
            }
            catch (InvalidClientRequestException invalidClientRequestException)
            {
                throw CreateAndLogValidationException(invalidClientRequestException);
            }
            catch (SqlException sqlException)
            {
                var failedUserStorageException =
                    new FailedUserStorageException(sqlException);

                throw CreateAndLogCriticalDependencyException(failedUserStorageException);
            }
            catch (Exception exception)
            {
                var failedUserServiceException =
                    new FailedUserServiceException(exception);

                throw CreateAndLogServiceException(failedUserServiceException);
            }
        }

        private UserValidationException CreateAndLogValidationException(Exception exception)
        {
            UserValidationException userValidationException = new UserValidationException(exception);
            this.loggingBroker.LogError(userValidationException);

            return userValidationException;
        }

        private UserDependencyException CreateAndLogDependencyException(Exception exception)
        {
            var userDependencyException = new UserDependencyException(exception);
            this.loggingBroker.LogError(userDependencyException);

            return userDependencyException;
        }

        private UserServiceException CreateAndLogServiceException(Exception exception)
        {
            var userServiceException = new UserServiceException(exception);
            this.loggingBroker.LogError(userServiceException);

            return userServiceException;
        }

        private UserDependencyException CreateAndLogCriticalDependencyException(Exception exception)
        {
            var userDependencyException = new UserDependencyException(exception);
            this.loggingBroker.LogCritical(userDependencyException);

            return userDependencyException;
        }


    }
}
