﻿using Microsoft.AspNetCore.Identity;
using OSCRM.Web.Api.Models.Auth.Exceptions;
using OSCRM.Web.Api.Models.Users;
using OSCRM.Web.Api.Models.Users.Exceptions;
using System.Data;
using System.Text.RegularExpressions;

namespace OSCRM.Web.Api.Services.Foundations.Auth
{
    public partial class AuthService
    {
        private void ValidateEmailAndToken(string email, string token)
        {
            Validate
            (
                (Rule: IsInvalidX(email), Parameter: nameof(User.Email)),
                (Rule: IsInvalidEmail(email), Parameter: nameof(User.Email))
            );

            ValidateToken(token);
        }

        private dynamic IsInvalidX(string text) => new
        {
            Condition = String.IsNullOrWhiteSpace(text) || text == string.Empty,
            Message = "Text is required"
        };

        private dynamic IsInvalidY(string? token) => new
        {
            Condition = String.IsNullOrWhiteSpace(token) || token == string.Empty || token == default || token is null,
            Message = "Token is empty or null"
        };

        private dynamic IsInvalidEmail(string email) => new
        {
            Condition = !Regex.IsMatch(email, @"^[A-Za-z][A-Za-z0-9._-]+@[A-Za-z][A-Za-z0-9-_]*[A-Za-z0-9]+(\.[A-Za-z]{2,}){1,2}$"),
            Message = "Email format is not correct"
        };

        private dynamic IsInvalidPassword(string password) => new
        {
            Condition = !Regex.IsMatch(password, @"^(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$"),
            Message = "Invalid Password.\nTry to have a password that satisfies the below given points:\n(a) contains at least one special symbol\n(b) contains at least one lowercase alphabet\n(c)contains at least one uppercase alphabet\n(d) contains at least one digit (e) must be at least 8 characters long"
        };

        private void Validate(params (dynamic Rule, string Parameter)[] validations)
        {
            foreach ((dynamic rule, string parameter) in validations)
            {
                if (rule.Condition)
                {
                    throw new InvalidUserException(parameter, rule.Message);
                }
            }
        }

        private void ValidateToken(string token)
        {
            dynamic rule = IsInvalidY(token);

            if (rule.Condition)
            {
                throw new InvalidTokenException("Token", rule.Message);
            }
        }

        private void ValidateRefreshToken(string refreshToken)
        {
            dynamic rule = IsInvalidY(refreshToken);

            if (rule.Condition)
            {
                throw new InvalidTokenException("RefreshToken", rule.Message);
            }
        }

        private void ValidateUserNotFound(User user, string email)
        {
            if (user is null)
            {
                throw new NotFoundUserException(email);
            }
        }

        private void ValidateEmailConfirmed(IdentityResult response)
        {
            if (!response.Succeeded)
            {
                throw new InvalidTokenException();
            }
        }

        private void ValidatePassword(string email, bool pwdStatus)
        {
            if (!pwdStatus)
            {
                throw new InvalidPasswordException(email);
            }
        }

        private void ValidateSignInResult(string email, SignInResult signInResult)
        {
            if (!signInResult.Succeeded)
            {
                throw new FailedToSignInException(email);
            }
        }

        private void ValidateEmailAndPassword(string email, string password)
        {
            Validate
            (
                (Rule: IsInvalidX(email), Parameter: nameof(User.Email)),
                (Rule: IsInvalidEmail(email), Parameter: nameof(User.Email)),
                (Rule: IsInvalidX(password), Parameter: nameof(User.Password)),
                (Rule: IsInvalidPassword(password), Parameter: nameof(User.Password))
            );
        }

        private void ValidateEmail(string email)
        {
            Validate
            (
                (Rule: IsInvalidX(email), Parameter: nameof(User.Email)),
                (Rule: IsInvalidEmail(email), Parameter: nameof(User.Email))
            );
        }
    }
}
