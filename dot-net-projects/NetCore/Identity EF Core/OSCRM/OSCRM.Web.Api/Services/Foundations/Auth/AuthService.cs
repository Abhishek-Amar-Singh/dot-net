﻿using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OSCRM.Web.Api.Brokers.AuthManagement;
using OSCRM.Web.Api.Brokers.DateTimes;
using OSCRM.Web.Api.Brokers.Loggings;
using OSCRM.Web.Api.Models.Auth.Exceptions;
using OSCRM.Web.Api.Models.Users.Exceptions;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace OSCRM.Web.Api.Services.Foundations.Auth
{
    public partial class AuthService : IAuthService
    {
        private readonly IAuthManagementBroker authManagementBroker;
        private readonly IDateTimeBroker dateTimeBroker;
        private readonly ILoggingBroker loggingBroker;
        private readonly IConfiguration configuration;

        public AuthService(
            IAuthManagementBroker authManagementBroker,
            IDateTimeBroker dateTimeBroker,
            ILoggingBroker loggingBroker,
            IConfiguration configuration)
        {
            this.authManagementBroker = authManagementBroker;
            this.dateTimeBroker = dateTimeBroker;
            this.loggingBroker = loggingBroker;
            this.configuration = configuration;
        }

        public ValueTask<string> ConfirmEmailAsync(string email, string token) =>
        TryCatch(async () =>
        {
            ValidateEmailAndToken(email, token);

            var storageUser = await this.authManagementBroker.SelectUserByEmailAsync(email);
            ValidateUserNotFound(storageUser, email);

            var response = await this.authManagementBroker.ConfirmEmailAsync(storageUser, token);
            ValidateEmailConfirmed(response);

            return "Success:: Your email address is verified!";
        });

        public ValueTask<string> LoginUserAsync(string email, string password) =>
        TryCatch(async () =>
        {
            ValidateEmailAndPassword(email, password);

            var storageUser = await this.authManagementBroker.SelectUserByEmailAsync(email);
            ValidateUserNotFound(storageUser, email);

            //--check the password
            var pwdStatus = await this.authManagementBroker.CheckPasswordAsync(storageUser, password);
            ValidatePassword(email, pwdStatus);

            //--continue to signIn
            var signInResult = await this.authManagementBroker.PasswordSignInUserAsync(email, password);
            ValidateSignInResult(email, signInResult);

            //--claimList creation
            var authClaims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, storageUser.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };
            //--we add roles to the list
            var userRoles = await this.authManagementBroker.GetRolesAsync(storageUser);
            foreach (var role in userRoles)
            {
                authClaims.Add(new Claim(ClaimTypes.Role, role));
            }
            /*
            //--Before refresh token
            //--generate the token with the claims
            var jwtToken = GenerateToken(authClaims);
            //--returniung the token
            var tokenDetails = new
            {
                token = new JwtSecurityTokenHandler().WriteToken(jwtToken),
                expiration = jwtToken.ValidTo
            };
            string userJwtSecurityTokenHandler = JsonConvert.SerializeObject(
                new { Token = tokenDetails.token, ValidTo = tokenDetails.expiration,
                    Username = storageUser.UserName, UserId = storageUser.Id, Email = storageUser.Email });
            
            return userJwtSecurityTokenHandler;
            */
            
            //--generate the token with the claims
            var jwtToken = GenerateToken(authClaims);

            //--generate refreshToken
            var refreshedToken = GenerateRefreshToken();
            storageUser.RefreshToken = refreshedToken;
            storageUser.RefreshTokenExpiryTime = DateTime.Now.AddMinutes(5);
            storageUser = await this.authManagementBroker.UpdateUserAsync(storageUser);

            //--returniung the token
            var tokenDetails = new
            {
                token = new JwtSecurityTokenHandler().WriteToken(jwtToken),
                tokenValidTo = jwtToken.ValidTo,
                refreshToken = refreshedToken
            };
            string userJwtSecurityTokenHandler = JsonConvert.SerializeObject(new { Token = tokenDetails.token, RefreshToken = tokenDetails.refreshToken, Username = storageUser.UserName, UserId = storageUser.Id, Email = storageUser.Email });
            
            return userJwtSecurityTokenHandler;
            
        });

        public ValueTask<string> RefreshAsync(string accessToken, string refreshToken) =>
        TryCatch(async () =>
        {
            ValidateToken(accessToken);
            ValidateRefreshToken(refreshToken);

            var principal = GetPrincipalFromExpiredToken(accessToken);

            var email = principal.Identity.Name;
            ValidateEmail(email);

            var storageUser = await this.authManagementBroker.SelectUserByEmailAsync(email);
            if (storageUser is null || storageUser.RefreshToken != refreshToken)// || user.RefreshTokenExpiryTime <= DateTime.Now )
            {
                throw new InvalidClientRequestException();
            }
            else
            {
                var newAccessToken = GenerateToken(principal.Claims);
                var newRefreshToken = GenerateRefreshToken();
                storageUser.RefreshToken = newRefreshToken;
                await this.authManagementBroker.UpdateUserAsync(storageUser);
                var userJwtSecurityTokenHandler = JsonConvert.SerializeObject(new { Token = new JwtSecurityTokenHandler().WriteToken(newAccessToken), RefreshToken = newRefreshToken, Username = storageUser.UserName, UserId = storageUser.Id, Email = storageUser.Email });

                return userJwtSecurityTokenHandler;
            }
        });

        public ValueTask<string> RevokeAsync(string email) =>
        TryCatch(async () =>
        {
            ValidateEmail(email);

            //--revoke token is used to remove token from database
            var storageUser = await this.authManagementBroker.SelectUserByEmailAsync(email);
            ValidateUserNotFound(storageUser, email);

            storageUser.RefreshToken = null;
            await this.authManagementBroker.UpdateUserAsync(storageUser);

            return "Success:: Token revoked!";
        });

        private string GenerateRefreshToken()
        {
            var randomNum = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNum);
                return Convert.ToBase64String(randomNum);
            }
        }

        private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = configuration["JWT:ValidIssuer"],
                ValidateAudience = true,
                ValidAudience = configuration["JWT:ValidAudience"],
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"])),
                ValidateLifetime = false
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken is null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
            {
                throw new SecurityTokenException("Invalid Token!");
            }
            else
            {
                return principal;
            }
        }

        private JwtSecurityToken GenerateToken(IEnumerable<Claim> authClaims)
        {
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]));
            var token = new JwtSecurityToken(
                issuer: configuration["JWT:ValidIssuer"],
                audience: configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddMinutes(1),//DateTime.UtcNow.AddMinutes(5)
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
            );

            return token;
        }
    }
}
