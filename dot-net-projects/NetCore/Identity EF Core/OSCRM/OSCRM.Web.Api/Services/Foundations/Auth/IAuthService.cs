﻿using Microsoft.AspNetCore.Mvc;
using OSCRM.Web.Api.Models.Users;

namespace OSCRM.Web.Api.Services.Foundations.Auth
{
    public interface IAuthService
    {
        ValueTask<string> ConfirmEmailAsync(string email, string token);
        ValueTask<string> LoginUserAsync(string email, string password);
        ValueTask<string> RefreshAsync(string accessToken, string refreshToken);
        ValueTask<string> RevokeAsync(string email);
    }
}
