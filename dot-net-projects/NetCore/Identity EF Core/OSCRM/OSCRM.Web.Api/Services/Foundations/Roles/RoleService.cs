﻿using OSCRM.Web.Api.Brokers.AuthManagement;
using OSCRM.Web.Api.Brokers.DateTimes;
using OSCRM.Web.Api.Brokers.Loggings;
using OSCRM.Web.Api.Models.Roles;
using System.Data;

namespace OSCRM.Web.Api.Services.Foundations.Roles
{
    public partial class RoleService : IRoleService
    {
        private readonly IAuthManagementBroker authManagementBroker;
        private readonly IDateTimeBroker dateTimeBroker;
        private readonly ILoggingBroker loggingBroker;

        public RoleService(
            IAuthManagementBroker authManagementBroker,
            IDateTimeBroker dateTimeBroker,
            ILoggingBroker loggingBroker)
        {
            this.authManagementBroker = authManagementBroker;
            this.dateTimeBroker = dateTimeBroker;
            this.loggingBroker = loggingBroker;
        }

        public ValueTask<Role> CreateRoleAsync(Role role) =>
        TryCatch(async () =>
        {
            ValidateRoleIsNull(role);

            ValidateRolePropertiesForCreate(role);

            var storageRole = await this.authManagementBroker.SelectRoleByNameAsync(role.Name);
            ValidateRoleAlreadyExists(storageRole);

            return await this.authManagementBroker.InsertRoleAsync(role);
        });

        public IQueryable<Role> RetrieveAllRoles() =>
        TryCatch(() => this.authManagementBroker.SelectAllRoles());

        public IQueryable<Role> RetrieveAllRoles(bool isActive) =>
        TryCatch(() =>
        {
            ValidateRoleStatus(isActive);

            return this.authManagementBroker.SelectAllRoles(isActive);
        });

        public ValueTask<Role> RetrieveRoleByNameAsync(string roleName) =>
        TryCatch(async () =>
        {
            ValidateRoleName(roleName);

            var storageRole = await this.authManagementBroker.SelectRoleByNameAsync(roleName);
            ValidateRoleNotFound(storageRole, roleName);

            return storageRole;
        });

        public ValueTask<Role> RetrieveRoleByIdAsync(Guid roleId) =>
        TryCatch(async () =>
        {
            ValidateRoleId(roleId);

            var storageRole = await this.authManagementBroker.SelectRoleByIdAsync(roleId);
            ValidateRoleNotFound(storageRole, roleId);

            return storageRole;
        });

        public ValueTask<Role> ModifyRoleAsync(Role role, Guid roleId) =>
        TryCatch(async () =>
        {
            ValidateRoleIsNull(role);

            ValidateRolePropertiesForUpdate(role, roleId);

            var storageRole = await this.authManagementBroker.SelectRoleByIdAsync(role.Id);
            ValidateRoleNotFound(storageRole, role.Id);

            ValidateRoleUnchanged(role, storageRole);

            if (role.IsActive)
            {
                role.LastModified = this.dateTimeBroker.GetCurrentDateTime();
                role.ModifiedBy = roleId;
            }
            else
            {
                role.LastModified = this.dateTimeBroker.GetCurrentDateTime();
                role.ModifiedBy = roleId;
                role.DeletedOn = this.dateTimeBroker.GetCurrentDateTime();
                role.DeletedBy = roleId;
            }
            role.CreatedOn = storageRole.CreatedOn;
            role.CreatedBy = storageRole.CreatedBy;

            ValidateAgainstStorageRoleOnUpdate(role, storageRole);

            role.ConcurrencyStamp = storageRole.ConcurrencyStamp;

            return await this.authManagementBroker.UpdateRoleAsync(role);
        });

        public ValueTask<Role> RemoveRoleAsync(Guid roleId) =>
        TryCatch(async () =>
        {
            ValidateRoleId(roleId);

            var storageRole = await this.authManagementBroker.SelectRoleByIdAsync(roleId);
            ValidateRoleNotFound(storageRole, roleId);

            return await this.authManagementBroker.DeleteRoleAsync(storageRole);
        });

        public ValueTask<Role> ModifyRoleStatusAsync(Role role, Guid userId) =>
        TryCatch(async () =>
        {
            ValidateRoleIsNull(role);

            ValidateRolePropertiesForRoleStatusUpdate(role, userId);

            var storageRole = await this.authManagementBroker.SelectRoleByIdAsync(role.Id);
            ValidateRoleNotFound(storageRole, role.Id);

            ValidateRoleStatusUnchanged(role.IsActive, storageRole.IsActive);

            if (role.IsActive)
            {
                role.LastModified = this.dateTimeBroker.GetCurrentDateTime();
                role.ModifiedBy = userId;
            }
            else
            {
                role.DeletedOn = this.dateTimeBroker.GetCurrentDateTime();
                role.DeletedBy = userId;
                role.LastModified = storageRole.LastModified;
                role.ModifiedBy = storageRole.ModifiedBy;
            }
            role.CreatedOn = storageRole.CreatedOn;
            role.CreatedBy = storageRole.CreatedBy;

            //--Suppose there is a case of you can modify only once for some period. And after that period ends, you can modify it again.
            ValidateAgainstStorageRoleOnUpdateRoleStatus(role, storageRole);

            role.Name = storageRole.Name;
            role.Description = storageRole.Description;
            role.ConcurrencyStamp = storageRole.ConcurrencyStamp;
            
            return await this.authManagementBroker.UpdateRoleStatusAsync(role);
        });
    }
}
