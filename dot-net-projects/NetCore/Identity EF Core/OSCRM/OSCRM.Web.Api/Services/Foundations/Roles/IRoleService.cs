﻿using OSCRM.Web.Api.Models.Roles;

namespace OSCRM.Web.Api.Services.Foundations.Roles
{
    public interface IRoleService
    {
        ValueTask<Role> CreateRoleAsync(Role role);
        IQueryable<Role> RetrieveAllRoles();
        IQueryable<Role> RetrieveAllRoles(bool isActive);
        ValueTask<Role> RetrieveRoleByNameAsync(string roleName);
        ValueTask<Role> RetrieveRoleByIdAsync(Guid roleId);
        ValueTask<Role> ModifyRoleAsync(Role role, Guid userId);
        ValueTask<Role> RemoveRoleAsync(Guid roleId);
        ValueTask<Role> ModifyRoleStatusAsync(Role role, Guid userId);
    }
}
