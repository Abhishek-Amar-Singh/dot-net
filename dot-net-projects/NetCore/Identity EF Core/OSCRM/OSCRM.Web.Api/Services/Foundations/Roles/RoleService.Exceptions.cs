﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using OSCRM.Web.Api.Models.Roles;
using OSCRM.Web.Api.Models.Roles.Exceptions;

namespace OSCRM.Web.Api.Services.Foundations.Roles
{
    public partial class RoleService
    {
        private delegate ValueTask<Role> ReturningRoleFunction();
        private delegate IQueryable<Role> ReturningQueryableRoleFunction();

        private async ValueTask<Role> TryCatch(ReturningRoleFunction returningRoleFunction)
        {
            try
            {
                return await returningRoleFunction();
            }
            catch (NullRoleException nullRoleException)
            {
                throw CreateAndLogValidationException(nullRoleException);
            }
            catch (InvalidRoleException invalidRoleException)
            {
                throw CreateAndLogValidationException(invalidRoleException);
            }
            catch (NotFoundRoleException nullRoleException)
            {
                throw CreateAndLogValidationException(nullRoleException);
            }
            catch (UnchangedRoleStatusException unchangedRoleStatusException)
            {
                throw CreateAndLogDependencyValidationException(unchangedRoleStatusException);
            }
            catch (UnchangedRoleException unchangedRoleException)
            {
                throw CreateAndLogDependencyValidationException(unchangedRoleException);
            }
            catch (AlreadyExistsRoleException alreadyExistsRoleException)
            {
                throw CreateAndLogDependencyValidationException(alreadyExistsRoleException);
            }
            catch (DbUpdateConcurrencyException dbUpdateConcurrencyException)
            {
                var lockedRoleException = new LockedRoleException(dbUpdateConcurrencyException);

                throw CreateAndLogDependencyException(lockedRoleException);
            }
            catch (DbUpdateException dbUpdateException)
            {
                var failedRoleStorageException =
                    new FailedRoleStorageException(dbUpdateException);

                throw CreateAndLogDependencyException(failedRoleStorageException);
            }
            catch (SqlException sqlException)
            {
                var failedRoleStorageException =
                    new FailedRoleStorageException(sqlException);

                throw CreateAndLogCriticalDependencyException(failedRoleStorageException);
            }
            catch (Exception exception)
            {
                var failedRoleServiceException =
                    new FailedRoleServiceException(exception);

                throw CreateAndLogServiceException(failedRoleServiceException);
            }
        }

        private IQueryable<Role> TryCatch(ReturningQueryableRoleFunction returningQueryableRoleFunction)
        {
            try
            {
                return returningQueryableRoleFunction();
            }
            catch (InvalidRoleException invalidRoleException)
            {
                throw CreateAndLogValidationException(invalidRoleException);
            }
            catch (SqlException sqlException)
            {
                throw CreateAndLogCriticalDependencyException(sqlException);
            }
            catch (Exception exception)
            {
                var failedRoleServiceException =
                    new FailedRoleServiceException(exception);

                throw CreateAndLogServiceException(failedRoleServiceException);
            }
        }

        private RoleValidationException CreateAndLogValidationException(Exception exception)
        {
            var roleValidationException = new RoleValidationException(exception);
            this.loggingBroker.LogError(roleValidationException);

            return roleValidationException;
        }

        private RoleDependencyValidationException CreateAndLogDependencyValidationException(Exception exception)
        {
            var roleDependencyValidationException =
                new RoleDependencyValidationException(exception);
            this.loggingBroker.LogError(roleDependencyValidationException);

            return roleDependencyValidationException;
        }

        private RoleDependencyException CreateAndLogDependencyException(Exception exception)
        {
            var roleDependencyException = new RoleDependencyException(exception);
            this.loggingBroker.LogError(roleDependencyException);

            return roleDependencyException;
        }

        private RoleDependencyException CreateAndLogCriticalDependencyException(Exception exception)
        {
            var roleDependencyException = new RoleDependencyException(exception);
            this.loggingBroker.LogCritical(roleDependencyException);

            return roleDependencyException;
        }

        private RoleServiceException CreateAndLogServiceException(Exception exception)
        {
            var roleServiceException = new RoleServiceException(exception);
            this.loggingBroker.LogError(roleServiceException);

            return roleServiceException;
        }
    }
}
