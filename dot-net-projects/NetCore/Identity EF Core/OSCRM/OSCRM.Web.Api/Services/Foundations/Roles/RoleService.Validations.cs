﻿using OSCRM.Web.Api.Models.Roles;
using OSCRM.Web.Api.Models.Roles.Exceptions;
using System.Data;
using System.Text.RegularExpressions;

namespace OSCRM.Web.Api.Services.Foundations.Roles
{
    public partial class RoleService
    {
        private void ValidateRoleIsNull(Role role)
        {
            if (role is null)
            {
                throw new NullRoleException();
            }
        }

        private void ValidateRolePropertiesForCreate(Role role)
        {
            Validate
            (
                (Rule: IsInvalidX(role.Name), Parameter: nameof(Role.Name)),
                (Rule: IsInvalidRoleName(role.Name), Parameter: nameof(Role.Name)),
                (Rule: IsInvalidX(role.IsActive), Parameter: nameof(Role.IsActive)),
                (Rule: IsInvalidX(role.CreatedOn), Parameter: nameof(Role.CreatedOn)),
                (Rule: IsInvalidX(role.CreatedBy), Parameter: nameof(Role.CreatedBy))
            );
        }

        private void ValidateRoleAlreadyExists(Role role)
        {
            if(role is not null)
            {
                throw new AlreadyExistsRoleException(role);
            }
        }

        private dynamic IsInvalidX(string text) => new
        {
            Condition = String.IsNullOrWhiteSpace(text) || text == string.Empty,
            Message = "Text is required"
        };

        private dynamic IsInvalidX(bool? status) => new
        {
            Condition = status is null,
            Message = "status is required"
        };

        private dynamic IsInvalidX(DateTimeOffset? date) => new
        {
            Condition = date == default || date is null,
            Message = "Date is required"
        };

        private dynamic IsInvalidX(Guid? id) => new
        {
            Condition = id == Guid.Empty || id is null,
            Message = "Id is required"
        };

        private dynamic IsInvalidRoleName(string text)
        {
            string alphanumericRegex = "^[a-zA-Z0-9 ]+$";
            return new
            {
                Condition = !(text.Length >= 1 && text.Length <= 40) || !Regex.IsMatch(text, alphanumericRegex),
                Message = "RoleName should contain only alphanumeric characters, with a length between 1 and 40 characters"
            };
        }

        private void Validate(params (dynamic Rule, string Parameter)[] validations)
        {
            var invalidRoleException = new InvalidRoleException();

            foreach ((dynamic rule, string parameter) in validations)
            {
                if (rule.Condition)
                {
                    //invalidRoleException.MaintainErrorList(
                    //    key: parameter,
                    //    value: rule.Message);
                    throw new InvalidRoleException(parameter, rule.Message);
                }
            }

            //invalidRoleException.ThrowIfContainsErrors();
        }

        private void ValidateRoleName(string roleName)
        {
            Validate
            (
                (Rule: IsInvalidX(roleName), Parameter: nameof(Role.Name)),
                (Rule: IsInvalidRoleName(roleName), Parameter: nameof(Role.Name))
            );

        }

        private void ValidateRoleNotFound(Role role, string roleName)
        {
            if (role is null)
            {
                throw new NotFoundRoleException(roleName);
            }
        }

        private void ValidateRoleId(Guid roleId)
        {
            Validate
            (
                (Rule: IsInvalidX(roleId), Parameter: nameof(Role.Id))
            );
        }

        private void ValidateRoleStatus(bool isActive)
        {
            Validate
            (
                (Rule: IsInvalidX(isActive), Parameter: nameof(Role.IsActive))
            );
        }

        private void ValidateRoleNotFound(Role role, Guid roleId)
        {
            if (role is null)
            {
                throw new NotFoundRoleException(roleId);
            }
        }

        private void ValidateRolePropertiesForRoleStatusUpdate(Role role, Guid userId)
        {
            Validate
            (
                (Rule: IsInvalidX(role.Id), Parameter: nameof(Role.Id)),
                (Rule: IsInvalidX(role.IsActive), Parameter: nameof(Role.IsActive)),
                (Rule: IsInvalidX(userId), Parameter: (role.IsActive is true) ? nameof(Role.ModifiedBy) : nameof(Role.DeletedBy))
            );
        }

        private void ValidateRolePropertiesForUpdate(Role role, Guid userId)
        {
            Validate
            (
                (Rule: IsInvalidX(role.Id), Parameter: nameof(Role.Id)),
                (Rule: IsInvalidX(role.Name), Parameter: nameof(Role.Name)),
                (Rule: IsInvalidRoleName(role.Name), Parameter: nameof(Role.Name)),
                (Rule: IsInvalidX(role.IsActive), Parameter: nameof(Role.IsActive)),
                (Rule: IsInvalidX(userId), Parameter: (role.IsActive is true) ? nameof(Role.ModifiedBy) : $"{nameof(Role.ModifiedBy)} & {nameof(Role.DeletedBy)}")
            );
        }
        private static dynamic IsSame(
            DateTimeOffset? firstDate,
            DateTimeOffset? secondDate,
            string firstDateName,
            string secondDateName) => new
            {
                Condition = firstDate == secondDate,
                Message = $"{firstDateName} is same as {secondDateName}"
            };

        private static dynamic IsSame(
            DateTimeOffset? firstDate,
            DateTimeOffset? secondDate,
            DateTimeOffset? thirdDate,
            string firstDateName,
            string secondDateName,
            string thirdDateName) => new
            {
                Condition = firstDate == secondDate || firstDate == thirdDate,
                Message = $"{firstDateName} is same as {secondDateName} or {thirdDateName}"
            };

        private static dynamic IsGreaterThan(
            DateTimeOffset? firstDate,
            DateTimeOffset? secondDate,
            DateTimeOffset? thirdDate,
            string firstDateName,
            string secondDateName,
            string thirdDateName) => new
            {
                Condition = firstDate > secondDate || firstDate > thirdDate,
                Message = $"{firstDateName} is greater than {secondDateName} or {thirdDateName}"
            };

        private static dynamic IsGreaterThan(
            DateTimeOffset? firstDate,
            DateTimeOffset? secondDate,
            string firstDateName,
            string secondDateName) => new
            {
                Condition = firstDate > secondDate,
                Message = $"{firstDateName} is greater than {secondDateName}"//--first you create then only you can update. So, your LastModified > CreateOn
            };

        private void ValidateAgainstStorageRoleOnUpdateRoleStatus(Role inputRole, Role storageRole)
        {
            if (inputRole.IsActive)
            {
                Validate
                (
                    (Rule: IsSame(
                        inputRole.CreatedOn, storageRole.LastModified,
                        nameof(Role.CreatedOn), nameof(Role.LastModified)), Parameter: nameof(Role.LastModified)),
                    (Rule: IsGreaterThan(
                        inputRole.CreatedOn, storageRole.LastModified,
                        nameof(Role.CreatedOn), nameof(Role.LastModified)), Parameter: nameof(Role.LastModified))
                );
            }
            else
            {
                Validate
                (
                    (Rule: IsSame(
                        inputRole.CreatedOn, storageRole.DeletedOn,
                        nameof(Role.CreatedOn), nameof(Role.DeletedOn)), Parameter: nameof(Role.DeletedOn)),
                    (Rule: IsGreaterThan(
                        inputRole.CreatedOn, storageRole.DeletedOn,
                        nameof(Role.CreatedOn), nameof(Role.DeletedOn)), Parameter: nameof(Role.DeletedOn))
                );
            }
        }

        private void ValidateAgainstStorageRoleOnUpdate(Role inputRole, Role storageRole)
        {
            if (inputRole.IsActive)
            {
                Validate
                (
                    (Rule: IsSame(
                        inputRole.CreatedOn, storageRole.LastModified,
                        nameof(Role.CreatedOn), nameof(Role.LastModified)), Parameter: nameof(Role.LastModified)),
                    (Rule: IsGreaterThan(
                        inputRole.CreatedOn, storageRole.LastModified,
                        nameof(Role.CreatedOn), nameof(Role.LastModified)), Parameter: nameof(Role.LastModified))
                );
            }
            else
            {
                Validate
                (
                    (Rule: IsSame(
                        inputRole.CreatedOn, storageRole.LastModified, storageRole.DeletedOn,
                        nameof(Role.CreatedOn), nameof(Role.LastModified), nameof(Role.DeletedOn)),
                        Parameter: $"{nameof(Role.LastModified)} or {nameof(Role.DeletedOn)}"),
                    (Rule: IsGreaterThan(
                        inputRole.CreatedOn, storageRole.LastModified, storageRole.DeletedOn,
                        nameof(Role.CreatedOn), nameof(Role.LastModified), nameof(Role.DeletedOn)),
                        Parameter: $"{nameof(Role.LastModified)} or {nameof(Role.DeletedOn)}")
                );
            }
        }

        private void ValidateRoleStatusUnchanged(bool inputRoleStatus, bool storageRoleStatus)
        {
            if (inputRoleStatus == storageRoleStatus)
            {
                throw new UnchangedRoleStatusException((inputRoleStatus is true) ? "active" : "inactive");
            }
        }

        private void ValidateRoleUnchanged(Role inputRole, Role storageRole)
        {
            if (inputRole.Name == storageRole.Name &&
                inputRole.Description == storageRole.Description &&
                inputRole.IsActive == storageRole.IsActive)
            {
                throw new UnchangedRoleException();
            }
        }
    }
}
