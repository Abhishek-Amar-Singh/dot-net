﻿using OSCRM.Web.Api.Models.Users;

namespace OSCRM.Web.Api.Services.Foundations.Users
{
    public interface IUserService
    {
        ValueTask<User> RegisterUserAsync(User user);
        IQueryable<User> RetrieveAllUsers();
        IQueryable<User> RetrieveAllUsers(bool isActive);
        ValueTask<User> RetrieveUserByEmailAsync(string email);
        ValueTask<User> RetrieveUserByIdAsync(Guid id);
    }
}
