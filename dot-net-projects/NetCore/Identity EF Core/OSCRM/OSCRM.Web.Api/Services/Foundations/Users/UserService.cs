﻿using MimeKit;
using OSCRM.Web.Api.Brokers.AuthManagement;
using OSCRM.Web.Api.Brokers.DateTimes;
using OSCRM.Web.Api.Brokers.Loggings;
using OSCRM.Web.Api.Mail.Models;
using OSCRM.Web.Api.Mail.Services;
using OSCRM.Web.Api.Models.Roles;
using OSCRM.Web.Api.Models.Users;

namespace OSCRM.Web.Api.Services.Foundations.Users
{
    public partial class UserService : IUserService
    {
        private readonly IAuthManagementBroker authManagementBroker;
        private readonly IDateTimeBroker dateTimeBroker;
        private readonly ILoggingBroker loggingBroker;
        private readonly IMailService mailService;

        public UserService(
            IAuthManagementBroker authManagementBroker,
            IDateTimeBroker dateTimeBroker,
            ILoggingBroker loggingBroker,
            IMailService mailService)
        {
            this.authManagementBroker = authManagementBroker;
            this.dateTimeBroker = dateTimeBroker;
            this.loggingBroker = loggingBroker;
            this.mailService = mailService;
        }

        public ValueTask<User> RegisterUserAsync(User user) =>
        TryCatch(async () =>
        {
            ValidateUserIsNull(user);

            ValidateUserPropertiesOnCreate(user);


            var storageUser = await this.authManagementBroker.SelectUserByEmailAsync(user.Email);
            #region Same user registering for different roles
            //var roleNames = await this.authManagementBroker.GetRolesAsync(storageUser);
            //--1st way
            //IList<Guid> roleIds = new List<Guid>();
            //foreach (var roleName in roleNames)
            //{
            //    roleIds.Add(this.authManagementBroker.SelectRoleByNameAsync(roleName).Result.Id);
            //}
            //--2nd way
            //var roleIds = await Task.WhenAll(roleNames.Select(async roleName =>
            //{
            //    var role = await this.authManagementBroker.SelectRoleByNameAsync(roleName);
            //    return role.Id;
            //}));
            #endregion
            ValidateUserAlreadyExists(storageUser);

            var storageRole = await this.authManagementBroker.SelectRoleByIdAsync(user.RoleId);
            ValidateRoleNotFound(storageRole, user.RoleId);
            ValidateRoleIsActiveOrNot(storageRole);

            using (var transaction = await this.authManagementBroker.BeginTransactionAsync())
            {
                storageUser = await this.authManagementBroker.InsertUserAsync(user);
                var storageUserRole = await this.authManagementBroker.InsertUserToRoleAsync(storageUser, storageRole);
                var token = await this.authManagementBroker.GenerateEmailConfirmationTokenAsync(storageUser);
                VerifyEmailOnMailServer(storageUser, storageRole, token);

                await transaction.CommitAsync();
            }

            return storageUser;
        });

        public IQueryable<User> RetrieveAllUsers() =>
        TryCatch(() => this.authManagementBroker.SelectAllUsers());

        public IQueryable<User> RetrieveAllUsers(bool isActive) =>
        TryCatch(() =>
        {
            ValidateUserStatus(isActive);

            return this.authManagementBroker.SelectAllUsers(isActive);
        });

        public ValueTask<User> RetrieveUserByEmailAsync(string email) =>
        TryCatch(async () =>
        {
            ValidateEmail(email);

            var storageUser = await this.authManagementBroker.SelectUserByEmailAsync(email);
            ValidateUserNotFound(storageUser, email);

            return storageUser;
        });

        public ValueTask<User> RetrieveUserByIdAsync(Guid id) =>
        TryCatch(async () =>
        {
            ValidateId(id);

            var storageUser = await this.authManagementBroker.SelectUserByIdAsync(id);
            ValidateUserNotFound(storageUser, id);

            return storageUser;
        });

        private void VerifyEmailOnMailServer(User user, Role role, string token)
        {
            var confirmationLink = $"https://localhost:7251/api/Auth/VerifyEmailAsync?email={user.Email}&token={token}";
            var message = new Message(
            new MailboxAddress[] {
                    new MailboxAddress($"{user.FirstName}{(user.MiddleName is null ? "" : " "+user.MiddleName)} {user.LastName}", user.Email)
            },
            $"Verify Your Email Address", $"<div>{token}</div><div>Registered for role={role.Name}</div><div><button><a href='{confirmationLink}'>Confirm</a></button></div>");

            mailService.SendMail(message);
        }
    }
}
