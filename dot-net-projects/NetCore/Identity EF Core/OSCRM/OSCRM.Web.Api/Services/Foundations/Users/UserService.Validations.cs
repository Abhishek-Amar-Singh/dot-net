﻿using Microsoft.Extensions.FileSystemGlobbing.Internal;
using OSCRM.Web.Api.Models.Roles;
using OSCRM.Web.Api.Models.Roles.Exceptions;
using OSCRM.Web.Api.Models.Users;
using OSCRM.Web.Api.Models.Users.Exceptions;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;

namespace OSCRM.Web.Api.Services.Foundations.Users
{
    public partial class UserService
    {
        private void ValidateUserIsNull(User user)
        {
            if (user is null)
            {
                throw new NullUserException();
            }
        }

        private void ValidateUserNotFound(User user, string email)
        {
            if (user is null)
            {
                throw new NotFoundUserException(email);
            }
        }

        private void ValidateUserNotFound(User user, Guid id)
        {
            if (user is null)
            {
                throw new NotFoundUserException(id);
            }
        }

        private void ValidateUserPropertiesOnCreate(User user)
        {
            Validate
            (
                (Rule: IsInvalidX(user.FirstName), Parameter: nameof(User.FirstName)),
                (Rule: IsInvalidName(user.FirstName), Parameter: nameof(User.FirstName)),
                (Rule: IsInvalidX(user.LastName), Parameter: nameof(User.LastName)),
                (Rule: IsInvalidName(user.LastName), Parameter: nameof(User.LastName)),
                (Rule: IsInvalidX(user.Email), Parameter: nameof(User.Email)),
                (Rule: IsInvalidEmail(user.Email), Parameter: nameof(User.Email)),
                (Rule: IsInvalidX(user.UserName), Parameter: nameof(User.UserName)),
                (Rule: IsInvalidEmail(user.UserName), Parameter: nameof(User.UserName)),
                (Rule: IsInvalidX(user.Password), Parameter: nameof(User.Password)),
                (Rule: IsInvalidPassword(user.Password), Parameter: nameof(User.Password)),
                (Rule: IsInvalidX(user.ConfirmPassword), Parameter: nameof(User.ConfirmPassword)),
                (Rule: IsInvalidPassword(user.ConfirmPassword), Parameter: nameof(User.ConfirmPassword)),
                (Rule: IsInvalidConfirmPassword(user.Password, user.ConfirmPassword), Parameter: nameof(User.ConfirmPassword)),
                (Rule: IsInvalidX(user.DOB), Parameter: nameof(User.DOB)),
                (Rule: IsInvalidX(user.GenderId), Parameter: nameof(User.GenderId)),
                (Rule: IsInvalidX(user.DistrictId), Parameter: nameof(User.DistrictId)),
                (Rule: IsInvalidX(user.CityId), Parameter: nameof(User.CityId)),
                (Rule: IsInvalidX(user.StateId), Parameter: nameof(User.StateId)),
                (Rule: IsInvalidX(user.NationId), Parameter: nameof(User.NationId)),
                (Rule: IsInvalidX(user.MobileNumber), Parameter: nameof(User.MobileNumber)),
                (Rule: IsInvalidIndianMobileNumber(user.MobileNumber), Parameter: nameof(User.MobileNumber)),
                (Rule: IsInvalidX(user.CreatedOn), Parameter: nameof(User.CreatedOn))
            );

            ValidateRoleId(user.RoleId);
        }

        private dynamic IsInvalidX(string text) => new
        {
            Condition = String.IsNullOrWhiteSpace(text) || text == string.Empty,
            Message = "Text is required"
        };

        private dynamic IsInvalidX(DateTime? date) => new
        {
            Condition = date == default || date is null,
            Message = "Date is required"
        };

        private dynamic IsInvalidX(int? id) => new
        {
            Condition = id == default || id is null || id is 0,
            Message = "Id is required"
        };

        private dynamic IsInvalidX(Guid id) => new
        {
            Condition = id == default || id == Guid.Empty,
            Message = "Id is required"
        };

        private dynamic IsInvalidX(DateTimeOffset? date) => new
        {
            Condition = date == default || date is null,
            Message = "Date is required"
        };

        private dynamic IsInvalidX(bool? isActive) => new
        {
            Condition = isActive is null,
            Message = "status is required"
        };

        private dynamic IsInvalidName(string text) => new
        {
            Condition = !Regex.IsMatch(text, @"^[A-Za-z]{2,20}$"),
            Message = "Text should only contain alphabets and should be minimum of 2 characters and maximum of 20 characters"
        };

        private dynamic IsInvalidEmail(string email) => new
        {
            Condition = !Regex.IsMatch(email, @"^[A-Za-z][A-Za-z0-9._-]+@[A-Za-z][A-Za-z0-9-_]*[A-Za-z0-9]+(\.[A-Za-z]{2,}){1,2}$"),
            Message = "Email format is not correct"
        };

        private dynamic IsInvalidPassword(string password) => new
        {
            Condition = !Regex.IsMatch(password, @"^(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$"),
            Message = "Invalid Password.\nTry to have a password that satisfies the below given points:\n(a) contains at least one special symbol\n(b) contains at least one lowercase alphabet\n(c)contains at least one uppercase alphabet\n(d) contains at least one digit (e) must be at least 8 characters long"
        };

        private dynamic IsInvalidConfirmPassword(string password, string confirmPassword) => new
        {
            Condition = !password.Equals(confirmPassword),
            Message = "Confirm password should be same as password"
        };

        private dynamic IsInvalidIndianMobileNumber(string mobile) => new
        {
            Condition = !Regex.IsMatch(mobile, @"^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[6789]\d{9}$"),
            Message = "Mobile number format is not correct.\nTry to have a mobile number that satisfies the below given points:\n" +
            "(a) allows for optional country code prefix variations. It supports variations like '+91', '0091', '91-', '91 -', or no prefix at all\n" +
            "(b) must start with a digit from 6 to 9\n" +
            "(c) followed by 9 digits following the initial digit"
        };

        private void Validate(params (dynamic Rule, string Parameter)[] validations)
        {
            foreach ((dynamic rule, string parameter) in validations)
            {
                if (rule.Condition)
                {
                    throw new InvalidUserException(parameter, rule.Message);
                }
            }
        }

        private void ValidateRoleId(Guid roleId)
        {
            dynamic rule = IsInvalidX(roleId);

            if (rule.Condition)
            {
                throw new InvalidRoleException(nameof(Role.Id), rule.Message);
            }
        }

        private void ValidateUserAlreadyExists(User user, IList<Guid> roleIds, Guid roleId)
        {
            if (user is not null && roleIds.Contains(roleId))
            {
                throw new AlreadyExistsUserException(user, roleId);
            }
        }
        private void ValidateUserAlreadyExists(User user)
        {
            if (user is not null)
            {
                throw new AlreadyExistsUserException(user);
            }
        }

        private void ValidateRoleNotFound(Role role, Guid roleId)
        {
            if (role is null)
            {
                throw new NotFoundRoleException(roleId);
            }
        }

        private void ValidateRoleIsActiveOrNot(Role role)
        {
            if (role.IsActive is false)
            {
                throw new InactiveRoleException(role.Name);
            }
        }

        private void ValidateUserStatus(bool isActive)
        {
            Validate
            (
                (Rule: IsInvalidX(isActive), Parameter: nameof(User.IsActive))
            );
        }

        private void ValidateEmail(string email)
        {

            Validate
            (
                (Rule: IsInvalidX(email), Parameter: nameof(User.Email)),
                (Rule: IsInvalidEmail(email), Parameter: nameof(User.Email))
            );
        }

        private void ValidateId(Guid id)
        {
            Validate
            (
                (Rule: IsInvalidX(id), Parameter: nameof(User.Id))
            );
        }
    }
}
