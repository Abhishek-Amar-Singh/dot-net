﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using OSCRM.Web.Api.Brokers.AuthManagement;
using OSCRM.Web.Api.Brokers.DateTimes;
using OSCRM.Web.Api.Brokers.Loggings;
using OSCRM.Web.Api.Brokers.Storages;
using OSCRM.Web.Api.Mail.Models;
using OSCRM.Web.Api.Mail.Services;
using OSCRM.Web.Api.Models.Roles;
using OSCRM.Web.Api.Models.Users;
using OSCRM.Web.Api.Services.Foundations.Auth;
using OSCRM.Web.Api.Services.Foundations.Roles;
using OSCRM.Web.Api.Services.Foundations.Users;
using System.Text;
using JsonStringEnumConverter = Newtonsoft.Json.Converters.StringEnumConverter;

namespace OSCRM.Web.Api.Extensions
{
    public static class PersistenceService
    {
        public static IServiceCollection AddPersistenceServices(this IServiceCollection services, IConfiguration configuration)
        {
            AddNewtonSoftJson(services);

            services.AddLogging();

            services.AddDbContext<StorageBroker>();

            AddBrokers(services);

            AddFoundationServices(services);

            //--AddIdentityCore....it was giving an error for SignInManager service
            //builder.Services.AddIdentityCore<User>()
            //        .AddRoles<Role>()
            //        .AddEntityFrameworkStores<StorageBroker>()
            //        .AddDefaultTokenProviders();
            //--AddIdentity...to resolve an error for SignInManager service
            services.AddIdentity<User, Role>()
                    .AddEntityFrameworkStores<StorageBroker>()
                    .AddDefaultTokenProviders();

            AddMailConfigurationServices(services, configuration);

            AddAuthenticationServices(services, configuration);

            //--Add Config for Required Email Address while signing in....working fine
            services.Configure<IdentityOptions>(option =>
            {
                option.SignIn.RequireConfirmedEmail = true;
                //option.SignIn.RequireConfirmedPhoneNumber = true;
            });

            return services;
        }

        private static void AddAuthenticationServices(IServiceCollection services, IConfiguration configuration)
        {
            //--Add authentication into swagger
            services.AddSwaggerGen(option =>
            {
                option.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "OSCRM.Web.Api", Version = "v1" });
                option.AddSecurityDefinition("Bearer", new Microsoft.OpenApi.Models.OpenApiSecurityScheme
                {
                    In = Microsoft.OpenApi.Models.ParameterLocation.Header,
                    Description = "Please enter a valid token",
                    Name = "Authorization",
                    Type = Microsoft.OpenApi.Models.SecuritySchemeType.Http,
                    BearerFormat = "JWT",
                    Scheme = "Bearer"
                });
                option.AddSecurityRequirement(new Microsoft.OpenApi.Models.OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme{Reference = new OpenApiReference{Type = ReferenceType.SecurityScheme,Id="Bearer"}},
                        new string[]{}
                    }
                });
            });

            //--Add authentication
            services.AddAuthentication(option =>
            {
                option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                option.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(option =>
            {
                option.SaveToken = true;
                option.RequireHttpsMetadata = false;
                option.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidIssuer = configuration["JWT:ValidIssuer"],
                    ValidAudience = configuration["JWT:ValidAudience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"])),
                    ClockSkew = TimeSpan.Zero
                
                };
            });
        }

        private static void AddMailConfigurationServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IMailService, MailService>();

            //--Add Email Config
            /* //--1st Way
            services.Configure<MailConfiguration>(configuration.GetSection("MailConfiguration"));
            var mailConfig = configuration.GetSection("MailConfiguration").Get<MailConfiguration>();
            services.AddSingleton(mailConfig);
            */
            //--2nd Way
            var mailConfig = new MailConfiguration();
            configuration.GetSection("MailConfiguration").Bind(mailConfig);
            services.AddSingleton(mailConfig);
        }

        private static void AddFoundationServices(IServiceCollection services)
        {
            services.AddTransient<IAuthService, AuthService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IRoleService, RoleService>();
        }

        private static void AddBrokers(IServiceCollection services)
        {
            services.AddScoped<IAuthManagementBroker, AuthManagementBroker>();
            services.AddScoped<IStorageBroker, StorageBroker>();
            services.AddTransient<ILoggingBroker, LoggingBroker>();
            services.AddTransient<IDateTimeBroker, DateTimeBroker>();
        }

        private static void AddNewtonSoftJson(IServiceCollection services)
        {
            //--AddNewtonSoftJson(services)
            /*
            If AddNewtonSoftJson services are not added the we'll get an error
            Some services are not able to be constructed (Error while validating the service descriptor 'ServiceType: Microsoft.AspNetCore.Identity.DataProtectorTokenProvider`1[OSCRM.Web.Api.Models.Users.User] Lifetime: Transient 
            When an exception comes, it will give you error 'MethodBase'(every thing runs fine but this error will come on the return statement in the catch block),
            so, to resolve this we add this so that we can get proper exception json response
             */
            services.AddMvc().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                options.SerializerSettings.Converters.Add(new JsonStringEnumConverter());
                options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });
        }
    }
}
