﻿namespace OSCRM.Web.Api.Mail.Exceptions
{
    public class FailedMailServiceException : Exception
    {
        public FailedMailServiceException(Exception innerException):base(message: "Mail service failed, contact support.", innerException)
        {
            
        }
    }
}
