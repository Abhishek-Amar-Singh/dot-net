﻿using OSCRM.Web.Api.Mail.Models;

namespace OSCRM.Web.Api.Mail.Services
{
    public interface IMailService
    {
        void SendMail(Message msg);
        void SendMail(Message msg, dynamic transaction);
    }
}
