﻿using MailKit;
using MailKit.Net.Imap;
using MailKit.Net.Smtp;
using MimeKit;
using OSCRM.Web.Api.Mail.Exceptions;
using OSCRM.Web.Api.Mail.Models;
using System.Net.Sockets;
using AuthenticationException = MailKit.Security.AuthenticationException;

namespace OSCRM.Web.Api.Mail.Services
{
    public class MailService : IMailService
    {
        private readonly MailConfiguration mailConfiguration;

        public MailService(MailConfiguration mailConfiguration) =>
            this.mailConfiguration = mailConfiguration;

        public void SendMail(Message msg)
        {
            var mailMessage = CreateMessageToMail(msg);
            Send(mailMessage);
        }

        public void SendMail(Message msg, dynamic transaction)
        {
            var mailMessage = CreateMessageToMail(msg);
            Send(mailMessage, transaction);
        }

        private MimeMessage CreateMessageToMail(Message msg)
        {
            var mailMessage = new MimeMessage();
            mailMessage.From.Add(new MailboxAddress("Abhishek Amar Singh", mailConfiguration.From));
            mailMessage.To.AddRange(msg.To);
            mailMessage.Subject = msg.Subject;
            mailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html) { Text = msg.Content };
            return mailMessage;
        }

        private void Send(MimeMessage emailMessage)
        {
            using var client = new SmtpClient();
            try
            {
                client.Connect(mailConfiguration.SmtpServer, mailConfiguration.Port, true);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                client.Authenticate(mailConfiguration.Username, mailConfiguration.Password);

                client.Send(emailMessage);
            }
            catch (SmtpProtocolException smtpProtocolException)
            {
                throw new FailedMailServiceException(smtpProtocolException);
            }
            catch (AuthenticationException authenticationException)
            {
                throw new FailedMailServiceException(authenticationException);
            }
            catch (SocketException socketException)
            {
                throw new FailedMailServiceException(socketException);
            }
            catch (SmtpCommandException smtpCommandException)
            {
                throw new FailedMailServiceException(smtpCommandException);
            }
            catch (ServiceNotConnectedException serviceNotConnectedException)
            {
                throw new FailedMailServiceException(serviceNotConnectedException);
            }
            catch (Exception exception)
            {
                throw new FailedMailServiceException(exception);
            }
            finally
            {
                client.Disconnect(true);
                client.Dispose();
            }
        }

        private void Send(MimeMessage emailMessage, dynamic transaction)
        {
            using var client = new SmtpClient();
            try
            {
                client.Connect(mailConfiguration.SmtpServer, mailConfiguration.Port, true);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                client.Authenticate(mailConfiguration.Username, mailConfiguration.Password);

                client.Send(emailMessage);
            }
            catch (SmtpProtocolException smtpProtocolException)
            {
                transaction.Rollback();
                throw new FailedMailServiceException(smtpProtocolException);
            }
            catch (AuthenticationException authenticationException)
            {
                transaction.Rollback();
                throw new FailedMailServiceException(authenticationException);
            }
            catch (SocketException socketException)
            {
                transaction.Rollback();
                throw new FailedMailServiceException(socketException);
            }
            catch (SmtpCommandException smtpCommandException)
            {
                transaction.Rollback();
                throw new FailedMailServiceException(smtpCommandException);
            }
            catch (ServiceNotConnectedException serviceNotConnectedException)
            {
                transaction.Rollback();
                throw new FailedMailServiceException(serviceNotConnectedException);
            }
            catch (Exception exception)
            {
                transaction.Rollback();
                throw new FailedMailServiceException(exception);
            }
            finally
            {
                client.Disconnect(true);
                client.Dispose();
            }
        }

        public void SendEmail()
        {
            var emailMessage = CreateMessageToEmail();
            Bhejo(emailMessage);
        }

        private MimeMessage CreateMessageToEmail()
        {
            var mailMessage = new MimeMessage();
            mailMessage.From.Add(new MailboxAddress("Abhishek Singh", "abhisheksingh.120120@gmail.com"));
            mailMessage.To.AddRange(new MailboxAddress[] {
                    new MailboxAddress("Abhishek Singh", "abhisheksingh.120120@gmail.com"),
                    new MailboxAddress("Team Corbet", "teamcorbet123@gmail.com"),
                    new MailboxAddress("Tejas Pawar", "tejaspawar1320@gmail.com")
                });
            mailMessage.Subject = "Multiple mail send karne bola hay kyaaaaa...";
            mailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html) { Text = "<h1>Experiance Lagte Ho.. Kitne saal se kaam kar rahee hoo..</h1>" };
            return mailMessage;
        }
        private void Bhejo(MimeMessage emailMessage)
        {
            using var client = new SmtpClient();
            try
            {
                client.Connect("smtp.gmail.com", 465, true);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                client.Authenticate("tejaspawar1320@gmail.com", "pwd");

                client.Send(emailMessage);
            }
            catch
            {
                throw;
            }
            finally
            {
                client.Disconnect(true);
                client.Dispose();
            }
        }
    }
}
