﻿$(document).ready(function () {
    //ProductList -> Extract ProductTitle
    var productNode = $("#ddlProduct");
    productNode.append($("<option></option>").val("all").html("Select Product"));
    var url = "/Stock/GetProductList";
    $.get(url, null, function (data) {
        console.log(data);

        $.each(data, function (i, product) {
            productNode.append($("<option></option>").val(product.productTitle).html(product.productTitle));
        });
    });
    //StockTypeList -> Extract StockTypeName
    var stockTypeNode = $("#ddlType");
    stockTypeNode.append($("<option></option>").val("all").html("Select Type"));
    var url = "/Stock/GetStockTypeList";
    $.get(url, null, function (data) {
        console.log(data);

        $.each(data, function (i, type) {
            stockTypeNode.append($("<option></option>").val(type.stockTypeName).html(type.stockTypeName));
        });
    });
    //StockList's Quantity -> Putting Quantity of range(1, 100)
    var stockQuantityNode = $("#ddlQuantity");
    stockQuantityNode.append($("<option></option>").val("all").html("Select Quantity"));
    for (let q = 1; q <= 100; q++) {
        stockQuantityNode.append($("<option></option>").val(q).html(q));
    }

    //OnChange dropdown list -> apply filters
    $("#ddlProduct,#ddlQuantity, #ddlType").on("change", function () {
        var product = $('#ddlProduct').find("option:selected").val();
        var quantity = $('#ddlQuantity').find("option:selected").val();
        var type = $('#ddlType').find("option:selected").val();
        SearchData(product, quantity, type);
    });
});

function SearchData(product, quantity, type) {
    if (product.toUpperCase() == 'ALL' && quantity.toUpperCase() == 'ALL' && type.toUpperCase() == 'ALL') {
        $('#table11 tbody tr').show();
    }
    else {
        $('#table11 tbody tr:has(td)').each(function () {
            var rowProduct = $.trim($(this).find('td:eq(1)').text());
            var rowQuantity = $.trim($(this).find('td:eq(2)').text());
            var rowType = $.trim($(this).find('td:eq(3)').text());

            if (product.toUpperCase() != 'ALL' && quantity.toUpperCase() != 'ALL' && rowType.toUpperCase() != 'ALL') {
                if (rowProduct.toUpperCase() == product.toUpperCase() && rowQuantity == quantity && rowType.toUpperCase() == type.toUpperCase()) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            }
            if (product.toUpperCase() != 'ALL' && type.toUpperCase() != 'ALL' && quantity.toUpperCase() == 'ALL') {
                //console.log("1");
                //console.log("product.toUpperCase() != 'ALL' && type.toUpperCase() != 'ALL' && quantity.toUpperCase() == 'ALL'");
                //console.log(`[rp:${rowProduct},p:${product}], [rt:${rowType},t:${type}], [rq:${rowQuantity},q:${quantity}]`);
                if (rowProduct.toUpperCase() == product.toUpperCase() && rowType.toUpperCase() == type.toUpperCase()) {
                    //console.log("1-true");
                    $(this).show();
                }
                else {
                    //console.log("1-false");
                    $(this).hide();
                }
                //console.log("===================================");
            }

            if (product.toUpperCase() != 'ALL' && type.toUpperCase() == 'ALL' && quantity.toUpperCase() != 'ALL') {
                //console.log("2");
                //console.log("product.toUpperCase() != 'ALL' && type.toUpperCase() == 'ALL' && quantity.toUpperCase() != 'ALL'");
                //console.log(`[rp:${rowProduct},p:${product}], [rt:${rowType},t:${type}], [rq:${rowQuantity},q:${quantity}]`);
                if (rowProduct.toUpperCase() == product.toUpperCase() && rowQuantity == quantity) {
                    //console.log("2-true");
                    $(this).show();
                }
                else {
                    //console.log("2-false");
                    $(this).hide();
                }
                //console.log("===================================");
            }

            if (product.toUpperCase() != 'ALL' && type.toUpperCase() == 'ALL' && quantity.toUpperCase() == 'ALL') {
                //console.log("3");
                //console.log("product.toUpperCase() != 'ALL' && type.toUpperCase() == 'ALL' && quantity.toUpperCase() == 'ALL'");
                //console.log(`[rp:${rowProduct},p:${product}], [rt:${rowType},t:${type}], [rq:${rowQuantity},q:${quantity}]`);
                if (rowProduct.toUpperCase() == product.toUpperCase()) {
                    //console.log("3-true");
                    $(this).show();
                }
                else {
                    //console.log("3-false");
                    $(this).hide();
                }
                //console.log("===================================");
            }

            if (product.toUpperCase() == 'ALL' && type.toUpperCase() != 'ALL' && quantity.toUpperCase() != 'ALL') {
                //console.log("4");
                //console.log("product.toUpperCase() == 'ALL' && type.toUpperCase() != 'ALL' && quantity.toUpperCase() != 'ALL'");
                //console.log(`[rp:${rowProduct},p:${product}], [rt:${rowType},t:${type}], [rq:${rowQuantity},q:${quantity}]`);
                if (rowType.toUpperCase() == type.toUpperCase() && rowQuantity == quantity) {
                    //console.log("4-true");
                    $(this).show();
                }
                else {
                    //console.log("4-false");
                    $(this).hide();
                }
                //console.log("===================================");
            }

            if (product.toUpperCase() == 'ALL' && type.toUpperCase() != 'ALL' && quantity.toUpperCase() == 'ALL') {
                //console.log("5");
                //console.log("product.toUpperCase() == 'ALL' && type.toUpperCase() != 'ALL' && quantity.toUpperCase() == 'ALL'");
                //console.log(`[rp:${rowProduct},p:${product}], [rt:${rowType},t:${type}], [rq:${rowQuantity},q:${quantity}]`);
                if (rowType.toUpperCase() == type.toUpperCase()) {
                    //console.log("5-true");
                    $(this).show();
                }
                else {

                    //console.log("5-false");
                    $(this).hide();
                }
                //console.log("===================================");
            }
            if (product.toUpperCase() == 'ALL' && type.toUpperCase() == 'ALL' && quantity.toUpperCase() != 'ALL') {
                //console.log("6");
                //console.log("product.toUpperCase() == 'ALL' && type.toUpperCase() == 'ALL' && quantity.toUpperCase() != 'ALL'");
                //console.log(`[rp:${rowProduct},p:${product}], [rt:${rowType},t:${type}], [rq:${rowQuantity},q:${quantity}]`);
                if (rowQuantity == quantity) {
                    //console.log("6-true");
                    $(this).show();
                }
                else {
                    //console.log("6-false");
                    $(this).hide();
                }
                //console.log("===================================");
            }

        });
    }
}