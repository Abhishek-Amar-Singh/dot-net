﻿using Microsoft.Build.Framework;

namespace Corbet16112022.Mvc.Models
{
    public class GetAllStockTypesModel
    {
        public int StockTypeId { get; set; }
        public string StockTypeName { get; set; }
    }
}
