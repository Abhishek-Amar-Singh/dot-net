﻿using Corbet16112022.Application.Contracts.Persistence;
using Corbet16112022.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corbet16112022.Persistence.Repositories
{
    public class StockTypeRepository : BaseRepository<StockType>, IStockTypeRepository
    {
        public StockTypeRepository(ApplicationDbContext dbContext, ILogger<StockType> logger) : base(dbContext, logger)
        {
        }

        public async Task<bool> CheckStockTypeExists(string stockTypeName)
        {
            var stockTypeProduct = await _dbContext.StockTypes.FirstOrDefaultAsync(s => s.StockTypeName.Equals(stockTypeName));
            if (stockTypeProduct is not null)
            {
                return false;
            }
            else return true;
        }

        public async Task<StockType> GetByIdAsync(int id)
        {
            return await _dbContext.StockTypes.AsNoTracking().FirstOrDefaultAsync(st => st.StockTypeId == id);
        }

        public async Task<StockType> GetByTypeAsync(string stockTypeName)
        {
            return await _dbContext.StockTypes.FirstOrDefaultAsync(st => st.StockTypeName.Equals(stockTypeName));
        }
    }
}
