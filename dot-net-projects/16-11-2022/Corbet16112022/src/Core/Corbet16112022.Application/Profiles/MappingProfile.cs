﻿using AutoMapper;
using Corbet16112022.Application.Features.Categories.Commands.CreateCategory;
using Corbet16112022.Application.Features.Categories.Commands.StoredProcedure;
using Corbet16112022.Application.Features.Categories.Queries.GetCategoriesList;
using Corbet16112022.Application.Features.Categories.Queries.GetCategoriesListWithEvents;
using Corbet16112022.Application.Features.Events.Commands.CreateEvent;
using Corbet16112022.Application.Features.Events.Commands.Transaction;
using Corbet16112022.Application.Features.Events.Commands.UpdateEvent;
using Corbet16112022.Application.Features.Events.Queries.GetEventDetail;
using Corbet16112022.Application.Features.Events.Queries.GetEventsExport;
using Corbet16112022.Application.Features.Events.Queries.GetEventsList;
using Corbet16112022.Application.Features.Orders.GetOrdersForMonth;
using Corbet16112022.Application.Features.Products.Queries.GetAllProducts;
using Corbet16112022.Application.Features.Products.Queries.GetProductByProductId;
using Corbet16112022.Application.Features.Stocks.Commands.AddStock;
using Corbet16112022.Application.Features.Stocks.Commands.DeleteStock;
using Corbet16112022.Application.Features.Stocks.Commands.UpdateStock;
using Corbet16112022.Application.Features.Stocks.Queries.GetAllStocks;
using Corbet16112022.Application.Features.Stocks.Queries.GetStockByStockId;
using Corbet16112022.Application.Features.StockTypes.Commands.AddStockType;
using Corbet16112022.Application.Features.StockTypes.Commands.DeleteStockType;
using Corbet16112022.Application.Features.StockTypes.Commands.UpdateStockType;
using Corbet16112022.Application.Features.StockTypes.Queries.GetAllStockTypes;
using Corbet16112022.Application.Features.StockTypes.Queries.GetStockTypeById;
using Corbet16112022.Domain.Entities;

namespace Corbet16112022.Application.Profiles
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {          
            CreateMap<Event, CreateEventCommand>().ReverseMap();
            CreateMap<Event, TransactionCommand>().ReverseMap();
            CreateMap<Event, UpdateEventCommand>().ReverseMap();
            CreateMap<Event, EventDetailVm>().ReverseMap();
            CreateMap<Event, CategoryEventDto>().ReverseMap();
            CreateMap<Event, EventExportDto>().ReverseMap();

            CreateMap<Category, CategoryDto>();
            CreateMap<Category, CategoryListVm>();
            CreateMap<Category, CategoryEventListVm>();
            CreateMap<Category, CreateCategoryCommand>();
            CreateMap<Category, CreateCategoryDto>();
            CreateMap<Category, StoredProcedureCommand>();
            CreateMap<Category, StoredProcedureDto>();

            CreateMap<Order, OrdersForMonthDto>();

            CreateMap<Event, EventListVm>().ConvertUsing<EventVmCustomMapper>();

            CreateMap<Product, GetAllProductsVm>();
            CreateMap<GetProductByProductIdQuery, Product>();
            CreateMap<Product, GetProductByProductIdVm>();

            CreateMap<Stock, GetAllStocksVm>();
            CreateMap<GetAllStocksVm, GetAllStocksVmOut>();
            CreateMap<AddStockCommand, Stock>();
            CreateMap<Stock, AddStockDto>();

            CreateMap<DeleteStockCommand, Stock>();
            CreateMap<Stock, DeleteStockDto>();

            CreateMap<UpdateStockCommand, Stock>();
            CreateMap<Stock, UpdateStockDto>();

            CreateMap<Stock, GetStockByStockIdVm>();

            CreateMap<GetAllStockTypesQuery, StockType>();
            CreateMap<StockType, GetAllStockTypesVm>();
            CreateMap<AddStockTypeCommand, StockType>();
            CreateMap<StockType, AddStockTypeDto>();
            CreateMap<StockType, GetStockTypeByIdVm>();
            CreateMap<DeleteStockTypeCommand, StockType>();
            CreateMap<StockType, DeleteStockTypeDto>();
            CreateMap<UpdateStockTypeCommand, StockType>();
            CreateMap<StockType, UpdateStockTypeDto>();
        }
    }
}
