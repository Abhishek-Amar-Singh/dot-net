﻿namespace Corbet16112022.Application.Models.Authentication
{
    public class RegistrationResponse
    {
        public string UserId { get; set; }
    }
}
