﻿using Corbet16112022.Application.Models.Mail;
using System.Threading.Tasks;

namespace Corbet16112022.Application.Contracts.Infrastructure
{
    public interface IEmailService
    {
        Task<bool> SendEmail(Email email);
    }
}
