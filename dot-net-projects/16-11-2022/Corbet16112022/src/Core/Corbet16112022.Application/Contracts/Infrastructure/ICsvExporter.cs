﻿using Corbet16112022.Application.Features.Events.Queries.GetEventsExport;
using System.Collections.Generic;

namespace Corbet16112022.Application.Contracts.Infrastructure
{
    public interface ICsvExporter
    {
        byte[] ExportEventsToCsv(List<EventExportDto> eventExportDtos);
    }
}
