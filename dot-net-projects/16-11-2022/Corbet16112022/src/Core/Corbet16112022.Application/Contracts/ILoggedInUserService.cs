﻿namespace Corbet16112022.Application.Contracts
{
    public interface ILoggedInUserService
    {
        public string UserId { get; }
    }
}
