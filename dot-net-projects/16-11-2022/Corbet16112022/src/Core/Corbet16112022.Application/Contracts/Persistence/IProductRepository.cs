﻿using Corbet16112022.Domain.Entities;

namespace Corbet16112022.Application.Contracts.Persistence
{
    public interface IProductRepository : IAsyncRepository<Product>
    {
        Task<Product> GetByIdAsync(int id);
    }
}
