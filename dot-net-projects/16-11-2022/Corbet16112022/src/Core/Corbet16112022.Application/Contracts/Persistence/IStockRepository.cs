﻿using Corbet16112022.Domain.Entities;

namespace Corbet16112022.Application.Contracts.Persistence
{
    public interface IStockRepository : IAsyncRepository<Stock>
    {
        Task<Stock> GetByProductIdAsync(int productId);
        Task<Stock> GetByIdAsync(int stockId);
        Task<bool> CheckProductExistsInStockList(int productId);
    }
}
