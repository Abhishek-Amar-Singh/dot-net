﻿using Corbet16112022.Domain.Entities;
using System.Threading.Tasks;

namespace Corbet16112022.Application.Contracts.Persistence
{
    public interface IMessageRepository : IAsyncRepository<Message>
    {
        public Task<Message> GetMessage(string Code, string Lang);
    }
}
