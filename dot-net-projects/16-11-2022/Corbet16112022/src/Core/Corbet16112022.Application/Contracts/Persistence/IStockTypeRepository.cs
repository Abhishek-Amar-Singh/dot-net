﻿using Corbet16112022.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corbet16112022.Application.Contracts.Persistence
{
    public interface IStockTypeRepository : IAsyncRepository<StockType>
    {
        Task<bool> CheckStockTypeExists(string stockTypeName);
        Task<StockType> GetByIdAsync(int id);
        Task<StockType> GetByTypeAsync(string stockTypeName);
    }
}
