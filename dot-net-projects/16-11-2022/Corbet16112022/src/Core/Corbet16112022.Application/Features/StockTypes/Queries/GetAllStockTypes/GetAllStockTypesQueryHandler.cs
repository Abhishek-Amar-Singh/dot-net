﻿using AutoMapper;
using Corbet16112022.Application.Contracts.Persistence;
using Corbet16112022.Application.Features.Stocks.Queries.GetAllStocks;
using Corbet16112022.Application.Responses;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Corbet16112022.Application.Features.StockTypes.Queries.GetAllStockTypes
{
    public class GetAllStockTypesQueryHandler : IRequestHandler<GetAllStockTypesQuery, Response<List<GetAllStockTypesVm>>>
    {
        private readonly ILogger<GetAllStockTypesQueryHandler> _logger;
        private readonly IMapper _mapper;
        private readonly IStockTypeRepository _stockTypeRepository;

        public GetAllStockTypesQueryHandler(ILogger<GetAllStockTypesQueryHandler> _logger, IMapper _mapper, IStockTypeRepository _stockTypeRepository)
        {
            this._logger = _logger;
            this._mapper = _mapper;
            this._stockTypeRepository = _stockTypeRepository;
        }

        public async Task<Response<List<GetAllStockTypesVm>>> Handle(GetAllStockTypesQuery request, CancellationToken cancellationToken)
        {
            var stockTypeList = await _stockTypeRepository.ListAllAsync();
            var stockTypeListVm = _mapper.Map<List<GetAllStockTypesVm>>(stockTypeList);
            return new Response<List<GetAllStockTypesVm>>() { Data = stockTypeListVm, Succeeded = true };
        }
    }
}
