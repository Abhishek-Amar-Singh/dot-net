﻿using Corbet16112022.Application.Responses;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corbet16112022.Application.Features.StockTypes.Commands.UpdateStockType
{
    public class UpdateStockTypeCommand : IRequest<Response<UpdateStockTypeDto>>
    {
        public int StockTypeId { get; set; }
        public string StockTypeName { get; set; }
    }
}
