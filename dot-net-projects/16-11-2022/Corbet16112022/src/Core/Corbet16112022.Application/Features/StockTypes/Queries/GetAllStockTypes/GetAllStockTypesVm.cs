﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corbet16112022.Application.Features.StockTypes.Queries.GetAllStockTypes
{
    public class GetAllStockTypesVm
    {
        public int StockTypeId { get; set; }
        public string StockTypeName { get; set; }
    }
}
