﻿using Corbet16112022.Application.Responses;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corbet16112022.Application.Features.StockTypes.Queries.GetAllStockTypes
{
    public class GetAllStockTypesQuery : IRequest<Response<List<GetAllStockTypesVm>>> { }
}
