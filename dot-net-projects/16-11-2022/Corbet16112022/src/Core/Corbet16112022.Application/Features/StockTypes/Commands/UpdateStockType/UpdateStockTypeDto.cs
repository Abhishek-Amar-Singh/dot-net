﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corbet16112022.Application.Features.StockTypes.Commands.UpdateStockType
{
    public class UpdateStockTypeDto
    {
        public int StockTypeId { get; set; }
        public string StockTypeName { get; set; }
    }
}
