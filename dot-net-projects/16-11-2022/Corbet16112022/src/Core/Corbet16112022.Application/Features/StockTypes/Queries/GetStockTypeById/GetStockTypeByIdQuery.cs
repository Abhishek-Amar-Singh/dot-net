﻿using Corbet16112022.Application.Responses;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corbet16112022.Application.Features.StockTypes.Queries.GetStockTypeById
{
    public class GetStockTypeByIdQuery : IRequest<Response<GetStockTypeByIdVm>>
    {
        public int StockTypeId { get; set; }
    }
}
