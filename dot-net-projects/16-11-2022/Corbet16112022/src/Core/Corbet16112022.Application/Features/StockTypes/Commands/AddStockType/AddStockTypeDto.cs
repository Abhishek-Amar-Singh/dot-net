﻿namespace Corbet16112022.Application.Features.StockTypes.Commands.AddStockType
{
    public class AddStockTypeDto
    {
        public int StockTypeId { get; set; }
        public string StockTypeName { get; set; }
    }
}
