﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corbet16112022.Application.Features.StockTypes.Queries.GetStockTypeById
{
    public class GetStockTypeByIdVm
    {
        public int StockTypeId { get; set; }
        public string StockTypeName { get; set; }
    }
}
