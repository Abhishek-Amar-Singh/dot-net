﻿using Corbet16112022.Application.Responses;
using MediatR;
using System.Collections.Generic;

namespace Corbet16112022.Application.Features.Events.Queries.GetEventsList
{
    public class GetEventsListQuery: IRequest<Response<IEnumerable<EventListVm>>>
    {

    }
}
