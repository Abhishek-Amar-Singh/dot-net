﻿using Corbet16112022.Application.Responses;
using MediatR;
using System;

namespace Corbet16112022.Application.Features.Events.Queries.GetEventDetail
{
    public class GetEventDetailQuery: IRequest<Response<EventDetailVm>>
    {
        public string Id { get; set; }
    }
}
