﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corbet16112022.Application.Features.Stocks.Queries.GetAllStocks
{
    public class GetAllStocksVmOut
    {
        public int StockId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public string StockTypeName { get; set; }
        public DateTime TimeIn { get; set; }
        public DateTime? TimeOut { get; set; }
    }
}
