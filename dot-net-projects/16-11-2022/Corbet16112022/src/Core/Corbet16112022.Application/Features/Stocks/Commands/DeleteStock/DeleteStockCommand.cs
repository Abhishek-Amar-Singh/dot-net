﻿using Corbet16112022.Application.Responses;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corbet16112022.Application.Features.Stocks.Commands.DeleteStock
{
    public class DeleteStockCommand : IRequest<Response<DeleteStockDto>>
    {
        public int StockId { get; set; }
    }
}
