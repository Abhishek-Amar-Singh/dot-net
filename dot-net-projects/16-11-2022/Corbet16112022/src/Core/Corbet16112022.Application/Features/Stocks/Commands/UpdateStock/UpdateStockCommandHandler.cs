﻿using AutoMapper;
using Corbet16112022.Application.Contracts.Persistence;
using Corbet16112022.Application.Features.Stocks.Commands.DeleteStock;
using Corbet16112022.Application.Responses;
using Corbet16112022.Domain.Entities;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corbet16112022.Application.Features.Stocks.Commands.UpdateStock
{
    public class UpdateStockCommandHandler : IRequestHandler<UpdateStockCommand, Response<UpdateStockDto>>
    {
        private readonly ILogger<UpdateStockCommandHandler> _logger;
        private readonly IMapper _mapper;
        private readonly IStockRepository _stockRepository;

        public UpdateStockCommandHandler(ILogger<UpdateStockCommandHandler> _logger, IMapper _mapper, IStockRepository _stockRepository)
        {
            this._logger = _logger;
            this._mapper = _mapper;
            this._stockRepository = _stockRepository;
        }

        public async Task<Response<UpdateStockDto>> Handle(UpdateStockCommand request, CancellationToken cancellationToken)
        {
            var stock = _mapper.Map<Stock>(request);
            var stockData = await _stockRepository.GetByIdAsync(request.StockId);
            if (stockData is not null)
            {
                stockData = _mapper.Map<Stock>(request);
                await _stockRepository.UpdateAsync(stockData);
                var stockDto = _mapper.Map<UpdateStockDto>(stockData);
                return new Response<UpdateStockDto>() { Data = stockDto, Succeeded = true };
            }
            else
            {
                return new Response<UpdateStockDto>() { Errors = new List<string>() { "404", "Not Found", "Doesn't Exists." }, Succeeded = false };
            }
        }
    }
}
