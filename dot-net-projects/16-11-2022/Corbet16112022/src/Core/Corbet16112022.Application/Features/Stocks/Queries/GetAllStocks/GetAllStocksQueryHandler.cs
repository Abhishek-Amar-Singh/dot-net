﻿using AutoMapper;
using Corbet16112022.Application.Contracts.Persistence;
using Corbet16112022.Application.Features.Stocks.Commands.AddStock;
using Corbet16112022.Application.Responses;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corbet16112022.Application.Features.Stocks.Queries.GetAllStocks
{
    public class GetAllStocksQueryHandler : IRequestHandler<GetAllStocksQuery, Response<List<GetAllStocksVmOut>>>
    {
        private readonly ILogger<GetAllStocksQueryHandler> _logger;
        private readonly IMapper _mapper;
        private readonly IStockRepository _stockRepository;
        private readonly IProductRepository _productRepository;
        private readonly IStockTypeRepository _stockTypeRepository;

        public GetAllStocksQueryHandler(ILogger<GetAllStocksQueryHandler> _logger, IMapper _mapper, IStockRepository _stockRepository, IProductRepository _productRepository, IStockTypeRepository _stockTypeRepository)
        {
            this._logger = _logger;
            this._mapper = _mapper;
            this._stockRepository = _stockRepository;
            this._productRepository = _productRepository;
            this._stockTypeRepository = _stockTypeRepository;
        }

        public async Task<Response<List<GetAllStocksVmOut>>> Handle(GetAllStocksQuery request, CancellationToken cancellationToken)
        {
            var stockList = await _stockRepository.ListAllAsync();
            var stockListVm = _mapper.Map<List<GetAllStocksVm>>(stockList);
            foreach(var stock in stockListVm)
            {
                stock.ProductName = _productRepository.GetByIdAsync(stock.ProductId).Result.ProductTitle;
                stock.StockTypeName = _stockTypeRepository.GetByIdAsync(stock.StockTypeId).Result.StockTypeName;
            }
            var stockListVmOut = _mapper.Map<List<GetAllStocksVmOut>>(stockListVm);
            return new Response<List<GetAllStocksVmOut>>() { Data = stockListVmOut, Succeeded = true };
        }
    }
}
