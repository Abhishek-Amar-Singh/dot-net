﻿using Corbet16112022.Application.Responses;
using MediatR;

namespace Corbet16112022.Application.Features.Products.Commands.CheckProductExistsInStockList
{
    public class CheckProductExistsInStockListCommand : IRequest<Response<bool>>
    {
        public int ProductId { get; set; }
    }
}
