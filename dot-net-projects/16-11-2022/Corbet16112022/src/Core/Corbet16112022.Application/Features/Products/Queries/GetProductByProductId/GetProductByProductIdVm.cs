﻿using Corbet16112022.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corbet16112022.Application.Features.Products.Queries.GetProductByProductId
{
    public class GetProductByProductIdVm
    {
        public int ProductId { get; set; }
        public string? ProductTitle { get; set; }
        public int UnitMeasurementId { get; set; }
        public virtual int ProductCategoryId { get; set; }
        public virtual int ProductSubCategoryId { get; set; }
        public int Quantity { get; set; }
        public bool TaxApplicable { get; set; }
        public string Status { get; set; } = null!;
    }
}
