﻿using AutoMapper;
using Corbet16112022.Application.Contracts.Persistence;
using Corbet16112022.Application.Responses;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Corbet16112022.Application.Features.Products.Queries.GetAllProducts
{
    public class GetAllProductsQueryHandler : IRequestHandler<GetAllProductsQuery, Response<List<GetAllProductsVm>>>
    {
        private readonly ILogger<GetAllProductsQueryHandler> _logger;
        private readonly IMapper _mapper;
        private readonly IProductRepository _productRepository;

        public GetAllProductsQueryHandler(ILogger<GetAllProductsQueryHandler> _logger, IMapper _mapper, IProductRepository _productRepository)
        {
            this._logger = _logger;
            this._mapper = _mapper;
            this._productRepository = _productRepository;
        }

        public async Task<Response<List<GetAllProductsVm>>> Handle(GetAllProductsQuery request, CancellationToken cancellationToken)
        {
            var productList = await _productRepository.ListAllAsync();
            var productListVm = _mapper.Map<List<GetAllProductsVm>>(productList);
            return new Response<List<GetAllProductsVm>>() { Data = productListVm, Succeeded = true };
        }
    }
}
