﻿using AutoMapper;
using Corbet16112022.Application.Contracts.Persistence;
using Corbet16112022.Application.Features.Stocks.Commands.AddStock;
using Corbet16112022.Application.Responses;
using Corbet16112022.Domain.Entities;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corbet16112022.Application.Features.Products.Queries.GetProductByProductId
{
    public class GetProductByProductIdQueryHandler : IRequestHandler<GetProductByProductIdQuery, Response<GetProductByProductIdVm>>
    {
        private readonly ILogger<GetProductByProductIdQueryHandler> _logger;
        private readonly IMapper _mapper;
        private readonly IProductRepository _productRepository;

        public GetProductByProductIdQueryHandler(ILogger<GetProductByProductIdQueryHandler> _logger, IMapper _mapper, IProductRepository _productRepository)
        {
            this._logger = _logger;
            this._mapper = _mapper;
            this._productRepository = _productRepository;
        }
        public async Task<Response<GetProductByProductIdVm>> Handle(GetProductByProductIdQuery request, CancellationToken cancellationToken)
        {
            var product = _mapper.Map<Product>(request);
            var productData = await _productRepository.GetByIdAsync(product.ProductId);
            if (productData is not null)
            {
                var productVm = _mapper.Map<GetProductByProductIdVm>(productData);
                return new Response<GetProductByProductIdVm>() { Data = productVm, Succeeded = true };
            }
            else
            {
                return new Response<GetProductByProductIdVm>() { Errors = new List<string>() { "404", "Not Found", $"Product having id '{request.ProductId}' does not exists." }, Succeeded = false };
            }
        }
    }
}
