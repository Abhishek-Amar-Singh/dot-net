﻿using Corbet16112022.Application.Responses;
using MediatR;

namespace Corbet16112022.Application.Features.Categories.Commands.StoredProcedure
{
    public class StoredProcedureCommand: IRequest<Response<StoredProcedureDto>>
    {
        public string Name { get; set; }
    }
}
