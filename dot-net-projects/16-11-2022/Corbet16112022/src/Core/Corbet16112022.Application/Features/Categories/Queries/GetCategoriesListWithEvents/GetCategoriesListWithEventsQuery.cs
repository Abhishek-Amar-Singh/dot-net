﻿using Corbet16112022.Application.Responses;
using MediatR;
using System.Collections.Generic;

namespace Corbet16112022.Application.Features.Categories.Queries.GetCategoriesListWithEvents
{
    public class GetCategoriesListWithEventsQuery: IRequest<Response<IEnumerable<CategoryEventListVm>>>
    {
        public bool IncludeHistory { get; set; }
    }
}
