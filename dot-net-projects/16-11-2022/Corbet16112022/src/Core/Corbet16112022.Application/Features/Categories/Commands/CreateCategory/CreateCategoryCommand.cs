﻿using Corbet16112022.Application.Responses;
using MediatR;

namespace Corbet16112022.Application.Features.Categories.Commands.CreateCategory
{
    public class CreateCategoryCommand: IRequest<Response<CreateCategoryDto>>
    {
        public string Name { get; set; }
    }
}
