﻿using Corbet16112022.Application.Responses;
using MediatR;
using System.Collections.Generic;

namespace Corbet16112022.Application.Features.Categories.Queries.GetCategoriesList
{
    public class GetCategoriesListQuery : IRequest<Response<IEnumerable<CategoryListVm>>>
    {
    }
}
