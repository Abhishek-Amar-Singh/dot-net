﻿using System;

namespace Corbet16112022.Application.Features.Categories.Commands.StoredProcedure
{
    public class StoredProcedureDto
    {
        public Guid CategoryId { get; set; }
        public string Name { get; set; }
    }
}
