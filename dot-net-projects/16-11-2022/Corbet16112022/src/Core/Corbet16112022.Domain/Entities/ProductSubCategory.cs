﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Corbet16112022.Domain.Common;

namespace Corbet16112022.Domain.Entities
{
    public class ProductSubCategory : AuditableEntity
    {
        //Id, Name, ProductCategoryId,  description, Status, taxApplicable
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductSubCategoryId { get; set; }

        [Column(TypeName = "varchar(35)")]
        public string Name { get; set; } = null!;

        public virtual int ProductCategoryId { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string? Description { get; set; }

        public string Status { get; set; } = null!;

        public bool TaxApplicable { get; set; }


        [ForeignKey("ProductCategoryId")]
        public ProductCategory ProductCategories { get; set; }
    }
}
