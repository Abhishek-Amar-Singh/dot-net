﻿using Corbet16112022.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corbet16112022.Domain.Entities
{
    public class StockType : AuditableEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int StockTypeId { get; set; }

        [Column(TypeName = "varchar(40)")]
        public string StockTypeName { get; set; } = null!;
    }
}
