﻿using Corbet16112022.Domain.Common;
using Corbet16112022.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Corbet16112022.Domain.Entities
{
    public class Product : AuditableEntity
    {
        //PrductId(varchar, PK), ProductTitle, UnitMeasurementId, ProductCategoryId, ProductSubCategoryId, Quantity,
        //SupplierIds(only dropdown in UI, no column in database since M2M),
        //TaxApplicable(if checked/true in ProductSubCategory), Status

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductId { get; set; }

        [Column(TypeName = "varchar(35)")]
        public string? ProductTitle { get; set; }


        [ForeignKey("Id")]
        public int UnitMeasurementId { get; set; }
        public virtual UnitMeasurement UnitMeasurements { get; set; }


        public virtual int ProductCategoryId { get; set; }

        public virtual int ProductSubCategoryId { get; set; }

        public int Quantity { get; set; }

        public bool TaxApplicable { get; set; }

        public string Status { get; set; } = null!;


        [ForeignKey("ProductCategoryId")]
        public virtual ProductCategory ProductCategories { get; set; }
        [ForeignKey("ProductSubCategoryId")]
        public virtual ProductSubCategory ProductSubCategories { get; set; }

    }
}
