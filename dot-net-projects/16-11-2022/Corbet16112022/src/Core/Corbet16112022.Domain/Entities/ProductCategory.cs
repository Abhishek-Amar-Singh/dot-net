﻿using Corbet16112022.Domain.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Corbet16112022.Domain.Entities
{
    public class ProductCategory : AuditableEntity
    {
        //Id, Name, description, Status
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductCategoryId { get; set; }

        [Column(TypeName ="varchar(35)")]
        public string Name { get; set; } = null!;

        [Column(TypeName = "varchar(300)")]
        public string? Description { get; set; }

        public string Status { get; set; } = null!;
    }
}
