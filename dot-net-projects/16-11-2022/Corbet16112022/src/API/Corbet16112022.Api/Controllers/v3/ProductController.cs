﻿using Corbet16112022.Application.Features.Products.Commands.CheckProductExistsInStockList;
using Corbet16112022.Application.Features.Products.Queries.GetAllProducts;
using Corbet16112022.Application.Features.Products.Queries.GetProductByProductId;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Corbet16112022.Api.Controllers.v3
{
    [ApiVersion("3")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ProductController(IMediator _mediator)
        {
            this._mediator = _mediator;
        }
        [Route("GetAllProducts")]
        [HttpGet]
        public async Task<IActionResult> GetAllProducts()
        {
            var response = await _mediator.Send(new GetAllProductsQuery());
            return Ok(response);
        }

        [Route("GetProductByProductId")]
        [HttpGet]
        public async Task<IActionResult> GetProductByProductId(int id)
        {
            var response = await _mediator.Send(new GetProductByProductIdQuery() { ProductId = id });
            return (response.Succeeded) ? Ok(response) : NotFound(response);
        }
    }
}
