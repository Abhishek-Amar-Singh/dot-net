﻿using Corbet16112022.Application.Contracts;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace Corbet16112022.Api.Services
{
    public class LoggedInUserService:ILoggedInUserService
    {
        public LoggedInUserService(IHttpContextAccessor httpContextAccessor)
        {
            UserId = httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier);
        }

        public string UserId { get; }
    }
}
