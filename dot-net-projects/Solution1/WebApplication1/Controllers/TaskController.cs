﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections;
using System.Data;
using WebApplication1.Repository;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        [Route("GetTotalOfEvenNumbers")]
        [HttpGet]
        public int GetTotalOfEvenNumbers()
        {
            var values = new int[10] { 10, 18, 33, 56, 57, 117, 87, 90, 99, 78 };
            return values.Where(v => Math.DivRem(v, 2).Remainder is 0).Sum();
        }


        [Route("PrintAlphaNumerics")]
        [HttpGet]
        public Dictionary<int, dynamic> PrintAlphaNumerics()
        {
            var dict = new Dictionary<int, dynamic>();

            for (var i = 1; i <= 100; i++)
            {
                if ((Math.DivRem(i, 3).Remainder == 0) && (Math.DivRem(i, 5).Remainder != 0))
                {
                    dict.Add(i, "valar");
                }
                else if ((Math.DivRem(i, 5).Remainder == 0) && (Math.DivRem(i, 3).Remainder != 0))
                {
                    dict.Add(i, "morghulis");
                }
                else if ((Math.DivRem(i, 3).Remainder is 0) && (Math.DivRem(i, 5).Remainder is 0))
                {
                    dict.Add(i, "valar morghulis");
                }
                else
                {
                    dict.Add(i, $"{i}");
                }
            }
            return dict;
        }


        [Route("Swap2NumbersWithout3rdVariable")]
        [HttpGet]
        public string Swap2NumbersWithout3rdVariable(int num1, int num2)
        {
            num1 += num2;
            num2 = num1 - num2;
            num1 -= num2;
            return $"Swapping two numbers:: num1={num1} and num2={num2}";
        }


        [Route("NthFibonacciSeries")]
        [HttpGet]
        public IList<int> NthFibonacciSeries(int n)
        {
            IList<int> fibSeries = new List<int>(n);
            int f0 = 0, f1 = 1;
            int f = f0 + f1;
            fibSeries.Add(f0);
            fibSeries.Add(f1);
            fibSeries.Add(f);
            while (n > 3)
            {
                f0 = f1;
                f1 = f;
                f = f0 + f1;
                n--;
                fibSeries.Add(f);
            }
            return fibSeries;
        }


        [Route("GetCircumferenceOfCircle")]
        [HttpGet]
        public double GetCircumferenceOfCircle()
        {
            Circle circle = new Circle();
            return circle.Calculate(CircumferenceOfCircle);
        }
        private double CircumferenceOfCircle(double r)
        {
            return (2 * Math.PI * r);
        }

        [Route("IterateThroughEnum")]
        [HttpGet]
        public IDictionary<string, int> IterateThroughEnum()
        {
            var c = new Dictionary<string, int>();
            Type typeOfColors = typeof(Colors);
            foreach (int i in Enum.GetValues(typeOfColors))
            {
                c.Add(Enum.GetName(typeOfColors, i), i);
            }
            return c;
        }


        [Route("GetAreas")]
        [HttpGet]
        public object GetArea()
        {
            Shape shape = new Shape();
            var areaOfSquare = shape.Area(7);
            var areaOfRectangle = shape.Area(3.73, 8);
            var areas = new
            {
                AreaOfSquare = areaOfSquare,
                AreaOfRectangle = areaOfRectangle
            };
            return areas;
        }

        [Route("BatsmanWithBestBattingMomentum")]
        [HttpGet]
        public IEnumerable<Batsman> BatsmanWithBestBattingMomentum()
        {
            var batsmen = new Batsman[]
            {
                new Batsman{ BatsmanName = "Virat Kohli", RunsScored = 50, StrikeRate = 78.30f },
                new Batsman{ BatsmanName = "M. S. Dhoni", RunsScored = 61, StrikeRate = 58.90f },
                new Batsman{ BatsmanName = "Rohit Sharma", RunsScored = 13, StrikeRate = 124.0f }
            };
            foreach (var batsman in batsmen)
            {
                batsman.Momentum = batsman.RunsScored * batsman.StrikeRate;
            }
            var batsmanWithBestBattingMomentum = (from b in batsmen
                                                  select b).Where(x => x.Momentum == batsmen.Max(y => y.Momentum));
            return batsmanWithBestBattingMomentum;
        }


        [Route("UpdateIsRetiredInversely")]
        [HttpGet]
        public IList<Player> UpdateIsRetiredInversely()
        {
            IList<Player> players = new List<Player>()
            {
                new Player() { PlayerName = "Virat Kholi", IsRetired = 1},
                new Player() { PlayerName = "M.S. Dhoni", IsRetired = 1},
                new Player() { PlayerName = "Hardik Pandya", IsRetired = 1},
                new Player() { PlayerName = "Rohit Sharma", IsRetired = 1},
                new Player() { PlayerName = "Sachin Tendulkar", IsRetired = 0},
                new Player() { PlayerName = "Rahul Dravid", IsRetired = 0},
                new Player() { PlayerName = "Sourav Ganguly", IsRetired = 0},
                new Player() { PlayerName = "VVS Laxman", IsRetired = 0}
            };
            players.ToList().ForEach(p =>
            {
                p.IsRetired = (p.IsRetired == 1) ? 0 : 1;
            });
            return players;
        }


        [Route("SerializeObjectToJson")]
        [HttpGet]
        public IActionResult SerializeObjectToJson()
        {
            var player1 = new _Player() { Id = 5830, Postion = 1, Name_Full = "David Warner", IsCaptain = true };
            var player2 = new _Player() { Id = 3722, Postion = 2, Name_Full = "Shikhar Dhawan", IsCaptain = false };
            var dict_players = new Dictionary<int, _Player>();
            dict_players.Add(player1.Id, player1);
            dict_players.Add(player2.Id, player2);

            var teamSRH = new Team()
            {
                Name_Full = "Sunrisers Hyderabad",
                Name_Short = "SRH",
                Players = dict_players
            };

            var serializedJson = JsonConvert.SerializeObject(teamSRH);
            return Ok(serializedJson);
        }


        [Route("WicketsTakenByPlayersLINQ")]
        [HttpGet]
        public IActionResult WicketsTakenByPlayersLINQ()
        {
            var lineups = new[]
            {
                new{PlayerId=21, PlayerName="Y. Chahal" },
                new {PlayerId = 22,PlayerName= "Bhuvneshwar Kumar"},
                new{PlayerId = 23,PlayerName= "Jasprit Bumrah"},
                new{PlayerId = 24,PlayerName= "Hardik Pandya"},
                new { PlayerId = 25,PlayerName= "Ravindra Jadeja"},
                new { PlayerId = 26,PlayerName="Mohammed Shami"}
            };
            var bowlingStats = new[]
            {
                new {PlayerId = 21, Wickets = 2},
                new{PlayerId = 22, Wickets= 1},
                new{PlayerId = 23, Wickets= 3},
                new { PlayerId = 26, Wickets= 1}
            };

            var query = from l in lineups
                        join b in bowlingStats
                        on l.PlayerId equals b.PlayerId
                        select new
                        {
                            PlayerId = l.PlayerId,
                            PlayerName = l.PlayerName,
                            Wickets = b.Wickets
                        };
            return Ok(query);
        }
    }
}
