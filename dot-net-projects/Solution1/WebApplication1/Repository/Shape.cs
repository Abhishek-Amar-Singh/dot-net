﻿namespace WebApplication1.Repository
{
    public class Shape
    {
        public double Area(double side)
        {
            return side * side;
        }
        public double Area(double length, double width)
        {
            return length * width;
        }
    }
}
