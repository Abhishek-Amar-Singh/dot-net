﻿namespace WebApplication1.Repository
{
    public class Player
    {
        public string PlayerName { get; set; } = null!;
        public int IsRetired { get; set; }
    }
}
