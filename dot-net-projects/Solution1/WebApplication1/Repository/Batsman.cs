﻿namespace WebApplication1.Repository
{
    public class Batsman
    {
        public string BatsmanName { get; set; } = null!;
        public int RunsScored { get; set; }
        public float StrikeRate { get; set; }
        public float Momentum { get; set; }
    }
}
