﻿namespace WebApplication1.Repository
{
    public class Team
    {
        public string? Name_Full { get; set; }
        public string? Name_Short { get; set; }

        public Dictionary<int, _Player>? Players { get; set; }
    }
}
