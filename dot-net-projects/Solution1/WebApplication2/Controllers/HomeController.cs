﻿using Microsoft.AspNetCore.Mvc;
using WebApplication2.Models;
using WebApplication2.Repository;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHomeRepository homeRepository;
        private static string oldEmailAddress = String.Empty;

        public HomeController(IHomeRepository homeRepository)
        {
            this.homeRepository = homeRepository;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var employeeList = this.homeRepository.GetAll();
            return View(employeeList);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Add(Employee employee)
        {
            homeRepository.Insert(employee);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(string email)
        {
            homeRepository.Delete(email);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(string email)
        {
            var employee = this.homeRepository.Get(email);
            oldEmailAddress = email;
            return View(employee);
        }

        [HttpPost]
        public IActionResult Edit(Employee employee)
        {
            homeRepository.Update(employee, oldEmailAddress);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Details(string email)
        {
            var employee = homeRepository.Get(email);
            return View(employee);
        }

        public IActionResult Privacy()
        {
            return View();
        }
    }
}