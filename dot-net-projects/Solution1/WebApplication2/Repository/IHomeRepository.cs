﻿using WebApplication2.Models;

namespace WebApplication2.Repository
{
    public interface IHomeRepository
    {
        void Delete(string email);

        List<Employee> GetAll();

        Employee Get(string email);

        void Insert(Employee employee);

        void Update(Employee employee, string oldEmailAddress);
    }
}
