﻿using System.Data.SqlClient;
using WebApplication2.Models;

namespace WebApplication2.Repository
{
    public class HomeRepository : IHomeRepository
    {
        string connectionStr = "Data source=NEOLAP000001915;Database=employeedb;Integrated security=true";

        public void Delete(string email)
        {
            SqlConnection con = new SqlConnection(connectionStr);
            string query = "DELETE FROM employee WHERE email=@email;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@email", email);
            con.Open();
            int insertionResult = cmd.ExecuteNonQuery();
            con.Close();
        }

        public Employee Get(string email)
        {
            SqlConnection con = new SqlConnection(connectionStr);
            string query = "SELECT * FROM employee WHERE email=@email;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@email", email);
            con.Open();
            var employee = new Employee();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    employee.Email = reader.GetValue(0).ToString();
                    employee.FirstName = reader.GetValue(1).ToString();
                    employee.LastName = reader.GetValue(2).ToString();
                    employee.Gender = reader.GetValue(3).ToString();
                }
            }
            con.Close();
            return employee;
        }

        public List<Employee> GetAll()
        {
            SqlConnection con = new SqlConnection(connectionStr);
            string query = "SELECT * FROM employee;";
            SqlCommand cmd = new SqlCommand(query, con);//Execute query on con
            con.Open();
            var employeeList = new List<Employee>();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    employeeList.Add(new Employee
                    {
                        Email = reader.GetValue(0).ToString(),
                        FirstName = reader.GetValue(1).ToString(),
                        LastName = reader.GetValue(2).ToString(),
                        Gender = reader.GetValue(3).ToString()
                    });
                }
            }
            con.Close();
            return employeeList;
        }

        public void Insert(Employee employee)
        {
            SqlConnection con = new SqlConnection(connectionStr);
            string query = "INSERT INTO employee VALUES(@email,@first_name,@last_name,@gender);";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@email", employee.Email);
            cmd.Parameters.AddWithValue("@first_name", employee.FirstName);
            cmd.Parameters.AddWithValue("@last_name", employee.LastName);
            cmd.Parameters.AddWithValue("@gender", employee.Gender);
            con.Open();
            int insertionResult = cmd.ExecuteNonQuery();
            con.Close();
            if (insertionResult > 0)
            {
                Console.WriteLine("Data Inserted Successfully");
            }
            else
            {
                Console.WriteLine("Insertion Failed");
            }
        }

        public void Update(Employee employee, string oldEmailAddress)
        {
            SqlConnection con = new SqlConnection(connectionStr);
            string query = "UPDATE employee SET Email=@email,FirstName=@first_name,LastName=@last_name,Gender=@gender WHERE email=@old_email_address;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@old_email_address", oldEmailAddress);
            cmd.Parameters.AddWithValue("@email", employee.Email);
            cmd.Parameters.AddWithValue("@first_name", employee.FirstName);
            cmd.Parameters.AddWithValue("@last_name", employee.LastName);
            cmd.Parameters.AddWithValue("@gender", employee.Gender);
            con.Open();
            int insertionResult = cmd.ExecuteNonQuery();
            con.Close();
        }
    }
}
