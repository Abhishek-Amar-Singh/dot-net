﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal class Singleton
    {
        private static int counter = 0;

        public Singleton()
        {
            counter++;
            Console.WriteLine($"counter value is:: {counter}");
        }

        public void myDetails(string msg)
        {
            Console.WriteLine(msg);
        }
    }
}
