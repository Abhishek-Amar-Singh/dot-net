﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    internal sealed class Singleton
    {
        private static int counter = 0;
        static Singleton instance = null;
        public static Singleton getInstance
        {
            get
            {
                if(instance is null)
                {
                    instance = new Singleton();
                }
                return instance;
            }
        }

        private Singleton()
        {
            counter++;
            Console.WriteLine($"the value of counter is:: {counter}");
        }

        public void myDetails(string msg)
        {
            Console.WriteLine(msg);
        }
    }
}
