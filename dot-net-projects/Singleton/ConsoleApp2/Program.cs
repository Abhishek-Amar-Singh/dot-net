﻿using ConsoleApp2;

Singleton formClass1 = Singleton.getInstance;
formClass1.myDetails($"this is formClass1");
Singleton formClass2 = Singleton.getInstance;
formClass1.myDetails("this is formClass2");

/*
 * IMPLEMENTATION OF SINGLETON
 * Ensures that only one instance of a class exists
 * Provides global access to that instance by::
   -> Declaring all constructors of a class as private
   -> Providing static method that returns a reference to that instance
   -> Instance is stored as a private static variable
*/