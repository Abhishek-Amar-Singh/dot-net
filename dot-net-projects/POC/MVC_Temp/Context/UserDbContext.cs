﻿using Microsoft.EntityFrameworkCore;
using MVC_Temp.Models;

namespace MVC_Temp.Context
{
    public class UserDbContext : DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext> dbContextOptions) : base(dbContextOptions)
        {
            //Database.EnsureCreated();//not follow this approach, instead do migration
        }


        //Table creation
        public DbSet<User> UserTempist { get; set; }
    }
}
