﻿using System.Text;

namespace MVC_Temp.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; } = null!;
        public string? OTP { get; set; }
    }
}
