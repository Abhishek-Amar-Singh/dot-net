﻿using MailKit.Security;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using MimeKit.Text;
using MVC_Temp.Context;
using MVC_Temp.Models;
using System.Text;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;

namespace MVC_Temp.Controllers
{
    public class UserController : Controller
    {
        private readonly UserDbContext userDbContext;
        private readonly IConfiguration _configuration;
        static string cOTP;
        static bool flagToGenerateOTP = true;
        public UserController(UserDbContext userDbContext, IConfiguration _configuration)
        {
            this.userDbContext = userDbContext;
            this._configuration = _configuration;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(User user)
        {
            User user1 = userDbContext.UserTempist.FirstOrDefault(u => u.Email == user.Email);
            if(user1 is null)
            {
                userDbContext.UserTempist.Add(user);
                userDbContext.SaveChanges();
            }
            else
            {
                user.Id = user1.Id;
            }

            return RedirectToAction("Red", user);
        }

        [HttpGet]
        public IActionResult Red(User user)
        {
            if(flagToGenerateOTP is true)
            {
                StringBuilder sb = new StringBuilder("");
                Random rnd = new Random();
                for (int j = 0; j < 4; j++)
                {
                    sb.Append(rnd.Next());
                }
                user.OTP = sb.ToString().Substring(3, 4);
                cOTP = user.OTP;
                userDbContext.Entry<User>(user).State = EntityState.Modified;
                userDbContext.SaveChanges();

                // Send this generated otp to email address and store it into database
                var email = new MimeMessage();
                email.From.Add(MailboxAddress.Parse(_configuration.GetSection("UserEmail").Value));
                email.To.Add(MailboxAddress.Parse(user.Email));
                email.Subject = "OTP Mailed Here";
                email.Body = new TextPart(TextFormat.Text) { Text = user.OTP };
                var smtp = new SmtpClient();
                smtp.Connect(_configuration.GetSection("EmailHost").Value, 587, SecureSocketOptions.StartTls);//host and port
                smtp.Authenticate(_configuration.GetSection("UserEmail").Value, _configuration.GetSection("EmailPassword").Value);
                smtp.Send(email);
                smtp.Disconnect(true);

                flagToGenerateOTP = false;
            }
            return View();
        }

        [HttpPost]
        public IActionResult Red(IFormCollection fc)
        {
            if (cOTP == fc["OTP"])
            {
                return RedirectToAction("Dashboard");
            }
            else
            {
                return RedirectToAction("WrongOTP");
            }
        }

        [HttpGet]
        public IActionResult Dashboard()
        {
            return View();
        }

        [HttpGet]
        public IActionResult WrongOTP()
        {
            return View();
        }
    }
}
//tejaspawar1320@gmail.com