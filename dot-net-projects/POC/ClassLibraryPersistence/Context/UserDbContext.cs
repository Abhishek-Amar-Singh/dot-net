﻿//--Generic Repository Pattern
using ClassLibraryPersistence.Models;
using Microsoft.EntityFrameworkCore;

namespace ClassLibraryPersistence.Context
{
    public class UserDbContext : DbContext
    {
        //Table creation
        public DbSet<User> UserList { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=DESKTOP-28COP13;Database=paperdb;Trusted_Connection=True");
            }
        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<User>().HasKey(user => new {user.Id, user.EmailAddress});
        //}
    }
}
