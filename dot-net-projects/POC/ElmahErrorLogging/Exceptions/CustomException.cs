﻿namespace APIVersioning.Exceptions
{
    public class CustomException : ApplicationException
    {
        public CustomException()
        {

        }

        public CustomException(string msg) : base(msg)
        {

        }
    }
}
