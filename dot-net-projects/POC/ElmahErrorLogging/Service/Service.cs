﻿using APIVersioning.Exceptions;

namespace APIVersioning.Service
{
    public class Service : IService
    {
        public void CustomService()
        {
            throw new CustomException("This is just a custom exception for elmah.");
        }
    }
}
