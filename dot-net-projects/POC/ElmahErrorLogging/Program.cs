using APIVersioning.Service;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IService, Service>();

//builder.Services.AddElmahIo(o =>
//{
//    o.ApiKey = "api_key";
//    o.LogId = new Guid("log_id");
//});

// ApiKey and LogId can be configured in appsettings.json as well, by calling the Configure-method instead of AddElmahIo.
builder.Services.Configure<Elmah.Io.AspNetCore.ElmahIoOptions>(builder.Configuration.GetSection("ElmahIo"));
// Still need to call this to register all dependencies
builder.Services.AddElmahIo();

// If you configure ApiKey and LogId through appsettings.json, you can still add event handlers, configure handled status codes, etc.
builder.Services.Configure<Elmah.Io.AspNetCore.ElmahIoOptions>(o =>
{
    o.OnMessage = msg =>
    {
        msg.Version = "6.0.0";
    };
});

var app = builder.Build();


app.UseElmahIo();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
