﻿using APIVersioning.Exceptions;
using APIVersioning.Service;
using Elmah.Io.AspNetCore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APIVersioning.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IService _service;
        public HomeController(IService _service)
        {
            this._service = _service;
        }

        [Route("Index")]
        [HttpGet]
        public void Index()
        {
            try
            {
                _service.CustomService();
            }
            catch (CustomException e)
            {
                // Either use the Ship extension method
                //e.Ship(HttpContext);

                // Or the Log method on ElmahIoApi
                ElmahIoApi.Log(e, HttpContext);
            }
            catch (Exception e)
            {
                // Either use the Ship extension method
                //e.Ship(HttpContext);

                // Or the Log method on ElmahIoApi
                ElmahIoApi.Log(e, HttpContext);
            }
        }

    }
}
