﻿using AutoMapper;
using ClassLibraryPersistence.Models;
using Microsoft.AspNetCore.Mvc;
using MVC_WebAPP2.Models;
using MVC_WebAPP2.Service;

namespace MVC_WebAPP2.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        public UserController(IUserService _userService)
        {
            this._userService = _userService;
        }

        [HttpGet]
        public IActionResult GetAllUsers()
        {
            List<UserViewModel> userList = _userService.GetAllUsers();
            return View(userList);
        }

        [HttpGet]
        public IActionResult RegisterUser()
        {
            return View();
        }

        [HttpPost]
        public IActionResult RegisterUser(UserViewModel user)
        {
            var registerUserStatus = _userService.RegisterUser(user);
            return RedirectToAction("GetAllUsers");
        }

        [HttpGet]
        public IActionResult UserDetails(string id)
        {
            UserViewModel user = _userService.GetUserByEmailId(id);
            return View(user);
        }

        public IActionResult DeleteUser(string id)
        {
            _userService.DeleteUser(id);
            return RedirectToAction("GetAllUsers");
        }

        [HttpGet]
        public IActionResult EditUser(int id)
        {
            UserViewModel user = _userService.GetUserById(id);
            return View(user);
        }

        [HttpPost]
        public IActionResult EditUser(UserViewModel user)
        {
            _userService.EditUser(user);
            return RedirectToAction("GetAllUsers");
        }
    }
}