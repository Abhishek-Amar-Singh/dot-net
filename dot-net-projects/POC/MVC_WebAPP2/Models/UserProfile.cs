﻿using AutoMapper;
using ClassLibraryPersistence.Models;

namespace MVC_WebAPP2.Models
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            //CreateMap<User, UserViewModel>();
            CreateMap<User, UserViewModel>().ReverseMap();
        }
    }
}
