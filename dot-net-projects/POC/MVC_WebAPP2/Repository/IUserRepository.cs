﻿using ClassLibraryPersistence.Models;
using MVC_WebAPP2.Models;

namespace MVC_WebAPP2.Repository
{
    public interface IUserRepository
    {
        List<UserViewModel> GetAllUsers();
        int RegisterUser(UserViewModel user);
        UserViewModel GetUserByEmail(string emailId);
        void DeleteUser(UserViewModel user);
        void EditUser(UserViewModel user);
        UserViewModel GetUserById(int id);
    }
}
