﻿using AutoMapper;
using ClassLibraryPersistence.Context;
using ClassLibraryPersistence.Models;
using Microsoft.EntityFrameworkCore;
using MVC_WebAPP2.Models;

namespace MVC_WebAPP2.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly UserDbContext userDbContext;
        private readonly IMapper _mapper;

        public UserRepository(UserDbContext userDbContext, IMapper _mapper)
        {
            this.userDbContext = userDbContext;
            this._mapper = _mapper;
        }

        public List<UserViewModel> GetAllUsers()
        {
            List<User> userList = userDbContext.UserList.ToList();
            List<UserViewModel> users = _mapper.Map<List<UserViewModel>>(userList);
            return users;
        }

        public int RegisterUser(UserViewModel user)
        {
            User user1 = _mapper.Map<UserViewModel, User>(user);
            userDbContext.UserList.Add(user1);
            return userDbContext.SaveChanges();
        }

        public UserViewModel GetUserByEmail(string emailId)
        {
            User user = userDbContext.UserList.AsNoTracking().FirstOrDefault(u => u.EmailAddress == emailId);
            UserViewModel user1 = _mapper.Map<User, UserViewModel>(user);
            return user1;
        }

        public void DeleteUser(UserViewModel user)
        {
            User user1 = _mapper.Map<UserViewModel, User>(user);
            userDbContext.UserList.Remove(user1);
            userDbContext.SaveChanges();
        }

        public void EditUser(UserViewModel user)
        {
            User user1 = _mapper.Map<UserViewModel, User>(user);
            userDbContext.Entry<User>(user1).State = EntityState.Modified;
            userDbContext.SaveChanges();
        }

        public UserViewModel GetUserById(int id)
        {
            User user = userDbContext.UserList.AsNoTracking().FirstOrDefault(u => u.Id == id);
            UserViewModel user1 = _mapper.Map<User, UserViewModel>(user);
            return user1;
        }
    }
}