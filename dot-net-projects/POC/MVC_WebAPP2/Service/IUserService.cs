﻿using ClassLibraryPersistence.Models;
using MVC_WebAPP2.Models;

namespace MVC_WebAPP2.Service
{
    public interface IUserService
    {
        List<UserViewModel> GetAllUsers();
        bool RegisterUser(UserViewModel user);
        UserViewModel GetUserByEmailId(string emailId);
        void DeleteUser(string emailId);
        void EditUser(UserViewModel user);
        UserViewModel GetUserById(int id);
    }
}
