﻿using AutoMapper;
using ClassLibraryPersistence.Models;
using MVC_WebAPP2.Models;
using MVC_WebAPP2.Repository;

namespace MVC_WebAPP2.Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository _userRepository)
        {
            this._userRepository = _userRepository;
        }

        public List<UserViewModel> GetAllUsers()
        {
            return _userRepository.GetAllUsers();
        }

        public bool RegisterUser(UserViewModel user)
        {
            var userExists = _userRepository.GetUserByEmail(user.EmailAddress);
            if (userExists == null)
            {
                int userRegisteredStatus = _userRepository.RegisterUser(user);
                return userRegisteredStatus == 1 ? true : false;
            }
            else return false;
        }

        public UserViewModel GetUserByEmailId(string emailId)
        {
            UserViewModel user = _userRepository.GetUserByEmail(emailId);
            return user;
        }

        public void DeleteUser(string emailId)
        {
            UserViewModel user = _userRepository.GetUserByEmail(emailId);
            _userRepository.DeleteUser(user);
            
        }

        public void EditUser(UserViewModel user)
        {
            // This is very important -- user.Id = 1005;
            _userRepository.EditUser(user);
        }

        public UserViewModel GetUserById(int id)
        {
            UserViewModel user = _userRepository.GetUserById(id);
            return user;
        }
    }
}
