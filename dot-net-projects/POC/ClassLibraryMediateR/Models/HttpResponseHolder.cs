﻿using System.Net;

namespace ClassLibraryMediateR.Models
{
    public class HttpResponseHolder
    {
        public HttpStatusCode StatusCode { get; set; }
        public string? StatusMessage { get; set; }
        public User? Data { get; set; }
    }
}
