﻿
using ClassLibraryMediateR.Context;
using ClassLibraryMediateR.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Net;

namespace ClassLibraryMediateR.Users.Queries
{
    public record EditUserQuery(string emailAddress, User user) : IRequest<HttpResponseHolder> { }

    public class EditUserQueryHandler : IRequestHandler<EditUserQuery, HttpResponseHolder>
    {
        private readonly UserDbContext userDbContext;
        public EditUserQueryHandler(UserDbContext userDbContext)
        {
            this.userDbContext = userDbContext;
        }

        public async Task<HttpResponseHolder> Handle(EditUserQuery request, CancellationToken cancellationToken)
        {
            var user = userDbContext.UserList_Mediater.FirstOrDefault(u => u.EmailAddress.Equals(request.emailAddress));
            if (user is not null)
            {
                user.EmailAddress = request.emailAddress;
                user.FirstName = request.user.FirstName;
                user.LastName = request.user.LastName;
                user.Password = request.user.Password;
                user.Address = request.user.Address;
                userDbContext.Entry<User>(user).State = EntityState.Modified;
                await Task.FromResult((userDbContext.SaveChanges() > 0) ? true : false);
                return new HttpResponseHolder(){StatusCode=HttpStatusCode.OK, StatusMessage=$"{user.FirstName}'s details successfully modified.", Data=user};
            }
            else
            {
                return new HttpResponseHolder(){ StatusCode = HttpStatusCode.NotFound, StatusMessage = $"User having email address '{request.emailAddress}' not found! Hence, updation operation cannot be performed.", Data=user};
            }
        }
    }
}
