﻿
using System.Net;
using ClassLibraryMediateR.Context;
using ClassLibraryMediateR.Models;
using MediatR;

namespace ClassLibraryMediateR.Users.Queries
{
    public record RegisterUserQuery(User user) : IRequest<HttpResponseHolder> { }

    public class RegisterUserQueryHandler : IRequestHandler<RegisterUserQuery, HttpResponseHolder>
    {
        private readonly UserDbContext userDbContext;
        public RegisterUserQueryHandler(UserDbContext userDbContext)
        {
            this.userDbContext = userDbContext;
        }

        public async Task<HttpResponseHolder> Handle(RegisterUserQuery request, CancellationToken cancellationToken)
        {
            var user = userDbContext.UserList_Mediater.FirstOrDefault(u => u.EmailAddress.Equals(request.user.EmailAddress));
            if (user is null)
            {
                await userDbContext.UserList_Mediater.AddAsync(request.user);
                await Task.FromResult((userDbContext.SaveChanges() > 0) ? true : false);
                return new HttpResponseHolder() { StatusCode = HttpStatusCode.OK, StatusMessage = $"User having email address '{request.user.EmailAddress}' registered successfully!", Data = request.user };
            }
            else return new HttpResponseHolder() { StatusCode = HttpStatusCode.Conflict, StatusMessage = $"User having email address '{user?.EmailAddress}' already exists! Try to register with different email address.", Data = user };
        }
    }
}
