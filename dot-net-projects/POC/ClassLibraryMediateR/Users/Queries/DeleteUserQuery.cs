﻿
using System.Net;
using ClassLibraryMediateR.Context;
using ClassLibraryMediateR.Models;
using MediatR;

namespace ClassLibraryMediateR.Users.Queries
{
    public record DeleteUserQuery(string emailAddress) : IRequest<HttpResponseHolder> { }

    public class DeleteUserQueryHandler : IRequestHandler<DeleteUserQuery, HttpResponseHolder>
    {
        private readonly UserDbContext userDbContext;
        public DeleteUserQueryHandler(UserDbContext userDbContext)
        {
            this.userDbContext = userDbContext;
        }

        public async Task<HttpResponseHolder> Handle(DeleteUserQuery request, CancellationToken cancellationToken)
        {
            var user = userDbContext.UserList_Mediater.FirstOrDefault(u => u.EmailAddress.Equals(request.emailAddress));
            if (user is not null)
            {
                userDbContext.UserList_Mediater.Remove(user);
                await Task.FromResult((userDbContext.SaveChanges() > 0) ? true : false);
                return new HttpResponseHolder() { StatusCode = HttpStatusCode.OK, StatusMessage = $"User having email address '{request.emailAddress}' has been deleted successfully.", Data = user };
            }
            else return new HttpResponseHolder() { StatusCode = HttpStatusCode.NotFound, StatusMessage = $"User having email address '{request.emailAddress}' not found! Hence, deletion operation cannot be performed.", Data = user }; ;
        }
    }
}
