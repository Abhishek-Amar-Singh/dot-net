﻿
using System.Net;
using ClassLibraryMediateR.Context;
using ClassLibraryMediateR.Models;
using MediatR;

namespace ClassLibraryMediateR.Users.Queries
{
    public class GetAllUsersQuery : IRequest<List<User>> { }

    //accept two things information coming in and information going out
    //return Unit if you don't want to return anything---like an empty object
    //In our case we return List<User>---information going out
    //GetAllUsersQuery---information coming in
    public class GetAllUsersQueryHandler : IRequestHandler<GetAllUsersQuery, List<User>>
    {
        private readonly UserDbContext userDbContext;
        public GetAllUsersQueryHandler(UserDbContext userDbContext)
        {
            this.userDbContext = userDbContext;
        }
        public async Task<List<User>> Handle(GetAllUsersQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(userDbContext.UserList_Mediater.ToList());
        }
    }
}
