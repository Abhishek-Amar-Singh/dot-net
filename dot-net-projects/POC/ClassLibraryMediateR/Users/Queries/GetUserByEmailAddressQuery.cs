﻿
using ClassLibraryMediateR.Context;
using ClassLibraryMediateR.Models;
using MediatR;
using System.Net;

namespace ClassLibraryMediateR.Users.Queries
{
    public record GetUserByEmailAddressQuery(string emailAddress) : IRequest<HttpResponseHolder>
    {
        //private string emailAddress;
        //public GetUserByEmailAddressQuery(string emailAddress)
        //{
        //    this.emailAddress = emailAddress;
        //}
        //public string getEmailAddress()
        //{
        //    return emailAddress;
        //}
    }

    public class GetUserByEmailAddressQueryHandler : IRequestHandler<GetUserByEmailAddressQuery, HttpResponseHolder>
    {
        private readonly UserDbContext userDbContext;
        public GetUserByEmailAddressQueryHandler(UserDbContext userDbContext)
        {
            this.userDbContext = userDbContext;
        }

        public async Task<HttpResponseHolder> Handle(GetUserByEmailAddressQuery request, CancellationToken cancellationToken)
        {
            User user = await Task.FromResult(userDbContext.UserList_Mediater.FirstOrDefault(u => u.EmailAddress.Equals(request.emailAddress)));
            if(user is null)
            {
                return new HttpResponseHolder() { StatusCode=HttpStatusCode.NotFound, StatusMessage=$"User having email address '{request.emailAddress}' not found! Hence, cannot retrieve data.", Data=user};
            }
            else
            {
                return new HttpResponseHolder() { StatusCode = HttpStatusCode.OK, StatusMessage = $"Retrieving details of user having email address '{request.emailAddress}'.", Data = user };
            }
        }
    }
}
