﻿

using ClassLibraryMediateR.Models;
using Microsoft.EntityFrameworkCore;

namespace ClassLibraryMediateR.Context
{
    public class UserDbContext : DbContext
    {
        //Table creation
        public DbSet<User>? UserList_Mediater { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=DESKTOP-28COP13;Database=paperdb;Trusted_Connection=True");
            }
        }
    }
}
