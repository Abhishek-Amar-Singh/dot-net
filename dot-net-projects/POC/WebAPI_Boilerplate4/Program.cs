using Asp.Versioning;
using Asp.Versioning.Conventions;
using ClassLibraryMediateR.Context;
using ClassLibraryMediateR.Users.Queries;
using MediatR;
using WebAPI_Boilerplate4.LoggingLogic;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddApiVersioning(options =>
{
    options.DefaultApiVersion = new ApiVersion(1, 0);
    options.AssumeDefaultVersionWhenUnspecified = true;
    options.ReportApiVersions = true;
    options.ApiVersionReader =
    ApiVersionReader.Combine(
       new HeaderApiVersionReader("Api-Version"),
       new QueryStringApiVersionReader("Query-String-Version"));
});



// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


//--Register Mediater
//--Provide your assmbly where your handler lives
builder.Services.AddMediatR(typeof(GetAllUsersQuery).Assembly);

builder.Services.AddDbContext<UserDbContext>();
//--Add CustomLogger here
builder.Services.AddSingleton<CustomLogger>();



var app = builder.Build();


var versionSet = app.NewApiVersionSet()
                    .HasApiVersion(1.0)
                    .HasApiVersion(2.0)
                    .ReportApiVersions()
                    .Build();
app.MapGet("/GetMessage", () => "This is an example of a minimal API").WithApiVersionSet(versionSet).HasApiVersion(new ApiVersion(2, 0));
app.MapGet("/GetText", () => "This is another example of a minimal API").WithApiVersionSet(versionSet).IsApiVersionNeutral();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
