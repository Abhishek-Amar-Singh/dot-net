﻿using Asp.Versioning;
using ClassLibraryMediateR.Models;
using ClassLibraryMediateR.Users.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using WebAPI_Boilerplate4.LoggingLogic;

namespace WebAPI_Boilerplate4.Controllers
{
    [ServiceFilter(typeof(CustomLogger))]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IMediator _mediator;
        public UserController(IMediator _mediator)
        {
            this._mediator = _mediator;
        }

        [Route("GetAllUsers")]
        [HttpGet]
        public async Task<List<User>> GetAllUsers()
        {
            return await _mediator.Send(new GetAllUsersQuery());
        }

        [Route("GetUserByEmailAddress/{emailAddress}")]
        [HttpGet]
        public async Task<HttpResponseHolder> GetUserByEmailAddress(string emailAddress)
        {
            return await _mediator.Send(new GetUserByEmailAddressQuery(emailAddress));
        }

        [Route("RegisterUser")]
        [HttpPost]
        public async Task<HttpResponseHolder> RegisterUser(User user)
        {
            return await _mediator.Send(new RegisterUserQuery(user));
        }

        [Route("DeleteUser")]
        [HttpDelete]
        public async Task<HttpResponseHolder> DeleteUser(string emailAddress)
        {
            return await _mediator.Send(new DeleteUserQuery(emailAddress));
        }

        [Route("EditUser")]
        [HttpPut]
        public async Task<HttpResponseHolder> EditUser(string emailAddress, User user)
        {
            return await _mediator.Send(new EditUserQuery(emailAddress, user));
        }
    }
}
