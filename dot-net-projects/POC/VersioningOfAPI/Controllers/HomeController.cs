﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace VersioningOfAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [ApiVersion("2.0")]
    public class HomeController : ControllerBase
    {
        [Route("Index")]
        [HttpGet]
        [MapToApiVersion("1.0")]
        public IActionResult Index()
        {
            return Ok("The Index Action.");
        }

        [Route("Style")]
        [HttpGet]
        [MapToApiVersion("2.0")]
        public IActionResult Style()
        {
            return Ok("The Style Action.");
        }
    }
}
