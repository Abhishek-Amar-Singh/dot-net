﻿using MVC_WebAPP.Exceptions;
using MVC_WebAPP.Repository;

namespace MVC_WebAPP.Service
{
    public class FilerService : IFilerService
    {
        private readonly IFilerRepository _filerRepository;
        private readonly IConfiguration _configuration;

        public FilerService(IFilerRepository _filerRepository, IConfiguration _configuration)
        {
            this._filerRepository = _filerRepository;
            this._configuration = _configuration;
        }

        public bool DriveStorage(IFormFile theFile, string operation)
        {
            //--Configuration
            //--Read Value from appsettings.json
            if (theFile is not null && operation.Equals(_configuration["ArithmeticOperations:Addition"]))
            {
                Predicate<IFormFile> delegateToUploadFile = _filerRepository.AddToDriveStorage;
                return delegateToUploadFile.Invoke(theFile);
            }
            else if (theFile is not null && operation.Equals(_configuration["ArithmeticOperations:Deletion"]))
            {
                Predicate<IFormFile> delegateToDeleteFile = _filerRepository.DeleteFromDriveStorage;
                return delegateToDeleteFile(theFile) ? true : throw new FileNotFoundInDriveStorageException("File Not Found! May be you've already deleted the file from the secondary/drive storage or haven't uploaded to it.");
            }
            else
            {
                throw new FileNotFoundInDriveStorageException("File Not Found! First choose a file to perform upload or delete operation. As of now, no file chosen.");
            }
        }
    }
}
