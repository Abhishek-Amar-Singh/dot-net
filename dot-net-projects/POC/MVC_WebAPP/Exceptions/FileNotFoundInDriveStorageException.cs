﻿namespace MVC_WebAPP.Exceptions
{
    public class FileNotFoundInDriveStorageException : ApplicationException
    {
        public FileNotFoundInDriveStorageException()
        {

        }

        public FileNotFoundInDriveStorageException(string msg):base(msg)
        {

        }
    }
}
