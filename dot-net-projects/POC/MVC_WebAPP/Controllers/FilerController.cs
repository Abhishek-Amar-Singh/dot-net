﻿using Microsoft.AspNetCore.Mvc;
using MVC_WebAPP.Service;
using MVC_WebAPP.SRPLogic;

namespace MVC_WebAPP.Controllers
{
    //--Global Exception Handling
    [ExceptionHandler]
    public class FilerController : Controller
    {
        private readonly IFilerService _filerService;
        private delegate bool DelegateToDriveStorage(IFormFile theFile, string opeartion);

        //--Dependency Injection
        //--Inject Service
        public FilerController(IFilerService _filerService)
        {
            this._filerService = _filerService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public bool FileOperation(IFormFile theFile, string operation)
        {
            //--Invoke Service in Action Method
            DelegateToDriveStorage delegateToDriveStorage = new DelegateToDriveStorage(_filerService.DriveStorage);
            return delegateToDriveStorage(theFile, operation);
        }
    }
}