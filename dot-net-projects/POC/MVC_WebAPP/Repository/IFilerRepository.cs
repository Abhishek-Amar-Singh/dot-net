﻿namespace MVC_WebAPP.Repository
{
    public interface IFilerRepository
    {
        bool AddToDriveStorage(IFormFile theFile);
        bool DeleteFromDriveStorage(IFormFile theFile);
    }
}
