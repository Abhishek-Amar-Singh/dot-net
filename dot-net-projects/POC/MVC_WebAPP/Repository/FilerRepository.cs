﻿namespace MVC_WebAPP.Repository
{
    public class FilerRepository : IFilerRepository
    {
        public bool AddToDriveStorage(IFormFile theFile)
        {
            string path = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, "UploadedFiles"));
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            using (var fileStream = new FileStream(Path.Combine(path, theFile.FileName), FileMode.Create))
            {
                theFile.CopyTo(fileStream);
            }
            return true;
        }

        public bool DeleteFromDriveStorage(IFormFile theFile)
        {
            //--Environments
            //--Read Value from Environment
            string filePath = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, "UploadedFiles", theFile.FileName));
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                return true;
            }
            else return false;
        }
    }
}