﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using MVC_WebAPP.Exceptions;

namespace MVC_WebAPP.SRPLogic
{
    public class ExceptionHandlerAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception.GetType() == typeof(FileNotFoundInDriveStorageException))
            {
                context.Result = new OkObjectResult(context.Exception.Message);
            }
            //else if(context.Exception.GetType() == typeof(FileNotFoundInBrowserStorageException)) {}
            else
            {
                context.Result = new StatusCodeResult(500);//any error related to server-side
            }
        }
    }
}
