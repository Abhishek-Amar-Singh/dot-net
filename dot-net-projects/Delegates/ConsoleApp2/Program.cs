﻿
namespace ConsoleApp2
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("--Demo::Delegate--");
            CoffeeMaker coffeeMaker = new CoffeeMaker();
            coffeeMaker.MakeCoffee(PrepareCoffee);
        }

        private static void PrepareCoffee()
        {
            string[] instructions = new string[]
            {
            "Adding hotwater", "Adding Coffee Powder", "Adding Milk"
            };
            foreach (string instruction in instructions)
            {
                Console.WriteLine(instruction);
            }
        }
    }
}