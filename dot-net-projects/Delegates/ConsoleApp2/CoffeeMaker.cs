﻿
namespace ConsoleApp2
{
    delegate void ProcessOrderDelegate();
    internal class CoffeeMaker
    {
        public void MakeCoffee(ProcessOrderDelegate handler)
        {
            Console.WriteLine("Coffee is being prepared...");
            handler();
            Console.WriteLine("Coffee prepared! Thank You!");
        }
    }
}
