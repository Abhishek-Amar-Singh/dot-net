﻿
namespace ConsoleApp3
{
    delegate int Calculator(int x);
    class Program
    {
        static int value = 10;
        public static int Addition(int x)
        {
            value = value + x;
            return value;
        }
        public static int Subtraction(int x)
        {
            value = value - x;
            return value;
        }
        public static int Multiplication(int x)
        {
            value = value * x;
            return value;
        }
        public static int Division(int x)
        {
            value = value / x;
            return value;
        }
        public static int GetValue()
        {
            return value;
        }

        public static void Main(string[] args)
        {
            Calculator calc1 = new Calculator(Addition);
            Calculator calc2 = new Calculator(Subtraction);
            Calculator calc3 = new Calculator(Multiplication);
            Calculator calc4 = new Calculator(Division);

            calc1(20);
            Console.WriteLine("After calc1 delegate, value is::" + GetValue());
            calc2(3);
            Console.WriteLine("After calc2 delegate, value is::" + GetValue());
            calc3(5);
            Console.WriteLine("After calc3 delegate, value is::" + GetValue());
            calc4(5);
            Console.WriteLine("After calc4 delegate, value is::" + GetValue());
        }
    }
}