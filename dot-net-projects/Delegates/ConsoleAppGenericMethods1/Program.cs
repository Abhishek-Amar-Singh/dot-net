﻿
namespace ConsoleAppGenericMethods1
{
    //Declare three delegates
    public delegate int DelegateToSquare(int num1, int num2);
    public delegate void DelegateToSquare1(int num1, int num2);
    public delegate bool DelegateToCount(string message);
    class Program
    {
        static void Main()
        {
            DelegateToSquare ds = new DelegateToSquare(Square);
            int result = ds(12, 12);
            Console.WriteLine(result);

            DelegateToSquare1 ds1 = new DelegateToSquare1(Square1);
            ds1(12, 12);

            DelegateToCount dc = CountLetters;// DelegateToCount dc = new DelegateToCount(CountLetters);
            bool resultOfCount = dc("Namaskaram Team");
            Console.WriteLine(resultOfCount);
        }

        public static int Square(int num1, int num2)
        {
            return num1 * num2;
        }

        public static void Square1(int num1, int num2)
        {
            Console.WriteLine(num1 * num2);
        }

        public static bool CountLetters(string msg)
        {
            if (msg.Count() > 4) return true;
            else return false;
        }
    }
}