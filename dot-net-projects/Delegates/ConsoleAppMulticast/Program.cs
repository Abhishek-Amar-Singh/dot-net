﻿// See https://aka.ms/new-console-template for more information
using A;
using ConsoleAppMulticast;
using System.Collections;

namespace A
{
    class H
    {
        public void show(ref int n, out int m)
        {
            n = 100;
            m = 45;
        }
    }
}
namespace ConsoleAppMulticast
{
    class Program
    {
        public int Id { get; set; } = 90003;
        delegate void Mailer(string mail);
        static void Main()
        {
            Console.WriteLine("Multicast Delegate");
            //Instance
            Mailer mailer = ExternalMail;
            mailer += InternalMail;//mailer -= InternalMail;---acts like delete method
            //Invoke a delegate
            mailer("Covid has created difficult situation.");
            H a = new H();
            int j = 90, k=89;
            a.show(ref j, out k);
            Console.WriteLine($"j={j} & k={k}");
        }

        public static void InternalMail(string msg)
        {
            Console.WriteLine($"Internal Mail::{msg}");
        }
        public static void ExternalMail(string msg)
        {
            Console.WriteLine($"External Mail::{msg}");
        }
    }
}