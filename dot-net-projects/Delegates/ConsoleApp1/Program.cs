﻿
namespace ConsoeApp1
{
    class Program
    {
        //declare delegate
        public delegate string DelegateToDetails(string details);//This represents StudentDetails
        static void Main()
        {
            //Instantiate a delegate
            DelegateToDetails delegateToDetails = new DelegateToDetails(StudentDetails);
            //Invoke a delegate--2 ways
            Console.WriteLine(delegateToDetails.Invoke("Student Name is::Student1"));
            Console.WriteLine(delegateToDetails("Student Name is::Student2"));
        }

        public static string StudentDetails(string name)
        {
            return name;
        }
    }
}