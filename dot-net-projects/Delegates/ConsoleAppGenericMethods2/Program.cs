﻿
namespace ConsoleAppGenericMethods2
{
    class Program
    {
        static void Main()
        {
            Func<int, int, int> ds = (a, b) => a * b;
            int result = ds(12, 12);
            Console.WriteLine(result);

            Action<int, int> ds1 = (a, b) => Console.WriteLine(a * b);
            ds1(12, 12);

            Predicate<string> dc = x => (x.Count() > 4) ? true : false;
            bool resultOfCount = dc("Namaskaram Team");
            Console.WriteLine(resultOfCount);
        }
    }
}
//Func Delegate----------when fuction returns a value, go for Func Delegate
//Action Delegate--------whenever function does not return a value, use Action Delegate
//Predicate Delegate-----bool return types, go for Predicate Delegate