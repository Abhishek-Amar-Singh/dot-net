﻿
namespace ConsoleAppCallBackMethods
{
    internal class Class1
    {
        //declare a delegate
        public delegate void CallBack(int i);
        public void LongRunningProcess(CallBack obj)
        {
            for(int i=0; i<100; i++)
            {
                obj(i);
            }
        }
    }
}
