﻿
using ConsoleAppCallBackMethods;

Class1 class1 = new Class1();
class1.LongRunningProcess(CallBackMethod);

static void CallBackMethod(int i)
{
    Console.WriteLine(i);
}