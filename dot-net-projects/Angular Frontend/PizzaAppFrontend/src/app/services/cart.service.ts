import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Admin } from '../models/admin';
import { CartPizza } from '../models/cart-pizza';

@Injectable({
  providedIn: 'root'
})
export class CartService
{
  cartUrl:string = environment.cartUrl;

  constructor(private httpClient:HttpClient) { }

  GetAllAdmins():Observable<Admin[]>
  {
    return this.httpClient.get<Admin[]>(`${this.cartUrl}/GetAllAdmins`);
  }

  ViewCart(userEmail: string | undefined):Observable<CartPizza[]>
  {
    return this.httpClient.get<CartPizza[]>(`${this.cartUrl}/ViewCart?email=${userEmail}`);
  }

  AddPizzaToCart(cartPizza: CartPizza):Observable<boolean>
  {
    return this.httpClient.post<boolean>(`${this.cartUrl}/AddPizzaToCart`, cartPizza);
  }

  UpdateAllCartPizzasByName(name: string | undefined, cartPizza: CartPizza):Observable<boolean>
  {
    return this.httpClient.put<boolean>(`${this.cartUrl}/UpdateAllCartPizzasByName/${name}`, cartPizza);
  }

  
  DeletePizzasFromCartByPizzaName(pizzaName: string | undefined):Observable<boolean>
  {
    return this.httpClient.delete<boolean>(`${this.cartUrl}/DeletePizzasFromCartByPizzaName/${pizzaName}`);
  }

  GetCartPizzaById(id: number | undefined):Observable<CartPizza>
  {
    return this.httpClient.get<CartPizza>(`${this.cartUrl}/GetCartPizzaById/${id}`);
  }

  EditQuantityUpdatePriceQuantityOfCartPizza(id: number | undefined, cartPizza: CartPizza):Observable<boolean>
  {
    return this.httpClient.put<boolean>(`${this.cartUrl}/EditQuantityUpdatePriceQuantityOfCartPizza/${id}`, cartPizza);
  }

  DeletePizzaFromCart(id: number | undefined):Observable<boolean>
  {
    return this.httpClient.delete<boolean>(`${this.cartUrl}/DeletePizzaFromCart/${id}`);
  }

  DeletePizzasFromCartByEmail(email: string | undefined):Observable<boolean>
  {
    return this.httpClient.delete<boolean>(`${this.cartUrl}/DeletePizzasFromCartByEmail/${email}`);
  }


  SendMailTo(emailId: string | undefined, emailDetails: any):Observable<void> {
    return this.httpClient.post<void>(`${this.cartUrl}/SendEmail?userEmail=${emailId}`, emailDetails);
  }
}
