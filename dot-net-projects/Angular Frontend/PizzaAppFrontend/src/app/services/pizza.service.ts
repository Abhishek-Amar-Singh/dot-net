import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pizza } from '../models/pizza';

@Injectable({
  providedIn: 'root'
})
export class PizzaService {

  pizzaUrl:string = environment.pizzaUrl;

  constructor(private httpClient:HttpClient)
  {

  }

  GetAllPizzas():Observable<Pizza[]>
  {
    return this.httpClient.get<Pizza[]>(`${this.pizzaUrl}/GetAllPizzas`);
  }

  GetPizzaById(id: number):Observable<Pizza>
  {
    return this.httpClient.get<Pizza>(`${this.pizzaUrl}/PizzaDetails/${id}`);
  }

  EditPizza(id: number|undefined, pizza: Pizza):Observable<boolean>
  {
    return this.httpClient.put<boolean>(`${this.pizzaUrl}/EditPizza/${id}`, pizza)
  }

  DeletePizza(pizzaName: string | undefined):Observable<boolean>
  {
    return this.httpClient.delete<boolean>(`${this.pizzaUrl}/DeletePizza/${pizzaName}`);
  }

  AddPizza(pizza: Pizza):Observable<boolean>
  {
    return this.httpClient.post<boolean>(`${this.pizzaUrl}/AddPizza`, pizza);
  }

}
