import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LoginInfo } from '../models/login-info';

@Injectable({
  providedIn: 'root'
})

export class UserService
{

  userUrl:string = environment.userUrl;

  constructor(private httpClient:HttpClient)
  {

  }

  RegisterUser(user: User):Observable<boolean>
  {
    return this.httpClient.post<boolean>(`${this.userUrl}/RegisterUser`, user);
  }

  LoginUser(loginInfo: LoginInfo):Observable<string>
  {
    return this.httpClient.post<string>(`${this.userUrl}/LogIn`, loginInfo)
  }

  GetAllUsers():Observable<User[]>
  {
    return this.httpClient.get<User[]>(`${this.userUrl}/GetAllUsers`);
  }

  DeleteUser(email: string|undefined):Observable<boolean>
  {
    return this.httpClient.delete<boolean>(`${this.userUrl}/DeleteUser/${email}`);
  }

}
