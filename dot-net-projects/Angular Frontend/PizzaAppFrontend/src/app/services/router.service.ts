import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RouterService
{
  constructor(private router:Router)
  {

  }

  GoToLogin()
  {
    this.router.navigate(["login"]);
  }
  
  GoToMenu()
  {
    this.router.navigate(["menu"]);
  }

  GoToEditPizza(id: number | undefined)
  {
    this.router.navigate(["menu", "edit-pizza", id]);
  }

  GoToEditPizzaPageInCart(id: number | undefined)
  {
    this.router.navigate(["menu", "view-cart", "edit-pizza-of-cart", id]);
  }

  GoToAddPizzaPage()
  {
    this.router.navigate(["menu", "add-pizza"]);
  }

  GoToViewCart()
  {
    this.router.navigate(["menu", "view-cart"]);
  }
}
