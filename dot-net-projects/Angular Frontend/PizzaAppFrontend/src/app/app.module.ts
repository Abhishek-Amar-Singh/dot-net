import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { GetAllPizzasComponent } from './components/get-all-pizzas/get-all-pizzas.component';
import { EditPizzaComponent } from './components/get-all-pizzas/edit-pizza/edit-pizza.component';
import { ViewCartComponent } from './components/get-all-pizzas/view-cart/view-cart.component';
import { EditPizzaOfCartComponent } from './components/get-all-pizzas/view-cart/edit-pizza-of-cart/edit-pizza-of-cart.component';
import { AddPizzaComponent } from './components/get-all-pizzas/add-pizza/add-pizza.component';
import { TokenInterceptorService } from './authorization/services/token-interceptor.service';
import { ViewUsersComponent } from './components/view-users/view-users.component';
import { GenerateBillComponent } from './components/get-all-pizzas/view-cart/generate-bill/generate-bill.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    RegisterComponent,
    LoginComponent,
    LogoutComponent,
    GetAllPizzasComponent,
    EditPizzaComponent,
    ViewCartComponent,
    EditPizzaOfCartComponent,
    AddPizzaComponent,
    ViewUsersComponent,
    GenerateBillComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi:true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
