import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './authorization/auth.guard';
import { AddPizzaComponent } from './components/get-all-pizzas/add-pizza/add-pizza.component';
import { EditPizzaComponent } from './components/get-all-pizzas/edit-pizza/edit-pizza.component';
import { GetAllPizzasComponent } from './components/get-all-pizzas/get-all-pizzas.component';
import { EditPizzaOfCartComponent } from './components/get-all-pizzas/view-cart/edit-pizza-of-cart/edit-pizza-of-cart.component';
import { GenerateBillComponent } from './components/get-all-pizzas/view-cart/generate-bill/generate-bill.component';
import { ViewCartComponent } from './components/get-all-pizzas/view-cart/view-cart.component';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { RegisterComponent } from './components/register/register.component';
import { ViewUsersComponent } from './components/view-users/view-users.component';

const routes: Routes = [
  {path:"register", component:RegisterComponent},
  {path:"login", component:LoginComponent},
  {path:"logout", component:LogoutComponent},
  {path:"menu", component:GetAllPizzasComponent, canActivate:[AuthGuard]},
  {path:"menu/edit-pizza/:id", component:EditPizzaComponent, canActivate:[AuthGuard]},
  {path:"menu/view-cart", component:ViewCartComponent, canActivate:[AuthGuard]},
  {path:"menu/view-cart/edit-pizza-of-cart/:id", component:EditPizzaOfCartComponent, canActivate:[AuthGuard]},
  {path:"menu/add-pizza", component:AddPizzaComponent, canActivate:[AuthGuard]},
  {path:"view-users", component:ViewUsersComponent, canActivate:[AuthGuard]},
  {path:"menu/view-cart/generate-bill", component:GenerateBillComponent, canActivate:[AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
