import { Component, OnInit } from '@angular/core';
import { LoginInfo } from 'src/app/models/login-info';
import { RouterService } from 'src/app/services/router.service';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit
{
  loginInfo:LoginInfo;

  constructor(private userService:UserService, private routerService:RouterService)
  {
    this.loginInfo = new LoginInfo();
  }

  ngOnInit(): void
  {

  }

  LoginUser()
  {
    this.userService.LoginUser(this.loginInfo).subscribe({
      next:(res) => {
        console.log(res);
        let jsonObject = JSON.stringify(res);
        let jsonToken = JSON.parse(jsonObject);
        //console.log(`userToken after login::${jsonToken["Token"]}`);
        localStorage.setItem("Token", jsonToken["Token"]);
        localStorage.setItem("Email", jsonToken["Email"]);
        Swal.fire(
          "User Login",
          "User Logged In Successfully!",
          "success"
        ).then(() => this.routerService.GoToMenu());
      },
      error:(e) => {
        Swal.fire({
          icon: "error",
          title: "Error!",
          text: e.error
        });
      }
    });
  }
  
}