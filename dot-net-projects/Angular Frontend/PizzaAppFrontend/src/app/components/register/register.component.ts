import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { RouterService } from 'src/app/services/router.service';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit
{
  user:User;
  constructor(private userService:UserService, private routerService:RouterService)
  {
    this.user = new User();
  }

  ngOnInit(): void
  {

  }

  RegisterUser()
  {
    console.log(this.user);
    this.userService.RegisterUser(this.user).subscribe({
      next:(res) => {
        Swal.fire(
          "User Registration",
          "User Registered Successfully!",
          "success"
        ).then(() => this.routerService.GoToLogin());
      },
      error:(e) => {
        Swal.fire({
          icon: "error",
          title: "Error!",
          text: e.error
        });
      }
    });
  }

}
