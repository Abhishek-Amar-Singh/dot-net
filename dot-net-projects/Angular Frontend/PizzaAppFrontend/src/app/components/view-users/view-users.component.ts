import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { CartService } from 'src/app/services/cart.service';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-users',
  templateUrl: './view-users.component.html',
  styleUrls: ['./view-users.component.css']
})
export class ViewUsersComponent implements OnInit {

  userList?:User[];

  constructor(private userService:UserService, private cartService:CartService) { }

  ngOnInit(): void
  {
    this.GetAllUsers();
  }

  GetAllUsers()
  {
    this.userService.GetAllUsers().subscribe(res =>{
      this.userList = res;
      console.log(this.userList);
      
    });
  }

  DeleteUser(email:string|undefined)
  {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    });
    
    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed)
      {
        //--
        this.userService.DeleteUser(email).subscribe(res =>{
          console.log(res);
          if(res)
          {
            console.log("User Deleted Successfully!");
            this.cartService.DeletePizzasFromCartByEmail(email).subscribe(res =>{
              console.log(res);
              if(res)
              {
                console.log(`All cart pizzas deleted for user having email address '${email}'`);
                swalWithBootstrapButtons.fire(
                  'Deleted!',
                  'User has been deleted and whatever pizzas s/he added into the the cart, all those items were also got deleted.',
                  'success'
                ).then(()=>window.location.reload());
              }
              else
              {
                swalWithBootstrapButtons.fire(
                  'Deleted!',
                  `User has been deleted and his/her cart was empty.`,
                  'success'
                ).then(()=>window.location.reload());
              }
            });
          }
        });

      }
      else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          "User isn't got deleted and his/her cart items are untouched.",
          'error'
        );
      }

    });
  }

}
