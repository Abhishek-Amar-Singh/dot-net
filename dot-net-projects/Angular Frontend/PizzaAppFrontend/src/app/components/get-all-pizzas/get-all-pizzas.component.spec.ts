import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetAllPizzasComponent } from './get-all-pizzas.component';

describe('GetAllPizzasComponent', () => {
  let component: GetAllPizzasComponent;
  let fixture: ComponentFixture<GetAllPizzasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetAllPizzasComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GetAllPizzasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
