import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/authorization/services/auth.service';
import { Admin } from 'src/app/models/admin';
import { CartPizza } from 'src/app/models/cart-pizza';
import { CartService } from 'src/app/services/cart.service';
import { RouterService } from 'src/app/services/router.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-cart',
  templateUrl: './view-cart.component.html',
  styleUrls: ['./view-cart.component.css']
})
export class ViewCartComponent implements OnInit {

  cartPizzas?:CartPizza[];
  adminList?:Admin[];
  flagSignalForAdmin?:boolean = false;

  constructor(private cartService:CartService, private authService:AuthService, private routerService:RouterService)
  {

  }

  ngOnInit(): void
  {
    this.GetAllAdmins();

    this.ViewCart();
  }

  GetAllAdmins()
  {
    this.cartService.GetAllAdmins().subscribe(res=>{
      this.adminList = res;
      console.log(res);
      console.log(this.flagSignalForAdmin);
      var userEmail = this.authService.GetCurrentlyLoggedInUserEmail();
      for(let item of this.adminList)
      {
        if(item.email == userEmail)
        {
          this.flagSignalForAdmin = true;
        }
      }
      
    });
    console.log(this.flagSignalForAdmin);

  }

  ViewCart()
  {
    let userEmail = this.authService.GetCurrentlyLoggedInUserEmail();
    this.cartService.ViewCart(userEmail).subscribe(res =>{
      console.log(res);
      this.cartPizzas = res;
    });
  }

  GoToEditPizzaFunction(id:number | undefined)
  {
    this.routerService.GoToEditPizzaPageInCart(id);
  }

  DeletePizzaFromCart(cartPizza:CartPizza)
  {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.cartService.DeletePizzaFromCart(cartPizza.id).subscribe(res =>{
          console.log(res);
          console.log(cartPizza);
        });
        Swal.fire(
          'Deleted!',
          `${cartPizza.name} has been deleted.`,
          'success'
        ).then(()=>window.location.reload());
      }
      else
      {
        console.log(`Pressed Cancel: ${cartPizza.name} not deleted.`);
      }
    });
  }
}
