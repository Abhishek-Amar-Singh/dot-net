import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CartPizza } from 'src/app/models/cart-pizza';
import { CartService } from 'src/app/services/cart.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-pizza-of-cart',
  templateUrl: './edit-pizza-of-cart.component.html',
  styleUrls: ['./edit-pizza-of-cart.component.css']
})
export class EditPizzaOfCartComponent implements OnInit {

  id?:number;
  cartPizza:CartPizza;
  mQuantity?:number;

  constructor(private route:ActivatedRoute, private cartService:CartService)
  {
    this.cartPizza = new CartPizza();
  }

  ngOnInit(): void {
    this.id = parseInt(this.route.snapshot.paramMap.get("id")!);
    this.GetPizzaByCartId();
  }

  GetPizzaByCartId()
  {
    this.cartService.GetCartPizzaById(this.id).subscribe(res =>{
      console.log(res);
      this.cartPizza = res;
    });
  }

  EditQuantityUpdatePriceQuantityOfCartPizza()
  {
      this.cartPizza.quantity = this.mQuantity;
      if(this.cartPizza.quantity != undefined && this.cartPizza.quantity > 0)
      {
        this.cartService.EditQuantityUpdatePriceQuantityOfCartPizza(this.id, this.cartPizza).subscribe(res=>{
          console.log(res);
          console.log(this.mQuantity);
          console.log(this.cartPizza);
        });
        window.location.reload();
      }
      else
      {
        Swal.fire({
          icon: 'error',
          title: 'Error!!',
          text: `Undefined value and defined zero value or negative values are not valid.`
        });
      }
  }

}
