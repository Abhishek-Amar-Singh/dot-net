import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPizzaOfCartComponent } from './edit-pizza-of-cart.component';

describe('EditPizzaOfCartComponent', () => {
  let component: EditPizzaOfCartComponent;
  let fixture: ComponentFixture<EditPizzaOfCartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditPizzaOfCartComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditPizzaOfCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
