import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/authorization/services/auth.service';
import { CartPizza } from 'src/app/models/cart-pizza';
import { EmailDetails } from 'src/app/models/email-details';
import { CartService } from 'src/app/services/cart.service';
import { RouterService } from 'src/app/services/router.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-generate-bill',
  templateUrl: './generate-bill.component.html',
  styleUrls: ['./generate-bill.component.css']
})
export class GenerateBillComponent implements OnInit {
  
  cartPizzas?:CartPizza[];
  sumPrice:number = 0;
  emailDetails?:EmailDetails;
  

  constructor(private cartService:CartService, private authService:AuthService, private routerService:RouterService)
  {
    this.emailDetails = new EmailDetails();
  }

  ngOnInit(): void {
    
    this.ViewCart();
  }

  ViewCart()
  {
    let userEmail = this.authService.GetCurrentlyLoggedInUserEmail();
    this.cartService.ViewCart(userEmail).subscribe(res =>{
      console.log(res);
      this.cartPizzas = res;
      console.log(res);
      
      for(let item of this.cartPizzas)
      {
        this.sumPrice += item.price!;
      }
    });
  }


  PlaceOrder()
  {
    Swal.fire({
      title: 'Do you want to place the order?',
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: 'Yes',
      denyButtonText: `No`,
    }).then((result) => {
      / Read more about isConfirmed, isDenied below /
      if (result.isConfirmed) {
        let emailId = this.authService.GetCurrentlyLoggedInUserEmail();
        this.cartService.DeletePizzasFromCartByEmail(emailId).subscribe(res =>{
          console.log(res);
          if(res)
          {
            Swal.fire('Order Placed Successfully!', '', 'success').then(()=>{
              //sending a mail
              console.log(`sending mail to ${emailId}`);
              
              var strPizzaListHolder = ``;
              this.cartPizzas?.forEach(r =>{
                //console.log(`Pizza [Id=${r.id}, Name=${r.name}, Price=${r.price}, Quantity=${r.quantity}, Category=${r.category}]`);
                strPizzaListHolder += `<tr><td>${r.id}</td><td>${r.name}</td><td>&#8377; ${r.price}</td><td>${r.quantity}</td><td>${r.category}</td></tr>`;
              });
              var strPizzaListContainer=`<table border='1' style='border-spacing: 0;text-align:left;'><tr><th>Id</th><th>Name</th><th>Price</th><th>Quantity</th><th>Category</th></tr>` + strPizzaListHolder + `</table>`;
              
              this.emailDetails!.body = `<b>Order Placed Successfully!</b><br><br><div>You've ordered the following pizzas:</div><br>${strPizzaListContainer}<br><br>Your total bill amount is:: &#8377; ${this.sumPrice}<br><br>Dhanyawad!<br>`;
              console.log(this.emailDetails!.body);
              
              this.cartService.SendMailTo(emailId, this.emailDetails).subscribe(res=>{
                console.log(res);
                
              });
              this.routerService.GoToViewCart()
            });
          }
        });
      }
      else if (result.isDenied)
      {
        Swal.fire('Order Not Placed!', '', 'info')
      }
    });
  }
}
