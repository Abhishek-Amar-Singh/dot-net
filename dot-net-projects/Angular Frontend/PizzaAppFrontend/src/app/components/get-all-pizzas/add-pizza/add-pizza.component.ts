import { Component, OnInit } from '@angular/core';
import { Pizza } from 'src/app/models/pizza';
import { PizzaService } from 'src/app/services/pizza.service';
import { RouterService } from 'src/app/services/router.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-pizza',
  templateUrl: './add-pizza.component.html',
  styleUrls: ['./add-pizza.component.css']
})
export class AddPizzaComponent implements OnInit {

  pizza:Pizza;
  constructor(private pizzaService:PizzaService, private routerService:RouterService) { 
    this.pizza = new Pizza();
  }

  ngOnInit(): void {
  }

  AddPizza()
  {
    // this.pizza.name = "Prima";
    // this.pizza.price = 413.39;
    // this.pizza.category = "Veg";
    this.pizzaService.AddPizza(this.pizza).subscribe({
      next:res =>{
        console.log(this.pizza);
        console.log(res);
        console.log("Pizza Added Successfully");
        Swal.fire(
          'Adding Pizza Operation',
          'Pizza Added Successfully!',
          'success'
        ).then(() => this.routerService.GoToMenu());
      },
      error:(e) =>{
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: e.error
        });
      }
    });
  }
}

