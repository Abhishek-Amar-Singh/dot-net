import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Admin } from 'src/app/models/admin';
import { CartPizza } from 'src/app/models/cart-pizza';
import { Pizza } from 'src/app/models/pizza';
import { CartService } from 'src/app/services/cart.service';
import { PizzaService } from 'src/app/services/pizza.service';
import { RouterService } from 'src/app/services/router.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-pizza',
  templateUrl: './edit-pizza.component.html',
  styleUrls: ['./edit-pizza.component.css']
})
export class EditPizzaComponent implements OnInit {

  pizza: Pizza;
  pizzaPrice?: number;
  pizzaCategory?: string;
  cartPizza: CartPizza;
  
  constructor(private route: ActivatedRoute, private pizzaService: PizzaService, private cartService: CartService, private routerService:RouterService) {
    this.pizza = new Pizza();
    this.cartPizza = new CartPizza();
  }

  ngOnInit(): void
  {
    let id = parseInt(this.route.snapshot.paramMap.get("id")!);
    this.GetPizzaById(id);
  }

  GetPizzaById(id: number) {
    this.pizzaService.GetPizzaById(id).subscribe(res => {
      this.pizza = res;
    });

  }

  EditPizza()
  {
    this.pizza.price = this.pizzaPrice;
    this.pizza.category = this.pizzaCategory;
    this.pizzaService.EditPizza(this.pizza.id, this.pizza).subscribe({
      next: (res) => {
        if (res)
        {
          console.log("Pizza Updated Successfully In Menu");
          this.cartPizza.category = this.pizza.category;
          this.cartPizza.price = this.pizza.price;
          this.UpdateAllCartPizzasByName(this.pizza.name, this.cartPizza);
          this.routerService.GoToMenu();
        }
      },
      error: (e) => {
        Swal.fire({
          icon: "error",
          title: "Error!",
          text: "Undefined value, defined zero value or negative values and null values are not valid."
        });
      }
    });
  }
  UpdateAllCartPizzasByName(name: string | undefined, cartPizza: CartPizza)
  {
    this.cartService.UpdateAllCartPizzasByName(name, cartPizza).subscribe(res => {
      if (res)
      {
        console.log("Pizza Updated Successfully In Cart.");
      }
      else
      {
        console.log("Pizza Not Updated Successfully In Cart.");
      }
    });
  }



}

