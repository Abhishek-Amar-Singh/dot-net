import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/authorization/services/auth.service';
import { Admin } from 'src/app/models/admin';
import { CartPizza } from 'src/app/models/cart-pizza';
import { Pizza } from 'src/app/models/pizza';
import { CartService } from 'src/app/services/cart.service';
import { PizzaService } from 'src/app/services/pizza.service';
import { RouterService } from 'src/app/services/router.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-get-all-pizzas',
  templateUrl: './get-all-pizzas.component.html',
  styleUrls: ['./get-all-pizzas.component.css']
})
export class GetAllPizzasComponent implements OnInit
{
  pizzaList?:Pizza[];
  cartPizza:CartPizza;
  adminList?:Admin[];
  flagSignalForAdmin?:boolean = false;
  
  constructor(private pizzaService:PizzaService, private cartService:CartService, private routerService:RouterService, private authService:AuthService)
  {
    this.cartPizza = new CartPizza();
  }

  ngOnInit(): void
  {
    this.GetAllAdmins();

    this.GetAllPizzas();
  }

  GetAllAdmins()
  {
    
    this.cartService.GetAllAdmins().subscribe(res=>{
      this.adminList = res;
      console.log(res);
      console.log(this.flagSignalForAdmin);
      var userEmail = this.authService.GetCurrentlyLoggedInUserEmail();
      for(let item of this.adminList)
      {
        if(item.email == userEmail)
        {
          this.flagSignalForAdmin = true;
        }
      }
      
    });
    console.log(this.flagSignalForAdmin);

  }
  
  GetAllPizzas()
  {
    this.pizzaService.GetAllPizzas().subscribe(res =>{
      this.pizzaList = res;
      console.log(res);
    });

  }

  GoToEditPizzaMethod(id: number | undefined)
  {
    this.routerService.GoToEditPizza(id);
  }

  AddPizzaToCart(pizza:Pizza)
  {
    this.cartPizza.name = pizza.name; 
    this.cartPizza.price = pizza.price;
    this.cartPizza.category = pizza.category;
    this.cartPizza.email = this.authService.GetCurrentlyLoggedInUserEmail();
    this.cartService.AddPizzaToCart(this.cartPizza).subscribe(res =>{
      console.log(res);
      console.log(this.cartPizza);
      if(res)
      {
        console.log("Pizza Added to the cart successfully");
      }
      else
      {
        console.log("Pizza not added to the cart");
      }
    });

  }


  DeletePizza(pizzaName:string|undefined)
  {
    console.log(pizzaName);
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed)
      {
        this.pizzaService.DeletePizza(pizzaName).subscribe({
          next:(res) => {
            console.log(res);
            if(res)
            {
              console.log("Pizza Deleted Successfully From Menu");
              this.cartService.DeletePizzasFromCartByPizzaName(pizzaName).subscribe(res=>{
                if(res)
                {
                  console.log("Pizzas Deleted Successfully From Cart");
                }
                else
                {
                  console.log("Pizza Not Deleted Successfully In Menu");
                }
              });
              Swal.fire(
                'Deleted!',
                `${pizzaName} has been removed from the menu and from the cart.`,
                'success'
              ).then(()=>window.location.reload());
            }
            else
            {
              console.log("Pizza Not Deleted Successfully");
            }
          },
          error:(e)=>{
            Swal.fire({
              icon: 'error',
              title: 'Error!',
              text: e.error
            });
          }
        });
      }
      else
      {
        console.log(`Pressed Cancel: ${pizzaName} not deleted.`);
      }
    });

  }

}
