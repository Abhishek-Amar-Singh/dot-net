﻿using Microsoft.EntityFrameworkCore;
using WebApplication1.Contexts;

namespace WebApplication1.RegisterServices
{
    public static class ServiceRegistration
    {
        public static IServiceCollection AddCustomServices(this IServiceCollection services, IConfiguration configuration)
        {
            var localDbConnectionString = configuration.GetConnectionString("localDbConnectionStr");
            services.AddDbContext<ProductDbContext>(options => options.UseNpgsql(localDbConnectionString), ServiceLifetime.Singleton);
            services.AddDbContext<PizzaDbContext>(options => options.UseNpgsql(localDbConnectionString));
            services.AddDbContext<UserDbContext>(options => options.UseNpgsql(localDbConnectionString));
            services.AddDbContext<DocsDbContext>(options => options.UseNpgsql(localDbConnectionString), ServiceLifetime.Scoped);
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            return services;
        }
    }
}
