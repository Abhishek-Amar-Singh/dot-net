﻿using Microsoft.EntityFrameworkCore;
using System.Runtime.CompilerServices;
using WebApplication1.Models;

namespace WebApplication1.Contexts
{
    public class ProductDbContext : DbContext
    {
        public ProductDbContext(DbContextOptions<ProductDbContext> dbContextOptions):base(dbContextOptions)
        {

        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.UseSerialColumns();
        //}

        public DbSet<Product> Products { get; set; }
    }
}
