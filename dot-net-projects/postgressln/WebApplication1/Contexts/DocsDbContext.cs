﻿using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;

namespace WebApplication1.Contexts
{
    public class DocsDbContext : DbContext
    {
        public DocsDbContext(DbContextOptions<DocsDbContext> dbContextOptions) : base(dbContextOptions)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseSerialColumns();
            modelBuilder.HasDefaultSchema("sample");
        }

        public DbSet<Docs> DocsTable { get; set; }
    }
}
