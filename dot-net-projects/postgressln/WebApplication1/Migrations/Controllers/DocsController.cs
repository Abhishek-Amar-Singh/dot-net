﻿using Microsoft.AspNetCore.Mvc;
using Nancy;
using Nancy.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO.Compression;
using WebApplication1.Contexts;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class DocsController : Controller
    {
        private readonly DocsDbContext docsDbContext;
        public DocsController(DocsDbContext docsDbContext)
        {
            this.docsDbContext = docsDbContext;
        }

        public IActionResult AllDocs()
        {
            var docsList = docsDbContext.DocsTable.ToList();
            return View(docsList);
        }

        [HttpGet]
        public IActionResult CreateDocs()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateDocs(Docs docs)
        {
            List<byte[]> fileByteArrayList = new List<byte[]>();
            IDictionary<string, List<byte[]>> docCodeWithFileDictionary = new Dictionary<string, List<byte[]>>();
            docs.fileExtensions = new string[docs.Documents.Count];
            int index = 0;
            foreach (var doc in docs.Documents)
            {
                docs.fileExtensions[index] = System.IO.Path.GetExtension(doc.FileName);
                index++;
                using (var ms = new MemoryStream())
                {
                    doc.CopyTo(ms);
                    var fileBytes = ms.ToArray();
                    fileByteArrayList.Add(fileBytes);
                }
            }
            docCodeWithFileDictionary.Add(docs.DocCode, fileByteArrayList);
            var json = new JavaScriptSerializer().Serialize(docCodeWithFileDictionary);
            docs.MetaData = json;
            var addUserObj = await docsDbContext.DocsTable.AddAsync(docs);
            await docsDbContext.SaveChangesAsync();
            return RedirectToAction("AllDocs");
        }

        [HttpGet]
        public IActionResult GetDocumentZip(int id)
        {
            var userData = docsDbContext.DocsTable.FirstOrDefault(j => j.Id == id);

            var metaData = userData.MetaData;

            string delFirAndLastIndexOfString = metaData.Substring(1, metaData.Length - 2);

            var jsonArrayResponse = JObject.Parse(delFirAndLastIndexOfString);
            var values = jsonArrayResponse["value"].ToString();
            var keys = jsonArrayResponse["key"].ToString();

            List<byte[]> fileByteArrayList = JsonConvert.DeserializeObject<List<byte[]>>(values);

            #region ZipTheFile
            using (MemoryStream ms = new MemoryStream())
            {
                using (var archive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                {
                    int i = 0;
                    foreach (var file in fileByteArrayList)
                    {
                        var entry = archive.CreateEntry($"{userData.DocCode}-{i}{userData.fileExtensions[i]}", CompressionLevel.Fastest);
                        i++;
                        using (var zipStream = entry.Open())
                        {
                            zipStream.Write(file, 0, file.Length);
                        }
                    }
                }
                return File(ms.ToArray(), "application/zip", "Archive.zip");
            }
            #endregion
        }

    }
}
