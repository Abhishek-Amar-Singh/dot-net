﻿using Microsoft.AspNetCore.Mvc;
using WebApplication1.Contexts;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class ProductController : Controller
    {
        private readonly ProductDbContext productDbContext;

        public ProductController(ProductDbContext productDbContext)
        {
            this.productDbContext = productDbContext;
        }

        [HttpGet]
        public IActionResult AllProducts()
        {
            var productList = productDbContext.Products.ToList();
            return View(productList);
        }

        [HttpGet]
        public IActionResult CreateProduct()
        {
            return View();
        }
        [HttpPost]
        public IActionResult CreateProduct(Product product)
        {
            var addProductObj = productDbContext.Products.Add(product);
            productDbContext.SaveChanges();
            return RedirectToAction("AllProducts");
        }

        [HttpGet]
        public IActionResult DeleteProduct(int id)
        {
            var product = productDbContext.Products.FirstOrDefault(p => p.Id == id);
            return (id is not 0) ? View(product) : Json($"404 - product having id={id} is nowhere to be found.");
        }
        [HttpPost]
        public IActionResult DeleteProductById(int id)
        {
            var product = productDbContext.Products.FirstOrDefault(p => p.Id == id);
            if (product is not null)
            {
                var delProductObj = productDbContext.Products.Remove(product);
                productDbContext.SaveChanges();
                return RedirectToAction("AllProducts");
            }
            return Json(false);
        }
    }
}
