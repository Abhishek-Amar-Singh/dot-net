﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.FlowAnalysis.DataFlow;
using Microsoft.EntityFrameworkCore;
using System.IO;
using System.Text;
using WebApplication1.Contexts;
using WebApplication1.Models;
using static System.Net.WebRequestMethods;

namespace WebApplication1.Controllers
{
    public class UserController : Controller
    {
        private readonly UserDbContext userDbContext;
        public UserController(UserDbContext userDbContext)
        {
            this.userDbContext = userDbContext;
        }

        [HttpGet]
        public async Task<IActionResult> AllUsers()
        {
            var userList = await userDbContext.Users.ToListAsync();
            return View(userList);
        }

        [HttpGet]
        public IActionResult RegisterUser()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> RegisterUser(User user)
        {
            using (var ms = new MemoryStream())
            {
                user.theFile.CopyTo(ms);
                var fileBytes = ms.ToArray();
                user.fileData = fileBytes;
            }
            user.fileExtension = System.IO.Path.GetExtension(user.theFile.FileName);
            var addUserObj = await userDbContext.Users.AddAsync(user);
            await userDbContext.SaveChangesAsync();
            return RedirectToAction("AllUsers");
        }

        [HttpGet]
        public string F(int id)
        {
            return $"{id}, hjjdkkdid mk";
        }
        [HttpGet]
        public IActionResult GetRespectiveDocument(int id)
        {
            var userData = userDbContext.Users.FirstOrDefault(j => j.Id == id);
            var fileName = $"{userData.Id}-{userData.Name}{userData.fileExtension}";
            HttpContext.Response.Headers.Add("Content-Disposition", $"inline; filename={fileName}");

            var generatedFile = File(userData.fileData, "application/vnd", fileName);
            return generatedFile;

            //var generatedFileStreamResult = new FileStreamResult(new MemoryStream(generatedFile.FileContents), generatedFile.ContentType);
            //generatedFileStreamResult.FileDownloadName = generatedFile.FileDownloadName;
            //using (var generatedFileStream = generatedFileStreamResult.FileStream)
            //{
            //    IFormFile theFile = new FormFile(generatedFileStream, 0, generatedFileStream.Length, "name", generatedFileStreamResult.FileDownloadName);
            //    return View(theFile);
            //}
            //return View();
        }

        [HttpGet]
        public byte[] GetRespectiveDocument2(int id)
        {
            var userData = userDbContext.Users.FirstOrDefault(j => j.Id == id);
            return userData.fileData;
            //StringBuilder sb = new StringBuilder("");
            //foreach (var b in userData.fileData)
            //{
            //    sb.Append(b + ",");
            //}
            //sb.Remove(sb.Length - 1, 1);
            //return sb;
        }

        [HttpGet]
        public IActionResult GetRespectiveDocument3(int id)
        {
            var userData = userDbContext.Users.FirstOrDefault(j => j.Id == id);
            return View(userData);
        }
    }
}
