﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Contexts;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class PizzaController : Controller
    {
        private readonly PizzaDbContext pizzaDbContext;

        public PizzaController(PizzaDbContext pizzaDbContext)
        {
            this.pizzaDbContext = pizzaDbContext;
        }

        public async Task<IActionResult> AllPizzas()
        {
            var pizzaList = await pizzaDbContext.Pizzas.ToListAsync();
            return View(pizzaList);
        }
        [HttpGet]
        public IActionResult CreatePizza()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreatePizza(Pizza pizza)
        {
            var addPizzaObj = await pizzaDbContext.Pizzas.AddAsync(pizza);
            await pizzaDbContext.SaveChangesAsync();
            return RedirectToAction("AllPizzas");
        }

        [HttpGet]
        public async Task<IActionResult> EditPizza(Guid id)
        {
            var pizza = await pizzaDbContext.Pizzas.FirstOrDefaultAsync(p => p.Id == id);
            return View(pizza);
        }

        [HttpPost]
        public async Task<IActionResult> EditPizza(Pizza pizza)
        {
            pizzaDbContext.Entry(pizza).State = EntityState.Modified;
            await pizzaDbContext.SaveChangesAsync();
            return RedirectToAction("AllPizzas");
        }
    }
}
