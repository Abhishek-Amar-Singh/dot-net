﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    public class Docs
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(TypeName = "CHAR(20)")]
        public string DocCode { get; set; } = null!;

        [NotMapped]
        public ICollection<IFormFile>? Documents { get; set; }

        [Column(TypeName = "jsonb")]
        public string MetaData { get; set; } = null!;

        public string[]? fileExtensions { get; set; }
    }
}
