﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    public class Pizza
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column(TypeName ="UUID")]
        public Guid Id { get; set; }
        public string Name { get; set; } = null!;
        public decimal price { get; set; }
        public string? origin { get; set; }
    }
}
