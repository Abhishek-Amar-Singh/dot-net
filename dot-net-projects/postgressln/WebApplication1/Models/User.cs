﻿

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Models
{
    public class User
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public float M0 { get; set; }
        public Single M1 { get; set; }
        public double M2 { get; set; }
        public DateTime DOB { get; set; }
        public bool IsDeleted { get; set; }
        [NotMapped]
        public IFormFile? theFile { get; set; }
        public byte[]? fileData { get; set; }
        public string? fileExtension { get; set; }
        public List<int>? LookingFor { get; set; }
    }
}
