﻿using CartAPI.Exceptions;
using CartAPI.Models;
using CartAPI.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Globalization;

namespace CartAPI.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly ICartService _cartService;

        public CartController(ICartService _cartService)
        {
            this._cartService = _cartService;
        }

        [Route("ViewCart")]
        [HttpGet]
        public IActionResult ViewCart(string email)
        {
            List<CartPizza> cartProducts = _cartService.ViewCart(email.ToLower());
            return Ok(cartProducts);
        }

        [Route("AddPizzaToCart")]
        [HttpPost]
        public IActionResult AddPizzaToCart(CartPizza cartPizza)
        {
            bool addPizzaToCartStatus = _cartService.AddPizzaToCart(cartPizza);
            return Ok(addPizzaToCartStatus);
        }

        [Route("DeletePizzaFromCart/{id:int}")]
        [HttpDelete]
        public IActionResult DeletePizzaFromCart(int id)
        {
            try
            {
                bool deletePizzaFromCartStatus = _cartService.DeletePizzaFromCart(id);
                return Ok(deletePizzaFromCartStatus);
            }
            catch(CartPizzaNotFoundException cpnfe)
            {
                return NotFound(cpnfe.Message);
            }
        }

        [Route("DeletePizzasFromCartByPizzaName/{pizzaName}")]
        [HttpDelete]
        public IActionResult DeletePizzasFromCartByPizzaName(string pizzaName)
        {
            pizzaName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(pizzaName);
            bool deletePizzaFromCartStatus = _cartService.DeletePizzasFromCartByPizzaName(pizzaName);
            return Ok(deletePizzaFromCartStatus);
        }

        [Route("DeletePizzasFromCartByEmail/{emailId}")]
        [HttpDelete]
        public IActionResult DeletePizzasFromCartByEmail(string emailId)
        {
            bool deletePizzaFromCartStatus = _cartService.DeletePizzasFromCartByEmail(emailId.ToLower());
            return Ok(deletePizzaFromCartStatus);
        }

        [Route("EditQuantityUpdatePriceQuantityOfCartPizza/{id:int}")]
        [HttpPut]
        public IActionResult EditQuantityUpdatePriceQuantityOfCartPizza(int id, CartPizza cartPizza)
        {
            try
            {
                bool editStatus = _cartService.EditQuantityUpdatePriceQuantityOfCartPizza(id, cartPizza);
                return Ok(editStatus);
            }
            catch (CartPizzaNotFoundException cpnfe)
            {
                return NotFound(cpnfe.Message);
            }
        }

        [Route("UpdateAllCartPizzasByName/{pizzaName}")]
        [HttpPut]
        public IActionResult UpdateAllCartPizzasByName(string pizzaName, CartPizza cartPizza)
        {
            bool updateStatus = _cartService.UpdateAllCartPizzasByName(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(pizzaName), cartPizza);
            return Ok(updateStatus);
        }

        [Route("GetAllAdmins")]
        [HttpGet]
        public IActionResult GetAllAdmins()
        {
            List<AdminLogin> adminList = _cartService.GetAllAdmins();
            return Ok(adminList);
        }

        [Route("GetCartPizzaById/{id:int}")]
        [HttpGet]
        public IActionResult GetCartPizzaById(int id)
        {
            try
            {
                CartPizza cartPizza = _cartService.GetCartPizzaById(id);
                return Ok(cartPizza);
            }
            catch(CartPizzaNotFoundException cpnfe)
            {
                return NotFound(cpnfe.Message);
            }
        }

        [Route("GetCountOfPizzasPresentInTheCartBasedOnEmailId/{emailId}")]
        [HttpGet]
        public IActionResult GetCountOfPizzasPresentInTheCartBasedOnEmailId(string emailId)
        {
            return Ok(_cartService.GetCountOfPizzasPresentInTheCartBasedOnEmailId(emailId));
        }


        //SendMail
        [Route("SendEmail")]
        [HttpPost]
        public IActionResult SendEmail(string userEmail, EmailDetails request)
        {
            _cartService.SendEmail(userEmail, request);
            return Ok();
        }
    }
}
