﻿namespace CartAPI.Exceptions
{
    public class CartPizzaNotFoundException : ApplicationException
    {
        public CartPizzaNotFoundException()
        {

        }

        public CartPizzaNotFoundException(string msg) : base(msg)
        {

        }
    }
}
