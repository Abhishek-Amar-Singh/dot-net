﻿using CartAPI.Models;

namespace CartAPI.Service
{
    public interface ICartService
    {
        List<CartPizza> ViewCart(string email);

        bool AddPizzaToCart(CartPizza cartPizza);

        bool DeletePizzaFromCart(int id);

        bool DeletePizzasFromCartByPizzaName(string pizzaName);

        bool DeletePizzasFromCartByEmail(string emailId);

        bool EditQuantityUpdatePriceQuantityOfCartPizza(int id, CartPizza cartPizza);

        bool UpdateAllCartPizzasByName(string pizzaName, CartPizza cartPizza);

        List<AdminLogin> GetAllAdmins();

        CartPizza GetCartPizzaById(int id);

        void SendEmail(string userEmail, EmailDetails request);
        int GetCountOfPizzasPresentInTheCartBasedOnEmailId(string emailId);
    }
}
