﻿using CartAPI.Exceptions;
using CartAPI.Models;
using CartAPI.Repository;
using MailKit.Security;
using MimeKit;
using MimeKit.Text;
using System.Globalization;
using System.Net.Mail;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;

namespace CartAPI.Service
{
    public class CartService : ICartService
    {
        private readonly ICartRepository _cartRepository;
        private readonly IConfiguration _configuration;

        public CartService(ICartRepository _cartRepository, IConfiguration _configuration)
        {
            this._cartRepository = _cartRepository;
            this._configuration = _configuration;
        }

        public List<CartPizza> ViewCart(string email)
        {
            List<AdminLogin> adminList = _cartRepository.GetAllAdmins();
            foreach (AdminLogin admin in adminList)
            {
                if(email.Equals(admin.Email))
                {
                    return _cartRepository.ViewCart();
                }
            }
            return _cartRepository.ViewCart(email);
        }

        public bool AddPizzaToCart(CartPizza cartPizza)
        {
            cartPizza.Name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cartPizza.Name);
            cartPizza.Category = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cartPizza.Category);
            cartPizza.Email = cartPizza.Email.ToLower();
            CartPizza mCartPizza = _cartRepository.GetCartPizzaByPizzaNameAndEmail(cartPizza.Name, cartPizza.Email);
            if (mCartPizza is null)
            {
                int addPizzaToCartStatus = _cartRepository.AddPizzaToCart(cartPizza);
                return (addPizzaToCartStatus == 1) ? true : false;
            }
            else
            {
                double singlePizzaPrice = mCartPizza.Price / mCartPizza.Quantity;
                mCartPizza.Quantity += 1;
                mCartPizza.Price = singlePizzaPrice * mCartPizza.Quantity;
                int updatePizzaPriceQuantityOfCartStatus = _cartRepository.UpdateCartPizza(mCartPizza);
                return updatePizzaPriceQuantityOfCartStatus == 1 ? true : false;
            }
        }

        public bool DeletePizzaFromCart(int id)
        {
            CartPizza cartPizza = _cartRepository.GetCartPizzaById(id);
            if (cartPizza is null)
            {
                throw new CartPizzaNotFoundException($"CartList having itemId='{id}' does not exists. Hence, deletion operation cannot be performed.");
            }
            else
            {
                int deletePizzaFromCartStatus = _cartRepository.DeletePizzaFromCart(cartPizza);
                return (deletePizzaFromCartStatus == 1) ? true : false;
            }
        }

        public bool DeletePizzasFromCartByPizzaName(string pizzaName)
        {
            List<CartPizza> cartPizzas = _cartRepository.GetAllCartPizzasByPizzaName(pizzaName);
            int deletePizzaFromCartStatus = 0;
            foreach (var item in cartPizzas)
            {
                deletePizzaFromCartStatus += _cartRepository.DeletePizzaFromCart(item);
            }
            return (deletePizzaFromCartStatus > 0) ? true : false;
        }

        public bool DeletePizzasFromCartByEmail(string emailId)
        {
            List<CartPizza> cartPizzas = _cartRepository.GetAllCartPizzasByEmail(emailId);
            int deletePizzaFromCartStatus = 0;
            foreach (var item in cartPizzas)
            {
                deletePizzaFromCartStatus += _cartRepository.DeletePizzaFromCart(item);
            }
            return (deletePizzaFromCartStatus > 0) ? true : false;
        }

        public bool EditQuantityUpdatePriceQuantityOfCartPizza(int id, CartPizza cartPizza)
        {
            CartPizza cartPizzaItem = _cartRepository.GetCartPizzaById(id);
            if (cartPizzaItem is null)
            {
                throw new CartPizzaNotFoundException($"CartList having itemId='{id}' does not exists. Hence, updation operation cannot be performed.");
            }
            else
            {
                double singlePizzaPrice = cartPizzaItem.Price / cartPizzaItem.Quantity;
                cartPizzaItem.Quantity = cartPizza.Quantity;
                cartPizzaItem.Price = singlePizzaPrice * cartPizzaItem.Quantity;
                int updatePriceQuantityOfCartPizzaStatus = _cartRepository.UpdateCartPizza(cartPizzaItem);
                return (updatePriceQuantityOfCartPizzaStatus == 1) ? true : false;
            }
        }

        public bool UpdateAllCartPizzasByName(string pizzaName, CartPizza cartPizza)
        {
            List<CartPizza> cartPizzas = _cartRepository.GetAllCartPizzasByPizzaName(pizzaName);
            int updateCartPizzaStatus = 0;
            foreach (var item in cartPizzas)
            {
                item.Category = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cartPizza.Category);
                item.Price = cartPizza.Price * item.Quantity;
                updateCartPizzaStatus += _cartRepository.UpdateCartPizza(item);
            }
            return (updateCartPizzaStatus > 0) ? true : false;
        }

        public List<AdminLogin> GetAllAdmins()
        {
            return _cartRepository.GetAllAdmins();
        }

        public CartPizza GetCartPizzaById(int id)
        {
            CartPizza cartPizza = _cartRepository.GetCartPizzaById(id);
            if(cartPizza is null)
            {
                throw new CartPizzaNotFoundException($"CartList having itemId='{id}' does not exists. Hence, result cannot be fetched.");
            }
            else
            {
                return cartPizza;
            }
        }


        //SendEmail
        public void SendEmail(string userEmail, EmailDetails request)
        {

            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(_configuration.GetSection("UserEmail").Value));
            email.To.Add(MailboxAddress.Parse(userEmail));
            email.Subject = "Invoice Details";
            email.Body = new TextPart(TextFormat.Html) { Text = request.Body };
            var smtp = new SmtpClient();
            smtp.Connect(_configuration.GetSection("EmailHost").Value, 587, SecureSocketOptions.StartTls);//host and port
            smtp.Authenticate(_configuration.GetSection("UserEmail").Value, _configuration.GetSection("EmailPassword").Value);
            smtp.Send(email);
            smtp.Disconnect(true);
        }

        public int GetCountOfPizzasPresentInTheCartBasedOnEmailId(string emailId)
        {
            return _cartRepository.GetCountOfPizzasPresentInTheCartBasedOnEmailId(emailId);
        }
    }
}
