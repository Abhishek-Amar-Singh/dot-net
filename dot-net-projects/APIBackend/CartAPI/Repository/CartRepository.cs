﻿using CartAPI.Context;
using CartAPI.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace CartAPI.Repository
{
    public class CartRepository : ICartRepository
    {
        private readonly AdminDbContext adminDbContext;
        private readonly CartDbContext cartDbContext;

        public CartRepository(AdminDbContext adminDbContext, CartDbContext cartDbContext)
        {
            this.adminDbContext = adminDbContext;
            this.cartDbContext = cartDbContext;
        }

        public List<AdminLogin> GetAllAdmins()
        {
            return adminDbContext.AdminList.ToList();
        }

        public List<CartPizza> ViewCart()
        {
            return cartDbContext.CartList.ToList();
        }

        public List<CartPizza> ViewCart(string email)
        {
            return cartDbContext.CartList.Where(c => c.Email == email).ToList();
        }

        public CartPizza GetCartPizzaByPizzaNameAndEmail(string pizzaName, string email)
        {
            return cartDbContext.CartList.FirstOrDefault(c => c.Name == pizzaName && c.Email == email);
        }

        public int AddPizzaToCart(CartPizza cartPizza)
        {
            cartDbContext.CartList.Add(cartPizza);
            return cartDbContext.SaveChanges();
        }

        public int UpdateCartPizza(CartPizza cartPizza)
        {
            cartDbContext.Entry<CartPizza>(cartPizza).State = EntityState.Modified;
            return cartDbContext.SaveChanges();
        }

        public CartPizza GetCartPizzaById(int id)
        {
            return cartDbContext.CartList.FirstOrDefault(c => c.Id == id);
        }

        public int DeletePizzaFromCart(CartPizza cartPizza)
        {
            cartDbContext.CartList.Remove(cartPizza);
            return cartDbContext.SaveChanges();
        }

        public List<CartPizza> GetAllCartPizzasByPizzaName(string pizzaName)
        {
            return cartDbContext.CartList.Where(c => c.Name == pizzaName).ToList();
        }

        public List<CartPizza> GetAllCartPizzasByEmail(string emailId)
        {
            return cartDbContext.CartList.Where(c => c.Email == emailId).ToList();
        }

        public int GetCountOfPizzasPresentInTheCartBasedOnEmailId(string emailId)
        {
            return cartDbContext.CartList.Where(c => c.Email == emailId).ToList().Count;
        }
    }
}