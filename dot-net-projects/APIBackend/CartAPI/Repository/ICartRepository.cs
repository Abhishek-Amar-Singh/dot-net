﻿using CartAPI.Models;

namespace CartAPI.Repository
{
    public interface ICartRepository
    {
        List<AdminLogin> GetAllAdmins();

        List<CartPizza> ViewCart();

        List<CartPizza> ViewCart(string email);

        CartPizza GetCartPizzaByPizzaNameAndEmail(string pizzaName, string email);

        int AddPizzaToCart(CartPizza cartPizza);

        int UpdateCartPizza(CartPizza cartPizza);

        CartPizza GetCartPizzaById(int id);

        int DeletePizzaFromCart(CartPizza cartPizza);

        List<CartPizza> GetAllCartPizzasByPizzaName(string pizzaName);

        List<CartPizza> GetAllCartPizzasByEmail(string emailId);
        int GetCountOfPizzasPresentInTheCartBasedOnEmailId(string emailId);
    }
}
