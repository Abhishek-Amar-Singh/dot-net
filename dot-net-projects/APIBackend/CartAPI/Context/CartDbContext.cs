﻿using CartAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace CartAPI.Context
{
    public class CartDbContext : DbContext
    {
        public CartDbContext(DbContextOptions<CartDbContext> dbContextOptions) : base(dbContextOptions)
        {

        }
        public DbSet<CartPizza> CartList { get; set; }
    }
}