﻿using CartAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace CartAPI.Context
{
    public class AdminDbContext : DbContext
    {
        public AdminDbContext(DbContextOptions<AdminDbContext> dbContextOptions) : base(dbContextOptions)
        {

        }
        public DbSet<AdminLogin> AdminList { get; set; }
    }
}
