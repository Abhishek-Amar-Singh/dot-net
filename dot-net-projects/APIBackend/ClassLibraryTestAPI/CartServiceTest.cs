﻿using CartAPI.Exceptions;
using CartAPI.Models;
using CartAPI.Repository;
using CartAPI.Service;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace ClassLibraryTestAPI
{
    public class CartServiceTest : IClassFixture<CartDbFixture>, IClassFixture<AdminDbFixture>
    {
        private readonly ICartService cartService;
        private readonly ICartRepository cartRepository;
        private readonly IConfiguration _configuration;

        public CartServiceTest(AdminDbFixture adminDbFixture, CartDbFixture cartDbFixture)
        {
            cartRepository = new CartRepository(adminDbFixture.adminDbContext, cartDbFixture.cartDbContext);
            cartService = new CartService(cartRepository, _configuration);
        }

        [Fact]
        public void TestGetAllAdmins()
        {
            var actualAdmins = cartService.GetAllAdmins();
            Assert.Single(actualAdmins);//we have multiple admin, so this test should fail
        }

        [Fact]
        public void TestViewCartForAdmin()
        {
            var actualCartListCount = cartService.ViewCart("admust@microsoft.com").Count;
            Assert.Equal(4, actualCartListCount);
        }

        [Fact]
        public void TestViewCartForUser()
        {
            var actualCartListCount = cartService.ViewCart("ram@gmail.com").Count;
            Assert.Equal(2, actualCartListCount);
        }

        [Fact]
        public void TestAddPizzaToCart()
        {
            CartPizza cartPizza = new CartPizza() { Name = "Neapolitan", Price = 430.9f, Category = "Veg", Email = "ram@gmail.com" };
            var addPizzaToCartStatus = cartService.AddPizzaToCart(cartPizza);
            Assert.True(addPizzaToCartStatus);
        }

        [Fact]
        public void TestDeletePizzaFromCart()
        {
            try
            {
                int id = 201;//2010
                var deletePizzaFromCartStatus = cartService.DeletePizzaFromCart(id);
                Assert.True(deletePizzaFromCartStatus);
            }
            catch(CartPizzaNotFoundException cpnfe)
            {
                Assert.True(false, cpnfe.Message);
            }
        }

        [Fact]
        public void TestDeletePizzasFromCartByPizzaName()
        {
            string pizzaName = "Ligurian";
            var deletePizzasFromCartByPizzaNameStatus = cartService.DeletePizzasFromCartByPizzaName(pizzaName);
            Assert.True(deletePizzasFromCartByPizzaNameStatus, $"No pizza exists having name='{pizzaName}'.");
        }

        [Fact]
        public void TestDeletePizzasFromCartByEmail()
        {
            string emailId = "ram@gmail.com";
            var deletePizzasFromCartByEmailStatus = cartService.DeletePizzasFromCartByEmail(emailId);
            Assert.True(deletePizzasFromCartByEmailStatus, $"No pizza items were present for user having email id ({emailId}).");
        }

        [Fact]
        public void TestEditQuantityUpdatePriceQuantityOfCartPizza()
        {
            try
            {
                CartPizza cartPizza = new CartPizza() { Quantity = 5 };
                var editStatus = cartService.EditQuantityUpdatePriceQuantityOfCartPizza(203, cartPizza);
                Assert.True(editStatus);
            }
            catch(CartPizzaNotFoundException cpnfe)
            {
                Assert.True(false, cpnfe.Message);
            }
        }

        [Fact]
        public void TestUpdateAllCartPizzasByName()
        {
            CartPizza cartPizza = new CartPizza() { Category = "Veg", Price=100 };
            var editStatus = cartService.UpdateAllCartPizzasByName("Cheese Overload", cartPizza);
            Assert.True(editStatus);
        }
    }
}
