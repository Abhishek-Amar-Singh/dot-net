﻿using CartAPI.Context;
using CartAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace ClassLibraryTestAPI
{
    public class CartDbFixture
    {
        internal CartDbContext cartDbContext;
        public CartDbFixture()
        {
            var cartDbContextOptions = new DbContextOptionsBuilder<CartDbContext>().UseInMemoryDatabase("cartDb").Options;
            cartDbContext = new CartDbContext(cartDbContextOptions);
            cartDbContext.Add(new CartPizza() { Id = 200, Name = "Neapolitan", Price = 430.9f, Category = "Veg", Quantity = 1, Email = "ram@gmail.com" });
            cartDbContext.Add(new CartPizza() { Id = 201, Name = "Ligurian", Price = 430.9f, Category = "Non-Veg", Quantity = 1, Email = "ravi@gmail.com" });
            cartDbContext.Add(new CartPizza() { Id = 202, Name = "Cheese Overload", Price = 430.9f, Category = "Veg", Quantity = 1, Email = "ram@gmail.com" });
            cartDbContext.Add(new CartPizza() { Id = 203, Name = "Cheese Overload", Price =150.9f, Category = "Veg", Quantity = 1, Email = "bhumi@gmail.com" });
            cartDbContext.SaveChanges();
        }
    }

    public class AdminDbFixture
    {
        internal AdminDbContext adminDbContext;
        public AdminDbFixture()
        {
            var adminDbContextOptions = new DbContextOptionsBuilder<AdminDbContext>().UseInMemoryDatabase("adminDb").Options;
            adminDbContext = new AdminDbContext(adminDbContextOptions);
            adminDbContext.Add(new AdminLogin() { Id = 300, Email = "admust@microsoft.com", Password = "admust" });
            adminDbContext.Add(new AdminLogin() { Id = 301, Email = "adpriv0@yahoo.com", Password = "adpriv@0" });
            adminDbContext.Add(new AdminLogin() { Id = 302, Email = "admin@gmail.com", Password = "admin" });
            adminDbContext.SaveChanges();
        }
    }
}
