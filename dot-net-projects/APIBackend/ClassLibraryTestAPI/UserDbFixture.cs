﻿using Microsoft.EntityFrameworkCore;
using UserAPI.Context;
using UserAPI.Models;

namespace ClassLibraryTestAPI
{
    public class UserDbFixture
    {
        internal UserDbContext userDbContext;
        public UserDbFixture()
        {
            var userDbContextOptions = new DbContextOptionsBuilder<UserDbContext>().UseInMemoryDatabase("userDb").Options;
            userDbContext = new UserDbContext(userDbContextOptions);
            userDbContext.Add(new User() { Id = 50, Name = "Bhumi", Email = "bhumi@gmail.com", Password = "bhumi", ConfirmPassword = "bhumi", Address = "Kashi" });
            userDbContext.Add(new User() { Id = 51, Name = "Rama", Email = "ram@gmail.com", Password = "rama", ConfirmPassword = "rama", Address = "Rameshwaram" });
            userDbContext.Add(new User() { Id = 52, Name = "Shruti", Email = "shruti@gmail.com", Password = "shruti", ConfirmPassword = "shruti", Address = "Mumbai" });
            userDbContext.SaveChanges();
        }
    }
}