﻿using UserAPI.Exceptions;
using UserAPI.Models;
using UserAPI.Repository;
using UserAPI.Service;
using Xunit;

namespace ClassLibraryTestAPI
{
    public class UserServiceTest : IClassFixture<UserDbFixture>
    {
        private readonly IUserService userService;
        private readonly IUserRepository userRepository;
        public UserServiceTest(UserDbFixture userDbFixture)
        {
            // here using reference variable of interface
            userRepository = new UserRepository(userDbFixture.userDbContext);
            userService = new UserService(userRepository);
        }

        [Fact]
        public void TestToGetAllUsers()
        {
            var actualUserDetails = userService.GetAllUsers();
            Assert.Equal(3, actualUserDetails.Count);
        }

        [Fact]
        public void LoginShouldThrowException()
        {
            string expectedExceptionMessage = "ram@gmail.com credentials are invalid. Email address and/or password are/is incorrect.";
            string actualExceptionMessage = Assert.ThrowsAny<UserCredentialsInvalidException>(() =>
            {
                userService.LogIn(new LoginInfo() { Email = "ram@gmail.com", Password = "ram" });
            }).Message;
            Assert.Equal(expectedExceptionMessage, actualExceptionMessage);
        }

        [Fact]
        public void RegisterUserReturnSuccess()
        {
            try
            {
                var expectedUserEmail = "vikram@gmail.com";//shruti@gmail.com
                //Act--call development code
                userService.RegisterUser(new User() { Id = 53, Name = "Vikram", Email = expectedUserEmail, Password = "vikram", ConfirmPassword = "vikram", Address = "Virar" });
                var lastIndex = userService.GetAllUsers().Count - 1;
                var actualUserEmail = userService.GetAllUsers()[lastIndex].Email;
                //Assert--verify your expected result and actual result
                Assert.Equal(expectedUserEmail, actualUserEmail);
            }
            catch(UserAlreadyExistsException uaee)
            {
                Assert.True(false, uaee.Message);
            }
        }

        [Fact]
        public void DeleteUserSuccess()
        {
            string emailId = "shruti@gmail.com";//sufiya@gmail.com
            try
            {
                bool deleteUserStatus = userService.DeleteUser(emailId);
                Assert.True(deleteUserStatus);
            }
            catch (UserNotFoundException unfe)
            {
                Assert.True(false, unfe.Message);
            }
        }

        [Fact]
        public void EditUserReturnSuccess()
        {
            try
            {
                string emailId = "bhumi@gmail.com";
                User user = new User() { Name = "Bhumi", Email = emailId, Password = "bhumi", ConfirmPassword = "bhumi", Address = "Vile Parle" };
                bool editUserStatus = userService.EditUser(emailId, user);
                Assert.True(editUserStatus);
            }
            catch(UserNotFoundException unfe)
            {
                Assert.True(false, unfe.Message);
            }
        }
    }
}
