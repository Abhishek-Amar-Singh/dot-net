﻿using Microsoft.EntityFrameworkCore;
using PizzaAPI.Context;
using PizzaAPI.Models;
using System.Xml.Linq;

namespace ClassLibraryTestAPI
{
    public class PizzaDbFixture
    {
        internal PizzaDbContext pizzaDbContext;
        public PizzaDbFixture()
        {
            var pizzaDbContextOptions = new DbContextOptionsBuilder<PizzaDbContext>().UseInMemoryDatabase("pizzaDb").Options;
            pizzaDbContext = new PizzaDbContext(pizzaDbContextOptions);
            pizzaDbContext.Add(new Pizza() { Id = 100, Name = "Neapolitan", Price = 430.9f, Category = "Non-Veg" });
            pizzaDbContext.Add(new Pizza() { Id = 101, Name = "Ligurian", Price = 430.9f, Category = "Veg" });
            pizzaDbContext.Add(new Pizza() { Id = 102, Name = "Cheese Overload", Price = 430.9f, Category = "Veg" });
            pizzaDbContext.SaveChanges();
        }
    }
}
