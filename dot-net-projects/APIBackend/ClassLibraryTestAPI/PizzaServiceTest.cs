﻿using PizzaAPI.Exceptions;
using PizzaAPI.Models;
using PizzaAPI.Repository;
using PizzaAPI.Service;
using Xunit;

namespace ClassLibraryTestAPI
{
    public class PizzaServiceTest : IClassFixture<PizzaDbFixture>
    {
        private readonly IPizzaService pizzaService;
        private readonly IPizzaRepository pizzaRepository;

        public PizzaServiceTest(PizzaDbFixture pizzaDbFixture)
        {
            pizzaRepository = new PizzaRepository(pizzaDbFixture.pizzaDbContext);
            pizzaService = new PizzaService(pizzaRepository);
        }

        [Fact]
        public void TestToGetAllPizzas()
        {
            var actualPizzaDetails = pizzaService.GetAllPizzas();
            Assert.Equal(3, actualPizzaDetails.Count);
        }

        [Fact]
        public void TestAddPizza()
        {
            try
            {
                var actualAddPizzaStatus = pizzaService.AddPizza(new Pizza() { Id = 103, Name = "Olive Garden", Price = 430.9f, Category = "Non-Veg" });
                Assert.True(actualAddPizzaStatus);
            }
            catch(PizzaAlreadyExistsException paee)
            {
                Assert.True(false, paee.Message);
            }
        }

        [Fact]
        public void TestDeletePizza()
        {
            string pizzaName = "Cheese Overload";
            try
            {
                var actualDeletePizzaStatus = pizzaService.DeletePizza(pizzaName);
                Assert.True(actualDeletePizzaStatus);
            }
            catch (PizzaNotFoundException pnfe)
            {
                Assert.True(false, pnfe.Message);
            }
        }

        [Fact]
        public void TestEditPizza()
        {
            try
            {
                Pizza pizza = new Pizza() { Name = "Little Neapolitan", Price = 430.9f, Category = "Veg" };
                var actualEditPizzaStatus = pizzaService.EditPizza(1011, pizza);
                Assert.True(actualEditPizzaStatus);
            }
            catch(PizzaNotFoundException pnfe)
            {
                Assert.True(false, pnfe.Message);
            }
        }

    }
}
