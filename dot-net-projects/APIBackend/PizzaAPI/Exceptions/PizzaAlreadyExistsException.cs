﻿namespace PizzaAPI.Exceptions
{
    public class PizzaAlreadyExistsException : ApplicationException
    {
        public PizzaAlreadyExistsException()
        {

        }

        public PizzaAlreadyExistsException(string msg) : base(msg)
        {

        }
    }
}
