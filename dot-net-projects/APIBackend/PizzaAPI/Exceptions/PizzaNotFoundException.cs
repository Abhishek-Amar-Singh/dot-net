﻿namespace PizzaAPI.Exceptions
{
    public class PizzaNotFoundException : ApplicationException
    {
        public PizzaNotFoundException()
        {

        }
        public PizzaNotFoundException(string msg) : base(msg)
        {

        }
    }
}
