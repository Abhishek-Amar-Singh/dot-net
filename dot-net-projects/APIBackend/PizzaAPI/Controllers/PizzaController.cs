﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PizzaAPI.Exceptions;
using PizzaAPI.Models;
using PizzaAPI.Service;

namespace PizzaAPI.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PizzaController : ControllerBase
    {
        private readonly IPizzaService _pizzaService;

        public PizzaController(IPizzaService _pizzaService)
        {
            this._pizzaService = _pizzaService;
        }

        [Route("GetAllPizzas")]
        [HttpGet]
        public IActionResult GetAllPizzas()
        {
            List<Pizza> pizzaList = _pizzaService.GetAllPizzas();
            return Ok(pizzaList);
        }

        [Route("AddPizza")]
        [HttpPost]
        public IActionResult AddPizza(Pizza pizza)
        {
            try
            {
                bool addPizzaStatus = _pizzaService.AddPizza(pizza);
                return Ok(addPizzaStatus);
            }
            catch (PizzaAlreadyExistsException paee)
            {
                return Conflict(paee.Message);
            }
        }

        [Route("PizzaDetails/{id:int}")]
        [HttpGet]
        public IActionResult PizzasDetails(int id)
        {
            try
            {
                Pizza pizza = _pizzaService.GetPizzaById(id);
                return Ok(pizza);
            }
            catch(PizzaNotFoundException pnfe)
            {
                return NotFound(pnfe.Message);
            }
        }

        [Route("DeletePizza/{pizzaName}")]
        [HttpDelete]
        public IActionResult DeletePizza(string pizzaName)
        {
            try
            {
                bool deletePizzaStatus = _pizzaService.DeletePizza(pizzaName);
                return Ok(deletePizzaStatus);
            }
            catch (PizzaNotFoundException pnfe)
            {
                return NotFound(pnfe.Message);
            }
        }

        [Route("EditPizza/{id:int}")]
        [HttpPut]
        public IActionResult EditPizza(int id, Pizza pizza)
        {
            try
            {
                bool editPizzaStatus = _pizzaService.EditPizza(id, pizza);
                return Ok(editPizzaStatus);
            }
            catch (PizzaNotFoundException pdnee)
            {
                return NotFound(pdnee.Message);
            }
        }
    }
}
