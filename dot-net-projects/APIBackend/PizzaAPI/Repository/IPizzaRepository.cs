﻿using PizzaAPI.Models;

namespace PizzaAPI.Repository
{
    public interface IPizzaRepository
    {
        List<Pizza> GetAllPizzas();

        int AddPizza(Pizza pizza);

        Pizza GetPizzaByName(string pizzaName);

        Pizza GetPizzaById(int id);

        int DeletePizza(Pizza pizza);

        int EditPizza(Pizza pizzaItem);
    }
}
