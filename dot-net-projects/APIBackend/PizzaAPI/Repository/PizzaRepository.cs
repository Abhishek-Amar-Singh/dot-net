﻿using Microsoft.EntityFrameworkCore;
using PizzaAPI.Context;
using PizzaAPI.Models;
using System.Linq;

namespace PizzaAPI.Repository
{
    public class PizzaRepository : IPizzaRepository
    {
        private readonly PizzaDbContext pizzaDbContext;

        public PizzaRepository(PizzaDbContext pizzaDbContext)
        {
            this.pizzaDbContext = pizzaDbContext;
        }

        public List<Pizza> GetAllPizzas()
        {
            return pizzaDbContext.PizzaList.ToList();
        }

        public int AddPizza(Pizza pizza)
        {
            pizzaDbContext.PizzaList.Add(pizza);
            return pizzaDbContext.SaveChanges();
        }

        public Pizza GetPizzaByName(string pizzaName)
        {
            return pizzaDbContext.PizzaList.FirstOrDefault(p => p.Name == pizzaName);
        }

        public Pizza GetPizzaById(int id)
        {
            return pizzaDbContext.PizzaList.FirstOrDefault(p => p.Id == id);
        }

        public int DeletePizza(Pizza pizza)
        {
            pizzaDbContext.PizzaList.Remove(pizza);
            return pizzaDbContext.SaveChanges();
        }

        public int EditPizza(Pizza pizzaItem)
        {
            pizzaDbContext.Entry(pizzaItem).State = EntityState.Modified;
            return pizzaDbContext.SaveChanges();
        }
    }
}
