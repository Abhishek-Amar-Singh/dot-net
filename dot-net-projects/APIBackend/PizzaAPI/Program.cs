using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using PizzaAPI.Context;
using PizzaAPI.Repository;
using PizzaAPI.Service;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//--Databse Connection
var localDbConnectionString = builder.Configuration.GetConnectionString("localDbConnectionStr");
builder.Services.AddDbContext<PizzaDbContext>(options => options.UseSqlServer(localDbConnectionString));

//--Services Used
builder.Services.AddScoped<IPizzaRepository, PizzaRepository>();
builder.Services.AddScoped<IPizzaService, PizzaService>();

//--Add CORS Policy
builder.Services.AddCors();

//--authentication and authorization
ValidateTokenWithParameters(builder.Services, builder.Configuration);
void ValidateTokenWithParameters(IServiceCollection services, ConfigurationManager configuration)
{
    var userSecretKey = configuration["JwtValidationDetails:UserApplicationSecretKey"];
    var userIssuer = configuration["JwtValidationDetails:UserIssuer"];
    var userAudience = configuration["JwtValidationDetails:UserAudience"];
    var userSecurityKeyInBytes = Encoding.UTF8.GetBytes(userSecretKey);
    var userSymmetricSecurity = new SymmetricSecurityKey(userSecurityKeyInBytes);
    var tokenValidationParameters = new TokenValidationParameters()
    {
        ValidateIssuer = true,
        ValidIssuer = userIssuer,

        ValidateAudience = true,
        ValidAudience = userAudience,

        ValidateIssuerSigningKey = true,
        IssuerSigningKey = userSymmetricSecurity,

        ValidateLifetime = true
    };
    builder.Services.AddAuthentication(u =>
    {
        u.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        u.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    }).AddJwtBearer(u => u.TokenValidationParameters = tokenValidationParameters);
}

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

//--Use CORS Policy
app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

//--authentication and authorization
app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
