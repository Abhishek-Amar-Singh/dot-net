﻿using PizzaAPI.Models;

namespace PizzaAPI.Service
{
    public interface IPizzaService
    {
        List<Pizza> GetAllPizzas();

        bool AddPizza(Pizza pizza);

        Pizza GetPizzaById(int id);

        bool DeletePizza(string pizzaName);

        bool EditPizza(int id, Pizza pizza);
    }
}
