﻿using PizzaAPI.Exceptions;
using PizzaAPI.Models;
using PizzaAPI.Repository;
using System.Globalization;

namespace PizzaAPI.Service
{
    public class PizzaService : IPizzaService
    {
        private readonly IPizzaRepository _pizzaRepository;

        public PizzaService(IPizzaRepository _pizzaRepository)
        {
            this._pizzaRepository = _pizzaRepository;
        }

        public List<Pizza> GetAllPizzas()
        {
            return _pizzaRepository.GetAllPizzas();
        }

        public bool AddPizza(Pizza pizza)
        {
            Pizza pizzaItem = _pizzaRepository.GetPizzaByName(pizza.Name);
            if (pizzaItem is null)
            {
                pizza.Name = pizza.Name.Replace(pizza.Name, CultureInfo.CurrentCulture.TextInfo.ToTitleCase(pizza.Name));
                pizza.Category = pizza.Category.Replace(pizza.Category, CultureInfo.CurrentCulture.TextInfo.ToTitleCase(pizza.Category));
                int addPizzaStatus = _pizzaRepository.AddPizza(pizza);
                return (addPizzaStatus == 1) ? true : false;
            }
            else
            {
                throw new PizzaAlreadyExistsException($"Pizza having name='{pizza.Name}' already exist. Hence, addition operation cannot be performed.");
            }
        }

        public Pizza GetPizzaById(int id)
        {
            Pizza pizza = _pizzaRepository.GetPizzaById(id);
            return (pizza is not null) ? pizza : throw new PizzaNotFoundException($"Pizza having id='{id}' does not exist. Hence, result cannot be fetched.");
        }

        public bool DeletePizza(string pizzaName)
        {
            Pizza pizza = _pizzaRepository.GetPizzaByName(pizzaName);
            if (pizza is null)
            {
                throw new PizzaNotFoundException($"Pizza having name='{pizzaName}' does not exist. Hence, deletion operation cannot be performed.");
            }
            else
            {
                int deletePizzaStatus = _pizzaRepository.DeletePizza(pizza);
                return (deletePizzaStatus == 1) ? true : false;
            }
        }

        public bool EditPizza(int id, Pizza pizza)
        {
            Pizza pizzaItem = _pizzaRepository.GetPizzaById(id);
            if (pizzaItem is null)
            {
                throw new PizzaNotFoundException($"Pizza having id='{id}' does not exist. Hence, updation operation cannot be performed.");
            }
            else
            {
                pizzaItem.Name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(pizza.Name);
                pizzaItem.Price = pizza.Price;
                pizzaItem.Category = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(pizza.Category);
                int editPizzaStatus = _pizzaRepository.EditPizza(pizzaItem);
                return (editPizzaStatus == 1) ? true : false;
            }
        }

    }
}
