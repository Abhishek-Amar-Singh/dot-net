﻿using Microsoft.EntityFrameworkCore;
using PizzaAPI.Models;

namespace PizzaAPI.Context
{
    public class PizzaDbContext : DbContext
    {
        public PizzaDbContext(DbContextOptions<PizzaDbContext> contextOptions) : base(contextOptions)
        {

        }

        public DbSet<Pizza> PizzaList { get; set; }
    }
}
