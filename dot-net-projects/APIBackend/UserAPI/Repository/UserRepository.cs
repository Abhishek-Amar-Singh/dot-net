﻿using Microsoft.EntityFrameworkCore;
using System.Xml.Linq;
using UserAPI.Context;
using UserAPI.Models;

namespace UserAPI.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly UserDbContext userDbContext;

        public UserRepository(UserDbContext userDbContext)
        {
            this.userDbContext = userDbContext;
        }

        public List<User> GetAllUsers()
        {
            return userDbContext.UserList.ToList();
        }

        public int RegisterUser(User user)
        {
            userDbContext.UserList.Add(user);
            return userDbContext.SaveChanges();
        }

        public User GetUserByEmail(string email)
        {
            return userDbContext.UserList.FirstOrDefault(u => u.Email == email);
        }

        public int DeleteUser(User user)
        {
            userDbContext.UserList.Remove(user);
            return userDbContext.SaveChanges();
        }

        public int EditUser(User user)
        {
            userDbContext.Entry(user).State = EntityState.Modified;
            return userDbContext.SaveChanges();
        }

        public User LogIn(string? email, string? password)
        {
            return userDbContext.UserList.FirstOrDefault(u => u.Email == email && u.Password == password);
        }

    }
}