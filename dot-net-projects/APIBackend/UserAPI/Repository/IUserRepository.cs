﻿using UserAPI.Models;

namespace UserAPI.Repository
{
    public interface IUserRepository
    {
        List<User> GetAllUsers();

        int RegisterUser(User user);

        User GetUserByEmail(string email);

        int DeleteUser(User user);

        int EditUser(User user);

        User LogIn(string? email, string? password);
    }
}
