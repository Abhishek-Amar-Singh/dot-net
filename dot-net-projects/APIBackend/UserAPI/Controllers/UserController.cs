﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserAPI.Exceptions;
using UserAPI.Models;
using UserAPI.Service;
using UserAPI.Service.TokenGeneration;

namespace UserAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ITokenGenerator _tokenGenerator;

        public UserController(IUserService _userService, ITokenGenerator _tokenGenerator)
        {
            this._userService = _userService;
            this._tokenGenerator = _tokenGenerator;
        }

        [Route("GetAllUsers")]
        [HttpGet]
        public IActionResult GetAllUsers()
        {
            List<User> userList = _userService.GetAllUsers();
            return Ok(userList);
        }

        [Route("RegisterUser")]
        [HttpPost]
        public IActionResult RegisterUser(User user)
        {
            try
            {
                bool registerUserStatus = _userService.RegisterUser(user);
                return Ok(registerUserStatus);
            }
            catch (UserAlreadyExistsException uaee)
            {
                return Conflict(uaee.Message);
            }
        }

        [Route("UserDetails/{emailId}")]
        [HttpGet]
        public IActionResult UserDetails(string emailId)
        {
            try
            {
                User user = _userService.GetUserByEmailId(emailId.ToLower());
                return Ok(user);
            }
            catch (UserNotFoundException unfe)
            {
                return NotFound(unfe.Message);
            }
        }

        [Route("DeleteUser/{emailId}")]
        [HttpDelete]
        public IActionResult DeleteUser(string emailId)
        {
            try
            {
                bool deleteUserStatus = _userService.DeleteUser(emailId.ToLower());
                return Ok(deleteUserStatus);
            }
            catch (UserNotFoundException unfe)
            {
                return NotFound(unfe.Message);
            }
        }

        [Route("EditUser/{emailId}")]
        [HttpPut]
        public IActionResult EditUser(string emailId, User user)
        {
            try
            {
                bool editUserStatus = _userService.EditUser(emailId.ToLower(), user);
                return Ok(editUserStatus);
            }
            catch (UserNotFoundException unfe)
            {
                return NotFound(unfe.Message);
            }
        }

        [Route("LogIn")]
        [HttpPost]
        public IActionResult LogIn(LoginInfo loginInfo)
        {
            try
            {
                User user = _userService.LogIn(loginInfo);
                string userToken = _tokenGenerator.GenerateToken(user.Id, user.Name, user.Email);
                return Ok(userToken);
            }
            catch (UserCredentialsInvalidException ucie)
            {
                return Unauthorized(ucie.Message);
            }
        }
    }
}