using Microsoft.EntityFrameworkCore;
using UserAPI.Context;
using UserAPI.Repository;
using UserAPI.Service;
using UserAPI.Service.TokenGeneration;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//--Databse Connection
var localDbConnectionString = builder.Configuration.GetConnectionString("localDbConnectionStr");
builder.Services.AddDbContext<UserDbContext>(options => options.UseSqlServer(localDbConnectionString));

//--Services Used
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<ITokenGenerator, TokenGenerator>();

//--Add CORS Policy
builder.Services.AddCors();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

//--Use CORS Policy
app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

app.UseAuthorization();

app.MapControllers();

app.Run();