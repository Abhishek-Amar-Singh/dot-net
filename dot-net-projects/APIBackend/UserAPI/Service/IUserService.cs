﻿using UserAPI.Models;

namespace UserAPI.Service
{
    public interface IUserService
    {
        List<User> GetAllUsers();

        bool RegisterUser(User user);

        User GetUserByEmailId(string emailId);

        bool DeleteUser(string emailId);

        bool EditUser(string emailId, User user);

        User LogIn(LoginInfo loginInfo);
    }
}
