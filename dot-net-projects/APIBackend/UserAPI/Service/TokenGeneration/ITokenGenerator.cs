﻿namespace UserAPI.Service.TokenGeneration
{
    public interface ITokenGenerator
    {
        abstract string GenerateToken(int id, string name, string emailId);
    }
}
