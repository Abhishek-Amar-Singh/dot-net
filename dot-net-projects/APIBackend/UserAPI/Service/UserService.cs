﻿using System.Globalization;
using UserAPI.Exceptions;
using UserAPI.Models;
using UserAPI.Repository;

namespace UserAPI.Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository _userRepository)
        {
            this._userRepository = _userRepository;
        }

        public List<User> GetAllUsers()
        {
            return _userRepository.GetAllUsers();
        }

        public bool RegisterUser(User user)
        {
            User userExists = _userRepository.GetUserByEmail(user.Email);
            if (userExists is null)
            {
                user.Email = user.Email.Replace(user.Email, user.Email.ToLower());
                user.Name = user.Name.Replace(user.Name, CultureInfo.CurrentCulture.TextInfo.ToTitleCase(user.Name));
                user.Address = user.Address.Replace(user.Address, CultureInfo.CurrentCulture.TextInfo.ToTitleCase(user.Address));
                int userRegisteredStatus = _userRepository.RegisterUser(user);
                return userRegisteredStatus == 1 ? true : false;
            }
            else
            {
                throw new UserAlreadyExistsException($"User having email address '{userExists.Email}' already exists. Use a different email id to register again.");
            }
        }

        public User GetUserByEmailId(string emailId)
        {
            User userExists = _userRepository.GetUserByEmail(emailId);
            return (userExists is not null) ? userExists : throw new UserNotFoundException($"User having email address '{emailId}' not found. Hence, result cannot be fetched. Please recheck the typed email id.");
        }

        public bool DeleteUser(string emailId)
        {
            User userExists = _userRepository.GetUserByEmail(emailId);
            if (userExists is not null)
            {
                int userDeletedStatus = _userRepository.DeleteUser(userExists);
                return userDeletedStatus == 1 ? true : false;
            }
            else
            {
                throw new UserNotFoundException($"User having email address '{emailId}' not found. Hence, deletion operation cannot be performed.");
            }
        }

        public bool EditUser(string emailId, User user)
        {
            User userExists = _userRepository.GetUserByEmail(emailId);
            if (userExists is not null)
            {
                userExists.Name = user.Name;
                userExists.Email = user.Email.ToLower();
                userExists.Password = user.Password;
                userExists.ConfirmPassword = user.ConfirmPassword;
                userExists.Address = user.Address;
                int editUserStatus = _userRepository.EditUser(userExists);
                return editUserStatus == 1 ? true : false;
            }
            else
            {
                throw new UserNotFoundException($"User having email address '{emailId}' not found. Hence, updation operation cannot be performed.");
            }
        }

        public User LogIn(LoginInfo loginInfo)
        {
            User userInfo = _userRepository.LogIn(loginInfo.Email, loginInfo.Password);
            if (userInfo is not null)
            {
                return userInfo;
            }
            else
            {
                throw new UserCredentialsInvalidException($"{loginInfo.Email} credentials are invalid. Email address and/or password are/is incorrect.");
            }
        }
    }
}
