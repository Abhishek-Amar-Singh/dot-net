﻿using ConsoleApp1GarbageCollectionDemo.Model;

namespace ConsoleApp1GarbageCollectionDemo.Respository
{
    internal interface IFile
    {
        void WriteContentsToFile(User user, string fileName);
        List<string> ReadContentsFromFile(string fileName);
    }
}
