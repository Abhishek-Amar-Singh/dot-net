﻿using ConsoleApp1GarbageCollectionDemo.Model;

namespace ConsoleApp1GarbageCollectionDemo.Respository
{
    internal class UserRepository : IFile
    {
        public List<string> ReadContentsFromFile(string fileName)
        {
            List<string> rowValues = new List<string>();
            using (StreamReader sr = new StreamReader(fileName))
            //we call garbage collection forcefully here(dispose--using keyword)
            //when we handle garbage collection the immidiate action happened..object gets destroyed at generation0
            {
                Console.WriteLine($"The Generation of Object-sr is::{GC.GetGeneration(sr)}");
                string rowLine;
                while ((rowLine = sr.ReadLine()) != null)
                {
                    rowValues.Add(rowLine);
                }
                return rowValues;
            }
        }

        public void WriteContentsToFile(User user, string fileName)
        {
            using (StreamWriter sw = new StreamWriter(fileName, true))//append bool which is true
            {
                sw.WriteLine($"{user}");
            }
        }
    }
}
