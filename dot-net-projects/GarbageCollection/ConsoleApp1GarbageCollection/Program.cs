﻿// See https://aka.ms/new-console-template for more information


using ConsoleApp1GarbageCollectionDemo.Model;
using ConsoleApp1GarbageCollectionDemo.Respository;

class Customer
{
    string name;
    public Customer(string customerName)
    {
        name = customerName;
    }

    ~Customer()
    {
        Console.WriteLine("Within Destructor");
    }

    public void Dispose()
    {
        Console.WriteLine("Within Dispose Method");
        GC.SuppressFinalize(this);
    }
}

class Program
{
    public static void Main()
    {
        #region Section1
        UserRepository userRepository = new UserRepository();
        User user = new User();
        Console.Write("Id::");
        user.Id = int.Parse(Console.ReadLine());
        Console.Write("Name::");
        user.Name = Console.ReadLine();
        Console.Write("Age::");
        user.Age = int.Parse(Console.ReadLine());
        Console.Write("City::");
        user.City = Console.ReadLine();

        string fileName = "userDatabase.txt";
        userRepository.WriteContentsToFile(user, fileName);

        Console.WriteLine("Reading contents from file::");
        List<string> rowValues = userRepository.ReadContentsFromFile(fileName);
        foreach (string rowValue in rowValues)
        {
            Console.WriteLine(rowValue);
        }

        Console.WriteLine($"The Total Memory Required::{GC.GetTotalMemory(true)}");// 517392 bytes
        Console.WriteLine($"The Generation of Object-userRepository is::{GC.GetGeneration(userRepository)}");
        Console.WriteLine($"The Generation of Object-user is::{GC.GetGeneration(user)}");
        #endregion

        Console.WriteLine("-------------------");

        #region Dispose
        Customer customer1 = new Customer("user1");
        Customer customer2 = new Customer("user2");
        Console.WriteLine($"Generation is {GC.GetGeneration(customer1)}");
        customer1.Dispose();
        GC.Collect(0);//0,1,2
        #endregion

        Console.WriteLine("-------------------");

        #region Current Directory
        string path = "";
        path = Environment.CurrentDirectory;
        Console.WriteLine($"Current Directory::{path}");
        #endregion
    }
}