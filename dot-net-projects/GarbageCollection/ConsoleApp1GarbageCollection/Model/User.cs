﻿
namespace ConsoleApp1GarbageCollectionDemo.Model
{
    internal class User
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public int Age { get; set; }
        public string? City { get; set; }

        public override string ToString()
        {
            return $"{Id},{Name},{Age},{City}";
        }
    }
}
