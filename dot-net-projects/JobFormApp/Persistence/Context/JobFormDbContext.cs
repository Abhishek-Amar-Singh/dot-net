﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Persistence.Models;

namespace Persistence.Context
{
    public partial class JobFormDbContext : DbContext
    {
        public JobFormDbContext()
        {
        }

        public JobFormDbContext(DbContextOptions<JobFormDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<City> Cities { get; set; } = null!;
        public virtual DbSet<Country> Countries { get; set; } = null!;
        public virtual DbSet<EducationalQualification> EducationalQualifications { get; set; } = null!;
        public virtual DbSet<Gender> Genders { get; set; } = null!;
        public virtual DbSet<HighestQualification> HighestQualifications { get; set; } = null!;
        public virtual DbSet<Industry> Industries { get; set; } = null!;
        public virtual DbSet<Institute> Institutes { get; set; } = null!;
        public virtual DbSet<JobCategory> JobCategories { get; set; } = null!;
        public virtual DbSet<Locality> Localities { get; set; } = null!;
        public virtual DbSet<LoginDetail> LoginDetails { get; set; } = null!;
        public virtual DbSet<LookingFor> LookingFors { get; set; } = null!;
        public virtual DbSet<MartialStatus> MartialStatuses { get; set; } = null!;
        public virtual DbSet<NoticePeriod> NoticePeriods { get; set; } = null!;
        public virtual DbSet<PersonalDetail> PersonalDetails { get; set; } = null!;
        public virtual DbSet<PreferredLocation> PreferredLocations { get; set; } = null!;
        public virtual DbSet<ProfessionalDetail> ProfessionalDetails { get; set; } = null!;
        public virtual DbSet<Specialization> Specializations { get; set; } = null!;
        public virtual DbSet<State> States { get; set; } = null!;
        public virtual DbSet<YourStatus> YourStatuses { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-28COP13;Database=JobFormDb;Trusted_Connection=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<City>(entity =>
            {
                entity.ToTable("City");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.City1)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("city");

                entity.Property(e => e.StateId).HasColumnName("stateId");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.Cities)
                    .HasForeignKey(d => d.StateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_City.stateId_State.id");
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.ToTable("Country");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Country1)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("country");
            });

            modelBuilder.Entity<EducationalQualification>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("EducationalQualification");

                entity.Property(e => e.HighestQualificationId).HasColumnName("highestQualificationId");

                entity.Property(e => e.Id).ValueGeneratedOnAdd().HasColumnName("id");

                entity.Property(e => e.InstituteId).HasColumnName("instituteId");

                entity.Property(e => e.LoginId).HasColumnName("loginId");

                entity.Property(e => e.OtherInstituteName)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("otherInstituteName");

                entity.Property(e => e.SpecializationId).HasColumnName("specializationId");

                entity.HasOne(d => d.HighestQualification)
                    .WithMany()
                    .HasForeignKey(d => d.HighestQualificationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EducationalQualification.highestQualificationId_HighestQualification.id");

                entity.HasOne(d => d.Institute)
                    .WithMany()
                    .HasForeignKey(d => d.InstituteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EducationalQualification.instituteId_Institute.id");

                entity.HasOne(d => d.Login)
                    .WithMany()
                    .HasForeignKey(d => d.LoginId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EducationalQualification.loginId_LoginDetails.id");

                entity.HasOne(d => d.Specialization)
                    .WithMany()
                    .HasForeignKey(d => d.SpecializationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EducationalQualification.specializationId_Specialization.id");
            });

            modelBuilder.Entity<Gender>(entity =>
            {
                entity.ToTable("Gender");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Gender1)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("gender");
            });

            modelBuilder.Entity<HighestQualification>(entity =>
            {
                entity.ToTable("HighestQualification");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.HighestQuaification)
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .HasColumnName("highestQuaification");
            });

            modelBuilder.Entity<Industry>(entity =>
            {
                entity.ToTable("Industry");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.CurrentIndustry)
                    .HasMaxLength(35)
                    .IsUnicode(false)
                    .HasColumnName("currentIndustry");
            });

            modelBuilder.Entity<Institute>(entity =>
            {
                entity.ToTable("Institute");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Institute1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("institute");
            });

            modelBuilder.Entity<JobCategory>(entity =>
            {
                entity.ToTable("JobCategory");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.JobCategory1)
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .HasColumnName("jobCategory");
            });

            modelBuilder.Entity<Locality>(entity =>
            {
                entity.ToTable("Locality");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.CityId).HasColumnName("cityId");

                entity.Property(e => e.Locality1)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("locality");

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Localities)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Locality.cityId_City.id");
            });

            modelBuilder.Entity<LoginDetail>(entity =>
            {
                entity.HasIndex(e => e.Id, "UK_LoginDetails.emailAddress")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.AlternateEmailId)
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .HasColumnName("alternateEmailId");

                entity.Property(e => e.EmailAddress)
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .HasColumnName("emailAddress");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(35)
                    .IsUnicode(false)
                    .HasColumnName("firstName");

                entity.Property(e => e.LastName)
                    .HasMaxLength(35)
                    .IsUnicode(false)
                    .HasColumnName("lastName");

                entity.Property(e => e.LookingForIds)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("lookingForIds");

                entity.Property(e => e.Password)
                    .HasMaxLength(35)
                    .IsUnicode(false)
                    .HasColumnName("password");

                entity.Property(e => e.YourStatusId).HasColumnName("yourStatusId");

                entity.HasOne(d => d.YourStatus)
                    .WithMany(p => p.LoginDetails)
                    .HasForeignKey(d => d.YourStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LoginDetails.yourStatusId_YourStatus.id");
            });

            modelBuilder.Entity<LookingFor>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("LookingFor");

                entity.Property(e => e.Id)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("id");

                entity.Property(e => e.JobType)
                    .HasMaxLength(75)
                    .IsUnicode(false)
                    .HasColumnName("jobType");
            });

            modelBuilder.Entity<MartialStatus>(entity =>
            {
                entity.ToTable("MartialStatus");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.MartialStatus1)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("martialStatus");
            });

            modelBuilder.Entity<NoticePeriod>(entity =>
            {
                entity.ToTable("NoticePeriod");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.NoticePeriod1)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("noticePeriod");
            });

            modelBuilder.Entity<PersonalDetail>(entity =>
            {
                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.CityId).HasColumnName("cityId");

                entity.Property(e => e.CountryId).HasColumnName("countryId");

                entity.Property(e => e.DateOfBirth)
                    .HasColumnType("date")
                    .HasColumnName("dateOfBirth");

                entity.Property(e => e.District)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("district");

                entity.Property(e => e.GenderId).HasColumnName("genderId");

                entity.Property(e => e.LocalityId).HasColumnName("localityId");

                entity.Property(e => e.LoginId).HasColumnName("loginId");

                entity.Property(e => e.MartialStatusId).HasColumnName("martialStatusId");

                entity.Property(e => e.MobileNumber)
                    .HasMaxLength(15)
                    .HasColumnName("mobileNumber");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(15)
                    .HasColumnName("phoneNumber");

                entity.Property(e => e.StateId).HasColumnName("stateId");

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(6)
                    .HasColumnName("zipCode");

                entity.HasOne(d => d.City)
                    .WithMany(p => p.PersonalDetails)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PersonalDetails.cityId_City.id");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.PersonalDetails)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PersonalDetails.countryId_Country.id");

                entity.HasOne(d => d.Gender)
                    .WithMany(p => p.PersonalDetails)
                    .HasForeignKey(d => d.GenderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PersonalDetails.genderId_Gender.id");

                entity.HasOne(d => d.Locality)
                    .WithMany(p => p.PersonalDetails)
                    .HasForeignKey(d => d.LocalityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PersonalDetails.localityId_Locality.id");

                entity.HasOne(d => d.Login)
                    .WithMany(p => p.PersonalDetails)
                    .HasForeignKey(d => d.LoginId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PersonalDetails.loginId_LoginDetails.id");

                entity.HasOne(d => d.MartialStatus)
                    .WithMany(p => p.PersonalDetails)
                    .HasForeignKey(d => d.MartialStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PersonalDetails.martialStatusId_MartialStatus.id");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.PersonalDetails)
                    .HasForeignKey(d => d.StateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PersonalDetails.stateId_State.id");
            });

            modelBuilder.Entity<PreferredLocation>(entity =>
            {
                entity.ToTable("PreferredLocation");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.PreferredLocation1)
                    .HasMaxLength(35)
                    .IsUnicode(false)
                    .HasColumnName("preferredLocation");
            });

            modelBuilder.Entity<ProfessionalDetail>(entity =>
            {
                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.AttachFileName)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("attachFileName");

                entity.Property(e => e.CtcLakhs).HasColumnName("ctc_lakhs");

                entity.Property(e => e.CtcThousands).HasColumnName("ctc_thousands");

                entity.Property(e => e.CurrentDesignation)
                    .HasMaxLength(35)
                    .IsUnicode(false)
                    .HasColumnName("currentDesignation");

                entity.Property(e => e.CurrentEmployer)
                    .HasMaxLength(35)
                    .IsUnicode(false)
                    .HasColumnName("currentEmployer");

                entity.Property(e => e.CurrentIndustryId).HasColumnName("currentIndustryId");

                entity.Property(e => e.JobCategoryId).HasColumnName("jobCategoryId");

                entity.Property(e => e.KeySkills)
                    .HasMaxLength(75)
                    .IsUnicode(false)
                    .HasColumnName("keySkills");

                entity.Property(e => e.LoginId).HasColumnName("loginId");

                entity.Property(e => e.NoticePeriodId).HasColumnName("noticePeriodId");

                entity.Property(e => e.PreferredLocationId).HasColumnName("preferredLocationId");

                entity.Property(e => e.PreviousDesignation)
                    .HasMaxLength(35)
                    .IsUnicode(false)
                    .HasColumnName("previousDesignation");

                entity.Property(e => e.PreviousEmployer)
                    .HasMaxLength(35)
                    .IsUnicode(false)
                    .HasColumnName("previousEmployer");

                entity.Property(e => e.ReadyToRelocate)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("readyToRelocate");

                entity.Property(e => e.ResumeTitle)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("resumeTitle");

                entity.Property(e => e.TotaExperienceMonths).HasColumnName("totaExperience_months");

                entity.Property(e => e.TotalExperienceYears).HasColumnName("totalExperience_years");

                entity.Property(e => e.TypeInResume).HasColumnName("typeInResume");

                entity.HasOne(d => d.CurrentIndustry)
                    .WithMany(p => p.ProfessionalDetails)
                    .HasForeignKey(d => d.CurrentIndustryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProfessionalDetails.currentIndustryId_Industry.id");

                entity.HasOne(d => d.JobCategory)
                    .WithMany(p => p.ProfessionalDetails)
                    .HasForeignKey(d => d.JobCategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProfessionalDetails.jobCategoryId_JobCategory.id");

                entity.HasOne(d => d.Login)
                    .WithMany(p => p.ProfessionalDetails)
                    .HasForeignKey(d => d.LoginId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProfessionalDetails.loginId_LoginDetails.id");

                entity.HasOne(d => d.NoticePeriod)
                    .WithMany(p => p.ProfessionalDetails)
                    .HasForeignKey(d => d.NoticePeriodId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProfessionalDetails.noticePeriodId_NoticePeriod.id");

                entity.HasOne(d => d.PreferredLocation)
                    .WithMany(p => p.ProfessionalDetails)
                    .HasForeignKey(d => d.PreferredLocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProfessionalDetails.preferredLocationId_PreferredLocation.id");
            });

            modelBuilder.Entity<Specialization>(entity =>
            {
                entity.ToTable("Specialization");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.HighestQualificationId).HasColumnName("highestQualificationId");

                entity.Property(e => e.Specialization1)
                    .HasMaxLength(60)
                    .IsUnicode(false)
                    .HasColumnName("specialization");

                entity.HasOne(d => d.HighestQualification)
                    .WithMany(p => p.Specializations)
                    .HasForeignKey(d => d.HighestQualificationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Specialization.highestQualificationId_HighestQualification.id");
            });

            modelBuilder.Entity<State>(entity =>
            {
                entity.ToTable("State");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.CountryId).HasColumnName("countryId");

                entity.Property(e => e.State1)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("state");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.States)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_State.countryId_Country.id");
            });

            modelBuilder.Entity<YourStatus>(entity =>
            {
                entity.ToTable("YourStatus");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Status)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("status");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
