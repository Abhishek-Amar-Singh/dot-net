﻿using System;
using System.Collections.Generic;

namespace Persistence.Models
{
    public partial class State
    {
        public State()
        {
            Cities = new HashSet<City>();
            PersonalDetails = new HashSet<PersonalDetail>();
        }

        public int Id { get; set; }
        public string State1 { get; set; } = null!;
        public int CountryId { get; set; }

        public virtual Country Country { get; set; } = null!;
        public virtual ICollection<City> Cities { get; set; }
        public virtual ICollection<PersonalDetail> PersonalDetails { get; set; }
    }
}
