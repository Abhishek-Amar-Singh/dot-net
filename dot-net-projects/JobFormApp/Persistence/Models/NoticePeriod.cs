﻿using System;
using System.Collections.Generic;

namespace Persistence.Models
{
    public partial class NoticePeriod
    {
        public NoticePeriod()
        {
            ProfessionalDetails = new HashSet<ProfessionalDetail>();
        }

        public int Id { get; set; }
        public string NoticePeriod1 { get; set; } = null!;

        public virtual ICollection<ProfessionalDetail> ProfessionalDetails { get; set; }
    }
}
