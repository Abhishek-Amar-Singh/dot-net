﻿using System;
using System.Collections.Generic;

namespace Persistence.Models
{
    public partial class HighestQualification
    {
        public HighestQualification()
        {
            Specializations = new HashSet<Specialization>();
        }

        public int Id { get; set; }
        public string HighestQuaification { get; set; } = null!;

        public virtual ICollection<Specialization> Specializations { get; set; }
    }
}
