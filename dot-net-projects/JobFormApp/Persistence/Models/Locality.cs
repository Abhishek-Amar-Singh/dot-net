﻿using System;
using System.Collections.Generic;

namespace Persistence.Models
{
    public partial class Locality
    {
        public Locality()
        {
            PersonalDetails = new HashSet<PersonalDetail>();
        }

        public int Id { get; set; }
        public string Locality1 { get; set; } = null!;
        public int CityId { get; set; }

        public virtual City City { get; set; } = null!;
        public virtual ICollection<PersonalDetail> PersonalDetails { get; set; }
    }
}
