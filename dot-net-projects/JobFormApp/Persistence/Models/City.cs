﻿using System;
using System.Collections.Generic;

namespace Persistence.Models
{
    public partial class City
    {
        public City()
        {
            Localities = new HashSet<Locality>();
            PersonalDetails = new HashSet<PersonalDetail>();
        }

        public int Id { get; set; }
        public string City1 { get; set; } = null!;
        public int StateId { get; set; }

        public virtual State State { get; set; } = null!;
        public virtual ICollection<Locality> Localities { get; set; }
        public virtual ICollection<PersonalDetail> PersonalDetails { get; set; }
    }
}
