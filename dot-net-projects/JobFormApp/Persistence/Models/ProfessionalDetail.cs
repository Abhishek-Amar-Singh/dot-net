﻿using System;
using System.Collections.Generic;

namespace Persistence.Models
{
    public partial class ProfessionalDetail
    {
        public int Id { get; set; }
        public int LoginId { get; set; }
        public int PreferredLocationId { get; set; }
        public string ReadyToRelocate { get; set; } = null!;
        public int TotalExperienceYears { get; set; }
        public int TotaExperienceMonths { get; set; }
        public int JobCategoryId { get; set; }
        public string KeySkills { get; set; } = null!;
        public int CurrentIndustryId { get; set; }
        public string? CurrentEmployer { get; set; }
        public string? CurrentDesignation { get; set; }
        public string? PreviousEmployer { get; set; }
        public string? PreviousDesignation { get; set; }
        public int NoticePeriodId { get; set; }
        public int CtcLakhs { get; set; }
        public int CtcThousands { get; set; }
        public string ResumeTitle { get; set; } = null!;
        public string? AttachFileName { get; set; }
        public string? TypeInResume { get; set; }

        public virtual Industry CurrentIndustry { get; set; } = null!;
        public virtual JobCategory JobCategory { get; set; } = null!;
        public virtual LoginDetail Login { get; set; } = null!;
        public virtual NoticePeriod NoticePeriod { get; set; } = null!;
        public virtual PreferredLocation PreferredLocation { get; set; } = null!;
    }
}
