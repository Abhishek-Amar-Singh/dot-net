﻿using System;
using System.Collections.Generic;

namespace Persistence.Models
{
    public partial class MartialStatus
    {
        public MartialStatus()
        {
            PersonalDetails = new HashSet<PersonalDetail>();
        }

        public int Id { get; set; }
        public string MartialStatus1 { get; set; } = null!;

        public virtual ICollection<PersonalDetail> PersonalDetails { get; set; }
    }
}
