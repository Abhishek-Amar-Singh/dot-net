﻿using System;
using System.Collections.Generic;

namespace Persistence.Models
{
    public partial class PreferredLocation
    {
        public PreferredLocation()
        {
            ProfessionalDetails = new HashSet<ProfessionalDetail>();
        }

        public int Id { get; set; }
        public string PreferredLocation1 { get; set; } = null!;

        public virtual ICollection<ProfessionalDetail> ProfessionalDetails { get; set; }
    }
}
