﻿using System;
using System.Collections.Generic;

namespace Persistence.Models
{
    public partial class LookingFor
    {
        public string Id { get; set; } = null!;
        public string JobType { get; set; } = null!;
    }
}
