﻿using System;
using System.Collections.Generic;

namespace Persistence.Models
{
    public partial class Country
    {
        public Country()
        {
            PersonalDetails = new HashSet<PersonalDetail>();
            States = new HashSet<State>();
        }

        public int Id { get; set; }
        public string Country1 { get; set; } = null!;

        public virtual ICollection<PersonalDetail> PersonalDetails { get; set; }
        public virtual ICollection<State> States { get; set; }
    }
}
