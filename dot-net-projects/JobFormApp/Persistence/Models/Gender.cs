﻿using System;
using System.Collections.Generic;

namespace Persistence.Models
{
    public partial class Gender
    {
        public Gender()
        {
            PersonalDetails = new HashSet<PersonalDetail>();
        }

        public int Id { get; set; }
        public string Gender1 { get; set; } = null!;

        public virtual ICollection<PersonalDetail> PersonalDetails { get; set; }
    }
}
