﻿using System;
using System.Collections.Generic;

namespace Persistence.Models
{
    public partial class Industry
    {
        public Industry()
        {
            ProfessionalDetails = new HashSet<ProfessionalDetail>();
        }

        public int Id { get; set; }
        public string CurrentIndustry { get; set; } = null!;

        public virtual ICollection<ProfessionalDetail> ProfessionalDetails { get; set; }
    }
}
