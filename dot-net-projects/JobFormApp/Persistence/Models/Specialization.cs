﻿using System;
using System.Collections.Generic;

namespace Persistence.Models
{
    public partial class Specialization
    {
        public int Id { get; set; }
        public string Specialization1 { get; set; } = null!;
        public int HighestQualificationId { get; set; }

        public virtual HighestQualification HighestQualification { get; set; } = null!;
    }
}
