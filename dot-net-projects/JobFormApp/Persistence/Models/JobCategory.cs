﻿using System;
using System.Collections.Generic;

namespace Persistence.Models
{
    public partial class JobCategory
    {
        public JobCategory()
        {
            ProfessionalDetails = new HashSet<ProfessionalDetail>();
        }

        public int Id { get; set; }
        public string JobCategory1 { get; set; } = null!;

        public virtual ICollection<ProfessionalDetail> ProfessionalDetails { get; set; }
    }
}
