﻿using System;
using System.Collections.Generic;

namespace Persistence.Models
{
    public partial class PersonalDetail
    {
        public int Id { get; set; }
        public int LoginId { get; set; }
        public string? PhoneNumber { get; set; }
        public string? MobileNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int GenderId { get; set; }
        public int MartialStatusId { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public string? District { get; set; }
        public int CityId { get; set; }
        public int LocalityId { get; set; }
        public string ZipCode { get; set; } = null!;

        public virtual City City { get; set; } = null!;
        public virtual Country Country { get; set; } = null!;
        public virtual Gender Gender { get; set; } = null!;
        public virtual Locality Locality { get; set; } = null!;
        public virtual LoginDetail Login { get; set; } = null!;
        public virtual MartialStatus MartialStatus { get; set; } = null!;
        public virtual State State { get; set; } = null!;
    }
}
