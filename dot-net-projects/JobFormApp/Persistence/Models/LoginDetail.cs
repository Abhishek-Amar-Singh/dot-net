﻿
using System.ComponentModel.DataAnnotations;

namespace Persistence.Models
{
    public partial class LoginDetail
    {
        public LoginDetail()
        {
            //EducationalQualifications = new HashSet<EducationalQualification>();
            PersonalDetails = new HashSet<PersonalDetail>();
            ProfessionalDetails = new HashSet<ProfessionalDetail>();
        }

        public int Id { get; set; }

        [Required]
        [RegularExpression(@"^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})*$", ErrorMessage = "Please enter a valid email address.")]
        public string EmailAddress { get; set; } = null!;
        public string? AlternateEmailId { get; set; }
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
        public string Password { get; set; } = null!;
        public string LookingForIds { get; set; } = null!;
        public int YourStatusId { get; set; }

        public virtual YourStatus YourStatus { get; set; } = null!;
        //public virtual ICollection<EducationalQualification> EducationalQualifications { get; set; }
        public virtual ICollection<PersonalDetail> PersonalDetails { get; set; }
        public virtual ICollection<ProfessionalDetail> ProfessionalDetails { get; set; }
    }
}
