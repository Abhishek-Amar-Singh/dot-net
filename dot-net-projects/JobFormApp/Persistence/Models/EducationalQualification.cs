﻿
namespace Persistence.Models
{
    public partial class EducationalQualification
    {
        public int Id { get; set; }
        public int LoginId { get; set; }
        public int HighestQualificationId { get; set; }
        public int SpecializationId { get; set; }
        public int InstituteId { get; set; }
        public string? OtherInstituteName { get; set; }

        public virtual HighestQualification HighestQualification { get; set; } = null!;
        public virtual Institute Institute { get; set; } = null!;
        public virtual LoginDetail Login { get; set; } = null!;
        public virtual Specialization Specialization { get; set; } = null!;
    }
}
