﻿using System;
using System.Collections.Generic;

namespace Persistence.Models
{
    public partial class YourStatus
    {
        public YourStatus()
        {
            LoginDetails = new HashSet<LoginDetail>();
        }

        public int Id { get; set; }
        public string Status { get; set; } = null!;

        public virtual ICollection<LoginDetail> LoginDetails { get; set; }
    }
}
