﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using Persistence.Mediater.Queries;
using Persistence.Models;

namespace WebAPP.Controllers
{
    public class GetYourStatusListQuery : IRequest<List<YourStatus>> { }

    public class GetYourStatusListQueryHandler : IRequestHandler<GetYourStatusListQuery, List<YourStatus>>
    {
        public async Task<List<YourStatus>> Handle(GetYourStatusListQuery request, CancellationToken cancellationToken)
        {
            using (JobFormDbContext context = new JobFormDbContext())
            {
                return await context.YourStatuses.ToListAsync();
            }

        }
    }
}