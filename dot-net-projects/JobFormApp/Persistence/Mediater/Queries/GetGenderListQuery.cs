﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using Persistence.Models;

namespace Persistence.Mediater.Queries
{
    public class GetGenderListQuery : IRequest<List<Gender>> { }

    public class GetGenderListQueryHandler : IRequestHandler<GetGenderListQuery, List<Gender>>
    {
        public async Task<List<Gender>> Handle(GetGenderListQuery request, CancellationToken cancellationToken)
        {
            using (JobFormDbContext context = new JobFormDbContext())
            {
                return await context.Genders.ToListAsync();
            }
        }
    }
}
