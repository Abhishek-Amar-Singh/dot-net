﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using Persistence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Mediater.Queries
{
    public record GetCityListQuery(int stateId) :IRequest<List<City>> { }

    public class GetCityListQueryHandler : IRequestHandler<GetCityListQuery, List<City>>
    {
        public async Task<List<City>> Handle(GetCityListQuery request, CancellationToken cancellationToken)
        {
            using(JobFormDbContext context = new JobFormDbContext())
            {
                return await context.Cities.Where(c => c.StateId == request.stateId).ToListAsync();
            }
        }
    }
}
