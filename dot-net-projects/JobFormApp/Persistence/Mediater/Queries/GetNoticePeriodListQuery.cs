﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using Persistence.Models;

namespace Persistence.Mediater.Queries
{
    public class GetNoticePeriodListQuery : IRequest<List<NoticePeriod>> { }

    public class GetNoticePeriodListQueryhandler : IRequestHandler<GetNoticePeriodListQuery, List<NoticePeriod>>
    {
        public async Task<List<NoticePeriod>> Handle(GetNoticePeriodListQuery request, CancellationToken cancellationToken)
        {
            using (JobFormDbContext context = new JobFormDbContext())
            {
                return await context.NoticePeriods.ToListAsync();
            }
        }
    }
}
