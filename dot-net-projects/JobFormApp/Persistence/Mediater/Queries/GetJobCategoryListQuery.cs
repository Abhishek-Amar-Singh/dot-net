﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using Persistence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Mediater.Queries
{
    public class GetJobCategoryListQuery : IRequest<List<JobCategory>> { }

    public class GetJobCategoryListQueryHandler : IRequestHandler<GetJobCategoryListQuery, List<JobCategory>>
    {
        public async Task<List<JobCategory>> Handle(GetJobCategoryListQuery request, CancellationToken cancellationToken)
        {
            using(JobFormDbContext context = new JobFormDbContext())
            {
                return await context.JobCategories.ToListAsync(); 
            }
        }
    }
}
