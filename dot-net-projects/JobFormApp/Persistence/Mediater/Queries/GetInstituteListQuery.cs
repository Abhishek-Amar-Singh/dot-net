﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using Persistence.Models;

namespace Persistence.Mediater.Queries
{
    public class GetInstituteListQuery : IRequest<List<Institute>> { }

    public class GetInstituteListQueryHandler : IRequestHandler<GetInstituteListQuery, List<Institute>>
    {
        public async Task<List<Institute>> Handle(GetInstituteListQuery request, CancellationToken cancellationToken)
        {
            using (JobFormDbContext context = new JobFormDbContext())
            {
                return await context.Institutes.ToListAsync();
            }
        }
    }
}
