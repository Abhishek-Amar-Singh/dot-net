﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using Persistence.Models;

namespace Persistence.Mediater.Queries
{
    public record GetStateListQuery(int countryId) : IRequest<List<State>> { }

    public class GetStateListQueryHandler : IRequestHandler<GetStateListQuery, List<State>>
    {
        public async Task<List<State>> Handle(GetStateListQuery request, CancellationToken cancellationToken)
        {
            using(JobFormDbContext context = new JobFormDbContext())
            {
                return await context.States.Where(s => s.CountryId == request.countryId).ToListAsync();
            }
        }
    }
}
