﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using Persistence.Models;

namespace Persistence.Mediater.Queries
{
    public class GetLookingForListQuery : IRequest<List<LookingFor>> { }

    public class GetLookingForListQueryHandler : IRequestHandler<GetLookingForListQuery, List<LookingFor>>
    {
        public async Task<List<LookingFor>> Handle(GetLookingForListQuery request, CancellationToken cancellationToken)
        {
            using (JobFormDbContext context = new JobFormDbContext())
            {
                return await context.LookingFors.ToListAsync();
            }
        }
    }
}
