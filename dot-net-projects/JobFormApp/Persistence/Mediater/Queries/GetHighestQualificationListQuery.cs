﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using Persistence.Models;

namespace Persistence.Mediater.Queries
{
    public class GetHighestQualificationListQuery : IRequest<List<HighestQualification>> { }

    public class GetHighestQualificationListQueryHandler : IRequestHandler<GetHighestQualificationListQuery, List<HighestQualification>>
    {
        public async Task<List<HighestQualification>> Handle(GetHighestQualificationListQuery request, CancellationToken cancellationToken)
        {
            using (JobFormDbContext context = new JobFormDbContext())
            {
                return await context.HighestQualifications.ToListAsync();
            }
        }
    }

}
