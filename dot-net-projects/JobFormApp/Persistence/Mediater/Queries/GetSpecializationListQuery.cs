﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using Persistence.Models;

namespace Persistence.Mediater.Queries
{
    public record GetSpecializationListQuery(int highestQulificationId) : IRequest<List<Specialization>> { }

    public class GetSpecializationListQueryHandler : IRequestHandler<GetSpecializationListQuery, List<Specialization>>
    {
        public async Task<List<Specialization>> Handle(GetSpecializationListQuery request, CancellationToken cancellationToken)
        {
            using (JobFormDbContext context = new JobFormDbContext())
            {
                return await context.Specializations.Where(c => c.HighestQualificationId == request.highestQulificationId).ToListAsync();
            }
        }
    }
}
