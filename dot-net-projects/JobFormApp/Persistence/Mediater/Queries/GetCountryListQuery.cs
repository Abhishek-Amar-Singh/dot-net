﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using Persistence.Models;

namespace Persistence.Mediater.Queries
{
    public class GetCountryListQuery : IRequest<List<Country>> { }

    public class GetCountryListQueryHandler : IRequestHandler<GetCountryListQuery, List<Country>>
    {
        public async Task<List<Country>> Handle(GetCountryListQuery request, CancellationToken cancellationToken)
        {
            using (JobFormDbContext context = new JobFormDbContext())
            {
                return await context.Countries.ToListAsync();
            }
        }
    }
}
