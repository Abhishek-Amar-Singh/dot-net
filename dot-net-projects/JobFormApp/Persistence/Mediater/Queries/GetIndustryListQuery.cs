﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using Persistence.Models;

namespace Persistence.Mediater.Queries
{
    public class GetIndustryListQuery : IRequest<List<Industry>> { }

    public class GetIndustryListQueryHandler : IRequestHandler<GetIndustryListQuery, List<Industry>>
    {
        public async Task<List<Industry>> Handle(GetIndustryListQuery request, CancellationToken cancellationToken)
        {
            using (JobFormDbContext context = new JobFormDbContext())
            {
                return await context.Industries.ToListAsync();
            }
        }
    }
}
