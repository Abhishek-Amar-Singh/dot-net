﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using Persistence.Models;

namespace Persistence.Mediater.Queries
{
    public class GetPrefferedLocationListQuery : IRequest<List<PreferredLocation>> { }

    public class GetPrefferedLocationListQueryHandler : IRequestHandler<GetPrefferedLocationListQuery, List<PreferredLocation>>
    {
        public async Task<List<PreferredLocation>> Handle(GetPrefferedLocationListQuery request, CancellationToken cancellationToken)
        {
            using(JobFormDbContext context = new JobFormDbContext())
            {
                return await context.PreferredLocations.ToListAsync();
            }
        }
    }
}
