﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using Persistence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Mediater.Queries
{
    public record GetLocalityListQuery(int cityId) : IRequest<List<Locality>> { }

    public class GetLocalityListQueryHandler : IRequestHandler<GetLocalityListQuery, List<Locality>>
    {
        public async Task<List<Locality>> Handle(GetLocalityListQuery request, CancellationToken cancellationToken)
        {
            using (JobFormDbContext context = new JobFormDbContext())
            {
                return await context.Localities.Where(c => c.CityId == request.cityId).ToListAsync();
            }
        }
    }
}
