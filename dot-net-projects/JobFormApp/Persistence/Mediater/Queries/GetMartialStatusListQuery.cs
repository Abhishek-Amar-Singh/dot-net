﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using Persistence.Models;

namespace Persistence.Mediater.Queries
{
    public class GetMartialStatusListQuery : IRequest<List<MartialStatus>> { }

    public class GetMartialStatusListQueryHandler : IRequestHandler<GetMartialStatusListQuery, List<MartialStatus>>
    {
        public async Task<List<MartialStatus>> Handle(GetMartialStatusListQuery request, CancellationToken cancellationToken)
        {
            using (JobFormDbContext context = new JobFormDbContext())
            {
                return await context.MartialStatuses.ToListAsync();
            }

        }
    }
}
