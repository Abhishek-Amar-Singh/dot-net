﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using WebAPP.Models;
using Persistence.Mediater.Queries;
using Persistence.Models;

namespace WebAPP.Controllers
{
    public class FormController : Controller
    {
        private readonly IMediator _mediator;
        private readonly RegisterModel registerModel;
        private static string lookingForIds = String.Empty;

        public FormController(IMediator _mediator)
        {
            this._mediator = _mediator;
            registerModel = new RegisterModel();
        }

        [HttpGet]
        public async Task<IActionResult> MainPage()
        {
            registerModel.lookinForList = await _mediator.Send(new GetLookingForListQuery());
            registerModel.yourStatusList = await _mediator.Send(new GetYourStatusListQuery());
            registerModel.genderList = await _mediator.Send(new GetGenderListQuery());
            registerModel.martialStatusList = await _mediator.Send(new GetMartialStatusListQuery());
            registerModel.countryList = await _mediator.Send(new GetCountryListQuery());
            registerModel.highestQualificationList = await _mediator.Send(new GetHighestQualificationListQuery());
            registerModel.instituteList = await _mediator.Send(new GetInstituteListQuery());
            registerModel.prefferedLocationList = await _mediator.Send(new GetPrefferedLocationListQuery());
            registerModel.jobCategoryList = await _mediator.Send(new GetJobCategoryListQuery());
            registerModel.industryList = await _mediator.Send(new GetIndustryListQuery());
            registerModel.noticePeriodList = await _mediator.Send(new GetNoticePeriodListQuery());
            return View(registerModel);
        }

        [HttpGet]
        public void GetterAndSetter(string lookingForValues)
        {
            lookingForIds = lookingForValues;
        }

        [HttpGet]
        public async Task<List<State>> GetStates(int id)
        {
            return await _mediator.Send(new GetStateListQuery(id));
        }

        [HttpGet]
        public async Task<List<City>> GetCities(int id)
        {
            return await _mediator.Send(new GetCityListQuery(id));
        }

        [HttpGet]
        public async Task<List<Locality>> GetLocalities(int id)
        {
            return await _mediator.Send(new GetLocalityListQuery(id));
        }

        [HttpGet]
        public async Task<List<Specialization>> GetSpecializations(int id)
        {
            return await _mediator.Send(new GetSpecializationListQuery(id));
        }

        [HttpPost]
        public IActionResult PostDetails(DataModel dataModel)
        {
            dataModel.loginDetails.LookingForIds = lookingForIds;
            return View(dataModel);
        }

        [HttpPost]
        public async void UploadFile(IFormFile theFile)
        {
            //var fileName = theFile.FileName;
            string path = "";
            try
            {
                if (theFile is not null)
                {
                    path = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory,"wwwroot", "UploadedFiles"));
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    using (var fileStream = new FileStream(Path.Combine(path, theFile.FileName), FileMode.Create))
                    {
                        await theFile.CopyToAsync(fileStream);
                    }
                }
                else
                {
                    //return "Failure";
                }
            }
            catch (Exception ex)
            {
                //throw new Exception("File Copy Failed", ex);
            }
        }
    }
}
