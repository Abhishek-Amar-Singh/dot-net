﻿using Persistence.Models;

namespace WebAPP.Models
{
    public class DataModel
    {
        public LoginDetail loginDetails { get; set; }
        public PersonalDetail personalDetails { get; set; }
        public EducationalQualification educationalQualification { get; set; }
        public ProfessionalDetail professionalDetails { get; set; }
    }
}
