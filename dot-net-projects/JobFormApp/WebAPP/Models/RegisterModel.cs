﻿using Persistence.Models;

namespace WebAPP.Models
{
    public class RegisterModel
    {
        public List<LookingFor> lookinForList { get; set; }
        public List<YourStatus> yourStatusList { get; set; }
        public LoginDetail loginDetails { get; set; }
        public List<Gender> genderList { get; set; }
        public List<MartialStatus> martialStatusList { get; set; }
        public List<Country> countryList { get; set; }
        public List<State> stateList { get; set; }
        public List<City> cityList { get; set; }
        public List<Locality> localityList { get; set; }
        public PersonalDetail personalDetails { get; set; }

        public List<HighestQualification> highestQualificationList { get; set; }
        public List<Specialization> specializationList { get; set; }
        public List<Institute> instituteList { get; set; }
        public List<PreferredLocation> prefferedLocationList { get; set; }
        public EducationalQualification educationalQualification { get; set; }
        public List<JobCategory> jobCategoryList { get; set; }
        public List<Industry> industryList { get; set; }
        public List<NoticePeriod> noticePeriodList { get; set; }
        public ProfessionalDetail professionalDetails { get; set; }
    }
}
