﻿//--Open for extension, closed for modification
namespace OpenCloseDemo1
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee emp1 = new PermanentEmployee(1, "Shruti");
            Employee emp2 = new TemperoryEmployee(2, "Ram");

            Console.WriteLine(string.Format("Employee {0}, getting bonus {1}", emp1.ToString(), emp1.CalculateBonus(13000)));
            Console.WriteLine(string.Format("Employee {0}, getting bonus {1}", emp2.ToString(), emp2.CalculateBonus(12000)));
        }
    }
}