﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenCloseDemo1
{
    public abstract class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;

        public Employee()
        {
        }
        public Employee(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }

        public abstract decimal CalculateBonus(decimal salary);

        public override string ToString()
        {
            return string.Format("[Id::{0}, Name::{1}]", this.Id, this.Name);
        }
    }

    public class PermanentEmployee : Employee
    {
        public PermanentEmployee()
        {
        }

        public PermanentEmployee(int id, string name) : base(id, name)
        {
        }

        public override decimal CalculateBonus(decimal salary)
        {
            return salary * .1M;
        }
    }

    public class TemperoryEmployee : Employee
    {
        public TemperoryEmployee()
        {
        }
        public TemperoryEmployee(int id, string name) : base(id, name)
        {
        }
        public override decimal CalculateBonus(decimal salary)
        {
            return salary * .05M;
        }
    }
}
