﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tynamix.ObjectFiller;
using WebApi.Brokers.DateTimes;
using WebApi.Brokers.Loggings;
using WebApi.Brokers.Storages;
using WebApi.Models.Employees;
using WebApi.Services.Foundations.Employees;

namespace WebApiUnitTests.Services.Foundations
{
    public partial class EmployeeServiceTests
    {
        private readonly Mock<IStorageBroker> storageBrokerMock;
        private readonly Mock<IDateTimeBroker> dateTimeBrokerMock;
        private readonly Mock<ILoggingBroker> loggingBrokerMock;
        private readonly IEmployeeService employeeService;

        public EmployeeServiceTests()
        {
            this.storageBrokerMock = new Mock<IStorageBroker>();
            this.dateTimeBrokerMock = new Mock<IDateTimeBroker>();
            this.loggingBrokerMock = new Mock<ILoggingBroker>();

            this.employeeService = new EmployeeService(
                storageBroker: this.storageBrokerMock.Object,
                dateTimeBroker: this.dateTimeBrokerMock.Object,
                loggingBroker: this.loggingBrokerMock.Object);
        }


        private static int GetRandomNumber() => new IntRange(min: 2, max: 10).GetValue();

        private static DateOnly GetRandomDate()
        {
            Random rnd = new Random();
            DateTime datetoday = DateTime.Now;

            int rndYear = rnd.Next(1970, datetoday.Year);
            int rndMonth = rnd.Next(1, 12);
            int rndDay = rnd.Next(1, 31);

            DateOnly generateDate = new DateOnly(rndYear, rndMonth, rndDay);
            return generateDate;
        }

        private static IQueryable<Employee> CreateRandomEmployees() =>
            CreateEmployeeFiller().Create(GetRandomNumber()).AsQueryable();

        private static Filler<Employee> CreateEmployeeFiller()
        {
            var filler = new Filler<Employee>();

            filler.Setup()
                .OnProperty(employee => employee.Id).Use(new IntRange(1, 99999))
                .OnProperty(employee => employee.FirstName).UseDefault()
                .OnProperty(employee => employee.LastName).UseDefault()
                .OnProperty(employee => employee.EmailAddress).UseDefault()
                .OnProperty(employee => employee.Salary).UseDefault()
                .OnProperty(employee => employee.JoiningDate).Use(GetRandomDate())
                .OnProperty(employee => employee.MgrId).IgnoreIt();

            return filler;
        }

    }
}
