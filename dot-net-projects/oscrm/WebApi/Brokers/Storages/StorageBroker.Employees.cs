﻿using Microsoft.EntityFrameworkCore;
using System.Collections;
using System.Text.Json.Nodes;
using WebApi.Models.Employees;

namespace WebApi.Brokers.Storages
{
    public partial class StorageBroker
    {
        public DbSet<Employee> Employees { get; set; }



        public IQueryable<Employee> SelectAllEmployees() => SelectAll<Employee>();
        public IQueryable<Employee> GetAllEmployees() => this.Set<Employee>().FromSqlRaw("SELECT * FROM fn_get_all_employees();");
        //public IQueryable GetAllEmployees2() => this.Database.SqlQueryRaw<string>("SELECT \"firstName\" AS fir FROM fn_get_all_employees();");

        public async ValueTask<Employee> InsertEmployeeAsync(Employee employee) =>
            await InsertAsync(employee);
    }
}
