﻿using WebApi.Models.Employees;

namespace WebApi.Brokers.Storages
{
    public partial interface IStorageBroker
    {
        IQueryable<Employee> SelectAllEmployees();
        IQueryable<Employee> GetAllEmployees();

        ValueTask<Employee> InsertEmployeeAsync(Employee employee);

    }
}
