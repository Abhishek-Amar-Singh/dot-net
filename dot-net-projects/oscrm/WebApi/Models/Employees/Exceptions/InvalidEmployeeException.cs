﻿using Xeptions;

namespace WebApi.Models.Employees.Exceptions
{
    public class InvalidEmployeeException : Xeption
    {
        public InvalidEmployeeException(string parameterName, object parameterValue)
            : base(message: $"Invalid employee, " +
                  $"parameter name: {parameterName}, " +
                  $"parameter value: {parameterValue}.")
        { }

        public InvalidEmployeeException()
            : base(message: "Invalid employee. Please fix the errors and try again.") { }
    }
}
