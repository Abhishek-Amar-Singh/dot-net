﻿using Xeptions;

namespace WebApi.Models.Employees.Exceptions
{
    public class EmployeeDependencyValidationException : Xeption
    {
        public EmployeeDependencyValidationException(Xeption innerException)
            : base(message: "Employee dependency validation error occurred, please try again.", innerException)
        { }
    }
}
