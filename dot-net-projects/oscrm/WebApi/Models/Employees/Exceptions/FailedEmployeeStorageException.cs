﻿using Xeptions;

namespace WebApi.Models.Employees.Exceptions
{
    public class FailedEmployeeStorageException : Xeption
    {
        public FailedEmployeeStorageException(Exception innerException)
            : base(message: "Failed post storage error occurred, contact support.", innerException)
        { }
    }
}
