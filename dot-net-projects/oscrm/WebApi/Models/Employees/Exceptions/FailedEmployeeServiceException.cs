﻿using Xeptions;

namespace WebApi.Models.Employees.Exceptions
{
    public class FailedEmployeeServiceException : Xeption
    {
        public FailedEmployeeServiceException(Exception innerException)
            : base(message: "Failed post storage error occurred, contact support.", innerException)
        { }
    }
}
