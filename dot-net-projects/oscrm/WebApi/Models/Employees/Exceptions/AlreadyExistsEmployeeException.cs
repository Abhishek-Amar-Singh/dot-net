﻿using Xeptions;

namespace WebApi.Models.Employees.Exceptions
{
    public class AlreadyExistsEmployeeException : Xeption
    {
        public AlreadyExistsEmployeeException(Exception innerException)
            : base(message: "Employee with the same id already exists.", innerException) { }
    }
}
