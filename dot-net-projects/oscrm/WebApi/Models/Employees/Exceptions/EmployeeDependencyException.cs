﻿namespace WebApi.Models.Employees.Exceptions
{
    public class EmployeeDependencyException : Exception
    {
        public EmployeeDependencyException(Exception innerException)
            : base(message: "Service dependency error occurred, contact support.", innerException) { }
    }
}
