﻿namespace WebApi.Models.Employees.Exceptions
{
    public class EmployeeServiceException : Exception
    {
        public EmployeeServiceException(Exception innerException)
            : base(message: "Service error occurred, contact support.", innerException) { }
    }
}
