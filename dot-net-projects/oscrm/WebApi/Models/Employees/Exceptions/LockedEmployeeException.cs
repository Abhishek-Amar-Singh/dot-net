﻿namespace WebApi.Models.Employees.Exceptions
{
    public class LockedEmployeeException : Exception
    {
        public LockedEmployeeException(Exception innerException)
            : base(message: "Locked employee record exception, please try again later.", innerException) { }
    }
}
