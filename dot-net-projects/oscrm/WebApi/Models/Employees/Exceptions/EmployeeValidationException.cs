﻿namespace WebApi.Models.Employees.Exceptions
{
    public class EmployeeValidationException : Exception
    {
        public EmployeeValidationException(Exception innerException)
            : base(message: "Invalid input, contact support.", innerException) { }
    }
}
