﻿namespace WebApi.Models.Employees.Exceptions
{
    public class NullEmployeeException : Exception
    {
        public NullEmployeeException() : base(message: "The employee is null.") { }
    }
}
