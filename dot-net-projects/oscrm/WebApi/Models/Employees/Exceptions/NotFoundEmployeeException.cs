﻿namespace WebApi.Models.Employees.Exceptions
{
    public class NotFoundEmployeeException : Exception
    {
        public NotFoundEmployeeException(int employeeId)
            : base(message: $"Couldn't find employee with id: {employeeId}.") { }
    }
}
