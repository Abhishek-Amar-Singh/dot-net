﻿using System;
using System.Collections.Generic;

namespace WebApi.Models.Employees;

public partial class Employee
{
    public int Id { get; set; }

    public string FirstName { get; set; } = null!;

    public string LastName { get; set; } = null!;

    public string EmailAddress { get; set; } = null!;

    public decimal Salary { get; set; }

    public DateOnly JoiningDate { get; set; }

    public int? MgrId { get; set; }
}
