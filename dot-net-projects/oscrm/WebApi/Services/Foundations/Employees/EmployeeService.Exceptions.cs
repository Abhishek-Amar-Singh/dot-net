﻿
using EFxceptions.Models.Exceptions;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using WebApi.Models.Employees;
using WebApi.Models.Employees.Exceptions;
using Xeptions;

namespace WebApi.Services.Foundations.Employees
{
    public partial class EmployeeService
    {
        private delegate IQueryable<Employee> ReturningEmployeesFunction();
        private delegate ValueTask<Employee> ReturningEmployeeFunction();

        private IQueryable<Employee> TryCatch(ReturningEmployeesFunction returningEmployeesFunction)
        {
            try
            {
                return returningEmployeesFunction();
            }
            catch (SqlException sqlException)
            {
                throw CreateAndLogCriticalDependencyException(sqlException);
            }
            catch (Exception exception)
            {
                var failedEmployeeServiceException =
                    new FailedEmployeeServiceException(exception);

                throw CreateAndLogServiceException(failedEmployeeServiceException);
            }
        }

        private async ValueTask<Employee> TryCatch(ReturningEmployeeFunction returningEmployeeFunction)
        {
            try
            {
                return await returningEmployeeFunction();
            }
            catch (NullEmployeeException nullEmployeeException)
            {
                throw CreateAndLogValidationException(nullEmployeeException);
            }
            catch (InvalidEmployeeException invalidEmployeeException)
            {
                throw CreateAndLogValidationException(invalidEmployeeException);
            }
            catch (NotFoundEmployeeException nullEmployeeException)
            {
                throw CreateAndLogValidationException(nullEmployeeException);
            }
            catch (SqlException sqlException)
            {
                var failedEmployeeStorageException =
                    new FailedEmployeeStorageException(sqlException);

                throw CreateAndLogCriticalDependencyException(failedEmployeeStorageException);
            }
            catch (DuplicateKeyException duplicateKeyException)
            {
                var alreadyExistsEmployeeException =
                    new AlreadyExistsEmployeeException(duplicateKeyException);

                throw CreateAndLogDependencyValidationException(alreadyExistsEmployeeException);
            }
            catch (DbUpdateConcurrencyException dbUpdateConcurrencyException)
            {
                var lockedEmployeeException = new LockedEmployeeException(dbUpdateConcurrencyException);

                throw CreateAndLogDependencyException(lockedEmployeeException);
            }
            catch (DbUpdateException dbUpdateException)
            {
                var failedEmployeeStorageException =
                    new FailedEmployeeStorageException(dbUpdateException);

                throw CreateAndLogDependencyException(failedEmployeeStorageException);
            }
            catch (Exception exception)
            {
                var failedEmployeeServiceException =
                    new FailedEmployeeServiceException(exception);

                throw CreateAndLogServiceException(failedEmployeeServiceException);
            }
        }

        private EmployeeDependencyException CreateAndLogDependencyException(Exception exception)
        {
            var employeeDependencyException = new EmployeeDependencyException(exception);
            this.loggingBroker.LogError(employeeDependencyException);

            return employeeDependencyException;
        }

        private EmployeeDependencyException CreateAndLogCriticalDependencyException(Exception exception)
        {
            var employeeDependencyException = new EmployeeDependencyException(exception);
            this.loggingBroker.LogCritical(employeeDependencyException);

            return employeeDependencyException;
        }

        private EmployeeServiceException CreateAndLogServiceException(Exception exception)
        {
            var employeeServiceException = new EmployeeServiceException(exception);
            this.loggingBroker.LogError(employeeServiceException);

            return employeeServiceException;
        }


        private EmployeeValidationException CreateAndLogValidationException(Exception exception)
        {
            var employeeValidationException = new EmployeeValidationException(exception);
            this.loggingBroker.LogError(employeeValidationException);

            return employeeValidationException;
        }

        private EmployeeDependencyValidationException CreateAndLogDependencyValidationException(Xeption exception)

        {
            var employeeDependencyValidationException =
                new EmployeeDependencyValidationException(exception);

            this.loggingBroker.LogError(employeeDependencyValidationException);

            return employeeDependencyValidationException;
        }

    }
}
