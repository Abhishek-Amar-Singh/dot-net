﻿using WebApi.Models.Employees;

namespace WebApi.Services.Foundations.Employees
{
    public interface IEmployeeService
    {
        IQueryable<Employee> RetrieveAllEmployees();
        IQueryable<Employee> GetAllEmployees();

        ValueTask<Employee> RegisterEmployeeAsync(Employee employee);
    }
}
