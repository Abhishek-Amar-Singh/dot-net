﻿using System.Text.RegularExpressions;
using WebApi.Models.Employees;
using WebApi.Models.Employees.Exceptions;

namespace WebApi.Services.Foundations.Employees
{
    public partial class EmployeeService
    {
        private void ValidateEmployeeOnRegister(Employee employee)
        {
            ValidateEmployee(employee);

            Validate(
                (Rule: IsInvalidX(employee.Id), Parameter: nameof(Employee.Id)),
                (Rule: IsInvalidX(employee.FirstName), Parameter: nameof(Employee.FirstName)),
                (Rule: IsInvalidX(employee.LastName), Parameter: nameof(Employee.LastName)),
                (Rule: IsInvalidEmail(employee.EmailAddress), Parameter: nameof(Employee.EmailAddress)),
                (Rule: IsInvalidX(employee.Salary), Parameter: nameof(Employee.Salary)),
                (Rule: IsInvalidX(employee.JoiningDate), Parameter: nameof(Employee.JoiningDate)),
                (Rule: IsInvalidX(employee.MgrId), Parameter: nameof(Employee.MgrId))
            );
        }

        private static dynamic IsInvalidX(int id) => new
        {
            Condition = id == default,
            Message = "Id is required"
        };

        private static dynamic IsInvalidX(int? mgrId) => new
        {
            Condition = mgrId == 0,
            Message = "mgrId is required"
        };

        private static dynamic IsInvalidX(decimal salary) => new
        {
            Condition = salary == default,
            Message = "Salary is required"
        };

        private static dynamic IsInvalidX(string text) => new
        {
            Condition = String.IsNullOrWhiteSpace(text),
            Message = "Text is required"
        };

        private static dynamic IsInvalidEmail(string email)
        {
            var regex = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
                RegexOptions.CultureInvariant | RegexOptions.Singleline);
            return new
            {
                Condition = regex.IsMatch(email) == false,
                Message = "Email is invalid"
            };
        }

        private static dynamic IsInvalidX(DateOnly date) => new
        {
            Condition = date == default,
            Message = "Date is required"
        };

        private static void ValidateEmployee(Employee employee)
        {
            if (employee is null)
            {
                throw new NullEmployeeException();
            }
        }

        private static bool IsInvalid(string input) => String.IsNullOrWhiteSpace(input);

        private static bool IsInvalid(int input) => input == default;

        private static void Validate(params (dynamic Rule, string Parameter)[] validations)
        {
            var invalidEmployeeException = new InvalidEmployeeException();

            foreach ((dynamic rule, string parameter) in validations)
            {
                if (rule.Condition)
                {
                    invalidEmployeeException.UpsertDataList(
                        key: parameter,
                        value: rule.Message);
                }
            }

            invalidEmployeeException.ThrowIfContainsErrors();
        }
    }
}
