﻿using WebApi.Brokers.DateTimes;
using WebApi.Brokers.Loggings;
using WebApi.Brokers.Storages;
using WebApi.Models.Employees;

namespace WebApi.Services.Foundations.Employees
{
    public partial class EmployeeService : IEmployeeService
    {
        private readonly IStorageBroker storageBroker;
        private readonly IDateTimeBroker dateTimeBroker;
        private readonly ILoggingBroker loggingBroker;

        public EmployeeService(IStorageBroker storageBroker, IDateTimeBroker dateTimeBroker, ILoggingBroker loggingBroker)
        {
            this.storageBroker = storageBroker;
            this.dateTimeBroker = dateTimeBroker;
            this.loggingBroker = loggingBroker;
        }

        public IQueryable<Employee> RetrieveAllEmployees() => 
            TryCatch(() => this.storageBroker.SelectAllEmployees());

        public IQueryable<Employee> GetAllEmployees() => 
            TryCatch(() => this.storageBroker.GetAllEmployees());

        public ValueTask<Employee> RegisterEmployeeAsync(Employee employee) =>
        TryCatch(async () =>
        {
            ValidateEmployeeOnRegister(employee);

            return await this.storageBroker.InsertEmployeeAsync(employee);
        });
    }
}
