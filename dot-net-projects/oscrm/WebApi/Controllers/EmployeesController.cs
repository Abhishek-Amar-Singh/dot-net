﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models.Employees;
using WebApi.Models.Employees.Exceptions;
using WebApi.Services.Foundations.Employees;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeService employeeService;

        public EmployeesController(IEmployeeService employeeService) =>
            this.employeeService = employeeService;


        [Route("RetrieveAllEmployees")]
        [HttpGet]
        public ActionResult<IQueryable<Employee>> RetrieveAllEmployees()
        {
            try
            {
                IQueryable<Employee> storageEmployees =
                    this.employeeService.RetrieveAllEmployees();

                return Ok(storageEmployees);
            }
            catch (EmployeeDependencyException employeeDependencyException)
            {
                return Problem(employeeDependencyException.Message);
            }
            catch (EmployeeServiceException employeeServiceException)
            {
                return Problem(employeeServiceException.Message);
            }
        }

        [Route("GetAllEmployees")]
        [HttpGet]
        public ActionResult<IQueryable<Employee>> GetAllEmployees()
        {
            try
            {
                IQueryable<Employee> storageEmployees =
                    this.employeeService.GetAllEmployees();

                return Ok(storageEmployees);
            }
            catch(EmployeeDependencyException employeeDependencyException)
            {
                return Problem(employeeDependencyException.Message);
            }
            catch (EmployeeServiceException employeeServiceException)
            {
                return Problem(employeeServiceException.Message);
            }
        }


        [Route("PostEmployeeAsync")]
        [HttpPost]
        public async ValueTask<ActionResult<Employee>> PostEmployeeAsync(Employee employee)
        {
            try
            {
                Employee registeredEmployee =
                       await this.employeeService.RegisterEmployeeAsync(employee);

                return Created("", registeredEmployee);//Created(registeredEmployee)
            }
            catch (EmployeeValidationException employeeValidationException)
                when (employeeValidationException.InnerException is AlreadyExistsEmployeeException)
            {
                return Conflict(employeeValidationException.InnerException);
            }
            catch (EmployeeValidationException employeeValidationException)
            {
                return BadRequest(employeeValidationException.InnerException);
            }
            catch (EmployeeDependencyException employeeDependencyException)
            {
                return BadRequest(employeeDependencyException);//InternalServerError(employeeDependencyException);
            }
            catch (EmployeeServiceException employeeServiceException)
            {
                return BadRequest(employeeServiceException);//InternalServerError(employeeServiceException);
            }
        }
    }
}
