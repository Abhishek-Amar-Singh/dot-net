﻿using FORM.Web.Api.Models.SystemModules;
using FORM.Web.Api.Models.SystemModules.Exceptions;

namespace FORM.Web.Api.Services.Foundations.SystemModules
{
    public partial class SystemModuleService
    {
        private void ValidateSystemModuleOnCreate(SystemModule systemModule)
        {
            ValidateSystemModule(systemModule);

            Validate(
                (Rule: IsInvalidX(systemModule.ModuleName), Parameter: nameof(SystemModule.ModuleName))
            );
        }

        private static dynamic IsInvalidX(int id) => new
        {
            Condition = id == default,
            Message = "Id is required"
        };

        private static dynamic IsInvalidX(int? id) => new
        {
            Condition = id == default,
            Message = "Id is required"
        };

        private static dynamic IsInvalidX(string text) => new
        {
            Condition = String.IsNullOrWhiteSpace(text),
            Message = "Text is required"
        };

        private static dynamic IsInvalidX(bool? status) => new
        {
            Condition = status is null,
            Message = "status is required"
        };

        private static dynamic IsInvalidX(DateTime? dateTime) => new
        {
            Condition = dateTime == default,
            Message = "DateTime is required"
        };

        private static dynamic IsSame(
            DateTime? firstDate,
            DateTime? secondDate,
            string secondDateName) => new
            {
                Condition = firstDate == secondDate,
                Message = $"Date is the same as {secondDateName}"
            };

        private static void ValidateSystemModule(SystemModule systemModule)
        {
            if (systemModule is null)
            {
                throw new NullSystemModuleException();
            }
        }

        private void ValidateSystemModuleAlreadyExists(SystemModule systemModule)
        {
            if (systemModule is not null)
            {
                throw new AlreadyExistsSystemModuleException();
            }
        }

        private void ValidateSystemModuleAgainstModify(
            SystemModule systemModule, SystemModule maybeSystemModule)
        {
            ValidateStorageSystemModule(maybeSystemModule, systemModule.SystemModuleId);

            if (systemModule.ModuleName == maybeSystemModule.ModuleName &&
                systemModule.IsActive == maybeSystemModule.IsActive)
            {
                throw new UnchangedSystemModuleException(systemModule.SystemModuleId);
            }
        }

        private void ValidateSystemModuleAgainstModifyOp(
            SystemModule systemModule, SystemModule maybeSystemModule)
        {
            if (maybeSystemModule is not null &&
                maybeSystemModule.SystemModuleId != systemModule.SystemModuleId &&
                maybeSystemModule.ModuleName == systemModule.ModuleName)
            {
                throw new CannotChangeSystemModuleException(
                    systemModule.ModuleName, maybeSystemModule.SystemModuleId);
            }
        }

        private static void ValidateSystemModuleId(int systemModuleId)
        {
            if (systemModuleId == default)
            {
                throw new InvalidSystemModuleException(
                    parameterName: nameof(SystemModule.SystemModuleId),
                    parameterValue: systemModuleId);
            }
        }

        private static void ValidateSystemModuleName(string systemModuleName)
        {
            if (systemModuleName == default)
            {
                throw new InvalidSystemModuleException(
                    parameterName: nameof(SystemModule.ModuleName),
                    parameterValue: systemModuleName);
            }
        }

        private static void ValidateStorageSystemModule(SystemModule storageSystemModule, int systemModuleId)
        {
            if (storageSystemModule is null)
            {
                throw new NotFoundSystemModuleException(systemModuleId);
            }
        }

        private static void ValidateStorageSystemModule(
            SystemModule storageSystemModule, string systemModuleName)
        {
            if (storageSystemModule is null)
            {
                throw new NotFoundSystemModuleException();
            }
        }

        private void ValidateSystemModuleOnModify(SystemModule systemModule)
        {
            ValidateSystemModule(systemModule);

            Validate(
                (Rule: IsInvalidX(systemModule.SystemModuleId), Parameter: nameof(SystemModule.SystemModuleId)),
                (Rule: IsInvalidX(systemModule.ModuleName), Parameter: nameof(SystemModule.ModuleName)),
                (Rule: IsInvalidX(systemModule.IsActive), Parameter: nameof(SystemModule.IsActive))
            );
        }

        private static void Validate(params (dynamic Rule, string Parameter)[] validations)
        {
            var invalidSystemModuleException = new InvalidSystemModuleException();

            foreach ((dynamic rule, string parameter) in validations)
            {
                if (rule.Condition)
                {
                    invalidSystemModuleException.UpsertDataList(
                        key: parameter,
                        value: rule.Message);
                }
            }

            invalidSystemModuleException.ThrowIfContainsErrors();
        }
    }
}
