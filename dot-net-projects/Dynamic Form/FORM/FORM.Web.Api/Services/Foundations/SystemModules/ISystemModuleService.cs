﻿using FORM.Web.Api.Models.SystemModules;

namespace FORM.Web.Api.Services.Foundations.SystemModules
{
    public interface ISystemModuleService
    {
        //IQueryable<SystemModuleVm> RetrieveSystemModuleDetailsById(int systemModuleId);

        ValueTask<SystemModule> CreateSystemModuleAsync(SystemModule systemModule, int userId);

        ValueTask<SystemModule> RetrieveSystemModuleByIdAsync(int systemModuleId);

        ValueTask<SystemModule> RetrieveSystemModuleByNameAsync(string systemModuleName);

        ValueTask<SystemModule> ModifySystemModuleAsync(SystemModule systemModule, int userId);

        ValueTask<SystemModule> RemoveSystemModuleByIdAsync(int systemModuleId, int userId);

        ValueTask<SystemModule> PutBackSystemModuleByIdAsync(int systemModuleId, int userId);

        IQueryable<SystemModule> RetrieveAllSystemModules();
    }
}
