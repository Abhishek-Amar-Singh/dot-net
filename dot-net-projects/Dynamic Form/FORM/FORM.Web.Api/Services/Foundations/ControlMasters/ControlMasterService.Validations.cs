﻿
using FORM.Web.Api.Models.ControlMasters;
using FORM.Web.Api.Models.ControlMasters.Exceptions;
using FORM.Web.Api.Models.SystemModules;
using FORM.Web.Api.Models.SystemModules.Exceptions;
using FORM.Web.Api.Models.SystemModules.Vms;

namespace FORM.Web.Api.Services.Foundations.ControlMasters
{
    public partial class ControlMasterService
    {
        private void ValidateControlMasterOnCreate(ControlMaster controlMaster)
        {
            ValidateControlMaster(controlMaster);

            Validate(
                (Rule: IsInvalidX(controlMaster.ControlName), Parameter: nameof(ControlMaster.ControlName))
            );
        }

        private static dynamic IsInvalidX(int id) => new
        {
            Condition = id == default,
            Message = "Id is required"
        };

        private static dynamic IsInvalidX(int? id) => new
        {
            Condition = id == default,
            Message = "Id is required"
        };

        private static dynamic IsInvalidX(string text) => new
        {
            Condition = String.IsNullOrWhiteSpace(text),
            Message = "Text is required"
        };

        private static dynamic IsInvalidX(bool? status) => new
        {
            Condition = status is null,
            Message = "status is required"
        };

        private static dynamic IsInvalidX(DateTime? dateTime) => new
        {
            Condition = dateTime == default,
            Message = "DateTime is required"
        };

        private static dynamic IsSame(
            DateTime? firstDate,
            DateTime? secondDate,
            string secondDateName) => new
            {
                Condition = firstDate == secondDate,
                Message = $"Date is the same as {secondDateName}"
            };

        private static void ValidateControlMaster(ControlMaster controlMaster)
        {
            if (controlMaster is null)
            {
                throw new NullControlMasterException();
            }
        }

        private void ValidateControlMasterAlreadyExists(ControlMaster controlMaster)
        {
            if (controlMaster is not null)
            {
                throw new AlreadyExistsControlMasterException();
            }
        }

        private void ValidateControlMasterAgainstModify(
            ControlMaster controlMaster, ControlMaster maybeControlMaster)
        {
            ValidateStorageControlMaster(maybeControlMaster, controlMaster.ControlMasterId);

            if (controlMaster.ControlName == maybeControlMaster.ControlName &&
                controlMaster.BasicSyntax == maybeControlMaster.BasicSyntax &&
                controlMaster.IsActive == maybeControlMaster.IsActive)
            {
                throw new UnchangedControlMasterException(controlMaster.ControlMasterId);
            }
        }

        private void ValidateControlMasterAgainstModifyOp(
            ControlMaster controlMaster, ControlMaster maybeControlMaster)
        {
            if (maybeControlMaster is not null &&
                maybeControlMaster.ControlMasterId != controlMaster.ControlMasterId &&
                maybeControlMaster.ControlName == controlMaster.ControlName)
            {
                throw new CannotChangeControlMasterException(
                    controlMaster.ControlName, maybeControlMaster.ControlMasterId);
            }
        }

        private static void ValidateControlMasterId(int controlMasterId)
        {
            if (controlMasterId == default)
            {
                throw new InvalidControlMasterException(
                    parameterName: nameof(ControlMaster.ControlMasterId),
                    parameterValue: controlMasterId);
            }
        }

        private static void ValidateControlMasterName(string controlName)
        {
            if (controlName == default)
            {
                throw new InvalidControlMasterException(
                    parameterName: nameof(ControlMaster.ControlName),
                    parameterValue: controlName);
            }
        }

        private static void ValidateStorageControlMaster(ControlMaster storageControlMaster, int controlMasterId)
        {
            if (storageControlMaster is null)
            {
                throw new NotFoundControlMasterException(controlMasterId);
            }
        }


        private static void ValidateStorageControlMaster(ControlMaster storageControlMaster)
        {
            if (storageControlMaster is null)
            {
                throw new NotFoundControlMasterException();
            }
        }

        private void ValidateControlMasterOnModify(ControlMaster controlMaster)
        {
            ValidateControlMaster(controlMaster);

            Validate(
                (Rule: IsInvalidX(controlMaster.ControlMasterId), Parameter: nameof(ControlMaster.ControlMasterId)),
                (Rule: IsInvalidX(controlMaster.ControlName), Parameter: nameof(ControlMaster.ControlName)),
                (Rule: IsInvalidX(controlMaster.IsActive), Parameter: nameof(ControlMaster.IsActive))
            );
        }

        private static void Validate(params (dynamic Rule, string Parameter)[] validations)
        {
            var invalidControlMasterException = new InvalidControlMasterException();

            foreach ((dynamic rule, string parameter) in validations)
            {
                if (rule.Condition)
                {
                    invalidControlMasterException.UpsertDataList(
                        key: parameter,
                        value: rule.Message);
                }
            }

            invalidControlMasterException.ThrowIfContainsErrors();
        }
    }
}
