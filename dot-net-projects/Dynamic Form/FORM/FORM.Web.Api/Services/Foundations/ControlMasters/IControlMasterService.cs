﻿using FORM.Web.Api.Models.ControlMasters;
using FORM.Web.Api.Models.SystemModules;

namespace FORM.Web.Api.Services.Foundations.ControlMasters
{
    public interface IControlMasterService
    {
        ValueTask<ControlMaster> CreateControlMasterAsync(ControlMaster controlMaster, int userId);

        ValueTask<ControlMaster> RetrieveControlMasterByIdAsync(int controlMasterId);

        ValueTask<ControlMaster> RetrieveControlMasterByNameAsync(string controlName);

        ValueTask<ControlMaster> ModifyControlMasterAsync(ControlMaster controlMaster, int userId);

        ValueTask<ControlMaster> RemoveControlMasterByIdAsync(int controlMasterId, int userId);

        ValueTask<ControlMaster> PutBackControlMasterByIdAsync(int controlMasterId, int userId);

        IQueryable<ControlMaster> RetrieveAllControlMasters();
    }
}
