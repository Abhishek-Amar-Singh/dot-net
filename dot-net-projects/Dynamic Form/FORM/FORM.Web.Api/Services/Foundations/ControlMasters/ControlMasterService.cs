﻿using FORM.Web.Api.Brokers.DateTimes;
using FORM.Web.Api.Brokers.Loggings;
using FORM.Web.Api.Brokers.Storages;
using FORM.Web.Api.Models.ControlMasters;
using FORM.Web.Api.Models.SystemModules;

namespace FORM.Web.Api.Services.Foundations.ControlMasters
{
    public partial class ControlMasterService : IControlMasterService
    {
        private readonly IStorageBroker storageBroker;
        private readonly ILoggingBroker loggingBroker;
        private readonly IDateTimeBroker dateTimeBroker;

        public ControlMasterService(
            IStorageBroker storageBroker,
            IDateTimeBroker dateTimeBroker,
            ILoggingBroker loggingBroker)
        {
            this.storageBroker = storageBroker;
            this.dateTimeBroker = dateTimeBroker;
            this.loggingBroker = loggingBroker;
        }

        public ValueTask<ControlMaster> CreateControlMasterAsync(
            ControlMaster controlMaster, int userId) =>
        TryCatch(async () =>
        {
            ValidateControlMasterOnCreate(controlMaster);

            var controlMasterData = await this.storageBroker
                .SelectControlMasterByNameAsync(controlMaster.ControlName);

            ValidateControlMasterAlreadyExists(controlMasterData);

            return await this.storageBroker.InsertControlMasterAsync(controlMaster, userId);
        });

        public ValueTask<ControlMaster> RetrieveControlMasterByIdAsync(int controlMasterId) =>
        TryCatch(async () =>
        {
            ValidateControlMasterId(controlMasterId);

            ControlMaster maybeControlMaster = await this.storageBroker
                .SelectControlMasterByIdAsync(controlMasterId);

            ValidateStorageControlMaster(maybeControlMaster, controlMasterId);

            return maybeControlMaster;
        });

        public ValueTask<ControlMaster> RetrieveControlMasterByNameAsync(string controlName) =>
        TryCatch(async () =>
        {
            ValidateControlMasterName(controlName);
            ControlMaster maybeControlMaster = await this.storageBroker.SelectControlMasterByNameAsync(controlName);
            ValidateStorageControlMaster(maybeControlMaster);

            return maybeControlMaster;
        });

        public ValueTask<ControlMaster> ModifyControlMasterAsync(ControlMaster controlMaster, int userId) =>
        TryCatch(async () =>
        {
            ValidateControlMasterOnModify(controlMaster);

            var maybeControlMaster =
                await this.storageBroker.SelectControlMasterByIdAsync(controlMaster.ControlMasterId);
            ValidateControlMasterAgainstModify(controlMaster, maybeControlMaster);

            maybeControlMaster =
                await this.storageBroker.SelectControlMasterByNameAsync(controlMaster.ControlName);

            ValidateControlMasterAgainstModifyOp(controlMaster, maybeControlMaster);

            return await this.storageBroker.UpdateControlMasterAsync(controlMaster, userId);
        });

        public ValueTask<ControlMaster> RemoveControlMasterByIdAsync(
            int controlMasterId, int userId) =>
        TryCatch(async () =>
        {
            ValidateControlMasterId(controlMasterId);

            var maybeControlMaster =
                await this.storageBroker.SelectControlMasterByIdAsync(controlMasterId);

            ValidateStorageControlMaster(maybeControlMaster, controlMasterId);

            return await this.storageBroker.DeleteControlMasterAsync(controlMasterId, userId);
        });

        public ValueTask<ControlMaster> PutBackControlMasterByIdAsync(
            int controlMasterId, int userId) =>
        TryCatch(async () =>
        {
            ValidateControlMasterId(controlMasterId);

            var maybeControlMaster =
                await this.storageBroker.SelectControlMasterByIdAsync(controlMasterId);

            ValidateStorageControlMaster(maybeControlMaster, controlMasterId);

            return await this.storageBroker.UndoDeleteControlMasterAsync(controlMasterId, userId);
        });

        public IQueryable<ControlMaster> RetrieveAllControlMasters() =>
        TryCatch(() => this.storageBroker.SelectAllControlMasters());
    }
}
