﻿using EFxceptions.Models.Exceptions;
using FORM.Web.Api.Models.ControlMasters;
using FORM.Web.Api.Models.ControlMasters.Exceptions;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using Xeptions;

namespace FORM.Web.Api.Services.Foundations.ControlMasters
{
    public partial class ControlMasterService
    {
        private delegate IQueryable<ControlMaster> ReturningControlMastersFunction();
        private delegate ValueTask<ControlMaster> ReturningControlMasterFunction();

        private IQueryable<ControlMaster> TryCatch(
            ReturningControlMastersFunction returningControlMastersFunction)
        {
            try
            {
                return returningControlMastersFunction();
            }
            catch (NpgsqlException npgsqlException)
            {
                throw CreateAndLogCriticalDependencyException(npgsqlException);
            }
            catch (Exception exception)
            {
                var failedControlMasterServiceException =
                    new FailedControlMasterServiceException(exception);

                throw CreateAndLogServiceException(failedControlMasterServiceException);
            }
        }

        private async ValueTask<ControlMaster> TryCatch(
            ReturningControlMasterFunction returningControlMasterFunction)
        {
            try
            {
                return await returningControlMasterFunction();
            }
            catch (NullControlMasterException nullControlMasterException)
            {
                throw CreateAndLogValidationException(nullControlMasterException);
            }
            catch (InvalidControlMasterException invalidControlMasterException)
            {
                throw CreateAndLogValidationException(invalidControlMasterException);
            }
            catch (NotFoundControlMasterException nullControlMasterException)
            {
                throw CreateAndLogValidationException(nullControlMasterException);
            }
            catch (PostgresException postgresException)
            {
                var failedControlMasterStorageException =
                    new FailedControlMasterStorageException(postgresException);

                throw CreateAndLogCriticalDependencyException(failedControlMasterStorageException);
            }
            catch (AlreadyExistsControlMasterException alreadyExistsControlMasterException)
            {
                throw CreateAndLogDependencyValidationException(alreadyExistsControlMasterException);
            }
            catch (DuplicateKeyException duplicateKeyException)
            {
                var alreadyExistsControlMasterException =
                    new AlreadyExistsControlMasterException(duplicateKeyException);

                throw CreateAndLogDependencyValidationException(alreadyExistsControlMasterException);
            }
            catch (DbUpdateConcurrencyException dbUpdateConcurrencyException)
            {
                var lockedControlMasterException = new LockedControlMasterException(dbUpdateConcurrencyException);

                throw CreateAndLogDependencyException(lockedControlMasterException);
            }
            catch (DbUpdateException dbUpdateException)
            {
                var failedControlMasterStorageException =
                    new FailedControlMasterStorageException(dbUpdateException);

                throw CreateAndLogDependencyException(failedControlMasterStorageException);
            }
            catch (UnchangedControlMasterException unchangedControlMasterException)
            {
                throw CreateAndLogValidationException(unchangedControlMasterException);
            }
            catch (CannotChangeControlMasterException unchangedControlMasterException)
            {
                throw CreateAndLogValidationException(unchangedControlMasterException);
            }
            catch (Exception exception)
            {
                var failedControlMasterServiceException =
                    new FailedControlMasterServiceException(exception);

                throw CreateAndLogServiceException(failedControlMasterServiceException);
            }
        }

        private ControlMasterDependencyException CreateAndLogDependencyException(Exception exception)
        {
            var controlMasterDependencyException = new ControlMasterDependencyException(exception);
            this.loggingBroker.LogError(controlMasterDependencyException);

            return controlMasterDependencyException;
        }

        private ControlMasterDependencyException CreateAndLogCriticalDependencyException(Exception exception)
        {
            var controlMasterDependencyException = new ControlMasterDependencyException(exception);
            this.loggingBroker.LogCritical(controlMasterDependencyException);

            return controlMasterDependencyException;
        }

        private ControlMasterServiceException CreateAndLogServiceException(Exception exception)
        {
            var controlMasterServiceException = new ControlMasterServiceException(exception);
            this.loggingBroker.LogError(controlMasterServiceException);

            return controlMasterServiceException;
        }

        private ControlMasterValidationException CreateAndLogValidationException(Exception exception)
        {
            var controlMasterValidationException = new ControlMasterValidationException(exception);
            this.loggingBroker.LogError(controlMasterValidationException);

            return controlMasterValidationException;
        }

        private ControlMasterDependencyValidationException CreateAndLogDependencyValidationException(Xeption exception)
        {
            var controlMasterDependencyValidationException =
                new ControlMasterDependencyValidationException(exception);

            this.loggingBroker.LogError(controlMasterDependencyValidationException);

            return controlMasterDependencyValidationException;
        }
    }
}
