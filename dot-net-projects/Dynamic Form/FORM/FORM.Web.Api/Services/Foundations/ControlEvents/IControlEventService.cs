﻿using FORM.Web.Api.Models.ControlEvents;

namespace FORM.Web.Api.Services.Foundations.ControlEvents
{
    public interface IControlEventService
    {
        ValueTask<ControlEvent> CreateControlEventAsync(ControlEvent controlEvent, int userId);

        ValueTask<ControlEvent> RetrieveControlEventByIdAsync(int controlEventId);

        ValueTask<ControlEvent> RetrieveControlEventByNameAsync(string eventName);

        ValueTask<ControlEvent> ModifyControlEventAsync(ControlEvent controlEvent, int userId);

        ValueTask<ControlEvent> RemoveControlEventByIdAsync(int controlEventId, int userId);

        ValueTask<ControlEvent> PutBackControlEventByIdAsync(int controlEventId, int userId);

        IQueryable<ControlEvent> RetrieveAllControlEvents();
    }
}
