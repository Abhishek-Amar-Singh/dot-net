﻿using FORM.Web.Api.Brokers.DateTimes;
using FORM.Web.Api.Brokers.Loggings;
using FORM.Web.Api.Brokers.Storages;
using FORM.Web.Api.Models.ControlEvents;

namespace FORM.Web.Api.Services.Foundations.ControlEvents
{
    public partial class ControlEventService : IControlEventService
    {
        private readonly IStorageBroker storageBroker;
        private readonly ILoggingBroker loggingBroker;
        private readonly IDateTimeBroker dateTimeBroker;

        public ControlEventService(
            IStorageBroker storageBroker,
            IDateTimeBroker dateTimeBroker,
            ILoggingBroker loggingBroker)
        {
            this.storageBroker = storageBroker;
            this.dateTimeBroker = dateTimeBroker;
            this.loggingBroker = loggingBroker;
        }

        public ValueTask<ControlEvent> CreateControlEventAsync(
            ControlEvent controlEvent, int userId) =>
        TryCatch(async () =>
        {
            ValidateControlEventOnCreate(controlEvent);

            var controlEventData = await this.storageBroker
                .SelectControlEventByNameAsync(controlEvent.EventName);

            ValidateControlEventAlreadyExists(controlEventData);

            return await this.storageBroker.InsertControlEventAsync(controlEvent, userId);
        });

        public ValueTask<ControlEvent> RetrieveControlEventByIdAsync(int controlEventId) =>
        TryCatch(async () =>
        {
            ValidateControlEventId(controlEventId);

            ControlEvent maybeControlEvent = await this.storageBroker
                .SelectControlEventByIdAsync(controlEventId);

            ValidateStorageControlEvent(maybeControlEvent, controlEventId);

            return maybeControlEvent;
        });

        public ValueTask<ControlEvent> RetrieveControlEventByNameAsync(string eventName) =>
        TryCatch(async () =>
        {
            ValidateControlEventName(eventName);
            ControlEvent maybeControlEvent =
            await this.storageBroker.SelectControlEventByNameAsync(eventName);
            ValidateStorageControlEvent(maybeControlEvent);

            return maybeControlEvent;
        });

        public ValueTask<ControlEvent> ModifyControlEventAsync(ControlEvent controlEvent, int userId) =>
        TryCatch(async () =>
        {
            ValidateControlEventOnModify(controlEvent);

            var maybeControlEvent =
                await this.storageBroker.SelectControlEventByIdAsync(controlEvent.ControlEventId);
            ValidateControlEventAgainstModify(controlEvent, maybeControlEvent);

            maybeControlEvent =
                await this.storageBroker.SelectControlEventByNameAsync(controlEvent.EventName);
            ValidateControlEventAgainstModifyOp(controlEvent, maybeControlEvent);

            return await this.storageBroker.UpdateControlEventAsync(controlEvent, userId);
        });

        public ValueTask<ControlEvent> RemoveControlEventByIdAsync(
                    int controlEventId, int userId) =>
        TryCatch(async () =>
        {
            ValidateControlEventId(controlEventId);

            var maybeControlEvent =
                await this.storageBroker.SelectControlEventByIdAsync(controlEventId);

            ValidateStorageControlEvent(maybeControlEvent, controlEventId);

            return await this.storageBroker.DeleteControlEventAsync(controlEventId, userId);
        });

        public ValueTask<ControlEvent> PutBackControlEventByIdAsync(
            int controlEventId, int userId) =>
        TryCatch(async () =>
        {
            ValidateControlEventId(controlEventId);

            var maybeControlEvent =
                await this.storageBroker.SelectControlEventByIdAsync(controlEventId);

            ValidateStorageControlEvent(maybeControlEvent, controlEventId);

            return await this.storageBroker.UndoDeleteControlEventAsync(controlEventId, userId);
        });

        public IQueryable<ControlEvent> RetrieveAllControlEvents() =>
        TryCatch(() => this.storageBroker.SelectAllControlEvents());
    }
}
