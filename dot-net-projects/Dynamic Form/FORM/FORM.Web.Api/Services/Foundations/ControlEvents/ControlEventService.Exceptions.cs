﻿using EFxceptions.Models.Exceptions;
using FORM.Web.Api.Models.ControlEvents;
using FORM.Web.Api.Models.ControlEvents.Exceptions;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using Xeptions;

namespace FORM.Web.Api.Services.Foundations.ControlEvents
{
    public partial class ControlEventService
    {
        private delegate IQueryable<ControlEvent> ReturningControlEventsFunction();
        private delegate ValueTask<ControlEvent> ReturningControlEventFunction();

        private IQueryable<ControlEvent> TryCatch(
            ReturningControlEventsFunction returningControlEventsFunction)
        {
            try
            {
                return returningControlEventsFunction();
            }
            catch (NpgsqlException npgsqlException)
            {
                throw CreateAndLogCriticalDependencyException(npgsqlException);
            }
            catch (Exception exception)
            {
                var failedControlEventServiceException =
                    new FailedControlEventServiceException(exception);

                throw CreateAndLogServiceException(failedControlEventServiceException);
            }
        }

        private async ValueTask<ControlEvent> TryCatch(
            ReturningControlEventFunction returningControlEventFunction)
        {
            try
            {
                return await returningControlEventFunction();
            }
            catch (NullControlEventException nullControlEventException)
            {
                throw CreateAndLogValidationException(nullControlEventException);
            }
            catch (InvalidControlEventException invalidControlEventException)
            {
                throw CreateAndLogValidationException(invalidControlEventException);
            }
            catch (NotFoundControlEventException nullControlEventException)
            {
                throw CreateAndLogValidationException(nullControlEventException);
            }
            catch (PostgresException postgresException)
            {
                var failedControlEventStorageException =
                    new FailedControlEventStorageException(postgresException);

                throw CreateAndLogCriticalDependencyException(failedControlEventStorageException);
            }
            catch (AlreadyExistsControlEventException alreadyExistsControlEventException)
            {
                throw CreateAndLogDependencyValidationException(alreadyExistsControlEventException);
            }
            catch (DuplicateKeyException duplicateKeyException)
            {
                var alreadyExistsControlEventException =
                    new AlreadyExistsControlEventException(duplicateKeyException);

                throw CreateAndLogDependencyValidationException(alreadyExistsControlEventException);
            }
            catch (DbUpdateConcurrencyException dbUpdateConcurrencyException)
            {
                var lockedControlEventException = new LockedControlEventException(dbUpdateConcurrencyException);

                throw CreateAndLogDependencyException(lockedControlEventException);
            }
            catch (DbUpdateException dbUpdateException)
            {
                var failedControlEventStorageException =
                    new FailedControlEventStorageException(dbUpdateException);

                throw CreateAndLogDependencyException(failedControlEventStorageException);
            }
            catch (UnchangedControlEventException unchangedControlEventException)
            {
                throw CreateAndLogValidationException(unchangedControlEventException);
            }
            catch (CannotChangeControlEventException unchangedControlEventException)
            {
                throw CreateAndLogValidationException(unchangedControlEventException);
            }
            catch (Exception exception)
            {
                var failedControlEventServiceException =
                    new FailedControlEventServiceException(exception);

                throw CreateAndLogServiceException(failedControlEventServiceException);
            }
        }

        private ControlEventDependencyException CreateAndLogDependencyException(Exception exception)
        {
            var controlEventDependencyException = new ControlEventDependencyException(exception);
            this.loggingBroker.LogError(controlEventDependencyException);

            return controlEventDependencyException;
        }

        private ControlEventDependencyException CreateAndLogCriticalDependencyException(Exception exception)
        {
            var controlEventDependencyException = new ControlEventDependencyException(exception);
            this.loggingBroker.LogCritical(controlEventDependencyException);

            return controlEventDependencyException;
        }

        private ControlEventServiceException CreateAndLogServiceException(Exception exception)
        {
            var controlEventServiceException = new ControlEventServiceException(exception);
            this.loggingBroker.LogError(controlEventServiceException);

            return controlEventServiceException;
        }

        private ControlEventValidationException CreateAndLogValidationException(Exception exception)
        {
            var controlEventValidationException = new ControlEventValidationException(exception);
            this.loggingBroker.LogError(controlEventValidationException);

            return controlEventValidationException;
        }

        private ControlEventDependencyValidationException CreateAndLogDependencyValidationException(Xeption exception)
        {
            var controlEventDependencyValidationException =
                new ControlEventDependencyValidationException(exception);

            this.loggingBroker.LogError(controlEventDependencyValidationException);

            return controlEventDependencyValidationException;
        }
    }
}
