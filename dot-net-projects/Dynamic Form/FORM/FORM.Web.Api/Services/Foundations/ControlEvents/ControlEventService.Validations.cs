﻿using FORM.Web.Api.Models.ControlEvents;
using FORM.Web.Api.Models.ControlEvents.Exceptions;

namespace FORM.Web.Api.Services.Foundations.ControlEvents
{
    public partial class ControlEventService
    {
        private void ValidateControlEventOnCreate(ControlEvent controlEvent)
        {
            ValidateControlEvent(controlEvent);

            Validate(
                (Rule: IsInvalidX(controlEvent.EventName), Parameter: nameof(ControlEvent.EventName))
            );
        }

        private static dynamic IsInvalidX(int id) => new
        {
            Condition = id == default,
            Message = "Id is required"
        };

        private static dynamic IsInvalidX(int? id) => new
        {
            Condition = id == default,
            Message = "Id is required"
        };

        private static dynamic IsInvalidX(string text) => new
        {
            Condition = String.IsNullOrWhiteSpace(text),
            Message = "Text is required"
        };

        private static dynamic IsInvalidX(bool? status) => new
        {
            Condition = status is null,
            Message = "status is required"
        };

        private static dynamic IsInvalidX(DateTime? dateTime) => new
        {
            Condition = dateTime == default,
            Message = "DateTime is required"
        };

        private static dynamic IsSame(
            DateTime? firstDate,
            DateTime? secondDate,
            string secondDateName) => new
            {
                Condition = firstDate == secondDate,
                Message = $"Date is the same as {secondDateName}"
            };

        private static void ValidateControlEvent(ControlEvent controlEvent)
        {
            if (controlEvent is null)
            {
                throw new NullControlEventException();
            }
        }

        private void ValidateControlEventAlreadyExists(ControlEvent controlEvent)
        {
            if (controlEvent is not null)
            {
                throw new AlreadyExistsControlEventException();
            }
        }

        private void ValidateControlEventAgainstModify(
            ControlEvent controlEvent, ControlEvent maybeControlEvent)
        {
            ValidateStorageControlEvent(maybeControlEvent, controlEvent.ControlEventId);

            if (controlEvent.EventName == maybeControlEvent.EventName &&
                controlEvent.IsActive == maybeControlEvent.IsActive)
            {
                throw new UnchangedControlEventException(controlEvent.ControlEventId);
            }
        }

        private void ValidateControlEventAgainstModifyOp(
            ControlEvent controlEvent, ControlEvent maybeControlEvent)
        {
            if (maybeControlEvent is not null &&
                maybeControlEvent.ControlEventId != controlEvent.ControlEventId &&
                maybeControlEvent.EventName == controlEvent.EventName)
            {
                throw new CannotChangeControlEventException(
                    controlEvent.EventName, maybeControlEvent.ControlEventId);
            }
        }

        private static void ValidateControlEventId(int controlEventId)
        {
            if (controlEventId == default)
            {
                throw new InvalidControlEventException(
                    parameterName: nameof(ControlEvent.ControlEventId),
                    parameterValue: controlEventId);
            }
        }

        private static void ValidateControlEventName(string controlEventName)
        {
            if (controlEventName == default)
            {
                throw new InvalidControlEventException(
                    parameterName: nameof(ControlEvent.EventName),
                    parameterValue: controlEventName);
            }
        }

        private static void ValidateStorageControlEvent(ControlEvent storageControlEvent, int controlEventId)
        {
            if (storageControlEvent is null)
            {
                throw new NotFoundControlEventException(controlEventId);
            }
        }

        private static void ValidateStorageControlEvent(ControlEvent storageControlEvent)
        {
            if (storageControlEvent is null)
            {
                throw new NotFoundControlEventException();
            }
        }

        private static void ValidateStorageControlEvent(
            ControlEvent storageControlEvent, string controlEventName)
        {
            if (storageControlEvent is null)
            {
                throw new NotFoundControlEventException();
            }
        }

        private void ValidateControlEventOnModify(ControlEvent controlEvent)
        {
            ValidateControlEvent(controlEvent);

            Validate(
                (Rule: IsInvalidX(controlEvent.ControlEventId), Parameter: nameof(ControlEvent.ControlEventId)),
                (Rule: IsInvalidX(controlEvent.EventName), Parameter: nameof(ControlEvent.EventName)),
                (Rule: IsInvalidX(controlEvent.IsActive), Parameter: nameof(ControlEvent.IsActive))
            );
        }

        private static void Validate(params (dynamic Rule, string Parameter)[] validations)
        {
            var invalidControlEventException = new InvalidControlEventException();

            foreach ((dynamic rule, string parameter) in validations)
            {
                if (rule.Condition)
                {
                    invalidControlEventException.UpsertDataList(
                        key: parameter,
                        value: rule.Message);
                }
            }

            invalidControlEventException.ThrowIfContainsErrors();
        }
    }
}
