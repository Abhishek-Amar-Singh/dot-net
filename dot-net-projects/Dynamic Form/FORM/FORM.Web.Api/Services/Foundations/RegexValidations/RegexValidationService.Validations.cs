﻿using FORM.Web.Api.Models.RegexValidations.Exceptions;
using FORM.Web.Api.Models.RegexValidations;

namespace FORM.Web.Api.Services.Foundations.RegexValidations
{
    public partial class RegexValidationService
    {
        private void ValidateRegexValidationOnCreate(RegexValidation regexValidation)
        {
            ValidateRegexValidation(regexValidation);

            Validate(
                (Rule: IsInvalidX(regexValidation.RegexFor), Parameter: nameof(RegexValidation.RegexFor)),
                (Rule: IsInvalidX(regexValidation.Pattern), Parameter: nameof(RegexValidation.Pattern)),
                (Rule: IsInvalidX(regexValidation.ErrorMessage), Parameter: nameof(RegexValidation.ErrorMessage))
            );
        }

        private static dynamic IsInvalidX(int id) => new
        {
            Condition = id == default,
            Message = "Id is required"
        };

        private static dynamic IsInvalidX(int? id) => new
        {
            Condition = id == default,
            Message = "Id is required"
        };

        private static dynamic IsInvalidX(string[] texts) => new
        {
            Condition = texts.Count() == 0,
            Message = "Have atleast one message"
        };

        private static dynamic IsInvalidX(string text) => new
        {
            Condition = String.IsNullOrWhiteSpace(text),
            Message = "Text is required"
        };

        private static dynamic IsInvalidX(bool? status) => new
        {
            Condition = status is null,
            Message = "status is required"
        };

        private static dynamic IsInvalidX(DateTime? dateTime) => new
        {
            Condition = dateTime == default,
            Message = "DateTime is required"
        };

        private static dynamic IsSame(
            DateTime? firstDate,
            DateTime? secondDate,
            string secondDateName) => new
            {
                Condition = firstDate == secondDate,
                Message = $"Date is the same as {secondDateName}"
            };

        private static void ValidateRegexValidation(RegexValidation regexValidation)
        {
            if (regexValidation is null)
            {
                throw new NullRegexValidationException();
            }
        }

        private void ValidateRegexValidationAlreadyExists(RegexValidation regexValidation)
        {
            if (regexValidation is not null)
            {
                throw new AlreadyExistsRegexValidationException();
            }
        }

        private void ValidateRegexValidationAgainstModify(
            RegexValidation regexValidation,
            RegexValidation maybeRegexValidation)
        {
            ValidateStorageRegexValidation(maybeRegexValidation, regexValidation.RegexValidationId);

            if (regexValidation.RegexFor == maybeRegexValidation.RegexFor &&
                regexValidation.Pattern == maybeRegexValidation.Pattern &&
                (regexValidation.ErrorMessage.OrderBy(x => x)
                    .SequenceEqual(maybeRegexValidation.ErrorMessage.OrderBy(x => x))) &&
                regexValidation.IsValid == maybeRegexValidation.IsValid &&
                regexValidation.IsActive == maybeRegexValidation.IsActive)
            {
                throw new UnchangedRegexValidationException(regexValidation.RegexValidationId);
            }
        }

        private void ValidateRegexValidationAgainstModifyOp(
            RegexValidation regexValidation, RegexValidation maybeRegexValidation)
        {
            if (maybeRegexValidation is not null &&
                maybeRegexValidation.RegexValidationId != regexValidation.RegexValidationId &&
                maybeRegexValidation.RegexFor == regexValidation.RegexFor)
            {
                throw new CannotChangeRegexValidationException(
                    regexValidation.RegexFor, maybeRegexValidation.RegexValidationId);
            }
        }

        private static void ValidateRegexValidationId(int regexValidationId)
        {
            if (regexValidationId == default)
            {
                throw new InvalidRegexValidationException(
                    parameterName: nameof(RegexValidation.RegexValidationId),
                    parameterValue: regexValidationId);
            }
        }

        private static void ValidateRegexValidationName(string regexFor)
        {
            if (regexFor == default)
            {
                throw new InvalidRegexValidationException(
                    parameterName: nameof(RegexValidation.RegexFor),
                    parameterValue: regexFor);
            }
        }

        private static void ValidateStorageRegexValidation(
            RegexValidation storageRegexValidation, int regexValidationId)
        {
            if (storageRegexValidation is null)
            {
                throw new NotFoundRegexValidationException(regexValidationId);
            }
        }


        private static void ValidateStorageRegexValidation(RegexValidation storageRegexValidation)
        {
            if (storageRegexValidation is null)
            {
                throw new NotFoundRegexValidationException();
            }
        }

        private void ValidateRegexValidationOnModify(RegexValidation regexValidation)
        {
            ValidateRegexValidation(regexValidation);

            Validate(
                (Rule: IsInvalidX(regexValidation.RegexFor), Parameter: nameof(RegexValidation.RegexFor)),
                (Rule: IsInvalidX(regexValidation.Pattern), Parameter: nameof(RegexValidation.Pattern)),
                (Rule: IsInvalidX(regexValidation.ErrorMessage), Parameter: nameof(RegexValidation.ErrorMessage)),
                (Rule: IsInvalidX(regexValidation.IsActive), Parameter: nameof(RegexValidation.IsActive))
            );
        }

        private static void Validate(params (dynamic Rule, string Parameter)[] validations)
        {
            var invalidRegexValidationException = new InvalidRegexValidationException();

            foreach ((dynamic rule, string parameter) in validations)
            {
                if (rule.Condition)
                {
                    invalidRegexValidationException.UpsertDataList(
                        key: parameter,
                        value: rule.Message);
                }
            }

            invalidRegexValidationException.ThrowIfContainsErrors();
        }
    }
}
