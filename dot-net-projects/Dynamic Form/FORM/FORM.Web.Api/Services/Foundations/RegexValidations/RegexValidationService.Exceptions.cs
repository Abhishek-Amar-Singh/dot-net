﻿using EFxceptions.Models.Exceptions;
using FORM.Web.Api.Models.RegexValidations;
using FORM.Web.Api.Models.RegexValidations.Exceptions;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using Xeptions;

namespace FORM.Web.Api.Services.Foundations.RegexValidations
{
    public partial class RegexValidationService
    {
        private delegate IQueryable<RegexValidation> ReturningRegexValidationsFunction();
        private delegate ValueTask<RegexValidation> ReturningRegexValidationFunction();

        private IQueryable<RegexValidation> TryCatch(
            ReturningRegexValidationsFunction returningRegexValidationsFunction)
        {
            try
            {
                return returningRegexValidationsFunction();
            }
            catch (NpgsqlException npgsqlException)
            {
                throw CreateAndLogCriticalDependencyException(npgsqlException);
            }
            catch (Exception exception)
            {
                var failedRegexValidationServiceException =
                    new FailedRegexValidationServiceException(exception);

                throw CreateAndLogServiceException(failedRegexValidationServiceException);
            }
        }

        private async ValueTask<RegexValidation> TryCatch(
            ReturningRegexValidationFunction returningRegexValidationFunction)
        {
            try
            {
                return await returningRegexValidationFunction();
            }
            catch (NullRegexValidationException nullRegexValidationException)
            {
                throw CreateAndLogValidationException(nullRegexValidationException);
            }
            catch (InvalidRegexValidationException invalidRegexValidationException)
            {
                throw CreateAndLogValidationException(invalidRegexValidationException);
            }
            catch (NotFoundRegexValidationException nullRegexValidationException)
            {
                throw CreateAndLogValidationException(nullRegexValidationException);
            }
            catch (PostgresException postgresException)
            {
                var failedRegexValidationStorageException =
                    new FailedRegexValidationStorageException(postgresException);

                throw CreateAndLogCriticalDependencyException(failedRegexValidationStorageException);
            }
            catch (AlreadyExistsRegexValidationException alreadyExistsRegexValidationException)
            {
                throw CreateAndLogDependencyValidationException(alreadyExistsRegexValidationException);
            }
            catch (DuplicateKeyException duplicateKeyException)
            {
                var alreadyExistsRegexValidationException =
                    new AlreadyExistsRegexValidationException(duplicateKeyException);

                throw CreateAndLogDependencyValidationException(alreadyExistsRegexValidationException);
            }
            catch (DbUpdateConcurrencyException dbUpdateConcurrencyException)
            {
                var lockedRegexValidationException = new LockedRegexValidationException(dbUpdateConcurrencyException);

                throw CreateAndLogDependencyException(lockedRegexValidationException);
            }
            catch (DbUpdateException dbUpdateException)
            {
                var failedRegexValidationStorageException =
                    new FailedRegexValidationStorageException(dbUpdateException);

                throw CreateAndLogDependencyException(failedRegexValidationStorageException);
            }
            catch (UnchangedRegexValidationException unchangedRegexValidationException)
            {
                throw CreateAndLogValidationException(unchangedRegexValidationException);
            }
            catch (CannotChangeRegexValidationException unchangedRegexValidationException)
            {
                throw CreateAndLogValidationException(unchangedRegexValidationException);
            }
            catch (Exception exception)
            {
                var failedRegexValidationServiceException =
                    new FailedRegexValidationServiceException(exception);

                throw CreateAndLogServiceException(failedRegexValidationServiceException);
            }
        }

        private RegexValidationDependencyException CreateAndLogDependencyException(Exception exception)
        {
            var regexValidationDependencyException = new RegexValidationDependencyException(exception);
            this.loggingBroker.LogError(regexValidationDependencyException);

            return regexValidationDependencyException;
        }

        private RegexValidationDependencyException CreateAndLogCriticalDependencyException(Exception exception)
        {
            var regexValidationDependencyException = new RegexValidationDependencyException(exception);
            this.loggingBroker.LogCritical(regexValidationDependencyException);

            return regexValidationDependencyException;
        }

        private RegexValidationServiceException CreateAndLogServiceException(Exception exception)
        {
            var regexValidationServiceException = new RegexValidationServiceException(exception);
            this.loggingBroker.LogError(regexValidationServiceException);

            return regexValidationServiceException;
        }

        private RegexValidationValidationException CreateAndLogValidationException(Exception exception)
        {
            var regexValidationValidationException = new RegexValidationValidationException(exception);
            this.loggingBroker.LogError(regexValidationValidationException);

            return regexValidationValidationException;
        }

        private RegexValidationDependencyValidationException CreateAndLogDependencyValidationException(Xeption exception)
        {
            var regexValidationDependencyValidationException =
                new RegexValidationDependencyValidationException(exception);

            this.loggingBroker.LogError(regexValidationDependencyValidationException);

            return regexValidationDependencyValidationException;
        }
    }
}
