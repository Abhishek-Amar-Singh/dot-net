﻿using FORM.Web.Api.Models.RegexValidations;

namespace FORM.Web.Api.Services.Foundations.RegexValidations
{
    public interface IRegexValidationService
    {
        ValueTask<RegexValidation> CreateRegexValidationAsync(RegexValidation regexValidation, int userId);

        ValueTask<RegexValidation> RetrieveRegexValidationByIdAsync(int regexValidationId);

        ValueTask<RegexValidation> RetrieveRegexValidationByNameAsync(string regexFor);

        ValueTask<RegexValidation> ModifyRegexValidationAsync(RegexValidation regexValidation, int userId);

        ValueTask<RegexValidation> RemoveRegexValidationByIdAsync(int regexValidationId, int userId);

        ValueTask<RegexValidation> PutBackRegexValidationByIdAsync(int regexValidationId, int userId);

        IQueryable<RegexValidation> RetrieveAllRegexValidations();
    }
}
