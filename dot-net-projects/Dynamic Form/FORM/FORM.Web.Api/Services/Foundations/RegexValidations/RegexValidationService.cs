﻿using FORM.Web.Api.Brokers.DateTimes;
using FORM.Web.Api.Brokers.Loggings;
using FORM.Web.Api.Brokers.Storages;
using FORM.Web.Api.Models.RegexValidations;

namespace FORM.Web.Api.Services.Foundations.RegexValidations
{
    public partial class RegexValidationService : IRegexValidationService
    {
        private readonly IStorageBroker storageBroker;
        private readonly ILoggingBroker loggingBroker;
        private readonly IDateTimeBroker dateTimeBroker;

        public RegexValidationService(
            IStorageBroker storageBroker,
            IDateTimeBroker dateTimeBroker,
            ILoggingBroker loggingBroker)
        {
            this.storageBroker = storageBroker;
            this.dateTimeBroker = dateTimeBroker;
            this.loggingBroker = loggingBroker;
        }

        public ValueTask<RegexValidation> CreateRegexValidationAsync(
            RegexValidation regexValidation, int userId) =>
        TryCatch(async () =>
        {
            ValidateRegexValidationOnCreate(regexValidation);

            var regexValidationData = await this.storageBroker
                .SelectRegexValidationByNameAsync(regexValidation.RegexFor);

            ValidateRegexValidationAlreadyExists(regexValidationData);

            return await this.storageBroker.InsertRegexValidationAsync(regexValidation, userId);
        });

        public ValueTask<RegexValidation> RetrieveRegexValidationByIdAsync(int regexValidationId) =>
        TryCatch(async () =>
        {
            ValidateRegexValidationId(regexValidationId);

            RegexValidation maybeRegexValidation = await this.storageBroker
                .SelectRegexValidationByIdAsync(regexValidationId);

            ValidateStorageRegexValidation(maybeRegexValidation, regexValidationId);

            return maybeRegexValidation;
        });

        public ValueTask<RegexValidation> RetrieveRegexValidationByNameAsync(string regexFor) =>
        TryCatch(async () =>
        {
            ValidateRegexValidationName(regexFor);
            RegexValidation maybeRegexValidation = await this.storageBroker
                .SelectRegexValidationByNameAsync(regexFor);
            ValidateStorageRegexValidation(maybeRegexValidation);

            return maybeRegexValidation;
        });




        public ValueTask<RegexValidation> ModifyRegexValidationAsync(RegexValidation regexValidation, int userId) =>
        TryCatch(async () =>
        {
            ValidateRegexValidationOnModify(regexValidation);

            var maybeRegexValidation =
                await this.storageBroker.SelectRegexValidationByIdAsync(regexValidation.RegexValidationId);
            ValidateRegexValidationAgainstModify(regexValidation, maybeRegexValidation);

            maybeRegexValidation =
                await this.storageBroker.SelectRegexValidationByNameAsync(regexValidation.RegexFor);
            ValidateRegexValidationAgainstModifyOp(regexValidation, maybeRegexValidation);

            return await this.storageBroker.UpdateRegexValidationAsync(regexValidation, userId);
        });

        public ValueTask<RegexValidation> RemoveRegexValidationByIdAsync(
            int regexValidationId, int userId) =>
        TryCatch(async () =>
        {
            ValidateRegexValidationId(regexValidationId);

            var maybeRegexValidation =
                await this.storageBroker.SelectRegexValidationByIdAsync(regexValidationId);

            ValidateStorageRegexValidation(maybeRegexValidation, regexValidationId);

            return await this.storageBroker.DeleteRegexValidationAsync(regexValidationId, userId);
        });

        public ValueTask<RegexValidation> PutBackRegexValidationByIdAsync(
            int regexValidationId, int userId) =>
        TryCatch(async () =>
        {
            ValidateRegexValidationId(regexValidationId);

            var maybeRegexValidation =
                await this.storageBroker.SelectRegexValidationByIdAsync(regexValidationId);

            ValidateStorageRegexValidation(maybeRegexValidation, regexValidationId);

            return await this.storageBroker.UndoDeleteRegexValidationAsync(regexValidationId, userId);
        });

        public IQueryable<RegexValidation> RetrieveAllRegexValidations() =>
        TryCatch(() => this.storageBroker.SelectAllRegexValidations());

    }
}
