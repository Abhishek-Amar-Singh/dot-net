﻿using FORM.Web.Api.Brokers.DateTimes;
using FORM.Web.Api.Brokers.Loggings;
using FORM.Web.Api.Brokers.Storages;
using FORM.Web.Api.Models.SystemModuleFields;

namespace FORM.Web.Api.Services.Foundations.SystemModuleFields
{
    public partial class SystemModuleFieldService : ISystemModuleFieldService
    {
        private readonly IStorageBroker storageBroker;
        private readonly ILoggingBroker loggingBroker;
        private readonly IDateTimeBroker dateTimeBroker;

        public SystemModuleFieldService(
            IStorageBroker storageBroker,
            IDateTimeBroker dateTimeBroker,
            ILoggingBroker loggingBroker)
        {
            this.storageBroker = storageBroker;
            this.dateTimeBroker = dateTimeBroker;
            this.loggingBroker = loggingBroker;
        }

        public ValueTask<SystemModuleField> CreateSystemModuleFieldAsync(
            SystemModuleField systemModuleField, int userId) =>
        TryCatch(async () =>
        {
            ValidateSystemModuleFieldOnCreate(systemModuleField);

            var systemModuleFieldData = 
                await this.storageBroker.SelectSystemModuleFieldByNameAndSystemModuleIdAsync(
                    systemModuleField.SystemModuleId, systemModuleField.FieldTitle);

            ValidateSystemModuleFieldAlreadyExists(systemModuleFieldData);

            return await this.storageBroker.InsertSystemModuleFieldAsync(systemModuleField, userId);
        });

        public ValueTask<SystemModuleField> RetrieveSystemModuleFieldByIdAsync(int systemModuleFieldId) =>
        TryCatch(async () =>
        {
            ValidateSystemModuleFieldId(systemModuleFieldId);

            SystemModuleField maybeSystemModuleField = await this.storageBroker
                .SelectSystemModuleFieldByIdAsync(systemModuleFieldId);
            
            ValidateStorageSystemModuleField(maybeSystemModuleField, systemModuleFieldId);

            return maybeSystemModuleField;
        });

        public ValueTask<SystemModuleField> RetrieveSystemModuleFieldByNameAndSystemModuleIdAsync(
            int systemModuleId, string fieldTitle) =>
        TryCatch(async () =>
        {
            ValidateSystemModuleId(systemModuleId);
            ValidateSystemModuleFieldTitle(fieldTitle);

            SystemModuleField maybeSystemModuleField = await this.storageBroker
                .SelectSystemModuleFieldByNameAndSystemModuleIdAsync(systemModuleId, fieldTitle);

            ValidateStorageSystemModuleField(maybeSystemModuleField);

            return maybeSystemModuleField;
        });

        public ValueTask<IQueryable<SystemModuleField>> RetrieveSystemModuleFieldsBySystemModuleIdAsync(
            int systemModuleId) =>
        TryCatch(async () =>
        {
            ValidateSystemModuleId(systemModuleId);

            var maybeSystemModuleFields = await this.storageBroker
                .SelectSystemModuleFieldsBySystemModuleIdAsync(systemModuleId);

            ValidateStorageSystemModuleFieldsUnderSystemModuleId(maybeSystemModuleFields, systemModuleId);

            return maybeSystemModuleFields;
        });

        public ValueTask<SystemModuleField> ModifySystemModuleFieldAsync(
            SystemModuleField systemModuleField, int userId) =>
        TryCatch(async () =>
        {
            ValidateSystemModuleFieldOnModify(systemModuleField);

            var maybeSystemModuleField =
                await this.storageBroker.SelectSystemModuleFieldByIdAsync(
                    systemModuleField.SystemModuleFieldId);

            ValidateSystemModuleFieldAgainstModify(systemModuleField, maybeSystemModuleField);

            maybeSystemModuleField =
                await this.storageBroker.SelectSystemModuleFieldByNameAndSystemModuleIdAsync(
                    systemModuleField.SystemModuleId, systemModuleField.FieldTitle);

            ValidateSystemModuleFieldAgainstModifyOp(systemModuleField, maybeSystemModuleField);

            return await this.storageBroker.UpdateSystemModuleFieldAsync(systemModuleField, userId);
        });

        public ValueTask<SystemModuleField> RemoveSystemModuleFieldByIdAsync(
            int systemModuleFieldId, int userId) =>
        TryCatch(async () =>
        {
            ValidateSystemModuleFieldId(systemModuleFieldId);

            var maybeSystemModuleField =
                await this.storageBroker.SelectSystemModuleFieldByIdAsync(systemModuleFieldId);

            ValidateStorageSystemModuleField(maybeSystemModuleField, systemModuleFieldId);

            return await this.storageBroker.DeleteSystemModuleFieldByIdAsync(systemModuleFieldId, userId);
        });

        public ValueTask<SystemModuleField> PutBackSystemModuleFieldByIdAsync(
            int systemModuleFieldId, int userId) =>
        TryCatch(async () =>
        {
            ValidateSystemModuleFieldId(systemModuleFieldId);

            var maybeSystemModuleField =
                await this.storageBroker.SelectSystemModuleFieldByIdAsync(systemModuleFieldId);

            ValidateStorageSystemModuleField(maybeSystemModuleField, systemModuleFieldId);

            return await this.storageBroker.UndoDeleteSystemModuleFieldByIdAsync(systemModuleFieldId, userId);
        });

        public IQueryable<SystemModuleField> RetrieveAllSystemModuleFields() =>
        TryCatch(() => this.storageBroker.SelectAllSystemModuleFields());
    }
}
