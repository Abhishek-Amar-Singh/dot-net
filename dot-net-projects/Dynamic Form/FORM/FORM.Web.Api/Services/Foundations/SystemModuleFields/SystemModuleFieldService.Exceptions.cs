﻿using EFxceptions.Models.Exceptions;
using FORM.Web.Api.Models.SystemModuleFields;
using FORM.Web.Api.Models.SystemModuleFields.Exceptions;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using Xeptions;

namespace FORM.Web.Api.Services.Foundations.SystemModuleFields
{

    public partial class SystemModuleFieldService
    {
        private delegate IQueryable<SystemModuleField> ReturningSystemModuleFieldsFunction();
        private delegate ValueTask<SystemModuleField> ReturningSystemModuleFieldFunction();
        private delegate ValueTask<IQueryable<SystemModuleField>> ReturnSystemModuleFieldsFunction();

        private IQueryable<SystemModuleField> TryCatch(
            ReturningSystemModuleFieldsFunction returningSystemModuleFieldsFunction)
        {
            try
            {
                return returningSystemModuleFieldsFunction();
            }
            catch (NpgsqlException npgsqlException)
            {
                throw CreateAndLogCriticalDependencyException(npgsqlException);
            }
            catch (Exception exception)
            {
                var failedSystemModuleFieldServiceException =
                    new FailedSystemModuleFieldServiceException(exception);

                throw CreateAndLogServiceException(failedSystemModuleFieldServiceException);
            }
        }

        private async ValueTask<SystemModuleField> TryCatch(
            ReturningSystemModuleFieldFunction returningSystemModuleFieldFunction)
        {
            try
            {
                return await returningSystemModuleFieldFunction();
            }
            catch (NullSystemModuleFieldException nullSystemModuleFieldException)
            {
                throw CreateAndLogValidationException(nullSystemModuleFieldException);
            }
            catch (InvalidSystemModuleFieldException invalidSystemModuleFieldException)
            {
                throw CreateAndLogValidationException(invalidSystemModuleFieldException);
            }
            catch (NotFoundSystemModuleFieldException nullSystemModuleFieldException)
            {
                throw CreateAndLogValidationException(nullSystemModuleFieldException);
            }
            catch (PostgresException postgresException)
            {
                var failedSystemModuleFieldStorageException =
                    new FailedSystemModuleFieldStorageException(postgresException);

                throw CreateAndLogCriticalDependencyException(failedSystemModuleFieldStorageException);
            }
            catch (AlreadyExistsSystemModuleFieldException alreadyExistsSystemModuleFieldException)
            {
                throw CreateAndLogDependencyValidationException(alreadyExistsSystemModuleFieldException);
            }
            catch (DuplicateKeyException duplicateKeyException)
            {
                var alreadyExistsSystemModuleFieldException =
                    new AlreadyExistsSystemModuleFieldException(duplicateKeyException);

                throw CreateAndLogDependencyValidationException(alreadyExistsSystemModuleFieldException);
            }
            catch (DbUpdateConcurrencyException dbUpdateConcurrencyException)
            {
                var lockedSystemModuleFieldException = new LockedSystemModuleFieldException(dbUpdateConcurrencyException);

                throw CreateAndLogDependencyException(lockedSystemModuleFieldException);
            }
            catch (DbUpdateException dbUpdateException)
            {
                var failedSystemModuleFieldStorageException =
                    new FailedSystemModuleFieldStorageException(dbUpdateException);

                throw CreateAndLogDependencyException(failedSystemModuleFieldStorageException);
            }
            catch (UnchangedSystemModuleFieldException unchangedSystemModuleFieldException)
            {
                throw CreateAndLogValidationException(unchangedSystemModuleFieldException);
            }
            catch (CannotChangeSystemModuleFieldException cannotChangeSystemModuleFieldException)
            {
                throw CreateAndLogValidationException(cannotChangeSystemModuleFieldException);
            }
            catch (Exception exception)
            {
                var failedSystemModuleFieldServiceException =
                    new FailedSystemModuleFieldServiceException(exception);

                throw CreateAndLogServiceException(failedSystemModuleFieldServiceException);
            }
        }

        private async ValueTask<IQueryable<SystemModuleField>> TryCatch(
            ReturnSystemModuleFieldsFunction returnSystemModuleFieldsFunction)
        {
            try
            {
                return await returnSystemModuleFieldsFunction();
            }
            catch (InvalidSystemModuleFieldException invalidSystemModuleFieldException)
            {
                throw CreateAndLogValidationException(invalidSystemModuleFieldException);
            }
            catch (
            NotFoundSystemModuleFieldsUnderSystemModuleIdException notFoundSystemModuleFieldsUnderSystemModuleIdException)
            {
                throw CreateAndLogValidationException(notFoundSystemModuleFieldsUnderSystemModuleIdException);
            }
            catch (NpgsqlException npgsqlException)
            {
                throw CreateAndLogCriticalDependencyException(npgsqlException);
            }
            catch (Exception exception)
            {
                var failedSystemModuleFieldServiceException =
                    new FailedSystemModuleFieldServiceException(exception);

                throw CreateAndLogServiceException(failedSystemModuleFieldServiceException);
            }
        }

        private SystemModuleFieldDependencyException CreateAndLogDependencyException(Exception exception)
        {
            var systemModuleFieldDependencyException = new SystemModuleFieldDependencyException(exception);
            this.loggingBroker.LogError(systemModuleFieldDependencyException);

            return systemModuleFieldDependencyException;
        }

        private SystemModuleFieldDependencyException CreateAndLogCriticalDependencyException(Exception exception)
        {
            var systemModuleFieldDependencyException = new SystemModuleFieldDependencyException(exception);
            this.loggingBroker.LogCritical(systemModuleFieldDependencyException);

            return systemModuleFieldDependencyException;
        }

        private SystemModuleFieldServiceException CreateAndLogServiceException(Exception exception)
        {
            var systemModuleFieldServiceException = new SystemModuleFieldServiceException(exception);
            this.loggingBroker.LogError(systemModuleFieldServiceException);

            return systemModuleFieldServiceException;
        }

        private SystemModuleFieldValidationException CreateAndLogValidationException(Exception exception)
        {
            var systemModuleFieldValidationException = new SystemModuleFieldValidationException(exception);
            this.loggingBroker.LogError(systemModuleFieldValidationException);

            return systemModuleFieldValidationException;
        }

        private SystemModuleFieldDependencyValidationException CreateAndLogDependencyValidationException(Xeption exception)
        {
            var systemModuleFieldDependencyValidationException =
                new SystemModuleFieldDependencyValidationException(exception);

            this.loggingBroker.LogError(systemModuleFieldDependencyValidationException);

            return systemModuleFieldDependencyValidationException;
        }
    }
}
