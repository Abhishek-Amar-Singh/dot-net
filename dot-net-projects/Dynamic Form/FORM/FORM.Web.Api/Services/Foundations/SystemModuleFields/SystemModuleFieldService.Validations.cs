﻿using FORM.Web.Api.Models.SystemModuleFields;
using FORM.Web.Api.Models.SystemModuleFields.Exceptions;

namespace FORM.Web.Api.Services.Foundations.SystemModuleFields
{
    public partial class SystemModuleFieldService
    {
        private void ValidateSystemModuleFieldOnCreate(SystemModuleField systemModuleField)
        {
            ValidateSystemModuleField(systemModuleField);

            Validate(
                (Rule: IsInvalidX(systemModuleField.SystemModuleId), Parameter: nameof(SystemModuleField.SystemModuleId)),
                (Rule: IsInvalidX(systemModuleField.FieldTitle), Parameter: nameof(SystemModuleField.FieldTitle)),
                (Rule: IsInvalidX(systemModuleField.ControlMasterId), Parameter: nameof(SystemModuleField.ControlMasterId)),
                (Rule: IsInvalidX(systemModuleField.ControlEventId), Parameter: nameof(SystemModuleField.ControlEventId))
            );
        }

        private static dynamic IsInvalidX(int id) => new
        {
            Condition = id == default,
            Message = "Id is required"
        };

        private static dynamic IsInvalidX(string text) => new
        {
            Condition = String.IsNullOrWhiteSpace(text),
            Message = "Text is required"
        };

        private static dynamic IsInvalidX(bool? status) => new
        {
            Condition = status is null,
            Message = "status is required"
        };


        private static void ValidateSystemModuleField(SystemModuleField systemModuleField)
        {
            if (systemModuleField is null)
            {
                throw new NullSystemModuleFieldException();
            }
        }

        private void ValidateSystemModuleFieldAlreadyExists(SystemModuleField systemModuleField)
        {
            if (systemModuleField is not null)
            {
                throw new AlreadyExistsSystemModuleFieldException();
            }
        }

        private static void ValidateSystemModuleFieldId(int systemModuleFieldId)
        {
            if (systemModuleFieldId == default)
            {
                throw new InvalidSystemModuleFieldException(
                    parameterName: nameof(SystemModuleField.SystemModuleFieldId),
                    parameterValue: systemModuleFieldId);
            }
        }

        private static void ValidateStorageSystemModuleField(
            SystemModuleField storageSystemModuleField, int systemModuleFieldId)
        {
            if (storageSystemModuleField is null)
            {
                throw new NotFoundSystemModuleFieldException(systemModuleFieldId);
            }
        }

        private static void ValidateStorageSystemModuleField(
            SystemModuleField storageSystemModuleField)
        {
            if (storageSystemModuleField is null)
            {
                throw new NotFoundSystemModuleFieldException();
            }
        }

        private static void ValidateSystemModuleId(int systemModuleId)
        {
            if (systemModuleId == default)
            {
                throw new InvalidSystemModuleFieldException(
                    parameterName: nameof(SystemModuleField.SystemModuleId),
                    parameterValue: systemModuleId);
            }
        }

        private static void ValidateSystemModuleFieldTitle(string fieldTitle)
        {
            if (fieldTitle == default)
            {
                throw new InvalidSystemModuleFieldException(
                    parameterName: nameof(SystemModuleField.FieldTitle),
                    parameterValue: fieldTitle);
            }
        }

        private static void ValidateStorageSystemModuleFieldsUnderSystemModuleId(
            IQueryable<SystemModuleField> storageSystemModuleFields, int systemModuleId)
        {
            if (storageSystemModuleFields.Count() is 0)
            {
                throw new NotFoundSystemModuleFieldsUnderSystemModuleIdException(systemModuleId);
            }
        }

        private void ValidateSystemModuleFieldOnModify(SystemModuleField systemModuleField)
        {
            ValidateSystemModuleField(systemModuleField);

            Validate(
                (Rule: IsInvalidX(systemModuleField.SystemModuleId), Parameter: nameof(SystemModuleField.SystemModuleId)),
                (Rule: IsInvalidX(systemModuleField.FieldTitle), Parameter: nameof(SystemModuleField.FieldTitle)),
                (Rule: IsInvalidX(systemModuleField.ControlMasterId), Parameter: nameof(SystemModuleField.ControlMasterId)),
                (Rule: IsInvalidX(systemModuleField.ControlEventId), Parameter: nameof(SystemModuleField.ControlEventId)),
                (Rule: IsInvalidX(systemModuleField.IsActive), Parameter: nameof(SystemModuleField.IsActive))
            );
        }

        private void ValidateSystemModuleFieldAgainstModify(
            SystemModuleField systemModuleField, SystemModuleField maybeSystemModuleField)
        {
            ValidateStorageSystemModuleField(maybeSystemModuleField, systemModuleField.SystemModuleFieldId);

            if (systemModuleField.SystemModuleId == maybeSystemModuleField.SystemModuleId &&
                systemModuleField.FieldTitle == maybeSystemModuleField.FieldTitle &&
                systemModuleField.ControlMasterId == maybeSystemModuleField.ControlMasterId &&
                systemModuleField.ControlEventId == maybeSystemModuleField.ControlEventId &&
                systemModuleField.IsVisible == maybeSystemModuleField.IsVisible &&
                systemModuleField.Position == maybeSystemModuleField.Position &&
                systemModuleField.Placeholder == maybeSystemModuleField.Placeholder &&
                systemModuleField.ParentControlId == maybeSystemModuleField.ParentControlId &&
                systemModuleField.IsRequired == maybeSystemModuleField.IsRequired &&
                systemModuleField.IsActive == maybeSystemModuleField.IsActive)
            {
                throw new UnchangedSystemModuleFieldException(systemModuleField.SystemModuleFieldId);
            }
        }

        private void ValidateSystemModuleFieldAgainstModifyOp(
            SystemModuleField systemModuleField, SystemModuleField maybeSystemModuleField)
        {
            if (maybeSystemModuleField is not null &&
                maybeSystemModuleField.SystemModuleFieldId != systemModuleField.SystemModuleFieldId &&
                (maybeSystemModuleField.SystemModuleId == systemModuleField.SystemModuleId &&
                maybeSystemModuleField.FieldTitle == systemModuleField.FieldTitle))
            {
                var model = new
                {
                    SystemModuleId = systemModuleField.SystemModuleId,
                    FieldTitle = systemModuleField.FieldTitle
                };
                throw new CannotChangeSystemModuleFieldException(
                    model, maybeSystemModuleField.SystemModuleFieldId);
            }
        }

        private static void Validate(params (dynamic Rule, string Parameter)[] validations)
        {
            var invalidSystemModuleFieldException = new InvalidSystemModuleFieldException();

            foreach ((dynamic rule, string parameter) in validations)
            {
                if (rule.Condition)
                {
                    invalidSystemModuleFieldException.UpsertDataList(
                        key: parameter,
                        value: rule.Message);
                }
            }

            invalidSystemModuleFieldException.ThrowIfContainsErrors();
        }
    }
}
