﻿using FORM.Web.Api.Models.SystemModuleFields;

namespace FORM.Web.Api.Services.Foundations.SystemModuleFields
{
    public interface ISystemModuleFieldService
    {
        ValueTask<SystemModuleField> CreateSystemModuleFieldAsync(
            SystemModuleField systemModuleField, int userId);

        ValueTask<SystemModuleField> RetrieveSystemModuleFieldByIdAsync(int systemModuleFieldId);

        ValueTask<SystemModuleField> RetrieveSystemModuleFieldByNameAndSystemModuleIdAsync(
            int systemModuleId, string fieldTitle);

        ValueTask<IQueryable<SystemModuleField>> RetrieveSystemModuleFieldsBySystemModuleIdAsync(
            int systemModuleId);

        ValueTask<SystemModuleField> ModifySystemModuleFieldAsync(
            SystemModuleField systemModuleField, int userId);

        ValueTask<SystemModuleField> RemoveSystemModuleFieldByIdAsync(
            int systemModuleFieldId, int userId);

        ValueTask<SystemModuleField> PutBackSystemModuleFieldByIdAsync(int systemModuleFieldId, int userId);

        IQueryable<SystemModuleField> RetrieveAllSystemModuleFields();
    }
}
