﻿using FORM.Web.Api.Models.ControlMasters;
using FORM.Web.Api.Models.ControlMasters.Dtos;
using FORM.Web.Api.Models.ControlMasters.Exceptions;
using FORM.Web.Api.Services.Foundations.ControlMasters;
using Microsoft.AspNetCore.Mvc;
using RESTFulSense.Controllers;
using System.Globalization;

namespace FORM.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ControlMasterController : RESTFulController
    {
        private readonly IControlMasterService controlMasterService;

        public ControlMasterController(IControlMasterService controlMasterService) =>
            this.controlMasterService = controlMasterService;

        [Route("PostControlMasterAsync")]
        [HttpPost]
        public async ValueTask<ActionResult<ControlMaster>> PostControlMasterAsync(
            CreateControlMasterDto controlMasterDto, int userId)
        {
            try
            {
                var controlMaster = new ControlMaster
                {
                    ControlName = ToTitleCase(controlMasterDto.ControlName),
                    BasicSyntax = null,
                    IsActive = true,
                    IsDeleted = false
                };
                var storageControlMaster = await this.controlMasterService.CreateControlMasterAsync(controlMaster, userId);

                return Created(storageControlMaster);
            }
            catch (ControlMasterValidationException controlMasterValidationException)
                when (controlMasterValidationException.InnerException is AlreadyExistsControlMasterException)
            {
                return Conflict(controlMasterValidationException.InnerException);
            }
            catch (ControlMasterValidationException controlMasterValidationException)
            {
                return BadRequest(controlMasterValidationException.InnerException);
            }
            catch (ControlMasterDependencyException controlMasterDependencyException)
            {
                return InternalServerError(controlMasterDependencyException);
            }
            catch (ControlMasterServiceException controlMasterServiceException)
            {
                return InternalServerError(controlMasterServiceException);
            }
        }

        [Route("GetControlMasterAsync/{controlMasterId:int}")]
        [HttpGet]
        public async ValueTask<ActionResult<ControlMaster>> GetControlMasterAsync(int controlMasterId)
        {
            try
            {
                var storageControlMaster =
                    await this.controlMasterService.RetrieveControlMasterByIdAsync(controlMasterId);

                return Ok(storageControlMaster);
            }
            catch (ControlMasterValidationException controlMasterValidationException)
                when (controlMasterValidationException.InnerException is NotFoundControlMasterException)
            {
                string innerMessage = GetInnerMessage(controlMasterValidationException);

                return NotFound(innerMessage);
            }
            catch (ControlMasterValidationException controlMasterValidationException)
            {
                string innerMessage = GetInnerMessage(controlMasterValidationException);

                return BadRequest(controlMasterValidationException);
            }
            catch (ControlMasterDependencyException controlMasterDependencyException)
            {
                return Problem(controlMasterDependencyException.Message);
            }
            catch (ControlMasterServiceException controlMasterServiceException)
            {
                return Problem(controlMasterServiceException.Message);
            }
        }

        [Route("GetControlMasterAsync/{controlName}")]
        [HttpGet]
        public async ValueTask<ActionResult<ControlMaster>> GetControlMasterAsync(string controlName)
        {
            try
            {
                var storageControlMaster =
                    await this.controlMasterService.RetrieveControlMasterByNameAsync(ToTitleCase(
                        controlName));

                return Ok(storageControlMaster);
            }
            catch (ControlMasterValidationException controlMasterValidationException)
                when (controlMasterValidationException.InnerException is NotFoundControlMasterException)
            {
                string innerMessage = GetInnerMessage(controlMasterValidationException);

                return NotFound(innerMessage);
            }
            catch (ControlMasterValidationException controlMasterValidationException)
            {
                string innerMessage = GetInnerMessage(controlMasterValidationException);

                return BadRequest(controlMasterValidationException);
            }
            catch (ControlMasterDependencyException controlMasterDependencyException)
            {
                return Problem(controlMasterDependencyException.Message);
            }
            catch (ControlMasterServiceException controlMasterServiceException)
            {
                return Problem(controlMasterServiceException.Message);
            }
        }

        [Route("PutControlMasterAsync")]
        [HttpPut]
        public async ValueTask<ActionResult<ControlMaster>> PutControlMasterAsync(
            EditControlMasterDto controlMasterDto, int userId)
        {
            try
            {
                var controlMaster = new ControlMaster
                {
                    ControlMasterId = controlMasterDto.Id,
                    ControlName = ToTitleCase(controlMasterDto.ControlName),
                    BasicSyntax = controlMasterDto.BasicSyntax,
                    IsActive = controlMasterDto.IsActive ?? true,
                    IsDeleted = (controlMasterDto.IsActive is true or null) ? false : true
                };
                var storageControlMaster =
                    await this.controlMasterService.ModifyControlMasterAsync(controlMaster, userId);

                return Ok(storageControlMaster);
            }
            catch (ControlMasterValidationException controlMasterValidationException)
                when (controlMasterValidationException.InnerException is NotFoundControlMasterException)
            {
                string innerMessage = GetInnerMessage(controlMasterValidationException);

                return NotFound(innerMessage);
            }
            catch (ControlMasterValidationException controlMasterValidationException)
                when (controlMasterValidationException.InnerException is UnchangedControlMasterException)
            {
                string innerMessage = GetInnerMessage(controlMasterValidationException);

                return Problem(innerMessage);
            }
            catch (ControlMasterValidationException controlMasterValidationException)
                when (controlMasterValidationException.InnerException is CannotChangeControlMasterException)
            {
                string innerMessage = GetInnerMessage(controlMasterValidationException);

                return Problem(innerMessage);
            }
            catch (ControlMasterValidationException controlMasterValidationException)
            {
                string innerMessage = GetInnerMessage(controlMasterValidationException);

                return BadRequest(innerMessage);
            }
            catch (ControlMasterDependencyException controlMasterDependencyException)
                when (controlMasterDependencyException.InnerException is LockedControlMasterException)
            {
                string innerMessage = GetInnerMessage(controlMasterDependencyException);

                return Locked(innerMessage);
            }
            catch (ControlMasterDependencyException controlMasterDependencyException)
            {
                return Problem(controlMasterDependencyException.Message);
            }
            catch (ControlMasterServiceException controlMasterServiceException)
            {
                return Problem(controlMasterServiceException.Message);
            }
        }

        [Route("DeleteControlMasterAsync")]
        [HttpDelete]
        public async ValueTask<ActionResult<ControlMaster>> DeleteControlMasterAsync(
            int controlMasterId, int userId)
        {
            try
            {
                var storageControlMaster =
                    await this.controlMasterService.RemoveControlMasterByIdAsync(controlMasterId, userId);

                return Ok(storageControlMaster);
            }
            catch (ControlMasterValidationException controlMasterValidationException)
                when (controlMasterValidationException.InnerException is NotFoundControlMasterException)
            {
                string innerMessage = GetInnerMessage(controlMasterValidationException);

                return NotFound(innerMessage);
            }
            catch (ControlMasterValidationException controlMasterValidationException)
            {
                string innerMessage = GetInnerMessage(controlMasterValidationException);

                return BadRequest(controlMasterValidationException);
            }
            catch (ControlMasterDependencyException controlMasterDependencyException)
               when (controlMasterDependencyException.InnerException is LockedControlMasterException)
            {
                string innerMessage = GetInnerMessage(controlMasterDependencyException);

                return Locked(innerMessage);
            }
            catch (ControlMasterDependencyException controlMasterDependencyException)
            {
                return Problem(controlMasterDependencyException.Message);
            }
            catch (ControlMasterServiceException controlMasterServiceException)
            {
                return Problem(controlMasterServiceException.Message);
            }
        }

        [Route("UndoDeleteControlMasterAsync")]
        [HttpPut]
        public async ValueTask<ActionResult<ControlMaster>> UndoDeleteControlMasterAsync(
            int controlMasterId, int userId)
        {
            try
            {
                var storageControlMaster =
                    await this.controlMasterService.PutBackControlMasterByIdAsync(controlMasterId, userId);

                return Ok(storageControlMaster);
            }
            catch (ControlMasterValidationException controlMasterValidationException)
                when (controlMasterValidationException.InnerException is NotFoundControlMasterException)
            {
                string innerMessage = GetInnerMessage(controlMasterValidationException);

                return NotFound(innerMessage);
            }
            catch (ControlMasterValidationException controlMasterValidationException)
            {
                string innerMessage = GetInnerMessage(controlMasterValidationException);

                return BadRequest(controlMasterValidationException);
            }
            catch (ControlMasterDependencyException controlMasterDependencyException)
               when (controlMasterDependencyException.InnerException is LockedControlMasterException)
            {
                string innerMessage = GetInnerMessage(controlMasterDependencyException);

                return Locked(innerMessage);
            }
            catch (ControlMasterDependencyException controlMasterDependencyException)
            {
                return Problem(controlMasterDependencyException.Message);
            }
            catch (ControlMasterServiceException controlMasterServiceException)
            {
                return Problem(controlMasterServiceException.Message);
            }
        }

        [Route("GetAllControlMasters")]
        [HttpGet]
        public ActionResult<IQueryable<ControlMaster>> GetAllControlMasters()
        {
            try
            {
                IQueryable<ControlMaster> storageControlMasters =
                    this.controlMasterService.RetrieveAllControlMasters();

                return Ok(storageControlMasters);
            }
            catch (ControlMasterDependencyException controlMasterDependencyException)
            {
                return Problem(controlMasterDependencyException.Message);
            }
            catch (ControlMasterServiceException controlMasterServiceException)
            {
                return Problem(controlMasterServiceException.Message);
            }
        }

        private static string GetInnerMessage(Exception exception) =>
            exception.InnerException.Message;

        private static string ToTitleCase(string str) =>
            new CultureInfo("en-US", false).TextInfo.ToTitleCase(str);
    }
}
