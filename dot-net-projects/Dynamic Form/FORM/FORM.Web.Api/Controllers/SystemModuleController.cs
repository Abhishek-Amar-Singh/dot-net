﻿using FORM.Web.Api.Models.SystemModules;
using FORM.Web.Api.Models.SystemModules.Dtos;
using FORM.Web.Api.Models.SystemModules.Exceptions;
using FORM.Web.Api.Services.Foundations.SystemModules;
using Microsoft.AspNetCore.Mvc;
using RESTFulSense.Controllers;
using System.Globalization;

namespace FORM.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SystemModuleController : RESTFulController
    {
        private readonly ISystemModuleService systemModuleService;

        public SystemModuleController(ISystemModuleService systemModuleService) =>
            this.systemModuleService = systemModuleService;
        /*
        [Route("GetSystemModuleDetails")]
        [HttpGet]
        public ActionResult<IQueryable<SystemModuleVm>> GetSystemModuleDetails(int systemModuleId)
        {
            try
            {
                IQueryable<SystemModuleVm> storageSystemModuleVm =
                    this.systemModuleService.RetrieveSystemModuleDetailsById(systemModuleId);
                return Ok(storageSystemModuleVm);
            }
            catch (SystemModuleValidationException systemModuleValidationException)
                when (systemModuleValidationException.InnerException is NotFoundSystemModuleException)
            {
                string innerMessage = GetInnerMessage(systemModuleValidationException);

                return NotFound(innerMessage);
            }
            catch (SystemModuleValidationException employeeValidationException)
            {
                string innerMessage = GetInnerMessage(employeeValidationException);

                return BadRequest(employeeValidationException);
            }
            catch (SystemModuleDependencyException systemModuleDependencyException)
            {
                return Problem(systemModuleDependencyException.Message);
            }
            catch (SystemModuleServiceException systemModuleServiceException)
            {
                return Problem(systemModuleServiceException.Message);
            }
        }*/

        [Route("PostSystemModuleAsync")]
        [HttpPost]
        public async ValueTask<ActionResult<SystemModule>> PostSystemModuleAsync(
            CreateSystemModuleDto systemModuleDto, int userId)
        {
            try
            {
                var systemModule = new SystemModule
                {
                    ModuleName = ToTitleCase(systemModuleDto.ModuleName),
                    IsActive = true,
                    IsDeleted = false
                };
                var storageSystemModule = await this.systemModuleService.CreateSystemModuleAsync(systemModule, userId);

                return Created(storageSystemModule);
            }
            catch (SystemModuleValidationException systemModuleValidationException)
                when (systemModuleValidationException.InnerException is AlreadyExistsSystemModuleException)
            {
                return Conflict(systemModuleValidationException.InnerException);
            }
            catch (SystemModuleValidationException systemModuleValidationException)
            {
                return BadRequest(systemModuleValidationException.InnerException);
            }
            catch (SystemModuleDependencyException systemModuleDependencyException)
            {
                return InternalServerError(systemModuleDependencyException);
            }
            catch (SystemModuleServiceException systemModuleServiceException)
            {
                return InternalServerError(systemModuleServiceException);
            }
        }
        
        [Route("GetSystemModuleAsync/{systemModuleId:int}")]
        [HttpGet]
        public async ValueTask<ActionResult<SystemModule>> GetSystemModuleAsync(int systemModuleId)
        {
            try
            {
                var storageSystemModule =
                    await this.systemModuleService.RetrieveSystemModuleByIdAsync(systemModuleId);

                return Ok(storageSystemModule);
            }
            catch (SystemModuleValidationException systemModuleValidationException)
                when (systemModuleValidationException.InnerException is NotFoundSystemModuleException)
            {
                string innerMessage = GetInnerMessage(systemModuleValidationException);

                return NotFound(innerMessage);
            }
            catch (SystemModuleValidationException systemModuleValidationException)
            {
                string innerMessage = GetInnerMessage(systemModuleValidationException);

                return BadRequest(systemModuleValidationException);
            }
            catch (SystemModuleDependencyException systemModuleDependencyException)
            {
                return Problem(systemModuleDependencyException.Message);
            }
            catch (SystemModuleServiceException systemModuleServiceException)
            {
                return Problem(systemModuleServiceException.Message);
            }
        }

        [Route("GetSystemModuleAsync/{systemModuleName}")]
        [HttpGet]
        public async ValueTask<ActionResult<SystemModule>> GetSystemModuleAsync(string systemModuleName)
        {
            try
            {
                var storageSystemModule =
                    await this.systemModuleService.RetrieveSystemModuleByNameAsync(ToTitleCase(
                        systemModuleName));

                return Ok(storageSystemModule);
            }
            catch (SystemModuleValidationException systemModuleValidationException)
                when (systemModuleValidationException.InnerException is NotFoundSystemModuleException)
            {
                string innerMessage = GetInnerMessage(systemModuleValidationException);

                return NotFound(innerMessage);
            }
            catch (SystemModuleValidationException systemModuleValidationException)
            {
                string innerMessage = GetInnerMessage(systemModuleValidationException);

                return BadRequest(systemModuleValidationException);
            }
            catch (SystemModuleDependencyException systemModuleDependencyException)
            {
                return Problem(systemModuleDependencyException.Message);
            }
            catch (SystemModuleServiceException systemModuleServiceException)
            {
                return Problem(systemModuleServiceException.Message);
            }
        }
        
        [Route("PutSystemModuleAsync")]
        [HttpPut]
        public async ValueTask<ActionResult<SystemModule>> PutSystemModuleAsync(
            EditSystemModuleDto systemModuleDto, int userId)
        {
            try
            {
                var systemModule = new SystemModule
                {
                    SystemModuleId = systemModuleDto.Id,
                    ModuleName = ToTitleCase(systemModuleDto.ModuleName),
                    IsActive = systemModuleDto.IsActive ?? true,
                    IsDeleted = (systemModuleDto.IsActive is true or null) ? false : true
                };
                var storageSystemModule =
                    await this.systemModuleService.ModifySystemModuleAsync(systemModule, userId);

                return Ok(storageSystemModule);
            }
            catch (SystemModuleValidationException systemModuleValidationException)
                when (systemModuleValidationException.InnerException is NotFoundSystemModuleException)
            {
                string innerMessage = GetInnerMessage(systemModuleValidationException);

                return NotFound(innerMessage);
            }
            catch (SystemModuleValidationException systemModuleValidationException)
                when (systemModuleValidationException.InnerException is UnchangedSystemModuleException)
            {
                string innerMessage = GetInnerMessage(systemModuleValidationException);

                return Problem(innerMessage);
            }
            catch (SystemModuleValidationException systemModuleValidationException)
                when (systemModuleValidationException.InnerException is CannotChangeSystemModuleException)
            {
                string innerMessage = GetInnerMessage(systemModuleValidationException);

                return Problem(innerMessage);
            }
            catch (SystemModuleValidationException systemModuleValidationException)
            {
                string innerMessage = GetInnerMessage(systemModuleValidationException);

                return BadRequest(innerMessage);
            }
            catch (SystemModuleDependencyException systemModuleDependencyException)
                when (systemModuleDependencyException.InnerException is LockedSystemModuleException)
            {
                string innerMessage = GetInnerMessage(systemModuleDependencyException);

                return Locked(innerMessage);
            }
            catch (SystemModuleDependencyException systemModuleDependencyException)
            {
                return Problem(systemModuleDependencyException.Message);
            }
            catch (SystemModuleServiceException systemModuleServiceException)
            {
                return Problem(systemModuleServiceException.Message);
            }
        }

        [Route("DeleteSystemModuleAsync")]
        [HttpDelete]
        public async ValueTask<ActionResult<SystemModule>> DeleteSystemModuleAsync(
            int systemModuleId, int userId)
        {
            try
            {
                var storageSystemModule =
                    await this.systemModuleService.RemoveSystemModuleByIdAsync(systemModuleId, userId);

                return Ok(storageSystemModule);
            }
            catch (SystemModuleValidationException systemModuleValidationException)
                when (systemModuleValidationException.InnerException is NotFoundSystemModuleException)
            {
                string innerMessage = GetInnerMessage(systemModuleValidationException);

                return NotFound(innerMessage);
            }
            catch (SystemModuleValidationException systemModuleValidationException)
            {
                string innerMessage = GetInnerMessage(systemModuleValidationException);

                return BadRequest(systemModuleValidationException);
            }
            catch (SystemModuleDependencyException systemModuleDependencyException)
               when (systemModuleDependencyException.InnerException is LockedSystemModuleException)
            {
                string innerMessage = GetInnerMessage(systemModuleDependencyException);

                return Locked(innerMessage);
            }
            catch (SystemModuleDependencyException systemModuleDependencyException)
            {
                return Problem(systemModuleDependencyException.Message);
            }
            catch (SystemModuleServiceException systemModuleServiceException)
            {
                return Problem(systemModuleServiceException.Message);
            }
        }

        [Route("UndoDeleteSystemModuleAsync")]
        [HttpPut]
        public async ValueTask<ActionResult<SystemModule>> UndoDeleteSystemModuleAsync(
            int systemModuleId, int userId)
        {
            try
            {
                var storageSystemModule =
                    await this.systemModuleService.PutBackSystemModuleByIdAsync(systemModuleId, userId);

                return Ok(storageSystemModule);
            }
            catch (SystemModuleValidationException systemModuleValidationException)
                when (systemModuleValidationException.InnerException is NotFoundSystemModuleException)
            {
                string innerMessage = GetInnerMessage(systemModuleValidationException);

                return NotFound(innerMessage);
            }
            catch (SystemModuleValidationException systemModuleValidationException)
            {
                string innerMessage = GetInnerMessage(systemModuleValidationException);

                return BadRequest(systemModuleValidationException);
            }
            catch (SystemModuleDependencyException systemModuleDependencyException)
               when (systemModuleDependencyException.InnerException is LockedSystemModuleException)
            {
                string innerMessage = GetInnerMessage(systemModuleDependencyException);

                return Locked(innerMessage);
            }
            catch (SystemModuleDependencyException systemModuleDependencyException)
            {
                return Problem(systemModuleDependencyException.Message);
            }
            catch (SystemModuleServiceException systemModuleServiceException)
            {
                return Problem(systemModuleServiceException.Message);
            }
        }

        [Route("GetAllSystemModules")]
        [HttpGet]
        public ActionResult<IQueryable<SystemModule>> GetAllSystemModules()
        {
            try
            {
                IQueryable<SystemModule> storageSystemModules =
                    this.systemModuleService.RetrieveAllSystemModules();

                return Ok(storageSystemModules);
            }
            catch (SystemModuleDependencyException systemModuleDependencyException)
            {
                return Problem(systemModuleDependencyException.Message);
            }
            catch (SystemModuleServiceException systemModuleServiceException)
            {
                return Problem(systemModuleServiceException.Message);
            }
        }

        private static string GetInnerMessage(Exception exception) =>
            exception.InnerException.Message;

        private static string ToTitleCase(string str) =>
            new CultureInfo("en-US", false).TextInfo.ToTitleCase(str);
    }
}
