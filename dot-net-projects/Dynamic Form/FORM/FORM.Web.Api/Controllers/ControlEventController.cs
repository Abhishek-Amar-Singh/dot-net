﻿using FORM.Web.Api.Models.ControlEvents;
using FORM.Web.Api.Models.ControlEvents.Dtos;
using FORM.Web.Api.Models.ControlEvents.Exceptions;
using FORM.Web.Api.Services.Foundations.ControlEvents;
using Microsoft.AspNetCore.Mvc;
using RESTFulSense.Controllers;
using System.Globalization;

namespace FORM.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ControlEventController : RESTFulController
    {
        private readonly IControlEventService controlEventService;

        public ControlEventController(IControlEventService controlEventService) =>
            this.controlEventService = controlEventService;

        [Route("PostControlEventAsync")]
        [HttpPost]
        public async ValueTask<ActionResult<ControlEvent>> PostControlEventAsync(
            CreateControlEventDto controlEventDto, int userId)
        {
            try
            {
                var controlEvent = new ControlEvent
                {
                    EventName = ToLowerCase(controlEventDto.EventName),
                    IsActive = true,
                    IsDeleted = false
                };
                var storageControlEvent = 
                    await this.controlEventService.CreateControlEventAsync(controlEvent, userId);

                return Created(storageControlEvent);
            }
            catch (ControlEventValidationException controlEventValidationException)
                when (controlEventValidationException.InnerException is AlreadyExistsControlEventException)
            {
                return Conflict(controlEventValidationException.InnerException);
            }
            catch (ControlEventValidationException controlEventValidationException)
            {
                return BadRequest(controlEventValidationException.InnerException);
            }
            catch (ControlEventDependencyException controlEventDependencyException)
            {
                return InternalServerError(controlEventDependencyException);
            }
            catch (ControlEventServiceException controlEventServiceException)
            {
                return InternalServerError(controlEventServiceException);
            }
        }

        [Route("GetControlEventAsync/{controlEventId:int}")]
        [HttpGet]
        public async ValueTask<ActionResult<ControlEvent>> GetControlEventAsync(int controlEventId)
        {
            try
            {
                var storageControlEvent =
                    await this.controlEventService.RetrieveControlEventByIdAsync(controlEventId);

                return Ok(storageControlEvent);
            }
            catch (ControlEventValidationException controlEventValidationException)
                when (controlEventValidationException.InnerException is NotFoundControlEventException)
            {
                string innerMessage = GetInnerMessage(controlEventValidationException);

                return NotFound(innerMessage);
            }
            catch (ControlEventValidationException controlEventValidationException)
            {
                string innerMessage = GetInnerMessage(controlEventValidationException);

                return BadRequest(controlEventValidationException);
            }
            catch (ControlEventDependencyException controlEventDependencyException)
            {
                return Problem(controlEventDependencyException.Message);
            }
            catch (ControlEventServiceException controlEventServiceException)
            {
                return Problem(controlEventServiceException.Message);
            }
        }

        [Route("GetControlEventAsync/{eventName}")]
        [HttpGet]
        public async ValueTask<ActionResult<ControlEvent>> GetControlEventAsync(string eventName)
        {
            try
            {
                var storageControlEvent =
                    await this.controlEventService.RetrieveControlEventByNameAsync(ToLowerCase(eventName));

                return Ok(storageControlEvent);
            }
            catch (ControlEventValidationException controlEventValidationException)
                when (controlEventValidationException.InnerException is NotFoundControlEventException)
            {
                string innerMessage = GetInnerMessage(controlEventValidationException);

                return NotFound(innerMessage);
            }
            catch (ControlEventValidationException controlEventValidationException)
            {
                string innerMessage = GetInnerMessage(controlEventValidationException);

                return BadRequest(controlEventValidationException);
            }
            catch (ControlEventDependencyException controlEventDependencyException)
            {
                return Problem(controlEventDependencyException.Message);
            }
            catch (ControlEventServiceException controlEventServiceException)
            {
                return Problem(controlEventServiceException.Message);
            }
        }

        [Route("PutControlEventAsync")]
        [HttpPut]
        public async ValueTask<ActionResult<ControlEvent>> PutControlEventAsync(
            EditControlEventDto controlEventDto, int userId)
        {
            try
            {
                var controlEvent = new ControlEvent
                {
                    ControlEventId = controlEventDto.Id,
                    EventName = ToLowerCase(controlEventDto.EventName),
                    IsActive = controlEventDto.IsActive ?? true,
                    IsDeleted = (controlEventDto.IsActive is true or null) ? false : true
                };
                var storageControlEvent =
                    await this.controlEventService.ModifyControlEventAsync(controlEvent, userId);

                return Ok(storageControlEvent);
            }
            catch (ControlEventValidationException controlEventValidationException)
                when (controlEventValidationException.InnerException is NotFoundControlEventException)
            {
                string innerMessage = GetInnerMessage(controlEventValidationException);

                return NotFound(innerMessage);
            }
            catch (ControlEventValidationException controlEventValidationException)
                when (controlEventValidationException.InnerException is UnchangedControlEventException)
            {
                string innerMessage = GetInnerMessage(controlEventValidationException);

                return Problem(innerMessage);
            }
            catch (ControlEventValidationException controlEventValidationException)
                when (controlEventValidationException.InnerException is CannotChangeControlEventException)
            {
                string innerMessage = GetInnerMessage(controlEventValidationException);

                return Problem(innerMessage);
            }
            catch (ControlEventValidationException controlEventValidationException)
            {
                string innerMessage = GetInnerMessage(controlEventValidationException);

                return BadRequest(innerMessage);
            }
            catch (ControlEventDependencyException controlEventDependencyException)
                when (controlEventDependencyException.InnerException is LockedControlEventException)
            {
                string innerMessage = GetInnerMessage(controlEventDependencyException);

                return Locked(innerMessage);
            }
            catch (ControlEventDependencyException controlEventDependencyException)
            {
                return Problem(controlEventDependencyException.Message);
            }
            catch (ControlEventServiceException controlEventServiceException)
            {
                return Problem(controlEventServiceException.Message);
            }
        }

        [Route("DeleteControlEventAsync")]
        [HttpDelete]
        public async ValueTask<ActionResult<ControlEvent>> DeleteControlEventAsync(
            int controlEventId, int userId)
        {
            try
            {
                var storageControlEvent =
                    await this.controlEventService.RemoveControlEventByIdAsync(controlEventId, userId);

                return Ok(storageControlEvent);
            }
            catch (ControlEventValidationException controlEventValidationException)
                when (controlEventValidationException.InnerException is NotFoundControlEventException)
            {
                string innerMessage = GetInnerMessage(controlEventValidationException);

                return NotFound(innerMessage);
            }
            catch (ControlEventValidationException controlEventValidationException)
            {
                string innerMessage = GetInnerMessage(controlEventValidationException);

                return BadRequest(controlEventValidationException);
            }
            catch (ControlEventDependencyException controlEventDependencyException)
               when (controlEventDependencyException.InnerException is LockedControlEventException)
            {
                string innerMessage = GetInnerMessage(controlEventDependencyException);

                return Locked(innerMessage);
            }
            catch (ControlEventDependencyException controlEventDependencyException)
            {
                return Problem(controlEventDependencyException.Message);
            }
            catch (ControlEventServiceException controlEventServiceException)
            {
                return Problem(controlEventServiceException.Message);
            }
        }

        [Route("UndoDeleteControlEventAsync")]
        [HttpPut]
        public async ValueTask<ActionResult<ControlEvent>> UndoDeleteControlEventAsync(
            int controlEventId, int userId)
        {
            try
            {
                var storageControlEvent =
                    await this.controlEventService.PutBackControlEventByIdAsync(controlEventId, userId);

                return Ok(storageControlEvent);
            }
            catch (ControlEventValidationException controlEventValidationException)
                when (controlEventValidationException.InnerException is NotFoundControlEventException)
            {
                string innerMessage = GetInnerMessage(controlEventValidationException);

                return NotFound(innerMessage);
            }
            catch (ControlEventValidationException controlEventValidationException)
            {
                string innerMessage = GetInnerMessage(controlEventValidationException);

                return BadRequest(controlEventValidationException);
            }
            catch (ControlEventDependencyException controlEventDependencyException)
               when (controlEventDependencyException.InnerException is LockedControlEventException)
            {
                string innerMessage = GetInnerMessage(controlEventDependencyException);

                return Locked(innerMessage);
            }
            catch (ControlEventDependencyException controlEventDependencyException)
            {
                return Problem(controlEventDependencyException.Message);
            }
            catch (ControlEventServiceException controlEventServiceException)
            {
                return Problem(controlEventServiceException.Message);
            }
        }

        [Route("GetAllControlEvents")]
        [HttpGet]
        public ActionResult<IQueryable<ControlEvent>> GetAllControlEvents()
        {
            try
            {
                IQueryable<ControlEvent> storageControlEvents =
                    this.controlEventService.RetrieveAllControlEvents();

                return Ok(storageControlEvents);
            }
            catch (ControlEventDependencyException controlEventDependencyException)
            {
                return Problem(controlEventDependencyException.Message);
            }
            catch (ControlEventServiceException controlEventServiceException)
            {
                return Problem(controlEventServiceException.Message);
            }
        }

        private static string GetInnerMessage(Exception exception) =>
            exception.InnerException.Message;

        private static string ToLowerCase(string str) =>
            new CultureInfo("en-US", false).TextInfo.ToLower(str);
    }
}
