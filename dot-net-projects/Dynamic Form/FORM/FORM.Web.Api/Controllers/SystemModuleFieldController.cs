﻿using FORM.Web.Api.Models.SystemModuleFields;
using FORM.Web.Api.Models.SystemModuleFields.Dtos;
using FORM.Web.Api.Models.SystemModuleFields.Exceptions;
using FORM.Web.Api.Services.Foundations.SystemModuleFields;
using Microsoft.AspNetCore.Mvc;
using RESTFulSense.Controllers;
using System.Globalization;

namespace FORM.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SystemModuleFieldController : RESTFulController
    {
        private readonly ISystemModuleFieldService systemModuleFieldService;

        public SystemModuleFieldController(ISystemModuleFieldService systemModuleFieldService) =>
            this.systemModuleFieldService = systemModuleFieldService;

        [Route("PostSystemModuleFieldAsync")]
        [HttpPost]
        public async ValueTask<ActionResult<SystemModuleField>> PostSystemModuleFieldAsync(
            CreateSystemModuleFieldDto systemModuleFieldDto, int userId)
        {
            try
            {
                var systemModuleField = new SystemModuleField
                {
                    SystemModuleId = systemModuleFieldDto.SystemModuleId,
                    FieldTitle = ToTitleCase(systemModuleFieldDto.FieldTitle),
                    ControlMasterId = systemModuleFieldDto.ControlMasterId,
                    ControlEventId = systemModuleFieldDto.ControlEventId,
                    IsVisible = systemModuleFieldDto.IsVisible,
                    Position = systemModuleFieldDto.Position,
                    Placeholder = (systemModuleFieldDto.Placeholder is null) ? null :
                                ToCapitalizeFirstLetterAndRestLowerCase(systemModuleFieldDto.Placeholder),
                    ParentControlId = systemModuleFieldDto.ParentControlId,
                    IsRequired = systemModuleFieldDto.IsRequired ?? false,
                    IsActive = true,
                    IsDeleted = false
                };
                var storageSystemModuleField =
                    await this.systemModuleFieldService.CreateSystemModuleFieldAsync(systemModuleField, userId);

                return Created(storageSystemModuleField);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
                when (systemModuleFieldValidationException.InnerException is NotFoundSystemModuleFieldException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return NotFound(innerMessage);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return BadRequest(systemModuleFieldValidationException);
            }
            catch (SystemModuleFieldDependencyException systemModuleFieldDependencyException)
            {
                return Problem(systemModuleFieldDependencyException.Message);
            }
            catch (SystemModuleFieldServiceException systemModuleFieldServiceException)
            {
                return Problem(systemModuleFieldServiceException.Message);
            }
        }

        [Route("GetSystemModuleFieldAsync/{systemModuleFieldId:int}")]
        [HttpGet]
        public async ValueTask<ActionResult<SystemModuleField>> GetSystemModuleFieldAsync(
            int systemModuleFieldId)
        {
            try
            {
                var storageSystemModuleField =
                    await this.systemModuleFieldService.RetrieveSystemModuleFieldByIdAsync(systemModuleFieldId);

                return Ok(storageSystemModuleField);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
                when (systemModuleFieldValidationException.InnerException is NotFoundSystemModuleFieldException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return NotFound(innerMessage);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return BadRequest(systemModuleFieldValidationException);
            }
            catch (SystemModuleFieldDependencyException systemModuleFieldDependencyException)
            {
                return Problem(systemModuleFieldDependencyException.Message);
            }
            catch (SystemModuleFieldServiceException systemModuleFieldServiceException)
            {
                return Problem(systemModuleFieldServiceException.Message);
            }
        }

        [Route("GetSystemModuleFieldAsync/{systemModuleId:int}/{fieldTitle}")]
        [HttpGet]
        public async ValueTask<ActionResult<SystemModuleField>> GetSystemModuleFieldAsync(
            int systemModuleFieldId, string fieldTitle)
        {
            try
            {
                var storageSystemModuleField =
                    await this.systemModuleFieldService.RetrieveSystemModuleFieldByNameAndSystemModuleIdAsync(
                        systemModuleFieldId, ToTitleCase(fieldTitle));

                return Ok(storageSystemModuleField);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
                when (
                systemModuleFieldValidationException.InnerException is NotFoundSystemModuleFieldException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return NotFound(innerMessage);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return BadRequest(systemModuleFieldValidationException);
            }
            catch (SystemModuleFieldDependencyException systemModuleFieldDependencyException)
            {
                return Problem(systemModuleFieldDependencyException.Message);
            }
            catch (SystemModuleFieldServiceException systemModuleFieldServiceException)
            {
                return Problem(systemModuleFieldServiceException.Message);
            }
        }

        [Route("GetAllSystemModuleFields/{systemModuleId}")]
        [HttpGet]
        public async ValueTask<ActionResult<IQueryable<SystemModuleField>>> GetAllSystemModuleFields(
            int systemModuleId)
        {
            try
            {
                IQueryable<SystemModuleField> storageSystemModuleFields =
                    await this.systemModuleFieldService.RetrieveSystemModuleFieldsBySystemModuleIdAsync(systemModuleId);

                return Ok(storageSystemModuleFields);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
                when (systemModuleFieldValidationException.InnerException is InvalidSystemModuleFieldException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return Problem(innerMessage);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
                when (
                systemModuleFieldValidationException.InnerException is NotFoundSystemModuleFieldsUnderSystemModuleIdException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return NotFound(innerMessage);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return Problem(innerMessage);
            }
            catch (SystemModuleFieldDependencyException systemModuleFieldDependencyException)
            {
                return Problem(systemModuleFieldDependencyException.Message);
            }
            catch (SystemModuleFieldServiceException systemModuleFieldServiceException)
            {
                return Problem(systemModuleFieldServiceException.Message);
            }
        }

        [Route("PutSystemModuleFieldAsync")]
        [HttpPut]
        public async ValueTask<ActionResult<SystemModuleField>> PutSystemModuleFieldAsync(
            EditSystemModuleFieldDto systemModuleFieldDto, int userId)
        {
            try
            {
                var systemModule = new SystemModuleField
                {
                    SystemModuleFieldId = systemModuleFieldDto.Id,
                    SystemModuleId = systemModuleFieldDto.SystemModuleId,
                    FieldTitle = ToTitleCase(systemModuleFieldDto.FieldTitle),
                    ControlMasterId = systemModuleFieldDto.ControlMasterId,
                    ControlEventId = systemModuleFieldDto.ControlEventId,
                    IsVisible = systemModuleFieldDto.IsVisible,
                    Position = systemModuleFieldDto.Position,
                    Placeholder = (systemModuleFieldDto.Placeholder is null) ? null :
                                ToCapitalizeFirstLetterAndRestLowerCase(systemModuleFieldDto.Placeholder),
                    ParentControlId = systemModuleFieldDto.ParentControlId,
                    IsRequired = systemModuleFieldDto.IsRequired,
                    IsActive = systemModuleFieldDto.IsActive ?? true,
                    IsDeleted = (systemModuleFieldDto.IsActive is true or null) ? false : true
                };
                var storageSystemModuleField =
                    await this.systemModuleFieldService.ModifySystemModuleFieldAsync(systemModule, userId);

                return Ok(storageSystemModuleField);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
                when (systemModuleFieldValidationException.InnerException is NotFoundSystemModuleFieldException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return NotFound(innerMessage);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
                when (systemModuleFieldValidationException.InnerException is UnchangedSystemModuleFieldException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return Problem(innerMessage);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
                when (systemModuleFieldValidationException.InnerException is CannotChangeSystemModuleFieldException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return Problem(innerMessage);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return BadRequest(innerMessage);
            }
            catch (SystemModuleFieldDependencyException systemModuleFieldDependencyException)
                when (systemModuleFieldDependencyException.InnerException is LockedSystemModuleFieldException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldDependencyException);

                return Locked(innerMessage);
            }
            catch (SystemModuleFieldDependencyException systemModuleFieldDependencyException)
            {
                return Problem(systemModuleFieldDependencyException.Message);
            }
            catch (SystemModuleFieldServiceException systemModuleFieldServiceException)
            {
                return Problem(systemModuleFieldServiceException.Message);
            }
        }

        [Route("DeleteSystemModuleFieldAsync")]
        [HttpDelete]
        public async ValueTask<ActionResult<SystemModuleField>> DeleteSystemModuleFieldAsync(
            int systemModuleFieldId, int userId)
        {
            try
            {
                var storageSystemModuleField =
                    await this.systemModuleFieldService.RemoveSystemModuleFieldByIdAsync(
                        systemModuleFieldId, userId);

                return Ok(storageSystemModuleField);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
                when (systemModuleFieldValidationException.InnerException is NotFoundSystemModuleFieldException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return NotFound(innerMessage);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
                when (systemModuleFieldValidationException.InnerException is InvalidSystemModuleFieldException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return Problem(innerMessage);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return BadRequest(innerMessage);
            }
            catch (SystemModuleFieldDependencyException systemModuleFieldDependencyException)
               when (systemModuleFieldDependencyException.InnerException is LockedSystemModuleFieldException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldDependencyException);

                return Locked(innerMessage);
            }
            catch (SystemModuleFieldDependencyException systemModuleFieldDependencyException)
            {
                return Problem(systemModuleFieldDependencyException.Message);
            }
            catch (SystemModuleFieldServiceException systemModuleFieldServiceException)
            {
                return Problem(systemModuleFieldServiceException.Message);
            }
        }

        [Route("UndoDeleteSystemModuleFieldAsync")]
        [HttpPut]
        public async ValueTask<ActionResult<SystemModuleField>> UndoDeleteSystemModuleAsync(
            int systemModuleFieldId, int userId)
        {
            try
            {
                var storageSystemModuleField =
                    await this.systemModuleFieldService.PutBackSystemModuleFieldByIdAsync(
                        systemModuleFieldId, userId);

                return Ok(storageSystemModuleField);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
                when (systemModuleFieldValidationException.InnerException is InvalidSystemModuleFieldException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return Problem(innerMessage);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
                when (systemModuleFieldValidationException.InnerException is NotFoundSystemModuleFieldException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return NotFound(innerMessage);
            }
            catch (SystemModuleFieldValidationException systemModuleFieldValidationException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldValidationException);

                return BadRequest(systemModuleFieldValidationException);
            }
            catch (SystemModuleFieldDependencyException systemModuleFieldDependencyException)
               when (systemModuleFieldDependencyException.InnerException is LockedSystemModuleFieldException)
            {
                string innerMessage = GetInnerMessage(systemModuleFieldDependencyException);

                return Locked(innerMessage);
            }
            catch (SystemModuleFieldDependencyException systemModuleFieldDependencyException)
            {
                return Problem(systemModuleFieldDependencyException.Message);
            }
            catch (SystemModuleFieldServiceException systemModuleFieldServiceException)
            {
                return Problem(systemModuleFieldServiceException.Message);
            }
        }

        [Route("GetAllSystemModuleFields")]
        [HttpGet]
        public ActionResult<IQueryable<SystemModuleField>> GetAllSystemModuleFields()
        {
            try
            {
                IQueryable<SystemModuleField> storageSystemModuleFields =
                    this.systemModuleFieldService.RetrieveAllSystemModuleFields();

                return Ok(storageSystemModuleFields);
            }
            catch (SystemModuleFieldDependencyException systemModuleFieldDependencyException)
            {
                return Problem(systemModuleFieldDependencyException.Message);
            }
            catch (SystemModuleFieldServiceException systemModuleFieldServiceException)
            {
                return Problem(systemModuleFieldServiceException.Message);
            }
        }

        private static string GetInnerMessage(Exception exception) =>
            exception.InnerException.Message;

        private static string ToTitleCase(string str) =>
            new CultureInfo("en-US", false).TextInfo.ToTitleCase(str);

        private static string ToCapitalizeFirstLetterAndRestLowerCase(string str) =>
            str.Substring(0, 1).ToUpper() + str.Substring(1).ToLower();
    }
}
