﻿using FORM.Web.Api.Models.RegexValidations;
using FORM.Web.Api.Models.RegexValidations.Dtos;
using FORM.Web.Api.Models.RegexValidations.Exceptions;
using FORM.Web.Api.Services.Foundations.RegexValidations;
using Microsoft.AspNetCore.Mvc;
using RESTFulSense.Controllers;
using System.Globalization;

namespace FORM.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegexValidationController : RESTFulController
    {
        private readonly IRegexValidationService regexValidationService;

        public RegexValidationController(IRegexValidationService regexValidationService) =>
            this.regexValidationService = regexValidationService;

        [Route("PostRegexValidationAsync")]
        [HttpPost]
        public async ValueTask<ActionResult<RegexValidation>> PostRegexValidationAsync(
            CreateRegexValidationDto regexValidationDto, int userId)
        {
            try
            {
                var regexValidation = new RegexValidation
                {
                    RegexFor = ToTitleCase(regexValidationDto.RegexFor),
                    Pattern = regexValidationDto.Pattern,
                    ErrorMessage = regexValidationDto.ErrorMessage,
                    IsValid = regexValidationDto.IsValid,
                    IsActive = regexValidationDto.IsActive ?? true,
                    IsDeleted = (regexValidationDto.IsActive is true or null) ? false : true
                };
                var storageRegexValidation =
                    await this.regexValidationService.CreateRegexValidationAsync(regexValidation, userId);
                return Created(storageRegexValidation);
            }
            catch (RegexValidationValidationException regexValidationValidationException)
                when (regexValidationValidationException.InnerException is AlreadyExistsRegexValidationException)
            {
                return Conflict(regexValidationValidationException.InnerException);
            }
            catch (RegexValidationValidationException regexValidationValidationException)
            {
                return BadRequest(regexValidationValidationException.InnerException);
            }
            catch (RegexValidationDependencyException regexValidationDependencyException)
            {
                return InternalServerError(regexValidationDependencyException);
            }
            catch (RegexValidationServiceException regexValidationServiceException)
            {
                return InternalServerError(regexValidationServiceException);
            }
        }

        [Route("GetRegexValidationAsync/{regexValidationId:int}")]
        [HttpGet]
        public async ValueTask<ActionResult<RegexValidation>> GetRegexValidationAsync(int regexValidationId)
        {
            try
            {
                var storageRegexValidation =
                    await this.regexValidationService.RetrieveRegexValidationByIdAsync(regexValidationId);

                return Ok(storageRegexValidation);
            }
            catch (RegexValidationValidationException regexValidationValidationException)
                when (regexValidationValidationException.InnerException is NotFoundRegexValidationException)
            {
                string innerMessage = GetInnerMessage(regexValidationValidationException);

                return NotFound(innerMessage);
            }
            catch (RegexValidationValidationException regexValidationValidationException)
            {
                string innerMessage = GetInnerMessage(regexValidationValidationException);

                return BadRequest(regexValidationValidationException);
            }
            catch (RegexValidationDependencyException regexValidationDependencyException)
            {
                return Problem(regexValidationDependencyException.Message);
            }
            catch (RegexValidationServiceException regexValidationServiceException)
            {
                return Problem(regexValidationServiceException.Message);
            }
        }

        [Route("GetRegexValidationAsync/{regexFor}")]
        [HttpGet]
        public async ValueTask<ActionResult<RegexValidation>> GetRegexValidationAsync(string regexFor)
        {
            try
            {
                var storageRegexValidation =
                    await this.regexValidationService.RetrieveRegexValidationByNameAsync(ToTitleCase(
                        regexFor));

                return Ok(storageRegexValidation);
            }
            catch (RegexValidationValidationException regexValidationValidationException)
                when (regexValidationValidationException.InnerException is NotFoundRegexValidationException)
            {
                string innerMessage = GetInnerMessage(regexValidationValidationException);

                return NotFound(innerMessage);
            }
            catch (RegexValidationValidationException regexValidationValidationException)
            {
                string innerMessage = GetInnerMessage(regexValidationValidationException);

                return BadRequest(regexValidationValidationException);
            }
            catch (RegexValidationDependencyException regexValidationDependencyException)
            {
                return Problem(regexValidationDependencyException.Message);
            }
            catch (RegexValidationServiceException regexValidationServiceException)
            {
                return Problem(regexValidationServiceException.Message);
            }
        }

        [Route("PutRegexValidationAsync")]
        [HttpPut]
        public async ValueTask<ActionResult<RegexValidation>> PutRegexValidationAsync(
            EditRegexValidationDto regexValidationDto, int userId)
        {
            try
            {
                var regexValidation = new RegexValidation
                {
                    RegexValidationId = regexValidationDto.Id,
                    RegexFor = ToTitleCase(regexValidationDto.RegexFor),
                    Pattern = regexValidationDto.Pattern,
                    ErrorMessage = regexValidationDto.ErrorMessage.Select(x => ToTitleCase(x)).ToArray(),
                    IsValid = regexValidationDto.IsValid,
                    IsActive = regexValidationDto.IsActive ?? true,
                    IsDeleted = (regexValidationDto.IsActive is true or null) ? false : true
                };
                var storageRegexValidation =
                    await this.regexValidationService.ModifyRegexValidationAsync(regexValidation, userId);

                return Ok(storageRegexValidation);
            }
            catch (RegexValidationValidationException regexValidationValidationException)
                when (regexValidationValidationException.InnerException is NotFoundRegexValidationException)
            {
                string innerMessage = GetInnerMessage(regexValidationValidationException);

                return NotFound(innerMessage);
            }
            catch (RegexValidationValidationException regexValidationValidationException)
                when (regexValidationValidationException.InnerException is UnchangedRegexValidationException)
            {
                string innerMessage = GetInnerMessage(regexValidationValidationException);

                return Problem(innerMessage);
            }
            catch (RegexValidationValidationException regexValidationValidationException)
                when (regexValidationValidationException.InnerException is CannotChangeRegexValidationException)
            {
                string innerMessage = GetInnerMessage(regexValidationValidationException);

                return Problem(innerMessage);
            }
            catch (RegexValidationValidationException regexValidationValidationException)
            {
                string innerMessage = GetInnerMessage(regexValidationValidationException);

                return BadRequest(innerMessage);
            }
            catch (RegexValidationDependencyException regexValidationDependencyException)
                when (regexValidationDependencyException.InnerException is LockedRegexValidationException)
            {
                string innerMessage = GetInnerMessage(regexValidationDependencyException);

                return Locked(innerMessage);
            }
            catch (RegexValidationDependencyException regexValidationDependencyException)
            {
                return Problem(regexValidationDependencyException.Message);
            }
            catch (RegexValidationServiceException regexValidationServiceException)
            {
                return Problem(regexValidationServiceException.Message);
            }
        }

        [Route("DeleteRegexValidationAsync")]
        [HttpDelete]
        public async ValueTask<ActionResult<RegexValidation>> DeleteRegexValidationAsync(
            int regexValidationId, int userId)
        {
            try
            {
                var storageRegexValidation =
                    await this.regexValidationService.RemoveRegexValidationByIdAsync(regexValidationId, userId);

                return Ok(storageRegexValidation);
            }
            catch (RegexValidationValidationException regexValidationValidationException)
                when (regexValidationValidationException.InnerException is NotFoundRegexValidationException)
            {
                string innerMessage = GetInnerMessage(regexValidationValidationException);

                return NotFound(innerMessage);
            }
            catch (RegexValidationValidationException regexValidationValidationException)
            {
                string innerMessage = GetInnerMessage(regexValidationValidationException);

                return BadRequest(regexValidationValidationException);
            }
            catch (RegexValidationDependencyException regexValidationDependencyException)
               when (regexValidationDependencyException.InnerException is LockedRegexValidationException)
            {
                string innerMessage = GetInnerMessage(regexValidationDependencyException);

                return Locked(innerMessage);
            }
            catch (RegexValidationDependencyException regexValidationDependencyException)
            {
                return Problem(regexValidationDependencyException.Message);
            }
            catch (RegexValidationServiceException regexValidationServiceException)
            {
                return Problem(regexValidationServiceException.Message);
            }
        }

        [Route("UndoDeleteRegexValidationAsync")]
        [HttpPut]
        public async ValueTask<ActionResult<RegexValidation>> UndoDeleteRegexValidationAsync(
            int regexValidationId, int userId)
        {
            try
            {
                var storageRegexValidation =
                    await this.regexValidationService.PutBackRegexValidationByIdAsync(regexValidationId, userId);

                return Ok(storageRegexValidation);
            }
            catch (RegexValidationValidationException regexValidationValidationException)
                when (regexValidationValidationException.InnerException is NotFoundRegexValidationException)
            {
                string innerMessage = GetInnerMessage(regexValidationValidationException);

                return NotFound(innerMessage);
            }
            catch (RegexValidationValidationException regexValidationValidationException)
            {
                string innerMessage = GetInnerMessage(regexValidationValidationException);

                return BadRequest(regexValidationValidationException);
            }
            catch (RegexValidationDependencyException regexValidationDependencyException)
               when (regexValidationDependencyException.InnerException is LockedRegexValidationException)
            {
                string innerMessage = GetInnerMessage(regexValidationDependencyException);

                return Locked(innerMessage);
            }
            catch (RegexValidationDependencyException regexValidationDependencyException)
            {
                return Problem(regexValidationDependencyException.Message);
            }
            catch (RegexValidationServiceException regexValidationServiceException)
            {
                return Problem(regexValidationServiceException.Message);
            }
        }

        [Route("GetAllRegexValidations")]
        [HttpGet]
        public ActionResult<IQueryable<RegexValidation>> GetAllRegexValidations()
        {
            try
            {
                IQueryable<RegexValidation> storageRegexValidations =
                    this.regexValidationService.RetrieveAllRegexValidations();

                return Ok(storageRegexValidations);
            }
            catch (RegexValidationDependencyException regexValidationDependencyException)
            {
                return Problem(regexValidationDependencyException.Message);
            }
            catch (RegexValidationServiceException regexValidationServiceException)
            {
                return Problem(regexValidationServiceException.Message);
            }
        }

        private static string GetInnerMessage(Exception exception) =>
            exception.InnerException.Message;

        private static string ToTitleCase(string str) =>
            new CultureInfo("en-US", false).TextInfo.ToTitleCase(str);
    }
}
