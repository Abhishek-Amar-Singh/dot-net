using FORM.Web.Api.Brokers.DateTimes;
using FORM.Web.Api.Brokers.Loggings;
using FORM.Web.Api.Brokers.Storages;
using FORM.Web.Api.Services.Foundations.ControlEvents;
using FORM.Web.Api.Services.Foundations.ControlMasters;
using FORM.Web.Api.Services.Foundations.RegexValidations;
using FORM.Web.Api.Services.Foundations.SystemModuleFields;
using FORM.Web.Api.Services.Foundations.SystemModules;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


//--Add services to the container.
builder.Services.AddLogging();

//--Add Brokers
builder.Services.AddScoped<IStorageBroker, StorageBroker>();
builder.Services.AddTransient<ILoggingBroker, LoggingBroker>();
builder.Services.AddScoped<IDateTimeBroker, DateTimeBroker>();

//--Add Services
builder.Services.AddScoped<ISystemModuleService, SystemModuleService>();
builder.Services.AddScoped<IControlMasterService, ControlMasterService>();
builder.Services.AddScoped<IRegexValidationService, RegexValidationService>();
builder.Services.AddScoped<IControlEventService, ControlEventService>();
builder.Services.AddScoped<ISystemModuleFieldService, SystemModuleFieldService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
