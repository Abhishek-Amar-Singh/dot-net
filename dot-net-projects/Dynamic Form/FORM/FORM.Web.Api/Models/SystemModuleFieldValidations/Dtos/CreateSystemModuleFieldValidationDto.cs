﻿namespace FORM.Web.Api.Models.SystemModuleFieldValidations.Dtos
{
    public class CreateSystemModuleFieldValidationDto
    {
        public int SystemModuleFieldId { get; set; }

        public int? RegexValidationId { get; set; }
    }
}
