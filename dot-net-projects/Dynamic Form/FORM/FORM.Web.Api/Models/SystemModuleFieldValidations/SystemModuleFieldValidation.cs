﻿namespace FORM.Web.Api.Models.SystemModuleFieldValidations
{
    public class SystemModuleFieldValidation
    {
        public int SystemModuleFieldValidationId { get; set; }

        public int SystemModuleFieldId { get; set; }

        public int RegexValidationId { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDeleted { get; set; }
    }
}
