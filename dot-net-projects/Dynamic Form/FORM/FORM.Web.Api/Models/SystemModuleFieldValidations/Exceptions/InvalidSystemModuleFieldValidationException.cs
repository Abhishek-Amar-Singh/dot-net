﻿using Xeptions;

namespace FORM.Web.Api.Models.SystemModuleFields.Exceptions
{
    public class InvalidSystemModuleFieldValidationException : Xeption
    {
        public InvalidSystemModuleFieldValidationException(string parameterName, object parameterValue)
            : base(message: $"Invalid system module field validation, " +
                  $"parameter name: {parameterName}, " +
                  $"parameter value: {parameterValue}.")
        { }

        public InvalidSystemModuleFieldValidationException()
            : base(message: "Invalid system module field validation. Please fix the errors and try again.") { }
    }
}
