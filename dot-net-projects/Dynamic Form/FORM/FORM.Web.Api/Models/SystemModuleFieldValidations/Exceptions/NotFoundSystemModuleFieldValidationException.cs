﻿namespace FORM.Web.Api.Models.SystemModuleFieldValidations.Exceptions
{
    public class NotFoundSystemModuleFieldValidationException : Exception
    {
        public NotFoundSystemModuleFieldValidationException()
            : base(message: $"Couldn't find system module field validation.") { }

        public NotFoundSystemModuleFieldValidationException(int systemModuleFieldValidationId)
            : base(message: $"Couldn't find system module field validation with id: {systemModuleFieldValidationId}.") { }
    }
}
