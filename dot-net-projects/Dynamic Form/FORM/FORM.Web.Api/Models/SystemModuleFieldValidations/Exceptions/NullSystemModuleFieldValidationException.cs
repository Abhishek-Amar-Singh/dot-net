﻿namespace FORM.Web.Api.Models.SystemModuleFieldValidations.Exceptions
{
    public class NullSystemModuleFieldValidationException : Exception
    {
        public NullSystemModuleFieldValidationException() : base(message: "The system module field validation is null.") { }
    }
}
