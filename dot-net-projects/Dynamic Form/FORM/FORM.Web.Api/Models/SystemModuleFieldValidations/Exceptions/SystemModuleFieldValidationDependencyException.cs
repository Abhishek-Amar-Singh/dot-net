﻿namespace FORM.Web.Api.Models.SystemModuleFieldValidations.Exceptions
{
    public class SystemModuleFieldValidationDependencyException : Exception
    {
        public SystemModuleFieldValidationDependencyException(Exception innerException)
            : base(message: "Service dependency error occurred, contact support.", innerException) 
        { }

    }
}
