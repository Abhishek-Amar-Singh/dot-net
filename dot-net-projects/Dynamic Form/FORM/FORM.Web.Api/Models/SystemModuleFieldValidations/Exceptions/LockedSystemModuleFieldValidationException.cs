﻿namespace FORM.Web.Api.Models.SystemModuleFieldValidations.Exceptions
{
    public class LockedSystemModuleFieldValidationException : Exception
    {
        public LockedSystemModuleFieldValidationException(Exception innerException)
            : base(message: "Locked system module field validation record exception, please try again later.", innerException) { }
    }
}
