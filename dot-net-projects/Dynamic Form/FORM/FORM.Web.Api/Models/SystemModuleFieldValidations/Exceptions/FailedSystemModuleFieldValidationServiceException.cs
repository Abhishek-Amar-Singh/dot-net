﻿using Xeptions;

namespace FORM.Web.Api.Models.SystemModuleFieldValidations.Exceptions
{
    public class FailedSystemModuleFieldValidationServiceException : Xeption
    {
        public FailedSystemModuleFieldValidationServiceException(Exception innerException)
           : base(message: "Failed system module field validation service error occurred, contact support.", innerException)
        { }
    }
}
