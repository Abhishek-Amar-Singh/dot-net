﻿namespace FORM.Web.Api.Models.SystemModuleFieldValidations.Exceptions
{
    public class UnchangedSystemModuleFieldValidationException : Exception
    {
        public UnchangedSystemModuleFieldValidationException(int systemModuleFieldValidationId)
            : base(message: $"System module field validation with id: {systemModuleFieldValidationId} remains unchanged.") { }
    }
}
