﻿namespace FORM.Web.Api.Models.SystemModuleFields.Exceptions
{
    public class SystemModuleFieldValidationValidationException : Exception
    {
        public SystemModuleFieldValidationValidationException(Exception innerException)
            : base(message: "Invalid input, contact support.", innerException) { }
    }
}
