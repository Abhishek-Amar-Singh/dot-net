﻿using Xeptions;

namespace FORM.Web.Api.Models.SystemModuleFieldValidations.Exceptions
{
    public class SystemModuleFieldValidationServiceException : Xeption
    {
        public SystemModuleFieldValidationServiceException(Exception innerException)
            : base(message: "Failed system module field validation service error occurred, contact support.", innerException)
        { }
     
        
    }
}
