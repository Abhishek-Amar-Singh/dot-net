﻿namespace FORM.Web.Api.Models.SystemModuleFieldValidations.Exceptions
{
    public class CannotChangeSystemModuleFieldValidationException : Exception
    {
        public CannotChangeSystemModuleFieldValidationException(dynamic model, int systemModuleFieldValidationId)
            : base(message: $"System module field validation with system module field id: '{model.systemModuleFieldId}' and system module id: '{model.SystemModuleId}' is already present on another id: {systemModuleFieldValidationId}.") { }
    }
}
