﻿using Xeptions;

namespace FORM.Web.Api.Models.SystemModuleFieldValidations.Exceptions
{
    public class FailedSystemModuleFieldValidationStorageException : Xeption
    {
        public FailedSystemModuleFieldValidationStorageException(Exception innerException)
            : base(message: "Failed post storage error occurred, contact support.", innerException)
        { }
    }
}
