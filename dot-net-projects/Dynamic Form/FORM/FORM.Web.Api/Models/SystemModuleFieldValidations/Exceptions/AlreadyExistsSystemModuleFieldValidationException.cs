﻿using Xeptions;

namespace FORM.Web.Api.Models.SystemModuleFieldValidations.Exceptions
{
    public class AlreadyExistsSystemModuleFieldValidationException : Xeption
    {
        public AlreadyExistsSystemModuleFieldValidationException()
            : base(message: "System module field vaidation is already exists.") { }

        public AlreadyExistsSystemModuleFieldValidationException(Exception innerException)
            : base(message: "System module field validation with the same id already exists.", innerException) { }
    }
}
