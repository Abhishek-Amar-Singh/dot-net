﻿using Xeptions;

namespace FORM.Web.Api.Models.SystemModuleFieldValidations.Exceptions
{
    public class SystemModuleFieldValidationDependencyValidationException : Xeption
    {
        public SystemModuleFieldValidationDependencyValidationException(Xeption innerException)
            : base(message: "System module field validation dependency validation error occurred, please try again.", innerException)
        { }
    }
}
