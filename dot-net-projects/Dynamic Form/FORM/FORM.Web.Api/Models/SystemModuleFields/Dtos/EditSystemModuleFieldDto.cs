﻿namespace FORM.Web.Api.Models.SystemModuleFields.Dtos
{
    public class EditSystemModuleFieldDto
    {
        public int Id { get; set; }

        public int SystemModuleId { get; set; }

        public string FieldTitle { get; set; } = null!;

        public int ControlMasterId { get; set; }

        public int ControlEventId { get; set; }

        public bool? IsVisible { get; set; }

        public int? Position { get; set; }

        public string? Placeholder { get; set; }

        public int? ParentControlId { get; set; }

        public bool IsRequired { get; set; }

        public bool? IsActive { get; set; }
    }
}
