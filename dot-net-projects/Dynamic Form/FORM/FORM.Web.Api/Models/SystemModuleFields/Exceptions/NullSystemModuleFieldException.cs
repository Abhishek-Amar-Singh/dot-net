﻿namespace FORM.Web.Api.Models.SystemModuleFields.Exceptions
{
    public class NullSystemModuleFieldException : Exception
    {
        public NullSystemModuleFieldException() : base(message: "The system module field is null.") { }
    }
}
