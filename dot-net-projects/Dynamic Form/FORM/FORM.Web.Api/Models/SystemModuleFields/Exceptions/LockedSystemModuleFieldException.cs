﻿namespace FORM.Web.Api.Models.SystemModuleFields.Exceptions
{
    public class LockedSystemModuleFieldException : Exception
    {
        public LockedSystemModuleFieldException(Exception innerException)
            : base(message: "Locked system module field record exception, please try again later.", innerException) { }
    }
}
