﻿namespace FORM.Web.Api.Models.SystemModuleFields.Exceptions
{
    public class SystemModuleFieldValidationException : Exception
    {
        public SystemModuleFieldValidationException(Exception innerException)
            : base(message: "Invalid input, contact support.", innerException) { }
    }
}
