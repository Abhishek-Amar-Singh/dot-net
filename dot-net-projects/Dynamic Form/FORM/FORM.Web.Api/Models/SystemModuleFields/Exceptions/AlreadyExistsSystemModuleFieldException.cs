﻿using Xeptions;

namespace FORM.Web.Api.Models.SystemModuleFields.Exceptions
{
    public class AlreadyExistsSystemModuleFieldException : Xeption
    {
        public AlreadyExistsSystemModuleFieldException()
            : base(message: "System module field is already exists.") { }

        public AlreadyExistsSystemModuleFieldException(Exception innerException)
            : base(message: "System module field with the same id already exists.", innerException) { }
    }
}
