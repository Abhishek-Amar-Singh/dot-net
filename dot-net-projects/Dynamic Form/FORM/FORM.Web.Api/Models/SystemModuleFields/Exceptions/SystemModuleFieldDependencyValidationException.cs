﻿using Xeptions;

namespace FORM.Web.Api.Models.SystemModuleFields.Exceptions
{
    public class SystemModuleFieldDependencyValidationException : Xeption
    {
        public SystemModuleFieldDependencyValidationException(Xeption innerException)
            : base(message: "System module field dependency validation error occurred, please try again.", innerException)
        { }
    }
}
