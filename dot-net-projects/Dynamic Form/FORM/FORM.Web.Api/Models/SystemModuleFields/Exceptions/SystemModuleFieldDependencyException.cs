﻿namespace FORM.Web.Api.Models.SystemModuleFields.Exceptions
{
    public class SystemModuleFieldDependencyException : Exception
    {
        public SystemModuleFieldDependencyException(Exception innerException)
            : base(message: "Service dependency error occurred, contact support.", innerException) 
        { }

    }
}
