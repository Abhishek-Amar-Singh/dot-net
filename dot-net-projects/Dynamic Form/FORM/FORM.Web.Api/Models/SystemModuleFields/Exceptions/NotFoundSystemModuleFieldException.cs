﻿namespace FORM.Web.Api.Models.SystemModuleFields.Exceptions
{
    public class NotFoundSystemModuleFieldException : Exception
    {
        public NotFoundSystemModuleFieldException()
            : base(message: $"Couldn't find system module field.") { }

        public NotFoundSystemModuleFieldException(int systemModuleFieldId)
            : base(message: $"Couldn't find system module field with id: {systemModuleFieldId}.") { }
    }
}
