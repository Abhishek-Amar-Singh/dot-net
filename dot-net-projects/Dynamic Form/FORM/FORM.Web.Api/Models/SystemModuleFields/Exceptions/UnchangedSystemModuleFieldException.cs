﻿namespace FORM.Web.Api.Models.SystemModuleFields.Exceptions
{
    public class UnchangedSystemModuleFieldException : Exception
    {
        public UnchangedSystemModuleFieldException(int systemModuleFieldId)
            : base(message: $"System module field with id: {systemModuleFieldId} remains unchanged.") { }
    }
}
