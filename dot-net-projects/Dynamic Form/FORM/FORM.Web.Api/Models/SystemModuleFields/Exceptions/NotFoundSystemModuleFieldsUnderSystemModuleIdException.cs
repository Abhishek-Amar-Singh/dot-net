﻿namespace FORM.Web.Api.Models.SystemModuleFields.Exceptions
{
    public class NotFoundSystemModuleFieldsUnderSystemModuleIdException : Exception
    {
        public NotFoundSystemModuleFieldsUnderSystemModuleIdException(int systemModuleId)
            : base(message: $"Couldn't find system module field(s) whose SystemModuleId: {systemModuleId}.") { }
    }
}
