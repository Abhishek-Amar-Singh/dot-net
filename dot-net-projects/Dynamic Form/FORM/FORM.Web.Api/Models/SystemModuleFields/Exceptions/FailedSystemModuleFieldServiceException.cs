﻿using Xeptions;

namespace FORM.Web.Api.Models.SystemModuleFields.Exceptions
{
    public class FailedSystemModuleFieldServiceException : Xeption
    {
        public FailedSystemModuleFieldServiceException(Exception innerException)
           : base(message: "Failed system module field service error occurred, contact support.", innerException)
        { }
    }
}
