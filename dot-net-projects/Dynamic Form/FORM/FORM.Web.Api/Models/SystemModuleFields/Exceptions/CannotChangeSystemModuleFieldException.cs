﻿namespace FORM.Web.Api.Models.SystemModuleFields.Exceptions
{
    public class CannotChangeSystemModuleFieldException : Exception
    {
        public CannotChangeSystemModuleFieldException(dynamic model, int systemModuleFieldId)
            : base(message: $"System module field with field title: '{model.FieldTitle}' and system module id: '{model.SystemModuleId}' is already present on another id: {systemModuleFieldId}.") { }
    }
}
