﻿using Xeptions;

namespace FORM.Web.Api.Models.SystemModuleFields.Exceptions
{
    public class InvalidSystemModuleFieldException : Xeption
    {
        public InvalidSystemModuleFieldException(string parameterName, object parameterValue)
            : base(message: $"Invalid system module field, " +
                  $"parameter name: {parameterName}, " +
                  $"parameter value: {parameterValue}.")
        { }

        public InvalidSystemModuleFieldException()
            : base(message: "Invalid system module field. Please fix the errors and try again.") { }
    }
}
