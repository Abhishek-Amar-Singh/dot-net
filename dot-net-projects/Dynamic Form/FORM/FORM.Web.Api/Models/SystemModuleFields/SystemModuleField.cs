﻿using FORM.Web.Api.Models.ControlEvents;

namespace FORM.Web.Api.Models.SystemModuleFields
{
    public class SystemModuleField
    {
        public int SystemModuleFieldId { get; set; }

        public int SystemModuleId { get; set; }

        public string FieldTitle { get; set; } = null!;

        public int ControlMasterId { get; set; }

        public int ControlEventId { get; set; }

        public bool? IsVisible { get; set; }

        public int? Position { get; set; }

        public string? Placeholder { get; set; }

        public int? ParentControlId { get; set; }

        public bool IsRequired { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDeleted { get; set; }
    }
}
