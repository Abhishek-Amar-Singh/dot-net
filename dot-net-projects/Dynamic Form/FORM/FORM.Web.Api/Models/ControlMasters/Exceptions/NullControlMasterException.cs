﻿namespace FORM.Web.Api.Models.ControlMasters.Exceptions
{
    public class NullControlMasterException : Exception
    {
        public NullControlMasterException() : base(message: "The control master is null.") { }
    }
}
