﻿using Xeptions;

namespace FORM.Web.Api.Models.ControlMasters.Exceptions
{
    public class AlreadyExistsControlMasterException : Xeption
    {
        public AlreadyExistsControlMasterException()
            : base(message: "Control master is already exists.") { }

        public AlreadyExistsControlMasterException(Exception innerException)
            : base(message: "Control master with the same id already exists.", innerException) { }
    }
}
