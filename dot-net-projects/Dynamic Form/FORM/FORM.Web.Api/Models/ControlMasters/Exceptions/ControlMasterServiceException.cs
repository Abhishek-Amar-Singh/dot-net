﻿using Xeptions;

namespace FORM.Web.Api.Models.ControlMasters.Exceptions
{
    public class ControlMasterServiceException : Xeption
    {
        public ControlMasterServiceException(Exception innerException)
            : base(message: "Failed control master service error occurred, contact support.", innerException)
        { }
     
        
    }
}
