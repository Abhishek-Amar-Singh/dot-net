﻿namespace FORM.Web.Api.Models.ControlMasters.Exceptions
{
    public class UnchangedControlMasterException : Exception
    {
        public UnchangedControlMasterException(int controlMasterId)
            : base(message: $"Control master with id: {controlMasterId} remains unchanged.") { }
    }
}
