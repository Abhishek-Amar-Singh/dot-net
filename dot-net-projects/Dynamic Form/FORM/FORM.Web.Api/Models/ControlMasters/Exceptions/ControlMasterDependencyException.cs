﻿namespace FORM.Web.Api.Models.ControlMasters.Exceptions
{
    public class ControlMasterDependencyException:Exception
    {
        public ControlMasterDependencyException(Exception innerException)
            : base(message: "Service dependency error occurred, contact support.", innerException) 
        { }

    }
}
