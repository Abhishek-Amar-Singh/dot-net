﻿using Xeptions;

namespace FORM.Web.Api.Models.ControlMasters.Exceptions
{
    public class ControlMasterDependencyValidationException : Xeption
    {
        public ControlMasterDependencyValidationException(Xeption innerException)
            : base(message: "Control master dependency validation error occurred, please try again.", innerException)
        { }
    }
}
