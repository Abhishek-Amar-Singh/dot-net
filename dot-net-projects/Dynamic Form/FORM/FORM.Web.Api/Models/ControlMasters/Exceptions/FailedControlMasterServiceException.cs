﻿using Xeptions;

namespace FORM.Web.Api.Models.ControlMasters.Exceptions
{
    public class FailedControlMasterServiceException:Xeption
    {
        public FailedControlMasterServiceException(Exception innerException)
           : base(message: "Failed control master service error occurred, contact support.", innerException)
        { }
    }
}
