﻿namespace FORM.Web.Api.Models.ControlMasters.Exceptions
{
    public class NotFoundControlMasterException : Exception
    {
        public NotFoundControlMasterException()
            : base(message: $"Couldn't find control master.") { }

        public NotFoundControlMasterException(int controlMasterId)
            : base(message: $"Couldn't find control master with id: {controlMasterId}.") { }
    }
}
