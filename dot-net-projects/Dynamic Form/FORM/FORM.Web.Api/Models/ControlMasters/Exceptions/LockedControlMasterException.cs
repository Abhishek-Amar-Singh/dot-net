﻿namespace FORM.Web.Api.Models.ControlMasters.Exceptions
{
    public class LockedControlMasterException : Exception
    {
        public LockedControlMasterException(Exception innerException)
            : base(message: "Locked control master record exception, please try again later.", innerException) { }
    }
}
