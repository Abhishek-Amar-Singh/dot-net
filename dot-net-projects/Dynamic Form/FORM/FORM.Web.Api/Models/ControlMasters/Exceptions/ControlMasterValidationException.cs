﻿namespace FORM.Web.Api.Models.ControlMasters.Exceptions
{
    public class ControlMasterValidationException : Exception
    {
        public ControlMasterValidationException(Exception innerException)
            : base(message: "Invalid input, contact support.", innerException) { }
    }
}
