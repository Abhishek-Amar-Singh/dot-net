﻿namespace FORM.Web.Api.Models.ControlMasters.Exceptions
{
    public class CannotChangeControlMasterException : Exception
    {
        public CannotChangeControlMasterException(string controlName, int controlMasterId)
            : base(message: $"Control master with name: '{controlName}' is already present on another id: {controlMasterId}.") { }
    }
}
