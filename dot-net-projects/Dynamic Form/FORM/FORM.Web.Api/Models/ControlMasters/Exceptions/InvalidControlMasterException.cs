﻿using Xeptions;

namespace FORM.Web.Api.Models.ControlMasters.Exceptions
{
    public class InvalidControlMasterException : Xeption
    {
        public InvalidControlMasterException(string parameterName, object parameterValue)
            : base(message: $"Invalid control master, " +
                  $"parameter name: {parameterName}, " +
                  $"parameter value: {parameterValue}.")
        { }

        public InvalidControlMasterException()
            : base(message: "Invalid control master. Please fix the errors and try again.") { }
    }
}
