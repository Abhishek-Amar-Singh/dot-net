﻿namespace FORM.Web.Api.Models.ControlMasters
{
    public class ControlMaster
    {
        public int ControlMasterId { get; set; }

        public string ControlName { get; set; } = null!;

        public string? BasicSyntax { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDeleted { get; set; }
    }
}
