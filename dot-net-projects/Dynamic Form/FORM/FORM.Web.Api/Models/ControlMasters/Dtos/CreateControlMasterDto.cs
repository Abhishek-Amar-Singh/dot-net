﻿namespace FORM.Web.Api.Models.ControlMasters.Dtos
{
    public class CreateControlMasterDto
    {
        public string ControlName { get; set; } = null!;
    }
}
