﻿namespace FORM.Web.Api.Models.ControlMasters.Dtos
{
    public class EditControlMasterDto
    {
        public int Id { get; set; }

        public string ControlName { get; set; } = null!;

        public string? BasicSyntax { get; set; }

        public bool? IsActive { get; set; }
    }
}
