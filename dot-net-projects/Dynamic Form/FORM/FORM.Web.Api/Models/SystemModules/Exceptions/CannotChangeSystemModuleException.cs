﻿namespace FORM.Web.Api.Models.SystemModules.Exceptions
{
    public class CannotChangeSystemModuleException : Exception
    {
        public CannotChangeSystemModuleException(string moduleName, int systemModuleId)
            : base(message: $"System module with name: '{moduleName}' is already present on another id: {systemModuleId}.") { }
    }
}
