﻿namespace FORM.Web.Api.Models.SystemModules.Exceptions
{
    public class NullSystemModuleException : Exception
    {
        public NullSystemModuleException() : base(message: "The system module is null.") { }
    }
}
