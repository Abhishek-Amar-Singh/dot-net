﻿namespace FORM.Web.Api.Models.SystemModules.Exceptions
{
    public class NotFoundSystemModuleException : Exception
    {
        public NotFoundSystemModuleException()
            : base(message: $"Couldn't find system module.") { }

        public NotFoundSystemModuleException(int systemModuleId)
            : base(message: $"Couldn't find system module with id: {systemModuleId}.") { }
    }
}
