﻿using Xeptions;

namespace FORM.Web.Api.Models.SystemModules.Exceptions
{
    public class FailedSystemModuleStorageException : Xeption
    {
        public FailedSystemModuleStorageException(Exception innerException)
            : base(message: "Failed post storage error occurred, contact support.", innerException)
        { }
    }
}
