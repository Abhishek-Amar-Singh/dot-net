﻿using Xeptions;

namespace FORM.Web.Api.Models.SystemModules.Exceptions
{
    public class SystemModuleDependencyValidationException : Xeption
    {
        public SystemModuleDependencyValidationException(Xeption innerException)
            : base(message: "System module dependency validation error occurred, please try again.", innerException)
        { }
    }
}
