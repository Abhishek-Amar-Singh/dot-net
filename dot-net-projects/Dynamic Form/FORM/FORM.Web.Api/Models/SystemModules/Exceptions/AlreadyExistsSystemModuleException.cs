﻿using Xeptions;

namespace FORM.Web.Api.Models.SystemModules.Exceptions
{
    public class AlreadyExistsSystemModuleException : Xeption
    {
        public AlreadyExistsSystemModuleException()
            : base(message: "System module is already exists.") { }

        public AlreadyExistsSystemModuleException(Exception innerException)
            : base(message: "System module with the same id already exists.", innerException) { }
    }
}
