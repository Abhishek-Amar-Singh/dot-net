﻿namespace FORM.Web.Api.Models.SystemModules.Exceptions
{
    public class SystemModuleValidationException : Exception
    {
        public SystemModuleValidationException(Exception innerException)
            : base(message: "Invalid input, contact support.", innerException) { }
    }
}
