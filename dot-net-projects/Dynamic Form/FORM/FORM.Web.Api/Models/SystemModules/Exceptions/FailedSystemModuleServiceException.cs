﻿using Xeptions;

namespace FORM.Web.Api.Models.SystemModules.Exceptions
{
    public class FailedSystemModuleServiceException:Xeption
    {
        public FailedSystemModuleServiceException(Exception innerException)
           : base(message: "Failed system module service error occurred, contact support.", innerException)
        { }
    }
}
