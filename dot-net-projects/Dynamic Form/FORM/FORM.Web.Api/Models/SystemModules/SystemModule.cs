﻿namespace FORM.Web.Api.Models.SystemModules
{
    public class SystemModule
    {
        public int SystemModuleId { get; set; }

        public string ModuleName { get; set; } = null!;

        public bool? IsActive { get; set; }

        public bool? IsDeleted { get; set; }
    }
}
