﻿namespace FORM.Web.Api.Models.SystemModules.Dtos
{
    public class CreateSystemModuleDto
    {
        public string ModuleName { get; set; } = null!;
    }
}
