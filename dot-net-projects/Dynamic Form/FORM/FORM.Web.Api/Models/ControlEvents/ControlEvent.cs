﻿namespace FORM.Web.Api.Models.ControlEvents
{
    public class ControlEvent
    {
        public int ControlEventId { get; set; }

        public string EventName { get; set; } = null!;

        public bool? IsActive { get; set; }

        public bool? IsDeleted { get; set; }
    }
}
