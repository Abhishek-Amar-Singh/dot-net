﻿namespace FORM.Web.Api.Models.ControlEvents.Exceptions
{
    public class CannotChangeControlEventException : Exception
    {
        public CannotChangeControlEventException(string eventName, int controlEventId)
            : base(message: $"Control Event with name: '{eventName}' is already present on another id: {controlEventId}.") { }
    }
}
