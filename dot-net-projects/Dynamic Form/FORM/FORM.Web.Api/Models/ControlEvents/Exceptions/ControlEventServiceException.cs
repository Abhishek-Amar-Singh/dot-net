﻿using Xeptions;

namespace FORM.Web.Api.Models.ControlEvents.Exceptions
{
    public class ControlEventServiceException : Xeption
    {
        public ControlEventServiceException(Exception innerException)
            : base(message: "Failed control event service error occurred, contact support.", innerException)
        { }
     
        
    }
}
