﻿using Xeptions;

namespace FORM.Web.Api.Models.ControlEvents.Exceptions
{
    public class FailedControlEventServiceException:Xeption
    {
        public FailedControlEventServiceException(Exception innerException)
           : base(message: "Failed control event service error occurred, contact support.", innerException)
        { }
    }
}
