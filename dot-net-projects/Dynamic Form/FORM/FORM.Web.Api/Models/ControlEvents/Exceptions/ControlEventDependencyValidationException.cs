﻿using Xeptions;

namespace FORM.Web.Api.Models.ControlEvents.Exceptions
{
    public class ControlEventDependencyValidationException : Xeption
    {
        public ControlEventDependencyValidationException(Xeption innerException)
            : base(message: "Control event dependency validation error occurred, please try again.", innerException)
        { }
    }
}
