﻿namespace FORM.Web.Api.Models.ControlEvents.Exceptions
{
    public class NullControlEventException : Exception
    {
        public NullControlEventException() : base(message: "The control event is null.") { }
    }
}
