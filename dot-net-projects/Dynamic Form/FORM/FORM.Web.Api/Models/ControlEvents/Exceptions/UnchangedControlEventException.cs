﻿namespace FORM.Web.Api.Models.ControlEvents.Exceptions
{
    public class UnchangedControlEventException : Exception
    {
        public UnchangedControlEventException(int controlEventId)
            : base(message: $"Control event with id: {controlEventId} remains unchanged.") { }
    }
}
