﻿using Xeptions;

namespace FORM.Web.Api.Models.ControlEvents.Exceptions
{
    public class InvalidControlEventException : Xeption
    {
        public InvalidControlEventException(string parameterName, object parameterValue)
            : base(message: $"Invalid control event, " +
                  $"parameter name: {parameterName}, " +
                  $"parameter value: {parameterValue}.")
        { }

        public InvalidControlEventException()
            : base(message: "Invalid control event. Please fix the errors and try again.") { }
    }
}
