﻿using Xeptions;

namespace FORM.Web.Api.Models.ControlEvents.Exceptions
{
    public class AlreadyExistsControlEventException : Xeption
    {
        public AlreadyExistsControlEventException()
            : base(message: "Control event is already exists.") { }

        public AlreadyExistsControlEventException(Exception innerException)
            : base(message: "Control event with the same id already exists.", innerException) { }
    }
}
