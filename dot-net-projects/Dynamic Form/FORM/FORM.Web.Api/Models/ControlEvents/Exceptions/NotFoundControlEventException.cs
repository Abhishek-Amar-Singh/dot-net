﻿namespace FORM.Web.Api.Models.ControlEvents.Exceptions
{
    public class NotFoundControlEventException : Exception
    {
        public NotFoundControlEventException()
            : base(message: $"Couldn't find control event.") { }

        public NotFoundControlEventException(int controlEventId)
            : base(message: $"Couldn't find control event with id: {controlEventId}.") { }
    }
}
