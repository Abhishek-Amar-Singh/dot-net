﻿namespace FORM.Web.Api.Models.ControlEvents.Exceptions
{
    public class ControlEventValidationException : Exception
    {
        public ControlEventValidationException(Exception innerException)
            : base(message: "Invalid input, contact support.", innerException) { }
    }
}
