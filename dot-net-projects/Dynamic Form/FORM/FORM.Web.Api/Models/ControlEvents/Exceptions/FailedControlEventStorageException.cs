﻿using Xeptions;

namespace FORM.Web.Api.Models.ControlEvents.Exceptions
{
    public class FailedControlEventStorageException : Xeption
    {
        public FailedControlEventStorageException(Exception innerException)
            : base(message: "Failed post storage error occurred, contact support.", innerException)
        { }
    }
}
