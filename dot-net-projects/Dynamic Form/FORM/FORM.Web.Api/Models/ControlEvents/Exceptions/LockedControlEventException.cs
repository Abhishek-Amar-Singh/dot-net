﻿namespace FORM.Web.Api.Models.ControlEvents.Exceptions
{
    public class LockedControlEventException : Exception
    {
        public LockedControlEventException(Exception innerException)
            : base(message: "Locked control event record exception, please try again later.", innerException) { }
    }
}
