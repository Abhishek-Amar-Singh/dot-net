﻿namespace FORM.Web.Api.Models.ControlEvents.Exceptions
{
    public class ControlEventDependencyException:Exception
    {
        public ControlEventDependencyException(Exception innerException)
            : base(message: "Service dependency error occurred, contact support.", innerException) 
        { }

    }
}
