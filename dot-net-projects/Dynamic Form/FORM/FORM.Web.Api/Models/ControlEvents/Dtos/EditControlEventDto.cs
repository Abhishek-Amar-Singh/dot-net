﻿namespace FORM.Web.Api.Models.ControlEvents.Dtos
{
    public class EditControlEventDto
    {
        public int Id { get; set; }

        public string EventName { get; set; } = null!;

        public bool? IsActive { get; set; }
    }
}
