﻿namespace FORM.Web.Api.Models.ControlEvents.Dtos
{
    public class CreateControlEventDto
    {
        public string EventName { get; set; } = null!;
    }
}
