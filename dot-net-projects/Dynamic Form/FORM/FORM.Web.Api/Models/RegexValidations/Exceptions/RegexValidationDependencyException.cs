﻿namespace FORM.Web.Api.Models.RegexValidations.Exceptions
{
    public class RegexValidationDependencyException : Exception
    {
        public RegexValidationDependencyException(Exception innerException)
            : base(message: "Service dependency error occurred, contact support.", innerException) 
        { }

    }
}
