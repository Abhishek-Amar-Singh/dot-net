﻿namespace FORM.Web.Api.Models.RegexValidations.Exceptions
{
    public class UnchangedRegexValidationException : Exception
    {
        public UnchangedRegexValidationException(int regexValidationId)
            : base(message: $"Regex validations with id: {regexValidationId} remains unchanged.") { }
    }
}
