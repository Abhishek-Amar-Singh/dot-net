﻿namespace FORM.Web.Api.Models.RegexValidations.Exceptions
{
    public class CannotChangeRegexValidationException : Exception
    {
        public CannotChangeRegexValidationException(string regexFor, int regexValidationId)
            : base(message: $"Regex validation with name: '{regexFor}' is already present on another id: {regexValidationId}.") { }
    }
}
