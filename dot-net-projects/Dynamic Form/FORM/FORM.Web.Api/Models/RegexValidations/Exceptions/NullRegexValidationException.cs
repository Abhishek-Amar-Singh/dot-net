﻿namespace FORM.Web.Api.Models.RegexValidations.Exceptions
{
    public class NullRegexValidationException : Exception
    {
        public NullRegexValidationException() : base(message: "The regex validation is null.") { }
    }
}
