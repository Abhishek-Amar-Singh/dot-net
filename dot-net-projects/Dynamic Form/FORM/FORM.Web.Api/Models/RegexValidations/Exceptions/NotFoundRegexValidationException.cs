﻿namespace FORM.Web.Api.Models.RegexValidations.Exceptions
{
    public class NotFoundRegexValidationException : Exception
    {
        public NotFoundRegexValidationException()
            : base(message: $"Couldn't find regex validation.") { }

        public NotFoundRegexValidationException(int regexValidationId)
            : base(message: $"Couldn't find regex validation with id: {regexValidationId}.") { }
    }
}
