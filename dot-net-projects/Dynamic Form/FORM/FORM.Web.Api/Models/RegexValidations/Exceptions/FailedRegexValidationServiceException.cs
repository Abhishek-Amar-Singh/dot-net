﻿using Xeptions;

namespace FORM.Web.Api.Models.RegexValidations.Exceptions
{
    public class FailedRegexValidationServiceException : Xeption
    {
        public FailedRegexValidationServiceException(Exception innerException)
           : base(message: "Failed regex validation service error occurred, contact support.", innerException)
        { }
    }
}
