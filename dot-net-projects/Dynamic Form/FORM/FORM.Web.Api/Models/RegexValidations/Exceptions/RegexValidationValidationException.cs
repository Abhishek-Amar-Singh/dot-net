﻿namespace FORM.Web.Api.Models.RegexValidations.Exceptions
{
    public class RegexValidationValidationException : Exception
    {
        public RegexValidationValidationException(Exception innerException)
            : base(message: "Invalid input, contact support.", innerException) { }
    }
}
