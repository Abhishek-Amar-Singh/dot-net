﻿namespace FORM.Web.Api.Models.RegexValidations.Exceptions
{
    public class LockedRegexValidationException : Exception
    {
        public LockedRegexValidationException(Exception innerException)
            : base(message: "Locked regex validation record exception, please try again later.", innerException) { }
    }
}
