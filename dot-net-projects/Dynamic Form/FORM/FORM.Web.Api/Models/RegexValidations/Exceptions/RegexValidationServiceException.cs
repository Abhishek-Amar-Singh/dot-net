﻿using Xeptions;

namespace FORM.Web.Api.Models.RegexValidations.Exceptions
{
    public class RegexValidationServiceException : Xeption
    {
        public RegexValidationServiceException(Exception innerException)
            : base(message: "Failed regex validation service error occurred, contact support.", innerException)
        { }
     
        
    }
}
