﻿using Xeptions;

namespace FORM.Web.Api.Models.RegexValidations.Exceptions
{
    public class RegexValidationDependencyValidationException : Xeption
    {
        public RegexValidationDependencyValidationException(Xeption innerException)
            : base(message: "Regex validation dependency validation error occurred, please try again.", innerException)
        { }
    }
}
