﻿using Xeptions;

namespace FORM.Web.Api.Models.RegexValidations.Exceptions
{
    public class AlreadyExistsRegexValidationException : Xeption
    {
        public AlreadyExistsRegexValidationException()
            : base(message: "Regex validation is already exists.") { }

        public AlreadyExistsRegexValidationException(Exception innerException)
            : base(message: "Regex validation with the same id already exists.", innerException) { }
    }
}
