﻿using Xeptions;

namespace FORM.Web.Api.Models.RegexValidations.Exceptions
{
    public class InvalidRegexValidationException : Xeption
    {
        public InvalidRegexValidationException(string parameterName, object parameterValue)
            : base(message: $"Invalid regex validation, " +
                  $"parameter name: {parameterName}, " +
                  $"parameter value: {parameterValue}.")
        { }

        public InvalidRegexValidationException()
            : base(message: "Invalid regex validation. Please fix the errors and try again.") { }
    }
}
