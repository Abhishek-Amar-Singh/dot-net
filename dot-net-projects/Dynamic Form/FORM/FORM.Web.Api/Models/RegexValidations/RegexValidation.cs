﻿namespace FORM.Web.Api.Models.RegexValidations
{
    public class RegexValidation
    {
        public int RegexValidationId { get; set; }

        public string RegexFor { get; set; } = null!;

        public string Pattern { get; set; } = null!;

        public string[] ErrorMessage { get; set; } = null!;

        public bool? IsValid { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDeleted { get; set; }
    }
}
