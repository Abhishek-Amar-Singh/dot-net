﻿using FORM.Web.Api.Models.ControlEvents;

namespace FORM.Web.Api.Brokers.Storages
{
    public partial interface IStorageBroker
    {
        ValueTask<ControlEvent> InsertControlEventAsync(ControlEvent controlEvent, int userId);

        ValueTask<ControlEvent> SelectControlEventByNameAsync(string eventName);

        ValueTask<ControlEvent> SelectControlEventByIdAsync(int controlEventId);

        ValueTask<ControlEvent> UpdateControlEventAsync(ControlEvent controlEvent, int userId);

        ValueTask<ControlEvent> DeleteControlEventAsync(int controlEventId, int userId);

        ValueTask<ControlEvent> UndoDeleteControlEventAsync(int controlEventId, int userId);

        IQueryable<ControlEvent> SelectAllControlEvents();
    }
}
