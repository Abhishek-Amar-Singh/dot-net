﻿using FORM.Web.Api.Models.SystemModuleFields;

namespace FORM.Web.Api.Brokers.Storages
{
    public partial interface IStorageBroker
    {
        ValueTask<SystemModuleField> InsertSystemModuleFieldAsync(
            SystemModuleField systemModuleField, int userId);

        ValueTask<SystemModuleField> SelectSystemModuleFieldByIdAsync(int systemModuleFieldId);

        ValueTask<SystemModuleField> SelectSystemModuleFieldByNameAndSystemModuleIdAsync(
            int systemModuleId, string fieldTitle);

        ValueTask<IQueryable<SystemModuleField>> SelectSystemModuleFieldsBySystemModuleIdAsync(
            int systemModuleId);

        ValueTask<SystemModuleField> UpdateSystemModuleFieldAsync(
            SystemModuleField systemModuleField, int userId);

        ValueTask<SystemModuleField> DeleteSystemModuleFieldByIdAsync(int systemModuleFieldId, int userId);

        ValueTask<SystemModuleField> UndoDeleteSystemModuleFieldByIdAsync(int systemModuleFieldId, int userId);

        IQueryable<SystemModuleField> SelectAllSystemModuleFields();
    }
}
