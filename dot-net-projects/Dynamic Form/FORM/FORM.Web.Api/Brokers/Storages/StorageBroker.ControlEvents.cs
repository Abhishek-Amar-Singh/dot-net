﻿using Dapper;
using FORM.Web.Api.Models;
using FORM.Web.Api.Models.ControlEvents;
using System.Data;

namespace FORM.Web.Api.Brokers.Storages
{
    public partial class StorageBroker
    {
        public async ValueTask<ControlEvent> InsertControlEventAsync(ControlEvent controlEvent, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_control_event(@control_event_id, @event_name, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("control_event_id", controlEvent.ControlEventId, DbType.Int32);
            parameters.Add("event_name", controlEvent.EventName, DbType.String);
            parameters.Add("is_active", controlEvent.IsActive, DbType.Boolean);
            parameters.Add("is_deleted", controlEvent.IsDeleted, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.INSERT.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<ControlEvent>(sql, parameters);
        }

        public async ValueTask<ControlEvent> SelectControlEventByIdAsync(int controlEventId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_control_event(@control_event_id, @event_name, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("control_event_id", controlEventId, DbType.Int32);
            parameters.Add("event_name", null, DbType.String);
            parameters.Add("is_active", null, DbType.Boolean);
            parameters.Add("is_deleted", null, DbType.Boolean);
            parameters.Add("user_id", null, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.RETRIEVE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<ControlEvent>(sql, parameters);
        }

        public async ValueTask<ControlEvent> SelectControlEventByNameAsync(string eventName)
        {
            var sql = "SELECT * FROM public.fn_crud_on_control_event(@control_event_id, @event_name, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("control_event_id", null, DbType.Int32);
            parameters.Add("event_name", eventName, DbType.String);
            parameters.Add("is_active", null, DbType.Boolean);
            parameters.Add("is_deleted", null, DbType.Boolean);
            parameters.Add("user_id", null, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.RETRIEVE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<ControlEvent>(sql, parameters);
        }

        public async ValueTask<ControlEvent> UpdateControlEventAsync(ControlEvent controlEvent, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_control_event(@control_event_id, @event_name, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("control_event_id", controlEvent.ControlEventId, DbType.Int32);
            parameters.Add("event_name", controlEvent.EventName, DbType.String);
            parameters.Add("is_active", controlEvent.IsActive, DbType.Boolean);
            parameters.Add("is_deleted", controlEvent.IsDeleted, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.UPDATE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<ControlEvent>(sql, parameters);
        }

        public async ValueTask<ControlEvent> DeleteControlEventAsync(int controlEventId, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_control_event(@control_event_id, @event_name, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("control_event_id", controlEventId, DbType.Int32);
            parameters.Add("event_name", null, DbType.String);
            parameters.Add("is_active", false, DbType.Boolean);
            parameters.Add("is_deleted", true, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.DELETE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<ControlEvent>(sql, parameters);
        }

        public async ValueTask<ControlEvent> UndoDeleteControlEventAsync(int controlEventId, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_control_event(@control_event_id, @event_name, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("control_event_id", controlEventId, DbType.Int32);
            parameters.Add("event_name", null, DbType.String);
            parameters.Add("is_active", true, DbType.Boolean);
            parameters.Add("is_deleted", false, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.UNDODELETE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<ControlEvent>(sql, parameters);
        }

        public IQueryable<ControlEvent> SelectAllControlEvents() =>
            SelectAll<ControlEvent>("SELECT * FROM fn_get_all_control_events();");
    }
}
