﻿using FORM.Web.Api.Models.RegexValidations;

namespace FORM.Web.Api.Brokers.Storages
{
    public partial interface IStorageBroker
    {
        ValueTask<RegexValidation> InsertRegexValidationAsync(RegexValidation regexValidation, int userId);

        ValueTask<RegexValidation> SelectRegexValidationByNameAsync(string regexFor);

        ValueTask<RegexValidation> SelectRegexValidationByIdAsync(int regexValidationId);

        ValueTask<RegexValidation> UpdateRegexValidationAsync(RegexValidation regexValidation, int userId);

        ValueTask<RegexValidation> DeleteRegexValidationAsync(int regexValidationId, int userId);

        ValueTask<RegexValidation> UndoDeleteRegexValidationAsync(int regexValidationId, int userId);

        IQueryable<RegexValidation> SelectAllRegexValidations();
    }
}
