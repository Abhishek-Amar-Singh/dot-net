﻿using Dapper;
using FORM.Web.Api.Models.ControlMasters;
using FORM.Web.Api.Models;
using FORM.Web.Api.Models.RegexValidations;
using System.Data;

namespace FORM.Web.Api.Brokers.Storages
{
    public partial class StorageBroker
    {
        public async ValueTask<RegexValidation> InsertRegexValidationAsync(RegexValidation regexValidation, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_regex_validation(@regex_validation_id, @regex_for, @pattern, @error_message, @is_valid, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("regex_validation_id", regexValidation.RegexValidationId, DbType.Int32);
            parameters.Add("regex_for", regexValidation.RegexFor, DbType.String);
            parameters.Add("pattern", regexValidation.Pattern, DbType.String);
            parameters.Add("error_message", regexValidation.ErrorMessage);
            parameters.Add("is_valid", regexValidation.IsValid, DbType.Boolean);
            parameters.Add("is_active", regexValidation.IsActive, DbType.Boolean);
            parameters.Add("is_deleted", regexValidation.IsDeleted, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.INSERT.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<RegexValidation>(sql, parameters);
        }

        public async ValueTask<RegexValidation> SelectRegexValidationByIdAsync(int regexValidationId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_regex_validation(@regex_validation_id, @regex_for, @pattern, @error_message, @is_valid, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("regex_validation_id", regexValidationId, DbType.Int32);
            parameters.Add("regex_for", null, DbType.String);
            parameters.Add("pattern", null, DbType.String);
            parameters.Add("error_message", null);
            parameters.Add("is_valid", null, DbType.Boolean);
            parameters.Add("is_active", null, DbType.Boolean);
            parameters.Add("is_deleted", null, DbType.Boolean);
            parameters.Add("user_id", null, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.RETRIEVE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<RegexValidation>(sql, parameters);
        }

        public async ValueTask<RegexValidation> SelectRegexValidationByNameAsync(string regexFor)
        {
            var sql = "SELECT * FROM public.fn_crud_on_regex_validation(@regex_validation_id, @regex_for, @pattern, @error_message, @is_valid, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("regex_validation_id", null, DbType.Int32);
            parameters.Add("regex_for", regexFor, DbType.String);
            parameters.Add("pattern", null, DbType.String);
            parameters.Add("error_message", null);
            parameters.Add("is_valid", null, DbType.Boolean);
            parameters.Add("is_active", null, DbType.Boolean);
            parameters.Add("is_deleted", null, DbType.Boolean);
            parameters.Add("user_id", null, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.RETRIEVE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<RegexValidation>(sql, parameters);
        }

        public async ValueTask<RegexValidation> UpdateRegexValidationAsync(RegexValidation regexValidation, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_regex_validation(@regex_validation_id, @regex_for, @pattern, @error_message, @is_valid, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("regex_validation_id", regexValidation.RegexValidationId, DbType.Int32);
            parameters.Add("regex_for", regexValidation.RegexFor, DbType.String);
            parameters.Add("pattern", regexValidation.Pattern, DbType.String);
            parameters.Add("error_message", regexValidation.ErrorMessage);
            parameters.Add("is_valid", regexValidation.IsValid, DbType.Boolean);
            parameters.Add("is_active", regexValidation.IsActive, DbType.Boolean);
            parameters.Add("is_deleted", regexValidation.IsDeleted, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.UPDATE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<RegexValidation>(sql, parameters);
        }

        public async ValueTask<RegexValidation> DeleteRegexValidationAsync(int regexValidationId, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_regex_validation(@regex_validation_id, @regex_for, @pattern, @error_message, @is_valid, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("regex_validation_id", regexValidationId, DbType.Int32);
            parameters.Add("regex_for", null, DbType.String);
            parameters.Add("pattern", null, DbType.String);
            parameters.Add("error_message", null);
            parameters.Add("is_valid", null, DbType.Boolean);
            parameters.Add("is_active", false, DbType.Boolean);
            parameters.Add("is_deleted", true, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.DELETE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<RegexValidation>(sql, parameters);
        }

        public async ValueTask<RegexValidation> UndoDeleteRegexValidationAsync(int regexValidationId, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_regex_validation(@regex_validation_id, @regex_for, @pattern, @error_message, @is_valid, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("regex_validation_id", regexValidationId, DbType.Int32);
            parameters.Add("regex_for", null, DbType.String);
            parameters.Add("pattern", null, DbType.String);
            parameters.Add("error_message", null);
            parameters.Add("is_valid", null, DbType.Boolean);
            parameters.Add("is_active", true, DbType.Boolean);
            parameters.Add("is_deleted", false, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.UNDODELETE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<RegexValidation>(sql, parameters);
        }

        public IQueryable<RegexValidation> SelectAllRegexValidations() =>
            SelectAll<RegexValidation>("SELECT * FROM fn_get_all_regex_validations();");
    }
}
