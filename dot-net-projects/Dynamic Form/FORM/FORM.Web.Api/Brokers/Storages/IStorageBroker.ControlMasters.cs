﻿using FORM.Web.Api.Models.ControlMasters;

namespace FORM.Web.Api.Brokers.Storages
{
    public partial interface IStorageBroker
    {
        ValueTask<ControlMaster> InsertControlMasterAsync(ControlMaster controlMaster, int userId);

        ValueTask<ControlMaster> SelectControlMasterByNameAsync(string controlName);

        ValueTask<ControlMaster> SelectControlMasterByIdAsync(int controlMasterId);

        ValueTask<ControlMaster> UpdateControlMasterAsync(ControlMaster controlMaster, int userId);

        ValueTask<ControlMaster> DeleteControlMasterAsync(int controlMasterId, int userId);

        ValueTask<ControlMaster> UndoDeleteControlMasterAsync(int controlMasterId, int userId);

        IQueryable<ControlMaster> SelectAllControlMasters();
    }
}
