﻿using Dapper;
using FORM.Web.Api.Models;
using FORM.Web.Api.Models.SystemModuleFields;
using FORM.Web.Api.Models.SystemModules;
using System.Data;

namespace FORM.Web.Api.Brokers.Storages
{
    public partial class StorageBroker
    {
        public async ValueTask<SystemModuleField> InsertSystemModuleFieldAsync(
            SystemModuleField systemModuleField, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_system_module_field(@system_module_field_id, @system_module_id, @field_title, @control_master_id, @control_event_id, @is_visible, @placeholder, @position, @parent_control_id, @is_required, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("system_module_field_id", systemModuleField.SystemModuleFieldId, DbType.Int32);
            parameters.Add("system_module_id", systemModuleField.SystemModuleId, DbType.Int32);
            parameters.Add("field_title", systemModuleField.FieldTitle, DbType.String);
            parameters.Add("control_master_id", systemModuleField.ControlMasterId, DbType.Int32);
            parameters.Add("control_event_id", systemModuleField.ControlEventId, DbType.Int32);
            parameters.Add("is_visible", systemModuleField.IsVisible, DbType.Boolean);
            parameters.Add("placeholder", systemModuleField.Placeholder, DbType.String);
            parameters.Add("position", systemModuleField.Position, DbType.Int32);
            parameters.Add("parent_control_id", systemModuleField.ParentControlId, DbType.Int32);
            parameters.Add("is_required", systemModuleField.IsRequired, DbType.Boolean);
            parameters.Add("is_active", systemModuleField.IsActive, DbType.Boolean);
            parameters.Add("is_deleted", systemModuleField.IsDeleted, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.INSERT.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<SystemModuleField>(sql, parameters);
        }

        public async ValueTask<SystemModuleField> SelectSystemModuleFieldByIdAsync(int systemModuleFieldId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_system_module_field(@system_module_field_id, @system_module_id, @field_title, @control_master_id, @control_event_id, @is_visible, @placeholder, @position, @parent_control_id, @is_required, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("system_module_field_id", systemModuleFieldId, DbType.Int32);
            parameters.Add("system_module_id", null, DbType.Int32);
            parameters.Add("field_title", null, DbType.String);
            parameters.Add("control_master_id", null, DbType.Int32);
            parameters.Add("control_event_id", null, DbType.Int32);
            parameters.Add("is_visible", null, DbType.Boolean);
            parameters.Add("placeholder", null, DbType.String);
            parameters.Add("position", null, DbType.Int32);
            parameters.Add("parent_control_id", null, DbType.Int32);
            parameters.Add("is_required", null, DbType.Boolean);
            parameters.Add("is_active", null, DbType.Boolean);
            parameters.Add("is_deleted", null, DbType.Boolean);
            parameters.Add("user_id", null, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.RETRIEVE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<SystemModuleField>(sql, parameters);
        }

        public async ValueTask<SystemModuleField> SelectSystemModuleFieldByNameAndSystemModuleIdAsync(
            int systemModuleId, string fieldTitle)
        {
            var sql = "SELECT * FROM public.fn_crud_on_system_module_field(@system_module_field_id, @system_module_id, @field_title, @control_master_id, @control_event_id, @is_visible, @placeholder, @position, @parent_control_id, @is_required, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("system_module_field_id", null, DbType.Int32);
            parameters.Add("system_module_id", systemModuleId, DbType.Int32);
            parameters.Add("field_title", fieldTitle, DbType.String);
            parameters.Add("control_master_id", null, DbType.Int32);
            parameters.Add("control_event_id", null, DbType.Int32);
            parameters.Add("is_visible", null, DbType.Boolean);
            parameters.Add("placeholder", null, DbType.String);
            parameters.Add("position", null, DbType.Int32);
            parameters.Add("parent_control_id", null, DbType.Int32);
            parameters.Add("is_required", null, DbType.Boolean);
            parameters.Add("is_active", null, DbType.Boolean);
            parameters.Add("is_deleted", null, DbType.Boolean);
            parameters.Add("user_id", null, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.RETRIEVE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<SystemModuleField>(sql, parameters);
        }

        public async ValueTask<IQueryable<SystemModuleField>> SelectSystemModuleFieldsBySystemModuleIdAsync(
            int systemModuleId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_system_module_field(@system_module_field_id, @system_module_id, @field_title, @control_master_id, @control_event_id, @is_visible, @placeholder, @position, @parent_control_id, @is_required, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("system_module_field_id", null, DbType.Int32);
            parameters.Add("system_module_id", systemModuleId, DbType.Int32);
            parameters.Add("field_title", null, DbType.String);
            parameters.Add("control_master_id", null, DbType.Int32);
            parameters.Add("control_event_id", null, DbType.Int32);
            parameters.Add("is_visible", null, DbType.Boolean);
            parameters.Add("placeholder", null, DbType.String);
            parameters.Add("position", null, DbType.Int32);
            parameters.Add("parent_control_id", null, DbType.Int32);
            parameters.Add("is_required", null, DbType.Boolean);
            parameters.Add("is_active", null, DbType.Boolean);
            parameters.Add("is_deleted", null, DbType.Boolean);
            parameters.Add("user_id", null, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.RETRIEVE.ToString(), DbType.String);

            return await DapperCrudReturnRecordsAsync<SystemModuleField>(sql, parameters);
        }

        public async ValueTask<SystemModuleField> UpdateSystemModuleFieldAsync(
            SystemModuleField systemModuleField, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_system_module_field(@system_module_field_id, @system_module_id, @field_title, @control_master_id, @control_event_id, @is_visible, @placeholder, @position, @parent_control_id, @is_required, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("system_module_field_id", systemModuleField.SystemModuleFieldId, DbType.Int32);
            parameters.Add("system_module_id", systemModuleField.SystemModuleId, DbType.Int32);
            parameters.Add("field_title", systemModuleField.FieldTitle, DbType.String);
            parameters.Add("control_master_id", systemModuleField.ControlMasterId, DbType.Int32);
            parameters.Add("control_event_id", systemModuleField.ControlEventId, DbType.Int32);
            parameters.Add("is_visible", systemModuleField.IsVisible, DbType.Boolean);
            parameters.Add("placeholder", systemModuleField.Placeholder, DbType.String);
            parameters.Add("position", systemModuleField.Position, DbType.Int32);
            parameters.Add("parent_control_id", systemModuleField.ParentControlId, DbType.Int32);
            parameters.Add("is_required", systemModuleField.IsRequired, DbType.Boolean);
            parameters.Add("is_active", systemModuleField.IsActive, DbType.Boolean);
            parameters.Add("is_deleted", systemModuleField.IsDeleted, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.UPDATE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<SystemModuleField>(sql, parameters);
        }

        public async ValueTask<SystemModuleField> DeleteSystemModuleFieldByIdAsync(
            int systemModuleFieldId, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_system_module_field(@system_module_field_id, @system_module_id, @field_title, @control_master_id, @control_event_id, @is_visible, @placeholder, @position, @parent_control_id, @is_required, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("system_module_field_id", systemModuleFieldId, DbType.Int32);
            parameters.Add("system_module_id", null, DbType.Int32);
            parameters.Add("field_title", null, DbType.String);
            parameters.Add("control_master_id", null, DbType.Int32);
            parameters.Add("control_event_id", null, DbType.Int32);
            parameters.Add("is_visible", null, DbType.Boolean);
            parameters.Add("placeholder", null, DbType.String);
            parameters.Add("position", null, DbType.Int32);
            parameters.Add("parent_control_id", null, DbType.Int32);
            parameters.Add("is_required", null, DbType.Boolean);
            parameters.Add("is_active", false, DbType.Boolean);
            parameters.Add("is_deleted", true, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.DELETE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<SystemModuleField>(sql, parameters);
        }

        public async ValueTask<SystemModuleField> UndoDeleteSystemModuleFieldByIdAsync(
            int systemModuleFieldId, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_system_module_field(@system_module_field_id, @system_module_id, @field_title, @control_master_id, @control_event_id, @is_visible, @placeholder, @position, @parent_control_id, @is_required, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("system_module_field_id", systemModuleFieldId, DbType.Int32);
            parameters.Add("system_module_id", null, DbType.Int32);
            parameters.Add("field_title", null, DbType.String);
            parameters.Add("control_master_id", null, DbType.Int32);
            parameters.Add("control_event_id", null, DbType.Int32);
            parameters.Add("is_visible", null, DbType.Boolean);
            parameters.Add("placeholder", null, DbType.String);
            parameters.Add("position", null, DbType.Int32);
            parameters.Add("parent_control_id", null, DbType.Int32);
            parameters.Add("is_required", null, DbType.Boolean);
            parameters.Add("is_active", true, DbType.Boolean);
            parameters.Add("is_deleted", false, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.UNDODELETE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<SystemModuleField>(sql, parameters);
        }

        public IQueryable<SystemModuleField> SelectAllSystemModuleFields() =>
            SelectAll<SystemModuleField>("SELECT * FROM fn_get_all_system_module_fields();");
    }
}
