﻿using Dapper;
using FORM.Web.Api.Models;
using FORM.Web.Api.Models.ControlMasters;
using System.Data;

namespace FORM.Web.Api.Brokers.Storages
{
    public partial class StorageBroker
    {
        public async ValueTask<ControlMaster> InsertControlMasterAsync(
            ControlMaster controlMaster, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_control_master(@control_master_id, @control_name, @basic_syntax, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("control_master_id", controlMaster.ControlMasterId, DbType.Int32);
            parameters.Add("control_name", controlMaster.ControlName, DbType.String);
            parameters.Add("basic_syntax", controlMaster.BasicSyntax, DbType.String);
            parameters.Add("is_active", controlMaster.IsActive, DbType.Boolean);
            parameters.Add("is_deleted", controlMaster.IsDeleted, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.INSERT.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<ControlMaster>(sql, parameters);
        }

        public async ValueTask<ControlMaster> SelectControlMasterByNameAsync(string controlName)
        {
            var sql = "SELECT * FROM public.fn_crud_on_control_master(@control_master_id, @control_name, @basic_syntax, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("control_master_id", null, DbType.Int32);
            parameters.Add("control_name", controlName, DbType.String);
            parameters.Add("basic_syntax", null, DbType.String);
            parameters.Add("is_active", null, DbType.Boolean);
            parameters.Add("is_deleted", null, DbType.Boolean);
            parameters.Add("user_id", null, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.RETRIEVE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<ControlMaster>(sql, parameters);
        }

        public async ValueTask<ControlMaster> SelectControlMasterByIdAsync(int controlMasterId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_control_master(@control_master_id, @control_name, @basic_syntax, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("control_master_id", controlMasterId, DbType.Int32);
            parameters.Add("control_name", null, DbType.String);
            parameters.Add("basic_syntax", null, DbType.String);
            parameters.Add("is_active", null, DbType.Boolean);
            parameters.Add("is_deleted", null, DbType.Boolean);
            parameters.Add("user_id", null, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.RETRIEVE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<ControlMaster>(sql, parameters);
        }

        public async ValueTask<ControlMaster> UpdateControlMasterAsync(
            ControlMaster controlMaster, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_control_master(@control_master_id, @control_name, @basic_syntax, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("control_master_id", controlMaster.ControlMasterId, DbType.Int32);
            parameters.Add("control_name", controlMaster.ControlName, DbType.String);
            parameters.Add("basic_syntax", controlMaster.BasicSyntax, DbType.String);
            parameters.Add("is_active", controlMaster.IsActive, DbType.Boolean);
            parameters.Add("is_deleted", controlMaster.IsDeleted, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.UPDATE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<ControlMaster>(sql, parameters);
        }

        public async ValueTask<ControlMaster> DeleteControlMasterAsync(
            int controlMasterId, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_control_master(@control_master_id, @control_name, @basic_syntax, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("control_master_id", controlMasterId, DbType.Int32);
            parameters.Add("control_name", null, DbType.String);
            parameters.Add("basic_syntax", null, DbType.String);
            parameters.Add("is_active", false, DbType.Boolean);
            parameters.Add("is_deleted", true, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.DELETE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<ControlMaster>(sql, parameters);
        }

        public async ValueTask<ControlMaster> UndoDeleteControlMasterAsync(
            int controlMasterId, int userId)
        {
            var sql = "SELECT * FROM public.fn_crud_on_control_master(@control_master_id, @control_name, @basic_syntax, @is_active, @is_deleted, @user_id, @TG_OP);";
            var parameters = new DynamicParameters();
            parameters.Add("control_master_id", controlMasterId, DbType.Int32);
            parameters.Add("control_name", null, DbType.String);
            parameters.Add("basic_syntax", null, DbType.String);
            parameters.Add("is_active", true, DbType.Boolean);
            parameters.Add("is_deleted", false, DbType.Boolean);
            parameters.Add("user_id", userId, DbType.Int32);
            parameters.Add("TG_OP", OPERATIONS.UNDODELETE.ToString(), DbType.String);

            return await DapperCrudReturnSingleObjectAsync<ControlMaster>(sql, parameters);
        }

        public IQueryable<ControlMaster> SelectAllControlMasters() =>
            SelectAll<ControlMaster>("SELECT * FROM fn_get_all_control_masters();");
    }
}
