﻿using Dapper;
using Npgsql;
using System.Data;

namespace FORM.Web.Api.Brokers.Storages
{
    public partial class StorageBroker : IStorageBroker
    {
        private readonly string? connectionString;

        public StorageBroker(IConfiguration configuration)
        {
            this.connectionString = configuration.GetConnectionString("localDbConnectionStr");
        }

        private async ValueTask<T> DapperCrudReturnSingleObjectAsync<T>(string sql, DynamicParameters parameters)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                await connection.OpenAsync();
                var result = await connection.QueryAsync<T>(sql, parameters, commandType: CommandType.Text);

                return result.FirstOrDefault();
            }
        }

        private async ValueTask<IQueryable<T>> DapperCrudReturnRecordsAsync<T>(string sql, DynamicParameters parameters)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                await connection.OpenAsync();
                var result = await connection.QueryAsync<T>(sql, parameters, commandType: CommandType.Text);

                return result.AsQueryable<T>();
            }
        }

        private IQueryable<T> SelectAll<T>(string sql)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                var results = connection.Query<T>(sql, commandType: CommandType.Text);

                return results.AsQueryable();
            }
        }
    }
}
