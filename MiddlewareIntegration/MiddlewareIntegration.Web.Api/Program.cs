using MiddlewareIntegration.Mail.Models;
using MiddlewareIntegration.Mail.Services;
using MiddlewareIntegration.Web.Api.Middlewares;
using Newtonsoft.Json;
using JsonStringEnumConverter = Newtonsoft.Json.Converters.StringEnumConverter;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddMvc().AddNewtonsoftJson(options =>
{
    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
    options.SerializerSettings.Converters.Add(new JsonStringEnumConverter());
    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
});

var mailConfig = new MailConfig();
builder.Configuration.GetSection("MailConfiguration").Bind(mailConfig);
builder.Services.AddSingleton(mailConfig);

builder.Services.AddScoped<IMailService, MailService>();

//--Add CORS services
builder.Services.AddCors(options =>
    options.AddPolicy("AngularCORS", policy =>
    {
        policy.AllowAnyOrigin();
        policy.AllowAnyHeader();
        policy.AllowAnyMethod();
    })
);

//builder.Services.AddAuthenticationServices(builder.Configuration);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//--CORS policy
app.UseCors("AngularCORS");

app.UseMiddleware<CustomMiddleware>();

app.UseHttpsRedirection();

app.UseAuthorization();

//app.UseAuthentication();

app.MapControllers();

app.Run();
