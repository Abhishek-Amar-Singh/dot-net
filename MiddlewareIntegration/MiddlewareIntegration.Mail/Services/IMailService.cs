﻿using MiddlewareIntegration.Mail.Models;

namespace MiddlewareIntegration.Mail.Services
{
    public interface IMailService
    {
        abstract void SendMail(Message message);
    }
}
