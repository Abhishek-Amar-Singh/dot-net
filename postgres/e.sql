SET SEARCH_PATH TO sample;

CREATE TABLE employees
(
	emp_id int primary key not null,
	first_name varchar(80) not null,
	last_name varchar(80) not null,
	joining_date date not null,
	salary decimal(10,3) not null,
	mgr_id int
);

CREATE TABLE departments
(
	dept_id int primary key not null,
	dept_name character varying not null
);

Create table emp_details
(
	id int primary key not null,
	emp_id int not null references employees(emp_id),
	dept_id int not null references departments(dept_id),
	address character varying null
);

SELECT * FROM departments;
SELECT * FROM managers;
SELECT * FROM employees;

insert into employees values(1, 'Ram', 'Sharma', '2022-10-24', 15000, 22);
insert into employees values(22, 'Shivangi', 'Singh', '2023-01-28', 16000, 0);
insert into employees values(39, 'Rohit', 'Sharma', '2019-12-09', 30000, 100);
insert into employees values(35, 'Viraj', 'Sharma', '2013-05-15', 18000, 100);
insert into employees values(100, 'Bhumi', 'Singh', '2019-12-09', 32000, 0);
insert into employees values(101, 'Prachi', 'Desai', '2019-07-30', 29000, 0);

insert into departments values(155, 'DevOps');
insert into departments values(156, 'Dot Net');
insert into departments values(157, 'Sales');

select * from employees;
select *  from emp_details;
select * from departments;

insert into emp_details values(1, 1, 156, 'Kashi');
insert into emp_details values(2, 35, 155, 'Mumbai');
insert into emp_details values(3, 39, 155, 'Dwarka');
insert into emp_details values(4, 100, 155, 'Noida');
insert into emp_details values(5, 22, 156, 'Vasai');
insert into emp_details values(6, 101, 157, 'Vasai');

create or replace procedure SP_EmployeeManagerReln
language plpgsql
as $$
declare
res refCursor;
begin
-- stored procedure body
OPEN res FOR SELECT * FROM employees;
end; $$

begin;
call SP_EmployeeManagerReln();
FETCH ALL FROM "res";
COMMIT;
end;


CREATE or REPLACE procedure get_highest_sal(s_id int)
language sql
as $$
SELECT * FROM(
SELECT emp_id, salary, DENSE_RANK() 
OVER(ORDER BY salary DESC) as HighestSalary FROM employees) as t  
WHERE HighestSalary=s_id;
end $$;

call get_highest_sal(3);


CREATE or replace PROCEDURE get_employees(var1 integer)
LANGUAGE SQL  
AS $$    
  SELECT * FROM employees LIMIT var1;  
  SELECT COUNT(emp_id) AS TotalEmployee FROM employees;
$$;

call get_employees(3);

select * FRom employees

SELECT emp_id, first_name||' '||last_name as emp_name, first_name||' '||last_name as manager_name
FROM employees where mgr_id = emp_id;


DO $$
DECLARE
	v_limit int := 3;
	v_offset int;
	sal decimal(10,3);
BEGIN
  v_offset := v_limit - 1;
  SELECT salary into sal from employees order by salary desc offset v_offset LIMIT v_limit; 
  RAISE NOTICE 'employees % highest salary is % rs',v_limit,sal;
END $$;

select * from employees order by salary desc;


create or replace procedure sp_get_nth_highest_salary
(
	inout n decimal(10,3)
)
language 'plpgsql'
as $$
declare
	sal decimal(10,3);
begin
SELECT salary into sal from employees order by salary desc offset n-1 LIMIT n;
RAISE NOTICE 'employees % highest salary is % rs', n, sal;
n := sal;
end;
$$;

CALL sp_get_nth_highest_salary(2);


CREATE OR REPLACE PROCEDURE sp_emp_mgr_relation()
language 'plpgsql'
as $$
declare
	sal decimal(10,3);
begin
SELECT salary into sal from employees order by salary desc offset n-1 LIMIT n; 
RAISE NOTICE 'employees % highest salary is % rs', n, sal;
n := sal;
end;
$$;

--2
SELECT e.first_name, e.emp_id, m.first_name as manager, e.mgr_id
FROM
    employees e, employees m
WHERE e.mgr_id = m.emp_id;

--2
SELECT
    e.first_name || ' ' || e.last_name employee, e.emp_id,
    m.first_name || ' ' || m.last_name manager, e.mgr_id
FROM
    employees e
FULL OUTER JOIN employees m ON m.emp_id = e.mgr_id
WHERE e.emp_id IS NOT NULL ORDER BY emp_id ASC;

--7
SELECT SUM(salary) as total_salary FROM employees;

--8
SELECT dept_name, SUM(salary) FROM departments
INNER JOIN emp_details using(dept_id)
INNER JOIN employees using(emp_id) group by dept_name;

--6
select * from employees where joining_date <= '2019-12-09';
select * from employees where joining_date >= '2019-12-09';
select * from employees where joining_date between '2019-12-09' and '2022-10-30';

DO $$
DECLARE
  s date := '2021-01-23';
  e date := '2022-01-23';
BEGIN
  IF s is null THEN
	--select * from employees where joining_date >= e;
	raise notice 'employees joined after a given date (%)', e;
  ELSIF e is null THEN
	--select * from employees where joining_date <= s;
	raise notice 'employees joined before a given date (%)', s;
  ELSE
  	--select * from employees where joining_date between s and e;
	raise notice 'employees joined between the given dates (% and %)', s, e;
  END IF;
END $$;


--7
SELECT * FROM employees where  salary between 10000 and 20000;

DO $$
DECLARE
  minSal decimal := 10000;
  maxSal decimal := 20000;
BEGIN
  SELECT * FROM employees where salary between minSal and maxSal;
END $$;

--3
SELECT
e.emp_id, e.first_name||' '||e.last_name as employee, m.first_name||' '||m.last_name as manager,
e.joining_date,e.salary, ed.address, d.dept_name
FROM emp_details ed
INNER JOIN departments d using(dept_id)
INNER JOIN employees e using(emp_id)
FULL OUTER JOIN employees m ON m.emp_id = e.mgr_id
WHERE e.emp_id IS NOT NULL
ORDER BY e.emp_id ASC;

--1) Get all employees with pagination using CTE(Common Table Expression) queries
SELECT * FROM employees
OFFSET 0 LIMIT 4;

DO $$
DECLARE
  pageNumber int = 1;
  v_offset int;
  v_limit int := 4;
  a int = 1;
  b int;
  c int = 4;
BEGIN
  v_offset := v_limit * pageNumber - v_limit;
  SELECT * FROM employees where salary offset v_offset limit v_limit;
END $$;


WITH cte_example AS(SELECT * FROM employees)
SELECT * FROM cte_example;

WITH cte2 AS
(
	SELECT dept_name, AVG(salary)::DECIMAL(10,2) AS avg_sal FROM departments
	INNER JOIN emp_details USING(dept_id)
	INNER JOIN employees USING(emp_id) GROUP BY dept_name
)
SELECT MAX(avg_sal) AS max_avg_sal FROM cte2;


CREATE OR REPLACE FUNCTION FN_GetEmployeesWithPaginationCTE
(
	page_number integer,
	page_size integer,
	filterByWhichColumn text,
	filterByColumValue text,
	isAscending boolean
)
    RETURNS TABLE(emp_id integer, employee text, manager text, joining_date date, salary numeric) 
    LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
	v_offset int;
BEGIN
  v_offset := page_size * page_number - page_size;
  RETURN QUERY
  WITH cte_example AS
  (
	SELECT
	  e.emp_id,
	  e.first_name || ' ' || e.last_name employee,
	  m.first_name || ' ' || m.last_name manager,
	  e.joining_date, e.salary
	FROM
		employees e
	FULL OUTER JOIN employees m ON m.emp_id = e.mgr_id
	WHERE e.emp_id IS NOT NULL
	OFFSET v_offset LIMIT page_size
  )
  --SELECT * FROM cte_example;
  IF filterByWhichColumn = 'emp_id' THEN
    SELECT * FROM cte_example where emp_id = filterByColumValue;
  ELSIF filterByWhichColumn = 'employee' THEN
    IF isAscending is true THEN
  	  SELECT * FROM cte_example where employee LIKE '%'||filterByColumValue||'%' ORDER BY employee ASC;
	ELSE
  	  SELECT * FROM cte_example where employee LIKE '%'||filterByColumValue||'%' ORDER BY employee DESC;
	END IF;
  ELSIF(filterByWhichColumn = 'manager') THEN
    IF(isAscending is true) THEN
  	  SELECT * FROM cte_example where manager LIKE '%'||filterByColumValue||'%' ORDER BY manager ASC;
	ELSE
  	  SELECT * FROM cte_example where manager LIKE '%'||filterByColumValue||'%' ORDER BY manager DESC;
	END IF;
  ELSIF(filterByWhichColumn = 'joining_date') THEN
    IF(isAscending is true) THEN
      SELECT * FROM cte_example where joining_date = filterByColumValue order by joining_date asc;
	ELSE
	  SELECT * FROM cte_example where joining_date = filterByColumValue order by joining_date desc;
	END IF;
  ELSE
    IF(isAscending is true) THEN
      SELECT * FROM cte_example where salary = filterByColumValue order by salary asc;
	ELSE
	  SELECT * FROM cte_example where salary = filterByColumValue order by salary desc;
	END IF;
  END IF;
END;
$BODY$;


SELECT * FROM FN_GetEmployeesWithPaginationCTE(1, 3, 'employee', 'ing', true);
emp_id,employee,manager,joining_date,salary

SELECT TABLE_NAME, COLUMN_NAME
FROM INFORMATION_SCHEMA.COLUMNS
WHERE
--table_schema='sample' AND
--table_name='employees' AND
COLUMN_NAME = 'emp_id';




create or replace procedure xyz
(
)
language 'plpgsql'
as $$
begin
select * from employees;
--SELECT salary into sal from employees order by salary desc offset n-1 LIMIT n;
--RAISE NOTICE 'employees % highest salary is % rs', n, sal;
--n := sal;
end;
$$;
call xyz();