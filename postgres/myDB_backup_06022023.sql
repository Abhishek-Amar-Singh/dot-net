PGDMP         %                {            myDB    15.1    15.1 Y    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    25014    myDB    DATABASE     �   CREATE DATABASE "myDB" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_United States.1252';
    DROP DATABASE "myDB";
                postgres    false                        2615    25131    sample    SCHEMA        CREATE SCHEMA sample;
    DROP SCHEMA sample;
                postgres    false            �           0    0    SCHEMA sample    COMMENT     i   COMMENT ON SCHEMA sample IS 'Using "sample" schema for employee, manager and department related tasks.';
                   postgres    false    6                        2615    25305    sample2    SCHEMA        CREATE SCHEMA sample2;
    DROP SCHEMA sample2;
                postgres    false            �            1255    25343 Z   sp_insert_emp(integer, character varying, character varying, numeric, date, integer, text) 	   PROCEDURE     �  CREATE PROCEDURE public.sp_insert_emp(IN id integer, IN fname character varying, IN lname character varying, IN sal numeric, IN joining_date date, IN dept_id integer DEFAULT NULL::integer, IN about_me text DEFAULT NULL::text)
    LANGUAGE plpgsql
    AS $$
DECLARE
-- variable declaration
BEGIN
-- stored procedure body
INSERT INTO employee VALUES(id, fname, lname, sal, joining_date, dept_id, about_me);
END; 
$$;
 �   DROP PROCEDURE public.sp_insert_emp(IN id integer, IN fname character varying, IN lname character varying, IN sal numeric, IN joining_date date, IN dept_id integer, IN about_me text);
       public          postgres    false            �            1255    25302 2   fn_getemployeeswithpaginationcte(integer, integer)    FUNCTION     �  CREATE FUNCTION sample.fn_getemployeeswithpaginationcte(page_number integer, page_size integer) RETURNS TABLE(emp_id integer, employee text, manager text, joining_date date, salary numeric)
    LANGUAGE plpgsql
    AS $$
DECLARE
	v_offset int;
BEGIN
  v_offset := page_size * page_number - page_size;
  RETURN QUERY
  WITH cte_example AS
  (
	SELECT
	  e.emp_id,
	  e.first_name || ' ' || e.last_name employee,
	  m.first_name || ' ' || m.last_name manager,
	  e.joining_date, e.salary
	FROM
		employees e
	FULL OUTER JOIN employees m ON m.emp_id = e.mgr_id
	WHERE e.emp_id IS NOT NULL and e.last_name LIKE '%ing%'
	OFFSET v_offset LIMIT page_size
  )
  SELECT * FROM cte_example;
END;
$$;
 _   DROP FUNCTION sample.fn_getemployeeswithpaginationcte(page_number integer, page_size integer);
       sample          postgres    false    6            �            1255    25304 G   fn_getemployeeswithpaginationcte(integer, integer, text, text, boolean)    FUNCTION     �  CREATE FUNCTION sample.fn_getemployeeswithpaginationcte(page_number integer, page_size integer, filterbywhichcolumn text, filterbycolumvalue text, isascending boolean) RETURNS TABLE(emp_id integer, employee text, manager text, joining_date date, salary numeric)
    LANGUAGE plpgsql
    AS $$
DECLARE
	v_offset int;
BEGIN
  v_offset := page_size * page_number - page_size;
  RETURN QUERY
  WITH cte_example AS
  (
	SELECT
	  e.emp_id,
	  e.first_name || ' ' || e.last_name employee,
	  m.first_name || ' ' || m.last_name manager,
	  e.joining_date, e.salary
	FROM
		employees e
	FULL OUTER JOIN employees m ON m.emp_id = e.mgr_id
	WHERE e.emp_id IS NOT NULL
	OFFSET v_offset LIMIT page_size
  )
  SELECT * FROM cte_example;
  
END;
$$;
 �   DROP FUNCTION sample.fn_getemployeeswithpaginationcte(page_number integer, page_size integer, filterbywhichcolumn text, filterbycolumvalue text, isascending boolean);
       sample          postgres    false    6            �            1255    25296 "   sp_get_nth_highest_salary(numeric) 	   PROCEDURE     !  CREATE PROCEDURE sample.sp_get_nth_highest_salary(INOUT n numeric)
    LANGUAGE plpgsql
    AS $$
declare
	sal decimal(10,3);
begin
SELECT salary into sal from employees order by salary desc offset n-1 LIMIT n;
RAISE NOTICE 'employees % highest salary is % rs', n, sal;
n := sal;
end;
$$;
 B   DROP PROCEDURE sample.sp_get_nth_highest_salary(INOUT n numeric);
       sample          postgres    false    6            �            1255    25342 Z   sp_insert_emp(integer, character varying, character varying, numeric, date, integer, text) 	   PROCEDURE     �  CREATE PROCEDURE sample2.sp_insert_emp(IN id integer, IN fname character varying, IN lname character varying, IN sal numeric, IN joining_date date, IN dept_id integer DEFAULT NULL::integer, IN about_me text DEFAULT NULL::text)
    LANGUAGE plpgsql
    AS $$
DECLARE
-- variable declaration
BEGIN
-- stored procedure body
INSERT INTO employee VALUES(id, fname, lname, sal, joining_date, dept_id, about_me);
END; 
$$;
 �   DROP PROCEDURE sample2.sp_insert_emp(IN id integer, IN fname character varying, IN lname character varying, IN sal numeric, IN joining_date date, IN dept_id integer, IN about_me text);
       sample2          postgres    false    7            �            1255    25349    sp_select_emp()    FUNCTION     Q  CREATE FUNCTION sample2.sp_select_emp() RETURNS TABLE(emp_id integer, first_name character varying, last_name character varying, salary numeric, joining_date date, dept_id integer, about_me text)
    LANGUAGE plpgsql
    AS $$
DECLARE
-- variable declaration
BEGIN
-- stored procedure body
RETURN QUERY
SELECT * FROM employee;
END; 
$$;
 '   DROP FUNCTION sample2.sp_select_emp();
       sample2          postgres    false    7            �            1255    25345 %   sp_update_emp(integer, numeric, date) 	   PROCEDURE     	  CREATE PROCEDURE sample2.sp_update_emp(IN id integer, IN sal numeric, IN join_date date)
    LANGUAGE plpgsql
    AS $$
DECLARE
-- variable declaration
BEGIN
-- stored procedure body
UPDATE employee SET salary=sal, joining_date=join_date WHERE emp_id=id;
END; 
$$;
 X   DROP PROCEDURE sample2.sp_update_emp(IN id integer, IN sal numeric, IN join_date date);
       sample2          postgres    false    7            �            1259    25036 	   Employees    TABLE       CREATE TABLE public."Employees" (
    "empId" integer NOT NULL,
    "firstName" character varying(35) NOT NULL,
    "lastName" character varying(35) NOT NULL,
    "directionType" character varying(20),
    "statusCode" character varying(17),
    "referenceNumber" bigint
);
    DROP TABLE public."Employees";
       public         heap    postgres    false            �            1259    25035    Employees_empId_seq    SEQUENCE     �   CREATE SEQUENCE public."Employees_empId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public."Employees_empId_seq";
       public          postgres    false    219            �           0    0    Employees_empId_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public."Employees_empId_seq" OWNED BY public."Employees"."empId";
          public          postgres    false    218            �            1259    25053    Films    TABLE     �   CREATE TABLE public."Films" (
    "filmId" integer NOT NULL,
    title character varying NOT NULL,
    "releasedTimestamp" timestamp without time zone NOT NULL,
    "releaseYear" integer
);
    DROP TABLE public."Films";
       public         heap    postgres    false            �            1259    25052    Films_filmId_seq    SEQUENCE     �   ALTER TABLE public."Films" ALTER COLUMN "filmId" ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public."Films_filmId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    221            �            1259    25060    basket_a    TABLE     f   CREATE TABLE public.basket_a (
    a integer NOT NULL,
    fruit_a character varying(100) NOT NULL
);
    DROP TABLE public.basket_a;
       public         heap    postgres    false            �            1259    25065    basket_b    TABLE     f   CREATE TABLE public.basket_b (
    b integer NOT NULL,
    fruit_b character varying(100) NOT NULL
);
    DROP TABLE public.basket_b;
       public         heap    postgres    false            �            1259    25090 
   categories    TABLE     x   CREATE TABLE public.categories (
    category_id integer NOT NULL,
    category_name character varying(255) NOT NULL
);
    DROP TABLE public.categories;
       public         heap    postgres    false            �            1259    25089    categories_category_id_seq    SEQUENCE     �   CREATE SEQUENCE public.categories_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.categories_category_id_seq;
       public          postgres    false    226            �           0    0    categories_category_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.categories_category_id_seq OWNED BY public.categories.category_id;
          public          postgres    false    225            �            1259    25025 	   customers    TABLE     |   CREATE TABLE public.customers (
    id bigint NOT NULL,
    name character varying,
    email character varying NOT NULL
);
    DROP TABLE public.customers;
       public         heap    postgres    false            �            1259    25024    customers_id_seq    SEQUENCE     y   CREATE SEQUENCE public.customers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.customers_id_seq;
       public          postgres    false    217            �           0    0    customers_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.customers_id_seq OWNED BY public.customers.id;
          public          postgres    false    216            �            1259    25141    departments    TABLE     l   CREATE TABLE public.departments (
    dept_id integer NOT NULL,
    dept_name character varying NOT NULL
);
    DROP TABLE public.departments;
       public         heap    postgres    false            �            1259    25077 	   employee2    TABLE     �   CREATE TABLE public.employee2 (
    employee_id integer NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    manager_id integer
);
    DROP TABLE public.employee2;
       public         heap    postgres    false            �            1259    25148    managers    TABLE     �   CREATE TABLE public.managers (
    mgr_id integer NOT NULL,
    mgr_name character varying NOT NULL,
    dept_id integer NOT NULL
);
    DROP TABLE public.managers;
       public         heap    postgres    false            �            1259    25125    most_popular_films    TABLE     l   CREATE TABLE public.most_popular_films (
    title character varying NOT NULL,
    release_year smallint
);
 &   DROP TABLE public.most_popular_films;
       public         heap    postgres    false            �            1259    25109    products    TABLE     �   CREATE TABLE public.products (
    product_id integer NOT NULL,
    product_name character varying(255) NOT NULL,
    category_id integer NOT NULL
);
    DROP TABLE public.products;
       public         heap    postgres    false            �            1259    25108    products_product_id_seq    SEQUENCE     �   CREATE SEQUENCE public.products_product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.products_product_id_seq;
       public          postgres    false    228            �           0    0    products_product_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.products_product_id_seq OWNED BY public.products.product_id;
          public          postgres    false    227            �            1259    25120    top_rated_films    TABLE     i   CREATE TABLE public.top_rated_films (
    title character varying NOT NULL,
    release_year smallint
);
 #   DROP TABLE public.top_rated_films;
       public         heap    postgres    false            �            1259    25263    departments    TABLE     l   CREATE TABLE sample.departments (
    dept_id integer NOT NULL,
    dept_name character varying NOT NULL
);
    DROP TABLE sample.departments;
       sample         heap    postgres    false    6            �            1259    25270    emp_details    TABLE     �   CREATE TABLE sample.emp_details (
    id integer NOT NULL,
    emp_id integer NOT NULL,
    dept_id integer NOT NULL,
    address character varying
);
    DROP TABLE sample.emp_details;
       sample         heap    postgres    false    6            �            1259    25258 	   employees    TABLE     �   CREATE TABLE sample.employees (
    emp_id integer NOT NULL,
    first_name character varying(80) NOT NULL,
    last_name character varying(80) NOT NULL,
    joining_date date NOT NULL,
    salary numeric(10,3) NOT NULL,
    mgr_id integer
);
    DROP TABLE sample.employees;
       sample         heap    postgres    false    6            �            1259    25306 
   department    TABLE     p   CREATE TABLE sample2.department (
    dept_id integer NOT NULL,
    dept_name character varying(49) NOT NULL
);
    DROP TABLE sample2.department;
       sample2         heap    postgres    false    7            �            1259    25313    employee    TABLE       CREATE TABLE sample2.employee (
    emp_id integer NOT NULL,
    first_name character varying(35) NOT NULL,
    last_name character varying(35) NOT NULL,
    salary numeric(10,2) DEFAULT 0 NOT NULL,
    joining_date date NOT NULL,
    dept_id integer,
    about_me text
);
    DROP TABLE sample2.employee;
       sample2         heap    postgres    false    7            �           2604    25039    Employees empId    DEFAULT     x   ALTER TABLE ONLY public."Employees" ALTER COLUMN "empId" SET DEFAULT nextval('public."Employees_empId_seq"'::regclass);
 B   ALTER TABLE public."Employees" ALTER COLUMN "empId" DROP DEFAULT;
       public          postgres    false    218    219    219            �           2604    25093    categories category_id    DEFAULT     �   ALTER TABLE ONLY public.categories ALTER COLUMN category_id SET DEFAULT nextval('public.categories_category_id_seq'::regclass);
 E   ALTER TABLE public.categories ALTER COLUMN category_id DROP DEFAULT;
       public          postgres    false    225    226    226            �           2604    25028    customers id    DEFAULT     l   ALTER TABLE ONLY public.customers ALTER COLUMN id SET DEFAULT nextval('public.customers_id_seq'::regclass);
 ;   ALTER TABLE public.customers ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    216    217    217            �           2604    25112    products product_id    DEFAULT     z   ALTER TABLE ONLY public.products ALTER COLUMN product_id SET DEFAULT nextval('public.products_product_id_seq'::regclass);
 B   ALTER TABLE public.products ALTER COLUMN product_id DROP DEFAULT;
       public          postgres    false    228    227    228            p          0    25036 	   Employees 
   TABLE DATA           y   COPY public."Employees" ("empId", "firstName", "lastName", "directionType", "statusCode", "referenceNumber") FROM stdin;
    public          postgres    false    219   %p       r          0    25053    Films 
   TABLE DATA           V   COPY public."Films" ("filmId", title, "releasedTimestamp", "releaseYear") FROM stdin;
    public          postgres    false    221   �p       s          0    25060    basket_a 
   TABLE DATA           .   COPY public.basket_a (a, fruit_a) FROM stdin;
    public          postgres    false    222   �q       t          0    25065    basket_b 
   TABLE DATA           .   COPY public.basket_b (b, fruit_b) FROM stdin;
    public          postgres    false    223   �q       w          0    25090 
   categories 
   TABLE DATA           @   COPY public.categories (category_id, category_name) FROM stdin;
    public          postgres    false    226   r       n          0    25025 	   customers 
   TABLE DATA           4   COPY public.customers (id, name, email) FROM stdin;
    public          postgres    false    217   Or       |          0    25141    departments 
   TABLE DATA           9   COPY public.departments (dept_id, dept_name) FROM stdin;
    public          postgres    false    231   �r       u          0    25077 	   employee2 
   TABLE DATA           S   COPY public.employee2 (employee_id, first_name, last_name, manager_id) FROM stdin;
    public          postgres    false    224   �r       }          0    25148    managers 
   TABLE DATA           =   COPY public.managers (mgr_id, mgr_name, dept_id) FROM stdin;
    public          postgres    false    232   Xs       {          0    25125    most_popular_films 
   TABLE DATA           A   COPY public.most_popular_films (title, release_year) FROM stdin;
    public          postgres    false    230   us       y          0    25109    products 
   TABLE DATA           I   COPY public.products (product_id, product_name, category_id) FROM stdin;
    public          postgres    false    228   �s       z          0    25120    top_rated_films 
   TABLE DATA           >   COPY public.top_rated_films (title, release_year) FROM stdin;
    public          postgres    false    229   7t                 0    25263    departments 
   TABLE DATA           9   COPY sample.departments (dept_id, dept_name) FROM stdin;
    sample          postgres    false    234   �t       �          0    25270    emp_details 
   TABLE DATA           C   COPY sample.emp_details (id, emp_id, dept_id, address) FROM stdin;
    sample          postgres    false    235   �t       ~          0    25258 	   employees 
   TABLE DATA           `   COPY sample.employees (emp_id, first_name, last_name, joining_date, salary, mgr_id) FROM stdin;
    sample          postgres    false    233   Ku       �          0    25306 
   department 
   TABLE DATA           9   COPY sample2.department (dept_id, dept_name) FROM stdin;
    sample2          postgres    false    236   �v       �          0    25313    employee 
   TABLE DATA           k   COPY sample2.employee (emp_id, first_name, last_name, salary, joining_date, dept_id, about_me) FROM stdin;
    sample2          postgres    false    237   �v       �           0    0    Employees_empId_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public."Employees_empId_seq"', 8, true);
          public          postgres    false    218            �           0    0    Films_filmId_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public."Films_filmId_seq"', 1, false);
          public          postgres    false    220            �           0    0    categories_category_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.categories_category_id_seq', 3, true);
          public          postgres    false    225            �           0    0    customers_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.customers_id_seq', 6, true);
          public          postgres    false    216            �           0    0    products_product_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.products_product_id_seq', 6, true);
          public          postgres    false    227            �           2606    25041    Employees Employees_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public."Employees"
    ADD CONSTRAINT "Employees_pkey" PRIMARY KEY ("empId");
 F   ALTER TABLE ONLY public."Employees" DROP CONSTRAINT "Employees_pkey";
       public            postgres    false    219            �           2606    25059    Films Films_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public."Films"
    ADD CONSTRAINT "Films_pkey" PRIMARY KEY ("filmId");
 >   ALTER TABLE ONLY public."Films" DROP CONSTRAINT "Films_pkey";
       public            postgres    false    221            �           2606    25064    basket_a basket_a_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.basket_a
    ADD CONSTRAINT basket_a_pkey PRIMARY KEY (a);
 @   ALTER TABLE ONLY public.basket_a DROP CONSTRAINT basket_a_pkey;
       public            postgres    false    222            �           2606    25069    basket_b basket_b_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.basket_b
    ADD CONSTRAINT basket_b_pkey PRIMARY KEY (b);
 @   ALTER TABLE ONLY public.basket_b DROP CONSTRAINT basket_b_pkey;
       public            postgres    false    223            �           2606    25095    categories categories_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (category_id);
 D   ALTER TABLE ONLY public.categories DROP CONSTRAINT categories_pkey;
       public            postgres    false    226            �           2606    25032    customers customers_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.customers
    ADD CONSTRAINT customers_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.customers DROP CONSTRAINT customers_pkey;
       public            postgres    false    217            �           2606    25147    departments departments_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.departments
    ADD CONSTRAINT departments_pkey PRIMARY KEY (dept_id);
 F   ALTER TABLE ONLY public.departments DROP CONSTRAINT departments_pkey;
       public            postgres    false    231            �           2606    25083    employee2 employee2_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.employee2
    ADD CONSTRAINT employee2_pkey PRIMARY KEY (employee_id);
 B   ALTER TABLE ONLY public.employee2 DROP CONSTRAINT employee2_pkey;
       public            postgres    false    224            �           2606    25154    managers managers_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.managers
    ADD CONSTRAINT managers_pkey PRIMARY KEY (mgr_id);
 @   ALTER TABLE ONLY public.managers DROP CONSTRAINT managers_pkey;
       public            postgres    false    232            �           2606    25114    products products_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (product_id);
 @   ALTER TABLE ONLY public.products DROP CONSTRAINT products_pkey;
       public            postgres    false    228            �           2606    25034    customers unique_email 
   CONSTRAINT     R   ALTER TABLE ONLY public.customers
    ADD CONSTRAINT unique_email UNIQUE (email);
 @   ALTER TABLE ONLY public.customers DROP CONSTRAINT unique_email;
       public            postgres    false    217            �           2606    25269    departments departments_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY sample.departments
    ADD CONSTRAINT departments_pkey PRIMARY KEY (dept_id);
 F   ALTER TABLE ONLY sample.departments DROP CONSTRAINT departments_pkey;
       sample            postgres    false    234            �           2606    25276    emp_details emp_details_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY sample.emp_details
    ADD CONSTRAINT emp_details_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY sample.emp_details DROP CONSTRAINT emp_details_pkey;
       sample            postgres    false    235            �           2606    25262    employees employees_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY sample.employees
    ADD CONSTRAINT employees_pkey PRIMARY KEY (emp_id);
 B   ALTER TABLE ONLY sample.employees DROP CONSTRAINT employees_pkey;
       sample            postgres    false    233            �           2606    25312 #   department department_dept_name_key 
   CONSTRAINT     d   ALTER TABLE ONLY sample2.department
    ADD CONSTRAINT department_dept_name_key UNIQUE (dept_name);
 N   ALTER TABLE ONLY sample2.department DROP CONSTRAINT department_dept_name_key;
       sample2            postgres    false    236            �           2606    25310    department department_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY sample2.department
    ADD CONSTRAINT department_pkey PRIMARY KEY (dept_id);
 E   ALTER TABLE ONLY sample2.department DROP CONSTRAINT department_pkey;
       sample2            postgres    false    236            �           2606    25318    employee employee_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY sample2.employee
    ADD CONSTRAINT employee_pkey PRIMARY KEY (emp_id);
 A   ALTER TABLE ONLY sample2.employee DROP CONSTRAINT employee_pkey;
       sample2            postgres    false    237            �           2606    25084 #   employee2 employee2_manager_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.employee2
    ADD CONSTRAINT employee2_manager_id_fkey FOREIGN KEY (manager_id) REFERENCES public.employee2(employee_id) ON DELETE CASCADE;
 M   ALTER TABLE ONLY public.employee2 DROP CONSTRAINT employee2_manager_id_fkey;
       public          postgres    false    224    3268    224            �           2606    25155    managers managers_dept_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.managers
    ADD CONSTRAINT managers_dept_id_fkey FOREIGN KEY (dept_id) REFERENCES public.departments(dept_id);
 H   ALTER TABLE ONLY public.managers DROP CONSTRAINT managers_dept_id_fkey;
       public          postgres    false    3274    231    232            �           2606    25115 "   products products_category_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_category_id_fkey FOREIGN KEY (category_id) REFERENCES public.categories(category_id);
 L   ALTER TABLE ONLY public.products DROP CONSTRAINT products_category_id_fkey;
       public          postgres    false    3270    226    228            �           2606    25282 $   emp_details emp_details_dept_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY sample.emp_details
    ADD CONSTRAINT emp_details_dept_id_fkey FOREIGN KEY (dept_id) REFERENCES sample.departments(dept_id);
 N   ALTER TABLE ONLY sample.emp_details DROP CONSTRAINT emp_details_dept_id_fkey;
       sample          postgres    false    234    3280    235            �           2606    25277 #   emp_details emp_details_emp_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY sample.emp_details
    ADD CONSTRAINT emp_details_emp_id_fkey FOREIGN KEY (emp_id) REFERENCES sample.employees(emp_id);
 M   ALTER TABLE ONLY sample.emp_details DROP CONSTRAINT emp_details_emp_id_fkey;
       sample          postgres    false    3278    233    235            �           2606    25319    employee employee_dept_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY sample2.employee
    ADD CONSTRAINT employee_dept_id_fkey FOREIGN KEY (dept_id) REFERENCES sample2.department(dept_id);
 I   ALTER TABLE ONLY sample2.employee DROP CONSTRAINT employee_dept_id_fkey;
       sample2          postgres    false    237    3286    236            p   �   x�]��n�0���c*�k�ɝK���H��lKT������c4M�՜�f��֧<�!~{|���������5���`�'l����׹JrN�Fhl�J��.>�x��S���\��E8���O�b͉���i��YcS���JTh�����&���'W�8��Jk")kQcɥ$�`����_?OI��,@�߄޺\E      r   �   x�]�1�0�W��Ύ�;(H�4��d��8��=rD�rG��4�!Otxuh�x�xMZ�����@�ށv�AG��,J��cq�Zf�4������.U���Kt�����[:�[����*S-��"� �(�      s   0   x�3�t,(�I�2��/J�KO�2�tJ�B.N����ܤ�"�=... �C�      t   2   x�3��/J�KO�2�t,(�I�2�O,I-�M����2�HM,����� ��      w   -   x�3��M,*Q���K�2��I,(�/�2�IL�I-����� ��
e      n   O   x�3�.��,Q���,1�s3s���s��R^P�,$9cΐԬ�b��$n�����Y$��2����1z\\\ �h#�      |      x������ � �      u   }   x�%��
�@D�ُr�hR(DR�&́<8�`W��.XΛ�L�G�g�%V�2Q@�E/M�a14�zkCau�G/qc���9~1}������W[:�^��\����圹����N�� �&�      }      x������ � �      {   C   x�s�Sp�M-�LN�S�L��I�4202�
�HUp�OIK,�H-�4�47�r/J���/�K������� u�.      y   _   x�3�����K�4�2�N�-.�KWpO�I��
sz(��d��rq�p�����+�dd�e$� �L�ځc.3N�̼��T�̢T ?F��� ��e      z   K   x��HU�H,/�H��VJMI�-(����4��4�
ʺ秤%�d��̍���ҋ*|SA�L͹b���� [l         ,   x�345�tI-�/(�245�t�/Q�K-��9�sR��b���� �Z	m      �   m   x�-���0 D经1��v7cG&�36B0m�om�ݻ300��]�h�|E�Ǿ>����ƷC�#^a��qK/���6�T����N��,*�WYjzZ)+s�����      ~   A  x�u�Mk�@�ϓ��23k���
�"
�zKp7&Qb���w�6]#�����y?f�R��I[02B�s�g��9bV�_��{��f�z�$�P2���G�0�rCl0�#F�`[�Jy��P�݂�`�.u9�Z�p����Sc5@8��RZi`[����qvP��%I��o�nj��jx
�F9;�z����3��M�-l�)��
ˣ�0���|��Z�Vsq_��Y�l�K��Acw�W并H����?i�~��)�q9�A6|Og��i�_ja���;,��z�/r�|#�K�d
�I\v�?ʰ�L'��,��o��,      �   .   x�3�t�/Q�K-�2�I-.��K�2����2��J,K����� �l	�      �   �  x�m�?KA���ﲲ�g�\)Q�+�UOo����]�N�F�R���)*�E�X�%r�j�Gp�Dr�0�{���f��y݂��Ö�
!&� %�å�J���P3�Ά�8l�&�T�
�� �#���h/���J�`��ހz��uGG�h�KɥKƚG�e��������YY1��N�ߠ���19�d��^m�
,�<�����ٖ,��<ɥ��ӡ^/I̪�H������fi
>ώ��u4{c��%��z��G�$�gd�Z��A^�`�5T�o�Ye�����b�e
�ZZ�aL�錭՜�ɭwh��ўgm;t�&5@./�z ���D�������$U�Af�
(j�fI4o����緧'��ȷ��iy��?���~��'c_k���     