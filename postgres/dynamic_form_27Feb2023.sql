drop table "SystemModule";

CREATE TABLE "SystemModule"
(
	"SystemModuleId" INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	"ModuleName" VARCHAR(50) UNIQUE NOT NULL,
	"IsActive" BOOLEAN NULL,
	"IsDeleted" BOOLEAN NULL
);



SELECT * FROM "SystemModule";
SELECT * FROM "SystemModuleLog";

ALTER TABLE "SystemModule" ALTER COLUMN "SystemModuleId" ADD GENERATED ALWAYS AS IDENTITY;
ALTER TABLE "SystemModuleLog" ALTER COLUMN "SystemModuleLogId" ADD GENERATED ALWAYS AS IDENTITY;

CREATE TABLE "SystemModuleLog"
(
	"SystemModuleLogId" INT GENERATED ALWAYS AS IDENTITY,
	"SystemModuleId" INT NOT NULL REFERENCES "SystemModule"("SystemModuleId"),
	"CreatedDate" TIMESTAMP WITH TIME ZONE NULL,
	"CreatedBy" INT NULL,
	"UpdatedDate" TIMESTAMP WITH TIME ZONE NULL,
	"UpdatedBy" INT NULL,
	"DeletedDate" TIMESTAMP WITH TIME ZONE NULL,
	"DeletedBy" INT NULL,
	"ActionMessage" TEXT NULL
);

insert into "SystemModuleLog"("SystemModuleId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy",
							 "DeletedDate","DeletedBy", "ActionMessage")
values(2,NOW(), 1, null,null,null,null,'Abc');

SELECT * FROM "SystemModuleLog";

CREATE TABLE "ControlMaster"
(
	"ControlMasterId" INT  PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	"ControlName" VARCHAR(100) NOT NULL,
	"BasicSyntax" TEXT NULL,
	"IsActive" BOOLEAN,
	"IsDeleted" BOOLEAN
);
SELECT * FROM "ControlMaster";

CREATE TABLE "ControlMasterLog"
(
	"ControlMasterLogId" INT GENERATED ALWAYS AS IDENTITY,
	"ControlMasterId" INT NOT NULL REFERENCES "ControlMaster"("ControlMasterId"),
	"CreatedDate" TIMESTAMP WITH TIME ZONE NULL,
	"CreatedBy" INT NULL,
	"UpdatedDate" TIMESTAMP WITH TIME ZONE NULL,
	"UpdatedBy" INT NULL,
	"DeletedDate" TIMESTAMP WITH TIME ZONE NULL,
	"DeletedBy" INT NULL,
	"ActionMessage" TEXT NULL
);

CREATE TABLE "RegexValidation"
(
	"RegexValidationId" INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	"RegexFor" VARCHAR(100) NOT NULL,
	"Pattern" TEXT NOT NULL,
	"ErrorMessage" CHARACTER VARYING[] NOT NULL,
	"IsValid" BOOLEAN NULL,
	"IsActive" BOOLEAN NULL,
	"IsDeleted" BOOLEAN NULL
);

CREATE TABLE "RegexValidationLog"
(
	"RegexValidationLogId" INT  GENERATED ALWAYS AS IDENTITY,
	"RegexValidationId" INT NOT NULL REFERENCES "RegexValidation"("RegexValidationId"),
	"CreatedDate" TIMESTAMP WITH TIME ZONE NULL,
	"CreatedBy" INT NULL,
	"UpdatedDate" TIMESTAMP WITH TIME ZONE NULL,
	"UpdatedBy" INT NULL,
	"DeletedDate" TIMESTAMP WITH TIME ZONE NULL,
	"DeletedBy" INT NULL,
	"ActionMessage" TEXT NULL
);

CREATE TABLE "ControlEvent"
(
	"ControlEventId" INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	"EventName" VARCHAR(100) NOT NULL,
	"IsActive" BOOLEAN,
	"IsDeleted" BOOLEAN
);

CREATE TABLE "ControlEventLog"
(
	"ControlEventLogId" INT  GENERATED ALWAYS AS IDENTITY,
	"ControlEventId" INT NOT NULL REFERENCES "ControlEvent"("ControlEventId"),
	"CreatedDate" TIMESTAMP WITH TIME ZONE NULL,
	"CreatedBy" INT NULL,
	"UpdatedDate" TIMESTAMP WITH TIME ZONE NULL,
	"UpdatedBy" INT NULL,
	"DeletedDate" TIMESTAMP WITH TIME ZONE NULL,
	"DeletedBy" INT NULL,
	"ActionMessage" TEXT NULL
);

CREATE TABLE "SystemModuleField"
(
	"SystemModuleFieldId" INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	"SystemModuleId" INT NOT NULL REFERENCES "SystemModule"("SystemModuleId"),
	"FieldTitle" VARCHAR(200) NULL,
	"ControlMasterId" INT NOT NULL REFERENCES "ControlMaster"("ControlMasterId"),
	"ControlEventId" INT NOT NULL REFERENCES "ControlEvent"("ControlEventId"),
	"IsVisible" BOOLEAN,
	"Placeholder" VARCHAR(100),
	"Position" INT,
	"ParentControlId" INT REFERENCES "SystemModuleField"("SystemModuleFieldId"),
	"IsActive" BOOLEAN,
	"IsDeleted" BOOLEAN
);
ALTER TABLE "SystemModuleField" ADD COLUMN "IsRequired" BOOL;
SELECT * FROM "SystemModuleField";

CREATE TABLE "SystemModuleFieldLog"
(
	"SystemModuleFieldLogId" INT  GENERATED ALWAYS AS IDENTITY,
	"SystemModuleFieldId" INT NOT NULL REFERENCES "SystemModuleField"("SystemModuleFieldId"),
	"CreatedDate" TIMESTAMP WITH TIME ZONE NULL,
	"CreatedBy" INT NULL,
	"UpdatedDate" TIMESTAMP WITH TIME ZONE NULL,
	"UpdatedBy" INT NULL,
	"DeletedDate" TIMESTAMP WITH TIME ZONE NULL,
	"DeletedBy" INT NULL,
	"ActionMessage" TEXT NULL
);

CREATE TABLE "SystemModuleFieldValidation"
(
	"SystemModuleFieldValidationId" INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	"SystemModuleFieldId" INT NOT NULL REFERENCES "SystemModuleField"("SystemModuleFieldId"),
	"RegexValidationId" INT REFERENCES "RegexValidation"("RegexValidationId"),
	"IsActive" BOOLEAN,
	"IsDeleted" BOOLEAN
);

CREATE TABLE "SystemModuleFieldValidationLog"
(
	"SystemModuleFieldValidationLogId" INT  GENERATED ALWAYS AS IDENTITY,
	"SystemModuleFieldValidationId" INT NOT NULL REFERENCES "SystemModuleFieldValidation"("SystemModuleFieldValidationId"),
	"CreatedDate" TIMESTAMP WITH TIME ZONE NULL,
	"CreatedBy" INT NULL,
	"UpdatedDate" TIMESTAMP WITH TIME ZONE NULL,
	"UpdatedBy" INT NULL,
	"DeletedDate" TIMESTAMP WITH TIME ZONE NULL,
	"DeletedBy" INT NULL,
	"ActionMessage" TEXT NULL
);

CREATE TABLE "SystemModuleFieldEventParameter"
(
	"SystemModuleFieldEventParameterId" INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	"SystemModuleFieldId" INT NOT NULL REFERENCES "SystemModuleField"("SystemModuleFieldId"),
	"ParameterName" VARCHAR(100) NULL,
	"IsActive" BOOLEAN,
	"IsDeleted" BOOLEAN
);
ALTER TABLE "SystemModuleFieldEventParameter" ALTER COLUMN
"ParameterName" type character varying[]  USING ARRAY["ParameterName"];
SELECT * FROM "SystemModuleFieldEventParameter";

CREATE TABLE "SystemModuleFieldEventParameterLog"
(
	"SystemModuleFieldEventParameterLogId" INT  GENERATED ALWAYS AS IDENTITY,
	"SystemModuleFieldEventParameterId" INT NOT NULL REFERENCES "SystemModuleFieldEventParameter"("SystemModuleFieldEventParameterId"),
	"CreatedDate" TIMESTAMP WITH TIME ZONE NULL,
	"CreatedBy" INT NULL,
	"UpdatedDate" TIMESTAMP WITH TIME ZONE NULL,
	"UpdatedBy" INT NULL,
	"DeletedDate" TIMESTAMP WITH TIME ZONE NULL,
	"DeletedBy" INT NULL,
	"ActionMessage" TEXT NULL
);

CREATE TABLE "SystemModuleFieldOption"
(
	"SystemModuleFieldOptionId" INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	"SystemModuleFieldId" INT NOT NULL REFERENCES "SystemModuleField"("SystemModuleFieldId"),
	"OptionKey" VARCHAR(100) NOT NULL,
	"OptionValue" VARCHAR(100) NOT NULL,
	"IsDefault" BOOLEAN NOT NULL,
	"IsActive" BOOLEAN,
	"IsDeleted" BOOLEAN
);

CREATE TABLE "SystemModuleFieldOptionLog"
(
	"SystemModuleFieldOptionLogId" INT GENERATED ALWAYS AS IDENTITY,
	"SystemModuleFieldOptionId" INT NOT NULL REFERENCES "SystemModuleFieldOption"("SystemModuleFieldOptionId"),
	"CreatedDate" TIMESTAMP WITH TIME ZONE NULL,
	"CreatedBy" INT NULL,
	"UpdatedDate" TIMESTAMP WITH TIME ZONE NULL,
	"UpdatedBy" INT NULL,
	"DeletedDate" TIMESTAMP WITH TIME ZONE NULL,
	"DeletedBy" INT NULL,
	"ActionMessage" TEXT NULL
);


INSERT INTO "SystemModule"("SystemModuleId","ModuleName", "IsActive", "IsDeleted") VALUES(101, 'Inquiry', true, false);
INSERT INTO "SystemModule"("ModuleName", "IsActive", "IsDeleted") VALUES(102, 'Model-B', true, false);
INSERT INTO "SystemModule"("ModuleName", "IsActive", "IsDeleted") VALUES(103, 'Model-C', true, false);
INSERT INTO "SystemModule"("ModuleName", "IsActive", "IsDeleted"
						  -- "CreatedDate","CreadtedBy","UpdatedDate","UpdatedBy",
						   --"DeletedDate","DeletedBy"
						  ) VALUES('Model-E', true, false);
						  
update "SystemModule" SET "ModuleName"='Model-B' WHERE "SystemModuleId"=2
SELECT * FROM "SystemModule";

INSERT INTO "ControlMaster" VALUES(101, 'TextBox', '<input type="text"/>', true, false);
INSERT INTO "ControlMaster" VALUES(102, 'CheckBox', '<input type="checkbox"/>', true, false);
INSERT INTO "ControlMaster" VALUES(103, 'RadioButton', '<input type="radio"/>', true, false);
INSERT INTO "ControlMaster" VALUES(104, 'Image', '<input type="image"/>', true, false);
INSERT INTO "ControlMaster" VALUES(105, 'Date', '<input type="date"/>', true, false);
INSERT INTO "ControlMaster" VALUES(106, 'DateTime', '<input type="datetime-local"/>', true, false);
INSERT INTO "ControlMaster" VALUES(107, 'File', '<input type="file"/>', true, false);
INSERT INTO "ControlMaster" VALUES(108, 'Dropdown', null, true, false);

INSERT INTO "RegexValidation" VALUES(101, 'Required',
									 '^(?=.*\S).+$',
									 '{
									 "This field cannot be blank",
									 "Any non-empty value is valid"
									 }', 
									 null, null, null);
INSERT INTO "RegexValidation" VALUES(102, 'Email Address',
									 '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$',
									'{
									 "This is not a valid email address",
									 "Format: abhishek.singh@gmail.com"
									 }',
									 null, null, null);
									 
INSERT INTO "RegexValidation" VALUES(103, 'Mobile Number',
									 '^([\+]?91[-]?|[0])?[6-9][0-9]{9}$',
									 '{
									 "This is not a valid mobile number",
									 "Format: +917887650033"
									 }', null, null, null);
									 									 
INSERT INTO "RegexValidation" VALUES(104, 'OnlyNumbers',
									 '^[0-9]+$',
									 '{
									 "Only digits are allowed",
									 "Format: 672"
									 }',
									 null, null, null);

INSERT INTO "RegexValidation" VALUES(105, 'Name',
									 '^[A-Za-z]+(?:\s[A-Za-z]+)+$',
									 '{
									 "Invalid Name",
									 "Format: John Doe"
									 }', null, null, null);
INSERT INTO "RegexValidation" VALUES(106, 'x',
									 '^[\da-zA-Z ]$',
									 '{
									 "Invalid X",
									 "Should only contains alphabets, spaces and digits",
									 "Format: Insource44 Doe 90A"
									 }', null, null, null);
INSERT INTO "RegexValidation" VALUES(107, 'Telephone Number',
									 '^(?:\+91|0)?(?:\d{3}-)?\d{8}$',
									 '{
									 "This is not a valid telephone number",
									 "Format: +912222222222"
									 }', null, null, null);
INSERT INTO "RegexValidation" VALUES(108, 'Username',
									 '^[a-zA-Z0-9]+$',
									 '{
									 "Invalid username",
									 "Format: Abhishek49"
									 }', null, null, null);
INSERT INTO "RegexValidation" VALUES(109, 'Fax Number',
									 '^(?:\+?\d{1,3}[- ]?)?\d{3,4}[- ]?\d{4,7}$',
									 '{
									 "Invalid fax number",
									 "Format: +1-123-456-7890"
									 }', null, null, null);

INSERT INTO "ControlEvent" VALUES(101, 'onclick', true, false);
INSERT INTO "ControlEvent" VALUES(102, 'onblur', true, false);
INSERT INTO "ControlEvent" VALUES(103, 'onchange', true, false);
INSERT INTO "ControlEvent" VALUES(104, 'onfocus', true, false);
INSERT INTO "ControlEvent" VALUES(105, 'onkeyup', true, false);
INSERT INTO "ControlEvent" VALUES(106, 'onkeydown', true, false);
INSERT INTO "ControlEvent" VALUES(107, 'onselect', true, false);
INSERT INTO "ControlEvent" VALUES(108, 'custom', true, false);

INSERT INTO "SystemModuleField" VALUES(101, 101, 'Name', 101, 102, true, 'ex: John Doe', 1, null, true, false);
INSERT INTO "SystemModuleField" VALUES(102, 101, 'Company', 101, 108, true, 'ex: Microsoft', 2, null, true, false);
INSERT INTO "SystemModuleField" VALUES(103, 101, 'Inquiry Title', 101, 108, true, null, 1, null, true, false);
INSERT INTO "SystemModuleField" VALUES(104, 101, 'Inquiry Name', 101, 108, true, null, 2, null, true, false);
INSERT INTO "SystemModuleField" VALUES(105, 101, 'Phone', 101, 108, true, 'ex: +912222222222', 1, null, true, false);
INSERT INTO "SystemModuleField" VALUES(106, 101, 'Email', 101, 108, true, 'ex: abhishek@example.com', 2, null, true, false);
INSERT INTO "SystemModuleField" VALUES(107, 101, 'Mobile', 101, 108, true, 'ex: +916789551011', 1, null, true, false);
INSERT INTO "SystemModuleField" VALUES(108, 101, 'Fax', 101, 108, true, '+1-123-456-7890', 2, null, true, false);
INSERT INTO "SystemModuleField" VALUES(109, 101, 'Inquiry Source', 108, 103, true, null, 1, null, true, false);
INSERT INTO "SystemModuleField" VALUES(110, 101, 'Inquiry Status', 108, 103, true, null, 2, null, true, false);
INSERT INTO "SystemModuleField" VALUES(111, 101, 'Industry', 108, 103, true, null, 1, null, true, false);
INSERT INTO "SystemModuleField" VALUES(112, 101, 'No. of Employees', 101, 108, true, null, 2, null, true, false);
INSERT INTO "SystemModuleField" VALUES(113, 101, 'Rating', 108, 103, true, null, 1, null, true, false);
INSERT INTO "SystemModuleField" VALUES(114, 102, 'ModelField-B', 101, 102, true, null, 1, null, true, false);
update "SystemModuleField" SET "FieldTitle"='ModelField-B' WHERE "SystemModuleFieldId" IN (14);

INSERT INTO "SystemModuleFieldValidation" VALUES(101, 101, 101, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(102, 101, 105, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(103, 102, 101, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(104, 102, 106, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(105, 103, 101, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(106, 103, 106, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(107, 104, 101, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(108, 104, 106, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(109, 105, 101, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(110, 105, 107, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(111, 106, 101, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(112, 106, 102, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(113, 107, 101, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(114, 107, 103, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(115, 108, 109, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(116, 109, 101, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(117, 110, 101, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(118, 111, 101, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(119, 112, 101, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(120, 112, 104, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(121, 113, 101, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(122, 113, 104, true, false);
INSERT INTO "SystemModuleFieldValidation" VALUES(123, 114, 101, true, false);

INSERT INTO "SystemModuleFieldEventParameter" VALUES(101, 101, 'param-a', true, false);
INSERT INTO "SystemModuleFieldEventParameter" VALUES(102, 101, 'param-b', true, false);
INSERT INTO "SystemModuleFieldEventParameter" VALUES(103, 114, 'model-B-param-a', true, false);
update "SystemModuleFieldEventParameter" SET "ParameterName"='param-b' WHERE 
"SystemModuleFieldEventParameterId" = 2;
delete from "SystemModuleFieldEventParameter" where "SystemModuleFieldEventParameterId"=3;

INSERT INTO "SystemModuleFieldOption" VALUES(101, 109, '-None-', '', true, true, false);
INSERT INTO "SystemModuleFieldOption" VALUES(102, 109, 'Internet', 'Internet', false, true, false);
INSERT INTO "SystemModuleFieldOption" VALUES(103, 109, 'Self', 'Self', false, true, false);
INSERT INTO "SystemModuleFieldOption" VALUES(104, 110, '-None-', '', true, true, false);
INSERT INTO "SystemModuleFieldOption" VALUES(105, 110, 'Open', 'Open', false, true, false);
INSERT INTO "SystemModuleFieldOption" VALUES(106, 110, 'Closed', 'Closed', false, true, false);
INSERT INTO "SystemModuleFieldOption" VALUES(107, 111, '-None-', '', true, true, false);
INSERT INTO "SystemModuleFieldOption" VALUES(108, 111, 'x', 'x', false, true, false);
INSERT INTO "SystemModuleFieldOption" VALUES(109, 113, '-None-', '', true, true, false);
INSERT INTO "SystemModuleFieldOption" VALUES(110, 113, '3.5', '3.5', false, true, false);
INSERT INTO "SystemModuleFieldOption" VALUES(111, 113, '4.0', '4.0', false, true, false);
INSERT INTO "SystemModuleFieldOption" VALUES(112, 113, '4.5', '4.5', false, true, false);
INSERT INTO "SystemModuleFieldOption" VALUES(113, 113, '5.0', '5.0', false, true, false);

ALTER TABLE "SystemModuleFieldOption"  
ADD COLUMN "CreatedDate" TIMESTAMP WITH TIME ZONE NULL,
ADD COLUMN "CreatedBy" INT NULL,
ADD COLUMN "UpdatedDate" TIMESTAMP WITH TIME ZONE NULL,
ADD COLUMN "UpdatedBy" INT NULL,
ADD COLUMN "DeletedDate" TIMESTAMP WITH TIME ZONE NULL,
ADD COLUMN "DeletedBy" INT NULL;

SELECT * FROM "SystemModule";
SELECT * FROM "ControlMaster";
SELECT * FROM "RegexValidation";
SELECT * FROM "ControlEvent";
SELECT * FROM "SystemModuleField";
SELECT * FROM "SystemModuleFieldValidation";
SELECT * FROM "SystemModuleFieldEventParameter";
SELECT * FROM "SystemModuleFieldOption";

CREATE OR REPLACE FUNCTION fn_get_system_module_details
(
	IN system_module_id INT DEFAULT 0
)
RETURNS TABLE
(
	"SystemModuleId" INT,
	"ModuleName" CHARACTER VARYING,
	"FieldTitle" CHARACTER VARYING,
	"ControlName" CHARACTER VARYING,
	"EventName" CHARACTER VARYING,
	"ParameterName" CHARACTER VARYING,
	"RegexFor" CHARACTER VARYING,
	"Pattern" TEXT,
	"ErrorMessage" CHARACTER VARYING[],
	"OptionKey" CHARACTER VARYING,
	"OptionValue" CHARACTER VARYING,
	"IsDefault" BOOLEAN
)
LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
BEGIN
	RETURN QUERY
	SELECT
	sm."SystemModuleId", sm."ModuleName", smf."FieldTitle",
	cm."ControlName", ce."EventName", smfep."ParameterName",
	rv."RegexFor", rv."Pattern", rv."ErrorMessage",
	smfo."OptionKey", smfo."OptionValue", smfo."IsDefault"
	FROM "SystemModule" sm
	INNER JOIN "SystemModuleField" smf USING("SystemModuleId")
	INNER JOIN "ControlMaster" cm USING("ControlMasterId")
	INNER JOIN "ControlEvent" ce USING("ControlEventId")
	FULL OUTER JOIN "SystemModuleFieldEventParameter" smfep USING("SystemModuleFieldId")
	FULL OUTER JOIN "SystemModuleFieldValidation" smfv USING("SystemModuleFieldId")
	FULL OUTER JOIN "RegexValidation" rv USING("RegexValidationId")
	FULL OUTER JOIN "SystemModuleFieldOption" smfo USING("SystemModuleFieldId")
	WHERE sm."SystemModuleId"=system_module_id
	ORDER BY smf."SystemModuleFieldId" ASC;
END; 
$BODY$;
select * from "SystemModule"
SELECT * FROM fn_get_system_module_details(3);

INSER
CREATE OR REPLACE TRIGGER trig_cud_on_system_module
    AFTER INSERT OR UPDATE OR DELETE ON "SystemModule"
    FOR EACH ROW
    EXECUTE FUNCTION fn_cud_on_system_module();
	
CREATE OR REPLACE FUNCTION fn_cud_on_system_module()
  RETURNS TRIGGER AS
$BODY$
DECLARE
user_id INT := 12345;
BEGIN
  IF (TG_OP = 'INSERT') THEN
	INSERT INTO "SystemModuleLog"(
			 "SystemModuleId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
			 "DeletedBy","ActionMessage")
    VALUES(
			NEW."SystemModuleId", NOW(), user_id,
			NULL,NULL,NULL,NULL,
		CONCAT('Record with ModuleName=',
								   NEW."ModuleName",' (having id=',
								   NEW."SystemModuleId", '), inserted by user (having id=', user_id, ').'));
  ELSIF (TG_OP = 'UPDATE') THEN
	INSERT INTO "SystemModuleLog"(
			 "SystemModuleId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
			 "DeletedBy","ActionMessage")
    VALUES(
			OLD."SystemModuleId",NULL,NULL,
			NOW(),user_id,NULL,NULL,
		CONCAT('Record having id=', OLD."SystemModuleId", ' modified by user (having id=', user_id, ').'));
  ELSIF (TG_OP = 'DELETE') THEN
	INSERT INTO "SystemModuleLog"(
			 "SystemModuleId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
			 "DeletedBy","ActionMessage")
    VALUES(
			OLD."SystemModuleId",NULL,NULL,
			NULL,NULL,NOW(),user_id,
		CONCAT('Record having id=', OLD."SystemModuleId", ' deleted by user (having id=', user_id, ').'));
  END IF;
  RETURN NULL;
END;
$BODY$
LANGUAGE 'plpgsql';

INSERT INTO "SystemModule" VALUES(5, 'Model-D', true, false);
INSERT INTO "SystemModule"("ModuleName", "IsActive", "IsDeleted") VALUES('Model-A', true, false);
INSERT INTO "SystemModule"("ModuleName", "IsActive", "IsDeleted") VALUES('Model-B', true, false);
UPDATE "SystemModule" SET "IsActive"=false WHERE "SystemModuleId" IN (1,2);
DELETE FROM "SystemModule"
SELECT * FROM "SystemModule";
SELECT * FROM "SystemModuleLog";
SELECT TG_OP;


DROP TRIGGER IF EXISTS trig_cud_on_system_module
ON "SystemModule"  CASCADE ;

SELECT * FROM "SystemModuleLog";
SELECT * FROM "SystemModule";

SELECT fn_insert_on_system_module('dddinsertss2ddd', false, true, 12890);
SELECT * FROM "SystemModule";
SELECT * FROM "SystemModuleLog";
DROP FUNCTION fn_insert_on_system_module;

CREATE OR REPLACE FUNCTION fn_crud_on_system_module(
	system_module_id int,
	module_name character varying,
	is_active boolean,
	is_deleted boolean,
	user_id integer,
	TG_OP character varying)
    RETURNS TABLE("SystemModuleId" integer, 
				  "ModuleName" character varying, 
				  "IsActive" boolean, 
				  "IsDeleted" boolean) 
    LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
    new_system_module_id int;
BEGIN
  IF (TG_OP = 'INSERT') THEN
    -- Insert a record into the table and get the inserted record
    INSERT INTO "SystemModule"("ModuleName","IsActive","IsDeleted")
    VALUES (module_name, is_active, is_deleted)
    RETURNING "SystemModule"."SystemModuleId" into new_system_module_id;
	
	INSERT INTO "SystemModuleLog"(
				 "SystemModuleId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
				 "DeletedBy","ActionMessage")
				 VALUES(
				new_system_module_id, NOW(), user_id,
				NULL,NULL,NULL,NULL,
			CONCAT('Record having id=', new_system_module_id, ' & ModuleName="',module_name,
				   '" inserted by user (having id=', user_id, ').'));

	RETURN QUERY SELECT * FROM "SystemModule" WHERE "SystemModule"."SystemModuleId" = new_system_module_id;
  ELSIF (TG_OP = 'UPDATE' AND is_active = true) THEN
	UPDATE "SystemModule" SET
        "ModuleName" = module_name,
        "IsActive" = is_active,
        "IsDeleted" = is_deleted
    WHERE "SystemModule"."SystemModuleId" = system_module_id;
    
	INSERT INTO "SystemModuleLog"(
			 "SystemModuleId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
			 "DeletedBy","ActionMessage")
    VALUES(
			system_module_id,NULL,NULL,
			NOW(),user_id,NULL,NULL,
		CONCAT('Record having id=', system_module_id, ' modified by user (having id=', user_id, ').'));
	
    RETURN QUERY SELECT * FROM "SystemModule" WHERE "SystemModule"."SystemModuleId" = system_module_id;
  ELSIF (TG_OP = 'UPDATE' AND is_active = false) THEN
	UPDATE "SystemModule" SET
        "ModuleName" = module_name,
        "IsActive" = is_active,
        "IsDeleted" = is_deleted
    WHERE "SystemModule"."SystemModuleId" = system_module_id;
    
	INSERT INTO "SystemModuleLog"(
			 "SystemModuleId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
			 "DeletedBy","ActionMessage")
    VALUES(
			system_module_id,NULL,NULL,
			NOW(),user_id,NOW(),user_id,
		CONCAT('Record having id=', system_module_id, ' modified but also deleted by user (having id=', user_id, ').'));
	
    RETURN QUERY SELECT * FROM "SystemModule" WHERE "SystemModule"."SystemModuleId" = system_module_id;
  ELSIF (TG_OP = 'DELETE') THEN
		UPDATE "SystemModule" SET
			"IsActive" = is_active,
			"IsDeleted" = is_deleted
		WHERE "SystemModule"."SystemModuleId" = system_module_id;

		INSERT INTO "SystemModuleLog"(
				 "SystemModuleId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
				 "DeletedBy","ActionMessage")
		VALUES(
				system_module_id,NULL,NULL,
				NULL,NULL,NOW(),user_id,
			CONCAT('Record having id=', system_module_id, ' deleted by user (having id=', user_id, ').'));

		RETURN QUERY SELECT * FROM "SystemModule" WHERE "SystemModule"."SystemModuleId" = system_module_id;
  ELSIF (TG_OP = 'UNDODELETE') THEN
		UPDATE "SystemModule" SET
				"IsActive" = is_active,
				"IsDeleted" = is_deleted
			WHERE "SystemModule"."SystemModuleId" = system_module_id;
		INSERT INTO "SystemModuleLog"(
				 "SystemModuleId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
				 "DeletedBy","ActionMessage")
		VALUES(
				system_module_id,NULL,NULL,
				NOW(),user_id,NULL,NULL,
			CONCAT('Record having id=', system_module_id, ' put back (undo delete) by user (having id=', user_id, ').'));

		RETURN QUERY SELECT * FROM "SystemModule" WHERE "SystemModule"."SystemModuleId" = system_module_id;
  ELSE
  	IF (system_module_id IS NULL) THEN
		RETURN QUERY  SELECT * FROM "SystemModule"  WHERE "SystemModule"."ModuleName" = module_name;
	ELSE
    	RETURN QUERY SELECT * FROM "SystemModule" WHERE "SystemModule"."SystemModuleId" = system_module_id;
	END IF;
  END IF;
END;
$BODY$;

SELECT * FROM fn_crud_on_system_module(59,'Enquiry', true, false, 54123, 'INSERT');

SELECT * FROM "SystemModule";
SELECT * FROM "SystemModuleLog";

CREATE OR REPLACE FUNCTION fn_get_all_system_modules()
    RETURNS TABLE("SystemModuleId" integer, 
				  "ModuleName" character varying, 
				  "IsActive" boolean, 
				  "IsDeleted" boolean) 
    LANGUAGE 'plpgsql'
AS $BODY$
BEGIN
	RETURN QUERY SELECT * FROM "SystemModule" ORDER BY "SystemModule"."SystemModuleId";
END;
$BODY$;

SELECT * FROM fn_get_all_system_modules();

ControlMaster
"ControlMasterId" INT  PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	"ControlName" VARCHAR(100) NOT NULL,
	"BasicSyntax" TEXT NULL,
	"IsActive" BOOLEAN,
	"IsDeleted" BOOLEAN

CREATE OR REPLACE FUNCTION fn_crud_on_control_master(
	control_master_id int,
	control_name character varying,
	basic_syntax text,
	is_active boolean,
	is_deleted boolean,
	user_id integer,
	TG_OP character varying)
    RETURNS TABLE("ControlMasterId" integer, 
				  "ControlName" character varying, 
				  "BasicSyntax" text,
				  "IsActive" boolean, 
				  "IsDeleted" boolean) 
    LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
    new_control_master_id int;
BEGIN
  IF (TG_OP = 'INSERT') THEN
    -- Insert a record into the table and get the inserted record
    INSERT INTO "ControlMaster"("ControlName","BasicSyntax","IsActive","IsDeleted")
    VALUES (control_name, basic_syntax, is_active, is_deleted)
    RETURNING "ControlMaster"."ControlMasterId" into new_control_master_id;
	
	INSERT INTO "ControlMasterLog"(
				 "ControlMasterId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
				 "DeletedBy","ActionMessage")
				 VALUES(
				new_control_master_id, NOW(), user_id,
				NULL,NULL,NULL,NULL,
			CONCAT('Record having id=', new_control_master_id, ' & ControlName="',control_name,
				   '" inserted by user (having id=', user_id, ').'));

	RETURN QUERY SELECT * FROM "ControlMaster" WHERE "ControlMaster"."ControlMasterId" = new_control_master_id;
  ELSIF (TG_OP = 'UPDATE' AND is_active = true) THEN
	UPDATE "ControlMaster" SET
        "ControlName" = control_name,
		"BasicSyntax" = basic_syntax,
        "IsActive" = is_active,
        "IsDeleted" = is_deleted
    WHERE "ControlMaster"."ControlMasterId" = control_master_id;
    
	INSERT INTO "ControlMasterLog"(
			 "ControlMasterId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
			 "DeletedBy","ActionMessage")
    VALUES(
			control_master_id,NULL,NULL,
			NOW(),user_id,NULL,NULL,
		CONCAT('Record having id=', control_master_id, ' modified by user (having id=', user_id, ').'));
	
    RETURN QUERY SELECT * FROM "ControlMaster" WHERE "ControlMaster"."ControlMasterId" = control_master_id;
  ELSIF (TG_OP = 'UPDATE' AND is_active = false) THEN
	UPDATE "ControlMaster" SET
        "ControlName" = control_name,
		"BasicSyntax" = basic_syntax,
        "IsActive" = is_active,
        "IsDeleted" = is_deleted
    WHERE "ControlMaster"."ControlMasterId" = control_master_id;
    
	INSERT INTO "ControlMasterLog"(
			 "ControlMasterId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
			 "DeletedBy","ActionMessage")
    VALUES(
			control_master_id,NULL,NULL,
			NOW(),user_id,NOW(),user_id,
		CONCAT('Record having id=', control_master_id, ' modified but also deleted by user (having id=', user_id, ').'));
	
    RETURN QUERY SELECT * FROM "ControlMaster" WHERE "ControlMaster"."ControlMasterId" = control_master_id;
  ELSIF (TG_OP = 'DELETE') THEN
		UPDATE "ControlMaster" SET
			"IsActive" = is_active,
			"IsDeleted" = is_deleted
		WHERE "ControlMaster"."ControlMasterId" = control_master_id;

		INSERT INTO "ControlMasterLog"(
				 "ControlMasterId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
				 "DeletedBy","ActionMessage")
		VALUES(
				control_master_id,NULL,NULL,
				NULL,NULL,NOW(),user_id,
			CONCAT('Record having id=', control_master_id, ' deleted by user (having id=', user_id, ').'));

		RETURN QUERY SELECT * FROM "ControlMaster" WHERE "ControlMaster"."ControlMasterId" = control_master_id;
  ELSIF (TG_OP = 'UNDODELETE') THEN
		UPDATE "ControlMaster" SET
				"IsActive" = is_active,
				"IsDeleted" = is_deleted
			WHERE "ControlMaster"."ControlMasterId" = control_master_id;
		INSERT INTO "ControlMasterLog"(
				 "ControlMasterId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
				 "DeletedBy","ActionMessage")
		VALUES(
				control_master_id,NULL,NULL,
				NOW(),user_id,NULL,NULL,
			CONCAT('Record having id=', control_master_id, ' put back (undo delete) by user (having id=', user_id, ').'));

		RETURN QUERY SELECT * FROM "ControlMaster" WHERE "ControlMaster"."ControlMasterId" = control_master_id;
  ELSE
  	IF (control_master_id IS NULL) THEN
		RETURN QUERY  SELECT * FROM "ControlMaster"  WHERE "ControlMaster"."ControlName" = control_name;
	ELSE
    	RETURN QUERY SELECT * FROM "ControlMaster" WHERE "ControlMaster"."ControlMasterId" = control_master_id;
	END IF;
  END IF;
END;
$BODY$;

SELECT * FROM fn_crud_on_control_master(null, 'Dummy', null, true, false, 5566, 'RETRIEVE');

SELECT * FROM "ControlMaster";
SELECT * FROM "ControlMasterLog";

CREATE OR REPLACE FUNCTION fn_get_all_control_masters()
    RETURNS TABLE("ControlMasterId" integer, 
				  "ControlName" character varying, 
				  "BasicSyntax" text,
				  "IsActive" boolean, 
				  "IsDeleted" boolean) 
    LANGUAGE 'plpgsql'
AS $BODY$
BEGIN
	RETURN QUERY SELECT * FROM "ControlMaster" ORDER BY "ControlMaster"."ControlMasterId";
END;
$BODY$;

SELECT * FROM fn_get_all_control_masters();


"RegexValidationId" INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	"RegexFor" VARCHAR(100) NOT NULL,
	"Pattern" TEXT NOT NULL,
	"ErrorMessage" CHARACTER VARYING[] NOT NULL,
	"IsValid" BOOLEAN NULL,
	"IsActive" BOOLEAN NULL,
	"IsDeleted" BOOLEAN NULL
	
CREATE OR REPLACE FUNCTION fn_crud_on_regex_validation(
	regex_validation_id int,
	regex_for character varying,
	pattern text,
	error_message character varying[],
	is_valid bool,
	is_active bool,
	is_deleted bool,
	user_id int,
	TG_OP character varying
)
    RETURNS TABLE("RegexValidationId" integer, 
				  "RegexFor" character varying, 
				  "Pattern" text,
				  "ErrorMessage" character varying[],
				  "IsValid" boolean,
				  "IsActive" boolean, 
				  "IsDeleted" boolean) 
    LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
    new_regex_validation_id int;
BEGIN
  IF (TG_OP = 'INSERT') THEN
    -- Insert a record into the table and get the inserted record
    INSERT INTO "RegexValidation"("RegexFor","Pattern","ErrorMessage","IsValid","IsActive","IsDeleted")
    VALUES (regex_for,pattern,error_message,is_valid,is_active,is_deleted)
    RETURNING "RegexValidation"."RegexValidationId" into new_regex_validation_id;
	
	INSERT INTO "RegexValidationLog"(
				 "RegexValidationId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
				 "DeletedBy","ActionMessage")
				 VALUES(
				new_regex_validation_id, NOW(), user_id,
				NULL,NULL,NULL,NULL,
			CONCAT('Record having id=', new_regex_validation_id, ' & RegexFor="',regex_for,
				   '" inserted by user (having id=', user_id, ').'));

	RETURN QUERY SELECT * FROM "RegexValidation" WHERE "RegexValidation"."RegexValidationId" = new_regex_validation_id;
  ELSIF (TG_OP = 'UPDATE' AND is_active = true) THEN
	UPDATE "RegexValidation" SET
        "RegexFor" = regex_for,
		"Pattern" = pattern,
        "ErrorMessage" = error_message,
		"IsValid" = is_valid,
		"IsActive" = is_active,
        "IsDeleted" = is_deleted
    WHERE "RegexValidation"."RegexValidationId" = regex_validation_id;
    
	INSERT INTO "RegexValidationLog"(
			 "RegexValidationId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
			 "DeletedBy","ActionMessage")
    VALUES(
			regex_validation_id,NULL,NULL,
			NOW(),user_id,NULL,NULL,
		CONCAT('Record having id=', regex_validation_id, ' modified by user (having id=', user_id, ').'));
	
    RETURN QUERY SELECT * FROM "RegexValidation" WHERE "RegexValidation"."RegexValidationId" = regex_validation_id;
  ELSIF (TG_OP = 'UPDATE' AND is_active = false) THEN
	UPDATE "RegexValidation" SET
        "RegexFor" = regex_for,
		"Pattern" = pattern,
        "ErrorMessage" = error_message,
		"IsValid" = is_valid,
		"IsActive" = is_active,
        "IsDeleted" = is_deleted
    WHERE "RegexValidation"."RegexValidationId" = regex_validation_id;
    
	INSERT INTO "RegexValidationLog"(
			 "RegexValidationId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
			 "DeletedBy","ActionMessage")
    VALUES(
			regex_validation_id,NULL,NULL,
			NOW(),user_id,NOW(),user_id,
		CONCAT('Record having id=', regex_validation_id, ' modified but also deleted by user (having id=', user_id, ').'));
	
    RETURN QUERY SELECT * FROM "RegexValidation" WHERE "RegexValidation"."RegexValidationId" = regex_validation_id;
  ELSIF (TG_OP = 'DELETE') THEN
		UPDATE "RegexValidation" SET
			"IsActive" = is_active,
			"IsDeleted" = is_deleted
		WHERE "RegexValidation"."RegexValidationId" = regex_validation_id;

		INSERT INTO "RegexValidationLog"(
				 "RegexValidationId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
				 "DeletedBy","ActionMessage")
		VALUES(
				regex_validation_id,NULL,NULL,
				NULL,NULL,NOW(),user_id,
			CONCAT('Record having id=', regex_validation_id, ' deleted by user (having id=', user_id, ').'));

		RETURN QUERY SELECT * FROM "RegexValidation" WHERE "RegexValidation"."RegexValidationId" = regex_validation_id;
  ELSIF (TG_OP = 'UNDODELETE') THEN
		UPDATE "RegexValidation" SET
				"IsActive" = is_active,
				"IsDeleted" = is_deleted
			WHERE "RegexValidation"."RegexValidationId" = regex_validation_id;
		INSERT INTO "RegexValidationLog"(
				 "RegexValidationId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
				 "DeletedBy","ActionMessage")
		VALUES(
				regex_validation_id,NULL,NULL,
				NOW(),user_id,NULL,NULL,
			CONCAT('Record having id=', regex_validation_id, ' put back (undo delete) by user (having id=', user_id, ').'));

		RETURN QUERY SELECT * FROM "RegexValidation" WHERE "RegexValidation"."RegexValidationId" = regex_validation_id;
  ELSE
  	IF (regex_validation_id IS NULL) THEN
		RETURN QUERY  SELECT * FROM "RegexValidation"  WHERE "RegexValidation"."RegexFor" = regex_for;
	ELSE
    	RETURN QUERY SELECT * FROM "RegexValidation" WHERE "RegexValidation"."RegexValidationId" = regex_validation_id;
	END IF;
  END IF;
END;
$BODY$;

SELECT * FROM "RegexValidation";
SELECT * FROM "RegexValidationLog";
delete from "RegexValidation" where "RegexValidationId"=12;
SELECT * FROM fn_crud_on_regex_validation(1, 'Email Address',
									 '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$',
									'{
									 "This is not a valid email address",
									 "Format: abhishek.singh@gmail.com"
									 }',
									 null, true, false, 17890, 'UPDATE');
SELECT * FROM fn_crud_on_regex_validation(1, 'Required',
									 '^(?=.*\S).+$',
									 '{
									 "This field cannot be blank",
									 "Any non-empty value is valid"
									 }', 
									 null, true, false, 17890, 'INSERT');
SELECT * FROM fn_crud_on_regex_validation(1, 'Required',
									 '^(?=.*\S).+$',
									 '{
									 "This field cannot be blank",
									 "Any non-empty value is valid"
									 }', 
									 null, true, false, 17890, 'INSERT');
SELECT * FROM fn_crud_on_regex_validation(1,  'Mobile Number',
									 '^([\+]?91[-]?|[0])?[6-9][0-9]{9}$',
									 '{
									 "This is not a valid mobile number",
									 "Format: +917887650033"
									 }', null, true, false, 17890, 'INSERT');
SELECT * FROM fn_crud_on_regex_validation(6, 'xyz',
									 '^[\da-zA-Z ]$',
									 '{
									 "Invalid X",
									 "Should only contains alphabets, spaces and digits",
									 "Format: Insource44 Doe 90A"
									 }', null, true, false, 17890, 'RETRIEVE');


CREATE OR REPLACE FUNCTION fn_get_all_regex_validations()
RETURNS TABLE("RegexValidationId" integer, 
				  "RegexFor" character varying, 
				  "Pattern" text,
				  "ErrorMessage" character varying[],
				  "IsValid" boolean,
				  "IsActive" boolean, 
				  "IsDeleted" boolean) 
    LANGUAGE 'plpgsql'
AS $BODY$
BEGIN
	RETURN QUERY SELECT * FROM "RegexValidation" ORDER BY "RegexValidation"."RegexValidationId";
END;
$BODY$;

SELECT * FROM fn_get_all_regex_validations();


CREATE OR REPLACE FUNCTION fn_crud_on_control_event(
	control_event_id int,
	event_name character varying,
	is_active boolean,
	is_deleted boolean,
	user_id integer,
	TG_OP character varying)
    RETURNS TABLE("ControlEventId" integer, 
				  "EventName" character varying, 
				  "IsActive" boolean, 
				  "IsDeleted" boolean) 
    LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
    new_control_event_id int;
BEGIN
  IF (TG_OP = 'INSERT') THEN
    -- Insert a record into the table and get the inserted record
    INSERT INTO "ControlEvent"("EventName","IsActive","IsDeleted")
    VALUES (event_name, is_active, is_deleted)
    RETURNING "ControlEvent"."ControlEventId" into new_control_event_id;
	
	INSERT INTO "ControlEventLog"(
				 "ControlEventId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
				 "DeletedBy","ActionMessage")
				 VALUES(
				new_control_event_id, NOW(), user_id,
				NULL,NULL,NULL,NULL,
			CONCAT('Record having id=', new_control_event_id, ' & EventName="',event_name,
				   '" inserted by user (having id=', user_id, ').'));

	RETURN QUERY SELECT * FROM "ControlEvent" WHERE "ControlEvent"."ControlEventId" = new_control_event_id;
  ELSIF (TG_OP = 'UPDATE' AND is_active = true) THEN
	UPDATE "ControlEvent" SET
        "EventName" = event_name,
        "IsActive" = is_active,
        "IsDeleted" = is_deleted
    WHERE "ControlEvent"."ControlEventId" = control_event_id;
    
	INSERT INTO "ControlEventLog"(
			 "ControlEventId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
			 "DeletedBy","ActionMessage")
    VALUES(
			control_event_id,NULL,NULL,
			NOW(),user_id,NULL,NULL,
		CONCAT('Record having id=', control_event_id, ' modified by user (having id=', user_id, ').'));
	
    RETURN QUERY SELECT * FROM "ControlEvent" WHERE "ControlEvent"."ControlEventId" = control_event_id;
  ELSIF (TG_OP = 'UPDATE' AND is_active = false) THEN
	UPDATE "ControlEvent" SET
        "EventName" = event_name,
        "IsActive" = is_active,
        "IsDeleted" = is_deleted
    WHERE "ControlEvent"."ControlEventId" = control_event_id;
    
	INSERT INTO "ControlEventLog"(
			 "ControlEventId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
			 "DeletedBy","ActionMessage")
    VALUES(
			control_event_id,NULL,NULL,
			NOW(),user_id,NOW(),user_id,
		CONCAT('Record having id=', control_event_id, ' modified but also deleted by user (having id=', user_id, ').'));
	
    RETURN QUERY SELECT * FROM "ControlEvent" WHERE "ControlEvent"."ControlEventId" = control_event_id;
  ELSIF (TG_OP = 'DELETE') THEN
		UPDATE "ControlEvent" SET
			"IsActive" = is_active,
			"IsDeleted" = is_deleted
		WHERE "ControlEvent"."ControlEventId" = control_event_id;

		INSERT INTO "ControlEventLog"(
				 "ControlEventId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
				 "DeletedBy","ActionMessage")
		VALUES(
				control_event_id,NULL,NULL,
				NULL,NULL,NOW(),user_id,
			CONCAT('Record having id=', control_event_id, ' deleted by user (having id=', user_id, ').'));

		RETURN QUERY SELECT * FROM "ControlEvent" WHERE "ControlEvent"."ControlEventId" = control_event_id;
  ELSIF (TG_OP = 'UNDODELETE') THEN
		UPDATE "ControlEvent" SET
				"IsActive" = is_active,
				"IsDeleted" = is_deleted
			WHERE "ControlEvent"."ControlEventId" = control_event_id;
		INSERT INTO "ControlEventLog"(
				 "ControlEventId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
				 "DeletedBy","ActionMessage")
		VALUES(
				control_event_id,NULL,NULL,
				NOW(),user_id,NULL,NULL,
			CONCAT('Record having id=', control_event_id, ' put back (undo delete) by user (having id=', user_id, ').'));

		RETURN QUERY SELECT * FROM "ControlEvent" WHERE "ControlEvent"."ControlEventId" = control_event_id;
  ELSE
  	IF (control_event_id IS NULL) THEN
		RETURN QUERY  SELECT * FROM "ControlEvent"  WHERE "ControlEvent"."EventName" = event_name;
	ELSE
    	RETURN QUERY SELECT * FROM "ControlEvent" WHERE "ControlEvent"."ControlEventId" = control_event_id;
	END IF;
  END IF;
END;
$BODY$;

SELECT * FROM fn_crud_on_control_event(null, 'onclick', false, true, 1199, 'RETRIEVE');
SELECT * FROM "ControlEvent";
SELECT * FROM "ControlEventLog";

CREATE OR REPLACE FUNCTION fn_get_all_control_events()
    RETURNS TABLE("ControlEventId" integer, 
				  "EventName" character varying, 
				  "IsActive" boolean, 
				  "IsDeleted" boolean) 
    LANGUAGE 'plpgsql'
AS $BODY$
BEGIN
	RETURN QUERY SELECT * FROM "ControlEvent" ORDER BY "ControlEvent"."ControlEventId";
END;
$BODY$;

SELECT * FROM fn_get_all_control_events();
/*
//var connectionString = builder.Configuration.GetConnectionString("localDbConnectionStr");
//builder.Services.AddSingleton<IDbConnection>((sp) => new NpgsqlConnection(connectionString));

//builder.Services.AddScoped<IDbConnection>(c =>
//{
//    //var configuration = c.GetRequiredService<IConfiguration>();
//    var connectionString = builder.Configuration.GetConnectionString("localDbConnectionStr");
//    return new NpgsqlConnection(connectionString);
//});
*/

--SystemModuleField
CREATE OR REPLACE FUNCTION fn_crud_on_system_module_field(
	system_module_field_id int,
	system_module_id int,
	field_title character varying,
	control_master_id int,
	control_event_id int,
	is_visible bool,
	placeholder character varying,
	"position" int,
	parent_control_id int,
	is_required boolean,
	is_active boolean,
	is_deleted boolean,
	user_id integer,
	TG_OP character varying)
    RETURNS TABLE("SystemModuleFieldId" integer, 
				  "SystemModuleId" integer,
				  "FieldTitle" character varying,
				  "ControlMasterId" integer,
				  "ControlEventId" integer,
				  "IsVisible" boolean,
				  "Placeholder" character varying,
				  "Position" integer,
				  "ParentControlId" integer,
				  "IsActive" boolean, 
				  "IsDeleted" boolean,
				  "IsRequired" boolean) 
    LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
    new_system_module_field_id int;
BEGIN
  IF (TG_OP = 'INSERT') THEN
    -- Insert a record into the table and get the inserted record
    INSERT INTO "SystemModuleField"("SystemModuleId","FieldTitle","ControlMasterId",
							   "ControlEventId","IsVisible","Position","Placeholder",
							   "ParentControlId","IsRequired","IsActive","IsDeleted")
    VALUES (system_module_id,field_title,control_master_id,control_event_id,is_visible,
			"position",placeholder,parent_control_id,is_required,is_active, is_deleted)
    RETURNING "SystemModuleField"."SystemModuleFieldId" into new_system_module_field_id;
	
	INSERT INTO "SystemModuleFieldLog"(
				 "SystemModuleFieldId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
				 "DeletedBy","ActionMessage")
				 VALUES(
				new_system_module_field_id, NOW(), user_id,
				NULL,NULL,NULL,NULL,
			CONCAT('Record having id=', new_system_module_field_id,
				   ' (of SystemModuleId=',system_module_id,') & FieldTitle="',field_title,
				   '" inserted by user (having id=', user_id, ').'));

	RETURN QUERY SELECT * FROM "SystemModuleField"
	WHERE "SystemModuleField"."SystemModuleFieldId" = new_system_module_field_id;
  ELSIF (TG_OP = 'UPDATE' AND is_active = true) THEN
	UPDATE "SystemModuleField" SET
		"SystemModuleId" = system_module_id,
        "FieldTitle" = field_title,
		"ControlMasterId" = control_master_id,
		"ControlEventId" = control_event_id,
		"IsVisible" = is_visible,
		"Position" = "position",
		"Placeholder" = placeholder,
		"ParentControlId" = parent_control_id,
		"IsRequired" = is_required,
        "IsActive" = is_active,
        "IsDeleted" = is_deleted
    WHERE "SystemModuleField"."SystemModuleFieldId" = system_module_field_id;
    
	INSERT INTO "SystemModuleFieldLog"(
			 "SystemModuleFieldId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
			 "DeletedBy","ActionMessage")
    VALUES(
			system_module_field_id,NULL,NULL,
			NOW(),user_id,NULL,NULL,
		CONCAT('Record having id=', system_module_field_id,
			   ' (of SystemModuleId=',system_module_id,')',
			   ' modified by user (having id=', user_id, ').'));
	
    RETURN QUERY SELECT * FROM "SystemModuleField"
	WHERE "SystemModuleField"."SystemModuleFieldId" = system_module_field_id;
  ELSIF (TG_OP = 'UPDATE' AND is_active = false) THEN
	UPDATE "SystemModuleField" SET
        "SystemModuleId" = system_module_id,
        "FieldTitle" = field_title,
		"ControlMasterId" = control_master_id,
		"ControlEventId" = control_event_id,
		"IsVisible" = is_visible,
		"Position" = "position",
		"Placeholder" = placeholder,
		"ParentControlId" = parent_control_id,
		"IsRequired" = is_required,
        "IsActive" = is_active,
        "IsDeleted" = is_deleted
    WHERE "SystemModuleField"."SystemModuleFieldId" = system_module_field_id;
    
	INSERT INTO "SystemModuleFieldLog"(
			 "SystemModuleFieldId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
			 "DeletedBy","ActionMessage")
    VALUES(
			system_module_field_id,NULL,NULL,
			NOW(),user_id,NOW(),user_id,
		CONCAT('Record having id=', system_module_field_id,
			   ' (of SystemModuleId=',system_module_id,')',
			   ' modified but also deleted by user (having id=', user_id, ').'));
	
    RETURN QUERY SELECT * FROM "SystemModuleField"
	WHERE "SystemModuleField"."SystemModuleFieldId" = system_module_field_id;
  ELSIF (TG_OP = 'DELETE') THEN
		UPDATE "SystemModuleField" SET
			"IsActive" = is_active,
			"IsDeleted" = is_deleted
		WHERE "SystemModuleField"."SystemModuleFieldId" = system_module_field_id;

		INSERT INTO "SystemModuleFieldLog"(
				 "SystemModuleFieldId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
				 "DeletedBy","ActionMessage")
		VALUES(
				system_module_field_id,NULL,NULL,
				NULL,NULL,NOW(),user_id,
			CONCAT('Record having id=',system_module_field_id,
				   ' (of SystemModuleId=',system_module_id,')',
				   ' deleted by user (having id=', user_id, ').'));

		RETURN QUERY SELECT * FROM "SystemModuleField"
		WHERE "SystemModuleField"."SystemModuleFieldId" = system_module_field_id;
  ELSIF (TG_OP = 'UNDODELETE') THEN
		UPDATE "SystemModuleField" SET
			"IsActive" = is_active,
			"IsDeleted" = is_deleted
		WHERE "SystemModuleField"."SystemModuleFieldId" = system_module_field_id;
		INSERT INTO "SystemModuleFieldLog"(
				 "SystemModuleFieldId","CreatedDate","CreatedBy","UpdatedDate","UpdatedBy","DeletedDate",
				 "DeletedBy","ActionMessage")
		VALUES(
				system_module_field_id,NULL,NULL,
				NOW(),user_id,NULL,NULL,
			CONCAT('Record having id=', system_module_field_id,
				   ' (of SystemModuleId=',system_module_id,')',
				   ' put back (undo delete) by user (having id=', user_id, ').'));

		RETURN QUERY SELECT * FROM "SystemModuleField" 
		WHERE "SystemModuleField"."SystemModuleFieldId" = system_module_field_id;
  ELSE
  	IF (system_module_field_id IS NULL AND field_title IS NOT NULL) THEN
		RETURN QUERY  SELECT * FROM "SystemModuleField" 
		WHERE "SystemModuleField"."FieldTitle"=field_title AND
		"SystemModuleField"."SystemModuleId"=system_module_id;
	ELSIF(system_module_field_id IS NULL AND field_title IS NULL) THEN
		RETURN QUERY  SELECT * FROM "SystemModuleField" 
		WHERE "SystemModuleField"."SystemModuleId"=system_module_id;
	ELSE
    	RETURN QUERY SELECT * FROM "SystemModuleField"
		WHERE "SystemModuleField"."SystemModuleFieldId" = system_module_field_id;
	END IF;
  END IF;
END;
$BODY$;
SELECT * FROM "ControlMaster";
SELECT * FROM "ControlEvent";
SELECT * FROM "SystemModuleField";
SELECT * FROM "SystemModuleFieldLog";
SELECT * FROM fn_crud_on_system_module_field(
	null, 63, null, 1, 3, null, 'Full Name', null, null, true, true, false, 81000, 'RETRIEVE'
);

CREATE OR REPLACE FUNCTION fn_get_all_system_module_fields()
    RETURNS TABLE("SystemModuleFieldId" integer, 
				  "SystemModuleId" integer,
				  "FieldTitle" character varying,
				  "ControlMasterId" integer,
				  "ControlEventId" integer,
				  "IsVisible" boolean,
				  "Placeholder" character varying,
				  "Position" integer,
				  "ParentControlId" integer,
				  "IsActive" boolean, 
				  "IsDeleted" boolean,
				  "IsRequired" boolean) 
    LANGUAGE 'plpgsql'
AS $BODY$
BEGIN
	RETURN QUERY SELECT * FROM "SystemModuleField" ORDER BY "SystemModuleField"."SystemModuleFieldId";
END;
$BODY$;

SELECT * FROM fn_get_all_system_module_fields();