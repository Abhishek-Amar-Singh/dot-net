CREATE TABLE "Category"
(
	"Id" INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	"CategoryName" VARCHAR(100) UNIQUE NOT NULL,
	"IsActive" BOOLEAN NULL,
	"IsDeleted" BOOLEAN NULL
);

CREATE TABLE "SubCategory"
(
	"Id" INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	"CategoryId" INT NOT NULL REFERENCES "Category"("Id"),
	"SubCategoryName" VARCHAR(100) NOT NULL,
	"IsActive" BOOLEAN NULL,
	"IsDeleted" BOOLEAN NULL
);

CREATE TABLE "Field"
(
	"Id" INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	"SubCategoryId" INT NOT NULL REFERENCES "SubCategory"("Id"),
	"FieldName" VARCHAR(100) NOT NULL,
	"IsActive" BOOLEAN NULL,
	"IsDeleted" BOOLEAN NULL
);

CREATE TABLE "Unit"
(
	"Id" INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	"UnitName" VARCHAR(100) UNIQUE NOT NULL,
	"IsActive" BOOLEAN NULL,
	"IsDeleted" BOOLEAN NULL
);

CREATE TABLE "SubField"
(
	"Id" INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	"FieldId" INT NOT NULL REFERENCES "Field"("Id"),
	"SubFieldName" VARCHAR(100) NOT NULL,
	"UnitId" INT NOT NULL REFERENCES "Unit"("Id"),
	"Value" VARCHAR(50),
	"IsActive" BOOLEAN NULL,
	"IsDeleted" BOOLEAN NULL
);


INSERT INTO "Category"("CategoryName") VALUES('Basic User Details');
INSERT INTO "Category"("CategoryName") VALUES('Income Profile');
INSERT INTO "Category"("CategoryName") VALUES('Existing User Assets');
INSERT INTO "Category"("CategoryName") VALUES('Existing User Liabilities');
INSERT INTO "Category"("CategoryName") VALUES('Monthly User Investments');
INSERT INTO "Category"("CategoryName") VALUES('Modeling Assumptions');
SELECT * FROM "Category";

INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(1, 'Personal Information');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(1, 'Family Information');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(2, 'Income Source');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(2, 'Income');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(3, 'Equity');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(3, 'Debt');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(3, 'Hybrid');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(3, 'Real Estate');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(4, 'Loans');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(4, 'Credit Card');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(4, 'Insurance');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(5, 'Equity');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(5, 'Debt');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(5, 'Real Estate');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(6, 'Asset Return Expectations');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(5, 'Bank Account');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(5, 'Pension Plans');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(5, 'Safe Deposits');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(3, 'Bank Account');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(3, 'Pension Plans');
INSERT INTO "SubCategory"("CategoryId","SubCategoryName") VALUES(3, 'Safe Deposits');
SELECT * FROM "SubCategory";

INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(1, 'Name');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(1, 'DOB');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(1, 'Gender');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(1, 'City');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(1, 'State');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(1, 'PAN');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(1, 'Aadhaar');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(2, 'Number of Dependent(s)');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(2, 'Age of Dependent(s)');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(3, 'Occupation');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(4, 'Salary Income');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(4, 'Other Income');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(5, 'Model Portfolio');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(5, 'Unlisted Stocks');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(5, 'Public Stocks (International)');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(6, 'Debt Funds');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(7, 'Hybrid Funds');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(8, 'Housing Loan');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(8, 'Property Loan');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(8, 'Auto Loan');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(8, 'Personal Loan');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(9, 'Credit Card');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(10, 'Life Insurance');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(10, 'Health Insurance');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(11, 'Public Stocks (India)');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(11, 'Equity Funds');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(12, 'Direct Bonds');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(12, 'Debt Funds');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(13, 'Rental Yielding (Residential)');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(13, 'Rental Yielding (Commercial)');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(13, 'Non-Yielding (Residential)');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(13, 'Non-Yielding (Commercial)');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(14, 'Rental Yielding (Residential)');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(14, 'Rental Yielding (Commercial)');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(14, 'Non-Yielding (Residential)');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(14, 'Non-Yielding (Commercial)');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(15, 'Equity');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(15, 'Debt');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(15, 'Hybrid');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(15, 'Real Estate');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(16, 'Savings');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(17, 'NPS Tier I');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(17, 'NPS Tier II');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(18, 'EPF');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(18, 'PPF');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(19, 'Savings');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(20, 'NPS Tier I');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(20, 'NPS Tier II');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(21, 'EPF');
INSERT INTO "Field"("SubCategoryId","FieldName") VALUES(21, 'PPF');
update "Field" SET "FieldName"='PPF' WHERE "Id"=45;
SELECT * FROM "Field";

INSERT INTO "Unit"("UnitName") VALUES('Char');
INSERT INTO "Unit"("UnitName") VALUES('Date');
INSERT INTO "Unit"("UnitName") VALUES('Number (Years)');
INSERT INTO "Unit"("UnitName") VALUES('Number (Months)');
INSERT INTO "Unit"("UnitName") VALUES('Number (Lakhs)');
INSERT INTO "Unit"("UnitName") VALUES('Number (Thousands)');
INSERT INTO "Unit"("UnitName") VALUES('Percentage');
SELECT * FROM "Unit";


INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(9, 'Dependent1', 3, NULL);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(9, 'Dependent2', 3, NULL);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(10, 'Occupation1', 1, 'Salaried');
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(10, 'Occupation2', 1, NULL);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(11, 'Value', 5, 120);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(12, 'Value', 5, 0);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(13, 'Value', 5, NULL);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(14, 'Value', 5, 0);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(15, 'Value', 5, 0);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(16, 'Value', 5, 0);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(17, 'Value', 5, 0);

UPDATE "SubField" SET "Value"=100 WHERE "FieldId"=14

INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(18, 'Pending Tenure', 4, 240);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(18, 'Outstanding Amount', 5, 230);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(18, 'Monthly EMI Amount', 6, 1.7);


INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(22, 'Outstanding Amount', 5, 0);

INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(22, 'Pending Tenure', 3, 25);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(22, 'Payment Frequency', 1, 'Yearly');
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(22, 'Periodic Premium Amount', 5, 0.3);



INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(37, 'Public Stocks (India)', 7, 12);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(37, 'Equity Funds', 7, 12);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(37, 'Model Portfolio', 7, 12);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(37, 'Unlisted Stocks', 7, 12);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(38, 'Direct Bonds', 7, 7);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(38, 'Debt Funds', 7, 7);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(39, 'Hybrid Funds', 7, 11);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(40, 'Rental Yielding (Residential)', 7, 8);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(40, 'Rental Yielding (Commercial)', 7, 8);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(40, 'Non-Yielding (Residential)', 7, 5);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(40, 'Non-Yielding (Commercial)', 7, 5);


INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(41, 'Value', 5, 0);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(42, 'Value', 5, 0.08);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(43, 'Value', 5, 0);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(44, 'Value', 5, 1.80);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(45, 'Value', 5, 0);


INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(46, 'Value', 5, 25);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(47, 'Value', 5, 5.3);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(48, 'Value', 5, 0);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(49, 'Value', 5, 121.0);
INSERT INTO "SubField"("FieldId","SubFieldName","UnitId","Value")
VALUES(50, 'Value', 5, 6.8);

SELECT * FROM "SubField";



CREATE OR REPLACE FUNCTION fn_existing_assets(
	IN existsing_user_assets_id integer,
	IN monthly_investment_id integer
)
    RETURNS TABLE("Assets" character varying, 
				  "Asset Class" character varying, 
				  "Market Value" double precision, 
				  "% of Total"  double precision,
				 "Monthly Investments"  double precision) 
    LANGUAGE 'plpgsql'
AS $BODY$
BEGIN
	RETURN QUERY
	WITH cte AS 
	(
		SELECT f."FieldName", sc."SubCategoryName",
		CASE
		WHEN sf."Value" IS NULL THEN 0::character varying
		ELSE sf."Value"
		END
		FROM "Field" f
		INNER JOIN "SubCategory" sc ON f."SubCategoryId" = sc."Id"
		INNER JOIN "Category" c ON sc."CategoryId"=c."Id"
		FULL OUTER JOIN "SubField" sf ON f."Id"=sf."FieldId"
		WHERE c."Id"=existsing_user_assets_id
	), cte2 AS (
		SELECT f."FieldName", sf."Value" AS "Monthly Investments"
		FROM "Category" c
		INNER JOIN "SubCategory" sc ON sc."CategoryId"=c."Id"
		INNER JOIN "Field" f ON f."SubCategoryId"=sc."Id"
		INNER JOIN "SubField" sf ON sf."FieldId"=f."Id"
		WHERE c."Id"=monthly_investment_id
	)
	SELECT
	c1."FieldName" AS "Assets",
	c1."SubCategoryName" AS "Asset Class",
	c1."Value"::FLOAT AS "Market Value",
	(((("Value"::FLOAT) / (SELECT SUM("Value"::FLOAT) FROM cte)) * 100)::NUMERIC(10,1))::FLOAT AS "% of Total",
	CASE
	WHEN c2."Monthly Investments" IS NULL THEN 0
	ELSE c2."Monthly Investments"::FLOAT END
	FROM cte c1
	FULL OUTER JOIN cte2 c2 ON c2."FieldName"=c1."FieldName"
	WHERE c1."FieldName" IS NOT NULL;
END;
$BODY$;

SELECT * FROM "Field";

SELECT * FROM fn_existing_assets(3, 5);




