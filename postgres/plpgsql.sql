do $$ 
declare
   counter    integer := 1;
   first_name varchar(50) := 'John';
   last_name  varchar(50) := 'Doe';
   payment    numeric(11,2) := 20.5;
begin 
   raise notice '% % % has been paid % USD', 
       counter, 
	   first_name, 
	   last_name, 
	   payment;
end $$;


--Write a pl/sql block to find whether the number if odd or even.
DO $$
DECLARE
  v_num INTEGER := 33;
BEGIN
  IF MOD(v_num,2) = 0 THEN
    RAISE NOTICE '% is even.', v_num;
  ELSE
    RAISE NOTICE '% is odd.', V_num;
  END IF;
END $$;

--Write a pl/sql block to find the give number is prime or not.
DO $$
DECLARE
  v_num INTEGER := 21;
  flag INTEGER := 0;
  v_counter INTEGER := 2;
BEGIN
  WHILE v_counter < v_num LOOP
    IF MOD(v_num, v_counter) = 0 THEN
      flag := 1;
    END IF;
    v_counter := v_counter + 1;
  END LOOP;
  IF flag = 0 THEN
    RAISE NOTICE '% is a prime number.', v_num;
  ELSE
    RAISE NOTICE '% is not a prime number.', v_num;
  END IF;
END $$;

--Write a pl/sql block to find the sum of 1 to n series where n is given by user.
DO $$
DECLARE
  v_num INT := 17;
  v_result INT := 0;
BEGIN
  FOR v_counter IN 1 .. v_num LOOP
    v_result := v_result + v_counter;
  END LOOP;
  RAISE NOTICE 'The sum of 1 to % series is %.', v_num, v_result;
END $$;

--Write a pl/sql block to reverse a number.
DO $$
DECLARE
  v_num INT := 103; 
  v_rev INT := 0;
BEGIN
  WHILE v_num>0 LOOP   
    v_rev := (v_rev*10) + MOD(v_num,10); 
    v_num := FLOOR(v_num/10); 
  END LOOP; 
  RAISE NOTICE 'Reverse of the number is %.', v_rev; 
END $$;

--5)Write a pl/sql block to find maximum of three numbers.
DO $$
DECLARE
  v_num1 INT := 69; 
  v_num2 INT := 77;
  v_num3 INT := 76;
BEGIN
  IF v_num1>v_num2 and v_num1>v_num3 THEN
    RAISE NOTICE '% is the maximum number.', v_num1;
  ELSIF v_num2>v_num1 and v_num2>v_num3 THEN
    RAISE NOTICE '% is the maximum number.', v_num2;
  ELSE
    RAISE NOTICE '% is the maximum number.', v_num3;
  END IF;
END $$;
--
DO $$
DECLARE
  v_num1 INT := 69; 
  v_num2 INT := 77;
  v_num3 INT := 76;
BEGIN
  RAISE NOTICE 'The maximum number is %', MAX(UNNEST(ARRAY[v_num1, v_num2, v_num3]));
END $$;