CREATE TABLE "Student"
(
	"Id" INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	"StudentId" INT NOT NULL,
	"Name" VARCHAR(60) NOT NULL,
	"Subject" VARCHAR(50) NOT NULL,
	"Marks" INT NOT NULL,
	"Result" BOOLEAN NOT NULL
);

INSERT INTO "Student"("StudentId","Name","Subject","Marks","Result") VALUES
(1, 'Nikhil', 'Maths', 34, false),
(1, 'Nikhil', 'Sci.', 56, true),
(1, 'Nikhil', 'Eng.', 44, true),
(2, 'Deepak', 'Maths', 45, true),
(2, 'Deepak', 'Sci.', 77, true),
(2, 'Deepak', 'Eng.', 57, true),
(3, 'Ruthvik', 'Maths', 77, true),
(3, 'Ruthvik', 'Sci.', 56, true),
(3, 'Ruthvik', 'Eng.', 20, false),
(4, 'Bharath', 'Maths', 10, false),
(4, 'Bharath', 'Eng.', 99, true),
(5, 'Runali', 'Eng.', 75, true);

SELECT * FROM "Student";

SELECT DISTINCT("Name") FROM "Student";

create extension tablefunc;


WITH cte AS(
	SELECT s."StudentId", s."Name", 
		   CASE
		   WHEN FALSE = ANY(s."Results")
		   THEN 'Fail' ELSE 'Pass' END AS "Result",
		   s."Results"
	FROM (
		SELECT "StudentId", "Name", ARRAY_AGG("Result") AS "Results"
		FROM "Student"
		GROUP BY "Name", "StudentId"
	) s
), cte2 AS
(
SELECT
ct."StudentId",
ct."Student",
COALESCE(ct."Maths", 'Absent') AS "Maths",
COALESCE(ct."Sci.", 'Absent') AS "Sci.",
COALESCE(ct."Eng.", 'Absent') AS "Eng."
FROM crosstab(
   $$
	SELECT "StudentId", "Name", "Subject", "Marks"
    FROM "Student"
    $$, $$VALUES ('Maths'), ('Sci.'), ('Eng.')$$
   ) AS ct ("StudentId" INT,
	   		"Student" character varying,
			"Maths" character varying,
			"Sci." character varying,
			"Eng." character varying)
)
SELECT
cte2."Student",
cte2."Maths" , cte2."Sci.", cte2."Eng.",
CASE WHEN cte."Result"='Pass' AND 'Absent' IN (cte2."Maths", cte2."Sci.", cte2."Eng.") THEN 'Fail'
ELSE cte."Result" END
FROM cte
INNER JOIN cte2 ON cte."StudentId"=cte2."StudentId";



CREATE TABLE tbl (
   section   text
 , status    text
 , ct        integer  -- "count" is a reserved word in standard SQL
);

INSERT INTO tbl VALUES 
  ('A', 'Active', 1), ('A', 'Inactive', 2)
, ('B', 'Active', 4), ('B', 'Inactive', 5)
                    , ('C', 'Inactive', 7); 
					
SELECT *
FROM   crosstab(
   'SELECT section, status, ct
    FROM   tbl
    ORDER  BY 1,2'  -- could also just be "ORDER BY 1" here

  , $$VALUES ('Active'), ('Inactive')$$
   ) AS ct ("Section" text, "Active" int, "Inactive" int);
   
 SELECT section,
       SUM(CASE WHEN status = 'Active' THEN ct ELSE 0 END) AS "Active",
       SUM(CASE WHEN status = 'Inactive' THEN ct ELSE 0 END) AS "Inactive"
FROM tbl
GROUP BY section
ORDER BY section, "Active", "Inactive";

SELECT
  s."StudentId",
  s."Name" AS "Student",
  COALESCE(MAX(CASE WHEN s."Subject" = 'Maths' THEN s."Marks" END)::varchar, 'Absent') AS "Maths",
  COALESCE(MAX(CASE WHEN s."Subject" = 'Sci.' THEN s."Marks" END)::varchar, 'Absent') AS "Sci.",
  COALESCE(MAX(CASE WHEN s."Subject" = 'Eng.' THEN s."Marks" END)::varchar, 'Absent') AS "Eng."
FROM
  "Student" s
GROUP BY
  s."StudentId",
  s."Name";


SELECT
ct."StudentId",
ct."Student",
COALESCE(ct."Maths", 'Absent') AS "Maths",
COALESCE(ct."Sci.", 'Absent') AS "Sci.",
COALESCE(ct."Eng.", 'Absent') AS "Eng."
FROM crosstab(
   $$
	SELECT "StudentId", "Name", "Subject", "Marks"
    FROM "Student"
    $$, $$VALUES ('Maths'), ('Sci.'), ('Eng.')$$
   ) AS ct ("StudentId" INT,
	   		"Student" character varying,
			"Maths" character varying,
			"Sci." character varying,
			"Eng." character varying)
			
			

WITH cte AS(
	SELECT s."StudentId", s."Name", 
		   CASE
		   WHEN FALSE = ANY(s."Results")
		   THEN 'Fail' ELSE 'Pass' END AS "Result",
		   s."Results"
	FROM (
		SELECT "StudentId", "Name", ARRAY_AGG("Result") AS "Results"
		FROM "Student"
		GROUP BY "Name", "StudentId"
	) s
), cte2 AS
(
SELECT
  s."StudentId",
  s."Name" AS "Student",
  COALESCE(MAX(CASE WHEN s."Subject" = 'Maths' THEN s."Marks" END)::varchar, 'Absent') AS "Maths",
  COALESCE(MAX(CASE WHEN s."Subject" = 'Sci.' THEN s."Marks" END)::varchar, 'Absent') AS "Sci.",
  COALESCE(MAX(CASE WHEN s."Subject" = 'Eng.' THEN s."Marks" END)::varchar, 'Absent') AS "Eng."
FROM
  "Student" s
GROUP BY
  s."StudentId",
  s."Name"
)
SELECT
cte2."Student",
cte2."Maths" , cte2."Sci.", cte2."Eng.",
CASE WHEN cte."Result"='Pass' AND 'Absent' IN (cte2."Maths", cte2."Sci.", cte2."Eng.") THEN 'Fail'
ELSE cte."Result" END
FROM cte
INNER JOIN cte2 ON cte."StudentId"=cte2."StudentId";



CREATE TABLE "EffectiveTaxRate"
(
	"Id" BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
	"MinAge" BIGINT,
	"MaxAge" BIGINT,
	"MinSal" DOUBLE PRECISION,
	"MaxSal" DOUBLE PRECISION,
	"OldRegime" FLOAT,
	"NewRegime" FLOAT,
	"Surcharge" FLOAT,
	"HE Cess" FLOAT
);

SELECT * FROM "EffectiveTaxRate"
delete FROM "EffectiveTaxRate"
INSERT INTO "EffectiveTaxRate"("MinAge","MaxAge","MinSal","MaxSal","OldRegime","NewRegime","Surcharge","HE Cess") VALUES
(0, 60, 0, 250000, 0, 0, 0, 4),
(0, 60, 250000, 300000, 5, 5, 0, 4),
(0, 60, 300000, 500000, 5, 5, 0, 4),
(0, 60, 500000, 750000, 20, 10, 0, 4),
(0, 60, 750000, 1000000, 20, 15, 0, 4),
(0, 60, 1000000, 1250000, 30, 20, 0, 4),
(0, 60, 1250000, 1500000, 30, 25, 0, 4),
(0, 60, 1500000, 5000000, 30, 30, 0, 4),
(0, 60, 5000000, 10000000, 30, 30, 10, 4),
(0, 60, 10000000, 20000000, 30, 30, 15, 4),
(0, 60, 20000000, 50000000, 30, 30, 25, 4),
(0, 60, 50000000, NULL, 30, 30, 37, 4),

(61, 80, 0, 250000, 0, 0, 0, 4),
(61, 80, 250000, 300000, 0, 5, 0, 4),
(61, 80, 300000, 500000, 5, 5, 0, 4),
(61, 80, 500000, 750000, 20, 10, 0, 4),
(61, 80, 750000, 1000000, 20, 15, 0, 4),
(61, 80, 1000000, 1250000, 30, 20, 0, 4),
(61, 80, 1250000, 1500000, 30, 25, 0, 4),
(61, 80, 1500000, 5000000, 30, 30, 0, 4),
(61, 80, 5000000, 10000000, 30, 30, 10, 4),
(61, 80, 10000000, 20000000, 30, 30, 15, 4),
(61, 80, 20000000, 50000000, 30, 30, 25, 4),
(61, 80, 50000000, NULL, 30, 30, 37, 4),

(81, NULL,  0, 250000, 0, 0, 0, 4),
(81, NULL, 250000, 300000, 0, 5, 0, 4),
(81, NULL, 300000, 500000, 5, 0, 0, 4),
(81, NULL, 500000, 750000, 20, 10, 0, 4),
(81, NULL, 750000, 1000000, 20, 15, 0, 4),
(81, NULL, 1000000, 1250000, 30, 20, 0, 4),
(81, NULL, 1250000, 1500000, 30, 25, 0, 4),
(81, NULL, 1500000, 5000000, 30, 30, 0, 4),
(81, NULL, 5000000, 10000000, 30, 30, 10, 4),
(81, NULL, 10000000, 20000000, 30, 30, 15, 4),
(81, NULL, 20000000, 50000000, 30, 30, 25, 4),
(81, NULL, 50000000, NULL, 30, 30, 37, 4);

SELECT * FROM "EffectiveTaxRate"
WHERE ("MinAge" BETWEEN 0 AND 60) OR ("MaxAge" BETWEEN 0 AND 60);

CREATE OR REPLACE FUNCTION g(
	age BIGINT,
	salary DOUBLE PRECISION
)
RETURNS TABLE(
	"Age" BIGINT,
	"AgeRange" TEXT,
	"Salary" DOUBLE PRECISION,
	"SalaryRange" TEXT,
	"OldRegimeInput (%)" FLOAT,
	"NewRegimeInput (%)" FLOAT,
	"Post-Deduction Income" DOUBLE PRECISION,
	"OldRegime" DOUBLE PRECISION,
	"NewRegime" DOUBLE PRECISION,
	"OldRegime (%)" FLOAT,
	"NewRegime (%)" FLOAT
)
LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
    row RECORD;
	old_regime DOUBLE PRECISION := 0;
	new_regime DOUBLE PRECISION := 0;
	lakh FLOAT := POWER(10, 5);
	post_deduction_income DOUBLE PRECISION := GREATEST(0, salary - 400000);
BEGIN
	FOR row IN SELECT * FROM "EffectiveTaxRate" etr2 WHERE (age BETWEEN etr2."MinAge" AND COALESCE(etr2."MaxAge", age)) ORDER BY etr2."MinSal" LOOP
		RAISE NOTICE '%,%',row."OldRegime",row."NewRegime";
		IF row."MaxSal" IS NULL THEN
				old_regime := old_regime + ((row."OldRegime"/100) * (1 + row."Surcharge"/100) * (1 + row."HE Cess"/100) * (post_deduction_income - row."MinSal"));
				new_regime := new_regime + ((row."NewRegime"/100) * (1 + row."Surcharge"/100) * (1 + row."HE Cess"/100) * (post_deduction_income - row."MinSal"));
				RAISE NOTICE 'last row execute.., old_regime: %, new_regime: %', old_regime, new_regime;
		ELSE
			IF post_deduction_income >= row."MinSal" AND post_deduction_income < row."MaxSal" THEN
				old_regime := old_regime + ((row."OldRegime"/100) * (1 + row."Surcharge"/100) * (1 + row."HE Cess"/100) * (post_deduction_income - row."MinSal"));
				new_regime := new_regime + ((row."NewRegime"/100) * (1 + row."Surcharge"/100) * (1 + row."HE Cess"/100) * (post_deduction_income - row."MinSal"));
				
				RAISE NOTICE '1)Age:%-%,OldRegime:%,MinSal: %, MaxSal: %, old_regime: %, new_regime: %', row."MinAge",row."MaxAge",row."OldRegime", row."MinSal", row."MaxSal", old_regime, new_regime;
				EXIT;
			ElSE
				old_regime := old_regime + (row."OldRegime"/100 * (1 + row."Surcharge"/100) * (1 + row."HE Cess"/100) * (row."MaxSal" - row."MinSal"));
				new_regime := new_regime + (row."NewRegime"/100 * (1 + row."Surcharge"/100) * (1 + row."HE Cess"/100) * (row."MaxSal" - row."MinSal"));
				RAISE NOTICE '2)Age:%-%,OldRegime:%,MinSal: %, MaxSal: %, old_regime: %, new_regime: %',row."MinAge",row."MaxAge",row."OldRegime", row."MinSal", row."MaxSal", old_regime, new_regime;
			END IF;
			
		END IF;
    END LOOP;
	
	RETURN QUERY
	SELECT age AS "Age",
	CONCAT(etr."MinAge",'-',COALESCE(etr."MaxAge",age)) AS "AgeRange",
	salary AS "Salary",
	CONCAT(etr."MinSal",'-',COALESCE(etr."MaxSal",salary)) AS "SalaryRange",
	etr."OldRegime" AS "OldRegimeInput (%)", etr."NewRegime" AS "NewRegimeInput (%)",
	post_deduction_income AS "Post-Deduction Income",
	old_regime/lakh AS "OldRegime",
	new_regime/lakh AS "NewRegime",
	CASE WHEN post_deduction_income = 0 THEN 0 ELSE (old_regime/post_deduction_income)*100 END AS "OldRegime (%)",
	CASE WHEN post_deduction_income = 0 THEN 0 ELSE (new_regime/post_deduction_income)*100 END AS "NewRegime (%)"
	FROM "EffectiveTaxRate" etr
	WHERE (age BETWEEN etr."MinAge" AND COALESCE(etr."MaxAge", age)) AND (post_deduction_income >= etr."MinSal" AND post_deduction_income < COALESCE(etr."MaxSal", salary));
END;
$BODY$;
--DROP FUNCTION g
SELECT * FROM g(60, 700000);
SELECT * FROM "EffectiveTaxRate";

