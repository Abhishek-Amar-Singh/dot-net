SET SEARCH_PATH TO sample2;

CREATE TABLE department
(
	dept_id INT PRIMARY KEY NOT NULL,
	dept_name VARCHAR(49) UNIQUE NOT NULL
);

CREATE TABLE employee
(
	emp_id INT PRIMARY KEY NOT NULL,
	first_name VARCHAR(35) NOT NULL,
	last_name VARCHAR(35) NOT NULL,
	salary DECIMAL(10,2) NOT NULL DEFAULT 0,
	joining_date DATE NOT NULL,
	dept_id INT REFERENCES department(dept_id)
);

SELECT * FROM department;
SELECT * FROM employee;

INSERT INTO department VALUES(1, 'Dot Net');
INSERT INTO department VALUES(2, 'Testing');
INSERT INTO department VALUES(3, 'iOS');
INSERT INTO department VALUES(4, 'Java');

INSERT INTO employee VALUES(1, 'Dimple', 'Purohit', 35000, '2013-09-10', 1);
INSERT INTO employee VALUES(2, 'Amaan', 'Shaikh', 30000, '2018-12-23', 1);
INSERT INTO employee VALUES(3, 'Raksha', 'Shetty', 21000, '2010-01-15', 1);
INSERT INTO employee VALUES(4, 'Shruti', 'Mutta', 40000, '2013-03-01', 2);
INSERT INTO employee VALUES(5, 'Vishal', 'Prajapati', 35000, '2000-11-19', 1);
INSERT INTO employee VALUES(6, 'Sufiya', 'Siddique', 38000, '2013-09-10', 1);
INSERT INTO employee VALUES(7, 'Vaishali', 'Kanojiya', 20000, '2014-12-29', 2);
INSERT INTO employee VALUES(8, 'Sunita', 'Purohit', 30000, '2022-08-10', 2);
INSERT INTO employee VALUES(9, 'Pinky', 'Purohit', 32000, '2022-07-12', 2);
INSERT INTO employee VALUES(10, 'Abhishek', 'Singh', 26000, '2013-10-20', 1);
INSERT INTO employee VALUES(11, 'Ram', 'Sharma', 29000, '2013-10-20', NULL);

--INNER JOIN
SELECT * FROM employee
INNER JOIN department USING(dept_id);
--INNER JOIN
SELECT * FROM employee e
INNER JOIN department d ON e.dept_id = d.dept_id;

--LEFT JOIN
SELECT * FROM employee
LEFT JOIN department USING(dept_id);

--RIGHT JOIN
SELECT * FROM employee
RIGHT JOIN department USING(dept_id);

--FULL OUTER JOIN
SELECT * FROM employee
FULL OUTER JOIN department USING(dept_id);

--CROSS JOIN
SELECT * FROM employee
CROSS JOIN department;

--Add a New field(AboutMe) in EMployee Table, which will store multi language value
ALTER TABLE employee ADD COLUMN about_me NTEXT

--Get Count of those employees which have a salary more than 15000 and that employee belonging to the IT department 
SELECT COUNT(*) FROM employee e
INNER JOIN department d USING(dept_id)
WHERE e.salary > 15000 AND d.dept_name = 'Testing';

--Write a store procedure to insert, update, Delete and select employee detail
CREATE OR REPLACE PROCEDURE sp_insert_emp(
	IN id integer,
	IN fname character varying,
	IN lname character varying,
	IN sal numeric,
	IN joining_date date,
	IN dept_id integer DEFAULT NULL::integer,
	IN about_me TEXT = NULL)
LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
-- variable declaration
BEGIN
-- stored procedure body
INSERT INTO employee VALUES(id, fname, lname, sal, joining_date, dept_id, about_me);
END; 
$BODY$;

CALL sp_insert_emp(1000, 'Saili', 'Rao', 70000, '2020-06-22', NULL, 'پليٽ');
CALL sp_insert_emp(1001, 'Disha', 'Singh', 22000, '2020-06-22', 2, 'پليٽ');

Select * from employee where emp_id=1000;

CREATE OR REPLACE PROCEDURE sp_update_emp(
	IN id integer,
	IN sal decimal,
	IN join_date DATE
)
LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
-- variable declaration
BEGIN
-- stored procedure body
UPDATE employee SET salary=sal, joining_date=join_date WHERE emp_id=id;
END; 
$BODY$;

CALL sp_update_emp(1000, 69000, '2022-06-23');


CREATE OR REPLACE PROCEDURE sp_delete_emp(
	IN id integer
)
LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
-- variable declaration
BEGIN
-- stored procedure body
DELETE FROM employee WHERE emp_id=id;
END; 
$BODY$;

CALL sp_delete_emp(1000);

SELECT * FROM employee;

CREATE OR REPLACE FUNCTION sp_select_emp()
RETURNS TABLE (emp_id int, first_name varchar(90), last_name varchar(90), salary decimal, 
			  joining_date DATE, dept_id int, about_me text)

LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
-- variable declaration
BEGIN
-- stored procedure body
RETURN QUERY
SELECT * FROM employee;
END; 
$BODY$;

SELECT * FROM sp_select_emp();
EXECUTE  sp_select_emp();


--10. Write a select query by using the below functions
--	1. ISNULL
--	2. scope_identity
--	3. CONVERT
--	4. LEN
--	5. CONCAT

SELECT * FROM employee WHERE dept_id IS NULL;

SELECT salary::DECIMAL(10,3) as salary--CAST(created_datetime AS DATE)
FROM employee;

SELECT
CAST(joining_date AS TIMESTAMP WITHOUT TIME ZONE) as joining_datetime
FROM employee;

SELECT first_name, LENGTH(first_name) as len FROM employee ORDER BY len;

SELECT
    emp_id, CONCAT(first_name, ' ', last_name) AS "Full Name"
FROM
    employee ORDER BY emp_id;

--Write a query to fetch the FIRST_NAME from the Employee table in upper case and use the ALIAS name as EmpName.
SELECT UPPER(first_name) AS empName FROM employee;

--Write a query to fetch the number of employees working in the department ‘HR’.
SELECT COUNT(*) FROM employee e
INNER JOIN department d USING(dept_id)
WHERE d.dept_name = 'Dot Net';

--Write a query to get the current date.
SELECT CURRENT_DATE;

--Write a query to retrieve the first four characters of FIRST_NAME from the Employee table.
SELECT SUBSTRING(first_name FROM 1 FOR 4) FROM employee;

--Write a query to create a new table that consists of data and structure copied from the other table.
CREATE TABLE employee_copy
AS SELECT * FROM employee;

SELECT * FROM employee_copy;

--Write a query to find the names of employees that begin with ‘S’.
SELECT * FROM employee WHERE first_name LIKE 'S%';

--Write a query to find the number of employees whose DOB is between 02/05/1970 to 31/12/1975 and are grouped according to gender
SELECT * FROM employee
WHERE joining_date BETWEEN '2019-10-09' AND '2022-01-25';

--Write a query to fetch all the records from the Employee table ordered by FIRST_NAME in descending order and DEPARTMENT_NAME in the ascending order.
SELECT * FROM employee
ORDER BY first_name ASC, last_name DESC;

SELECT * FROM department;
--Write a query to retrieve Departments that have less than 2 employees working in it.
SELECT COUNT(*), dept_id FROM employee
full outer join department using(dept_id)

GROUP BY dept_id HAVING COUNT(*) > 2;

SELECT * FROM employee;

--Write a query to display the first and the last record from the EmployeeInfo table.
--1
(SELECT * FROM employee FETCH FIRST 1 ROW ONLY)
UNION
(SELECT * FROM employee OFFSET((SELECT COUNT(*) FROM employee)-1));
--2
(SELECT * FROM employee FETCH FIRST 1 ROW ONLY)
UNION
(SELECT * FROM employee OFFSET((SELECT COUNT(*)-1 FROM employee)));

--fetch alternate records from the table 
SELECT * FROM employee where mod(emp_id,2)=1;
INSERT INTO employee VALUES( 'Amrita', 'Singh', 39000, '2020-06-22', 54, 'Eng');

CREATE CLUSTERED INDEX IX_tblEmployee_Salary
ON employee(salary ASC);

CREATE UNIQUE INDEX "test_emp_id"
ON employee USING btree
(emp_id ASC NULLS LAST)
WITH (FILLFACTOR=10);
--TABLESPACE pg_default;

ALTER TABLE employee
CLUSTER ON "test_emp_id";

SELECT * FROM employee;

--add 1 tringger for employees after insert(Log that emp in Log Table)
DROP TABLE IF EXISTS emp_log;
CREATE TABLE emp_log(
	operation VARCHAR(90),
   emp_id INT NOT NULL,
   entry_date TIMESTAMP WITH TIME ZONE NOT NULL,
	msg TEXT
);

CREATE OR REPLACE  TRIGGER insert_after_trig AFTER INSERT ON employee
FOR EACH ROW EXECUTE PROCEDURE fn_insertion_made_table_changes();

CREATE OR REPLACE FUNCTION fn_insertion_made_table_changes()
  RETURNS TRIGGER 
  LANGUAGE PLPGSQL
  AS
$$
BEGIN
	INSERT INTO emp_log VALUES('insertion',NEW.emp_id, now(), CONCAT('employee having emp_id=',NEW.emp_id, ' is inserted into employee table.'));
	RETURN NEW;
END;
$$;
INSERT INTO employee VALUES( 1003, 'Amrita', 'Singh', 39000, '2020-06-22', 3, 'Eng');
--SELECT * FROM pg_trigger;
SELECT * FROM employee;
SELECT * FROM emp_log;

--add 1 tringger for employees delete (Log that emp in Log Table)
CREATE OR REPLACE TRIGGER delete_after_trig AFTER DELETE ON employee
FOR EACH ROW EXECUTE PROCEDURE fn_deletion_made_table_changes();

CREATE OR REPLACE FUNCTION fn_deletion_made_table_changes()
  RETURNS TRIGGER 
  LANGUAGE PLPGSQL
  AS
$$
BEGIN
	INSERT INTO emp_log VALUES('deletion',OLD.emp_id, now(), CONCAT('employee having emp_id=',OLD.emp_id, ' is deleted from employee table.'));
	RETURN NEW;
END;
$$;

SELECT * FROM employee;
SELECT * FROM emp_log;
DELETE FROM employee WHERE emp_id=1001;
DELETE FROM employee WHERE emp_id=1002;


Question
Sign in to vote
0
Sign in to vote
User940174373 posted
Yes that is certainly possible.  Use if exists to see if the record exists or not:

CREATE OR REPLACE PROCEDURE sp_checkin_insert_update(
	IN id INT,
	IN fname varchar(35),
	IN lname varchar(35),
	IN sal numeric(10,2),
	IN join_date DATE,
	IN deptId INT,
	IN aboutMe TEXT
)
LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
-- variable declaration
BEGIN
-- stored procedure body
	IF EXISTS(SELECT * FROM employee WHERE emp_id = id) THEN
			 -- update your data
			 UPDATE employee
			 SET first_name=fname,last_name=lname,salary=sal,joining_date=join_date,dept_id=deptId,about_me=aboutMe
			 WHERE emp_id = id;
	ELSE
		   --insert data
		   INSERT INTO employee VALUES(id, fname, lname, sal, join_date, deptId, aboutMe);
	END IF;
END;
$BODY$;

CALL sp_checkin_insert_update(1004, 'Amrita', 'Singh',37300,'2021-09-18',2,'日本');
SELECT * FROM employee;
SELECT * FROM emp_log;





