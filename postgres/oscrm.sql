SET SEARCH_PATH TO public;
drop table "Employees"
CREATE TABLE "Employees"
(
	"Id" INT NOT NULL PRIMARY KEY,
	"FirstName" VARCHAR(60) NOT NULL,
	"LastName" VARCHAR(60) NOT NULL,
	"EmailAddress" VARCHAR(100) UNIQUE NOT NULL,
	"Salary" DECIMAL(10,2) NOT NULL,
	"JoiningDate" DATE NOT NULL,
	"MgrId" INT
);


INSERT INTO "Employees" VALUES(1, 'Ram', 'Sharma', 'ram@gmail.com', 15000,'2022-10-24', 102);
INSERT INTO "Employees" VALUES(2, 'Shivangi', 'singh','shivangi@gmail.com', 16000, '2023-01-28', 102);
INSERT INTO "Employees" VALUES(3, 'Rohit', 'sharma','rohit@gmail.com', 21000, '2019-12-09', 103);
INSERT INTO "Employees" VALUES(102, 'Viraj', 'sharma','viraj@yahoo.com', 45000, '2013-05-15', NULL);
INSERT INTO "Employees" VALUES(103, 'Bhumi', 'Singh', 'bhumi@yahoo.com' , 46000,'2019-12-09', NULL);
INSERT INTO "Employees" VALUES(104, 'Prachi', 'Desai', 'prachi@yahoo.com' ,47000,'2019-07-30', NULL);
INSERT INTO "Employees" VALUES(4, 'Bharat', 'Goyal', 'bharat@gmail.com',17000, '2002-11-02', 103);
INSERT INTO "Employees" VALUES(5, 'Abhijith', 'Nayar', 'abhijith@gmail.com',17000, '2012-05-23', 103);
INSERT INTO "Employees" VALUES(6, 'Hardik', 'Bharia', 'hardik@gmail.com',18000, '2018-02-15', 103);
INSERT INTO "Employees" VALUES(7, 'Vishal', 'Verma', 'vishal@gmail.com',30000, '2009-07-30', 104);

SELECT * FROM "Employees";
drop function fn_get_all_employees()
--CREATE FUNCTION fn_get_all_employees()
CREATE OR REPLACE FUNCTION fn_get_all_employees()
RETURNS TABLE
(
	"Id" INT,
	"FirstName" CHARACTER VARYING,
	"LastName" CHARACTER VARYING,
	"EmailAddress" CHARACTER VARYING,
	"Salary" NUMERIC,
	"JoiningDate" DATE,
	"MgrId" INT
) 
LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
BEGIN
	RETURN QUERY
	SELECT * FROM "Employees";
END;
$BODY$;

--CALL THE FUNCTION fn_get_all_employees()
SELECT * FROM fn_get_all_employees();

--Scaffold
--Scaffold-DbContext "Host=localhost;Database=crm;Search Path=public;Username=postgres;Password=root@2012;" Npgsql.EntityFrameworkCore.PostgreSQL -o Models/Employees