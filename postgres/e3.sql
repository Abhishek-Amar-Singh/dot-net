SET SEARCH_PATH TO sample2;

CREATE TABLE person(
	id INT GENERATED ALWAYS AS IDENTITY,
	name VARCHAR(45) NOT NULL,
	father_name VARCHAR(45) NULL
);

INSERT INTO person(name, father_name) VALUES('Sushee Singh', 'I. B. Singh');
INSERT INTO person(name, father_name) VALUES('I. B. Singh', 'R. B. Singh');
INSERT INTO person(name, father_name) VALUES('Rudra Singh', 'Anil Singh');
INSERT INTO person(name, father_name) VALUES('Anil Singh', 'R. J. Singh');
INSERT INTO person(name, father_name) VALUES('R. J. Singh', 'R. L. Singh');
INSERT INTO person(name, father_name) VALUES('R. B. Singh', 'R. L. Singh');

SELECT * FROM person;

SELECT name,(SELECT p2.father_name FROM person p2 WHERE p2.name=p1.father_name) AS grand_father_name FROM person p1;

SELECT REVERSE('ABC XYZ');

CREATE TABLE team
(
	id INT GENERATED ALWAYS AS IDENTITY,
	team VARCHAR(40)
);

INSERT INTO team(team) VALUES('IND'), ('AUS'), ('SL'), ('PAK');

SELECT * FROM team;

SELECT CONCAT(a.team, ' vs. ', b.team) AS "Team Played"
FROM team a INNER JOIN team b ON a.id < b.id;


CREATE TABLE num
(
	id INT PRIMARY KEY NOT NULL,
	data INT[]
);

INSERT INTO num VALUES(1, ARRAY[1,2]);
INSERT INTO num VALUES(2, ARRAY[3,4]);
INSERT INTO num VALUES(3, ARRAY[5,4]);
INSERT INTO num VALUES(4, ARRAY[3,2]);
INSERT INTO num VALUES(5, ARRAY[11,2]);

SELECT * FROM num;

CREATE TABLE alpha
(
	id INT NOT NULL PRIMARY KEY,
	name CHARACTER VARYING
);

INSERT INTO alpha VALUES(1, 'A');
INSERT INTO alpha VALUES(2, 'B');
INSERT INTO alpha VALUES(3, 'C');
INSERT INTO alpha VALUES(4, 'D');
INSERT INTO alpha VALUES(5, 'E');
INSERT INTO alpha VALUES(11, 'K');

SELECT * FROM alpha;

SELECT n.id, name FROM num n
INNER JOIN alpha a ON FOR EACH ROW IN data;
SELECT data[:] FROM num;



--!!1#3%H@4=8A9RRT1
SELECT TO_NUMBER('!!1#3%H@4=8A9RRT1' );

SELECT '!!1#3%H@4=8A9RRT1' ~ '^[[:digit:]]?$';
SELECT Patindex('%[^0-9]%', '!!1#3%H@4=8A9RRT1');
select regexp_replace('!!1#3%H@4=8A9RRT1', '[^a-zA-Z]', '');
SELECT ISNUMERIC();


